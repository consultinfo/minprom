<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Департамент развития предпринимательства и лицензирования");
?><table align="left" style="border-collapse: collapse;">
<tbody>
<tr>
	<td colspan="3">
		<p style="text-align: center;">
 <span style="font-size: 9px;"><b><span style="font-size: 12pt;">Департамент развития предпринимательства и торговли</span></b><span style="color: #438ccb; font-size: 12pt;">&nbsp;</span></span>
		</p>
		<p style="text-align: center;">
 <span style="font-size: 9px;"><span style="color: #438ccb;"> <br>
 </span></span>
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Начальник департамента
		</p>
	</td>
	<td>
 <b>Гракова </b>Ирина Николаевна
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-338, к.325-М
		</p>
	</td>
</tr>
<tr>
	<td colspan="3">
		<p style="text-align: center;">
 <span style="font-size: 9px;"><span style="font-size: 11pt;"><br>
 </span></span>
		</p>
		<p style="text-align: center;">
 <span style="font-size: 9px;"><span style="font-size: 11pt;"><b>Отдел малого и среднего предпринимательства</b></span></span>
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Заместитель начальника
		</p>
		<p>
			 департамента - начальник отдела
		</p>
	</td>
	<td>
		<p>
 <b>Татьянин&nbsp;</b>Илья Владимирович
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-323, к.325-М
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Заместитель начальника отдела
		</p>
	</td>
	<td>
		<p>
		</p>
		<p>
 <span style="font-weight: bold;">Лебедев </span>Георгий Валерьевич
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-309, к.316-М
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Ведущий консультант
		</p>
	</td>
	<td>
 <b>Залужная </b>Дария Юрьевна
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-144, к.329-М
		</p>
	</td>
</tr>
<tr>
	<td colspan="3">
		<p style="text-align: center;">
 <span style="font-size: 11pt;"><b><br>
 </b></span>
		</p>
		<p style="text-align: center;">
 <span style="font-size: 11pt;"><b>Отдел развития торговли</b></span>
		</p>
		<p style="text-align: center;">
 <span style="font-size: 11pt;">236022, го­род Ка­ли­нинг­рад, Со­ветс­кий прос­пект, 13<b><br>
 </b></span>
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Начальник отдела
		</p>
	</td>
	<td>
		<p>
 <b>Батманов&nbsp;</b>Николай Викторович
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-246, к.214
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Ведущий консультант
		</p>
	</td>
	<td>
		<p>
 <b>Мальцева&nbsp;</b>Надежда Николаевна
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-337, к.214&nbsp; &nbsp;
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Ведущий консультант
		</p>
	</td>
	<td>
		<p>
		</p>
		<p>
 <span style="font-weight: bold;">Степанова&nbsp;</span>Ольга Алексеевна
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-349, к.214
		</p>
	</td>
</tr>
<tr>
	<td colspan="3">
		<p style="text-align: center;">
 <span style="font-size: 9px;"><b><span style="font-size: 12pt;"><br>
 </span></b></span>
		</p>
		<p style="text-align: center;">
 <span style="font-size: 9px;"><b><span style="font-size: 12pt;">Отдел правового и административного обеспечения</span></b></span>
		</p>
	</td>
</tr>
<tr>
	<td colspan="1">
		 Начальник отдела
	</td>
	<td colspan="1">
 <b>Нагорная</b> Надежда Владимировна
	</td>
	<td colspan="1" style="text-align: center;">
		 599-325, к.329-М
	</td>
</tr>
<tr>
	<td colspan="1">
		 Заместитель начальника отдела
	</td>
	<td colspan="1">
 <b>Лягунова</b>&nbsp;Людмила&nbsp;Семеновна
	</td>
	<td colspan="1" style="text-align: center;">
		 599-342, к.316-М
	</td>
</tr>
<tr>
	<td colspan="1">
		 Ведущий консультант
	</td>
	<td colspan="1">
 <b>Казакова&nbsp;</b>&nbsp;Елена Николаевна
	</td>
	<td colspan="1" style="text-align: center;">
		 599-337, к.401
	</td>
</tr>
</tbody>
</table><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>