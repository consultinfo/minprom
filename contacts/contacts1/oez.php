<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Департамент промышленной политики, ОЭЗ и потребительского рынка");
?><table align="left" style="border-collapse: collapse;">
<tbody>
<tr>
	<td colspan="3">
		<h4 style="text-align: center;"> </h4>
		<p style="text-align: center;">
 <b style="font-size: large;">Департамент промышленной политики&nbsp;</b><b style="font-size: large;"><br>
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>
		</p>
 <span style="font-size: 9px;"> </span>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Начальник департамента
		</p>
	</td>
	<td>
		<p>
 <b>Абрамова&nbsp;</b>Анастасия&nbsp;Александровна
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-327, к.320-М&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
		</p>
	</td>
</tr>
<tr>
	<td colspan="3">
		<p style="text-align: center;">
 <b style="font-size: large;"><br>
 </b>
		</p>
		<p style="text-align: center;">
 <b style="font-size: large;">Отдел промышленности</b>
		</p>
		<p style="text-align: center;">
 <b style="font-size: large;"><br>
 </b>
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Начальник отдела
		</p>
	</td>
	<td>
 <b>Худоба&nbsp;</b>Дмитрий Владимирович
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-144, к.329-М
		</p>
	</td>
</tr>
<tr>
	<td colspan="1">
		 Ведущий<br>
		 консультант<br>
	</td>
	<td colspan="1">
 <b>Харитоненко <br>
 </b>Александр Анатольевич
	</td>
	<td colspan="1">
		 &nbsp; &nbsp; &nbsp;599-316, к.341<br>
	</td>
</tr>
<tr>
	<td colspan="1">
		 Ведущий консультант <br>
	</td>
	<td colspan="1">
 <b>Ушакова&nbsp;</b>Мария Сергеевна
	</td>
	<td colspan="1">
		<p align="right" style="text-align: center;">
			 599-349, к.329-М
		</p>
	</td>
</tr>
<tr>
	<td colspan="3">
		<p style="text-align: center;">
 <b style="font-size: 12pt;"><br>
 </b>
		</p>
		<p style="text-align: center;">
 <b style="font-size: 12pt;">Отдел ОЭЗ и инвестиций</b><br>
		</p>
		<p style="text-align: center;">
		</p>
		<table cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
			<td>
 <br>
				 Начальник отдела &nbsp;&nbsp; <br>
			</td>
			<td>
 <b><br>
 </b><b>&nbsp; &nbsp;Литвиненко </b>Алла Викторовна&nbsp;
			</td>
			<td>
				 &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;599-347, к.341<br>
			</td>
		</tr>
		<tr>
			<td>
 <br>
				 Ведущий консультант&nbsp; <br>
			</td>
			<td>
				 &nbsp;<br>
				 &nbsp;&nbsp;<b>Фролова </b>Светлана Леонидовна<br>
			</td>
			<td>
				 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; 599-334, к.329-М
			</td>
		</tr>
		<tr>
			<td>
				 &nbsp;<br>
				 Эксперт <br>
			</td>
			<td>
				 &nbsp;<br>
				 &nbsp;&nbsp;<b>Селезнева </b>Дарья Андреевна
			</td>
			<td>
				 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;599-824, к.341
			</td>
		</tr>
		</tbody>
		</table>
		<p style="text-align: center;">
 <b><span style="font-size: 12pt;"><br>
 </span></b>
		</p>
		<p style="text-align: center;">
 <b><span style="font-size: 12pt;">Отдел лицензирования</span></b>
		</p>
		<p style="text-align: center;">
 <span style="font-size: 10pt;">236022, город Калининград, Советский проспект, 13</span>
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Начальник отдела
		</p>
		<p>
		</p>
		<p>
		</p>
	</td>
	<td>
		<p>
 <br>
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-558, к.214
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Ведущий консультант
		</p>
		<p>
		</p>
		<p>
		</p>
	</td>
	<td>
		<p>
		</p>
		<p>
 <span style="font-weight: bold;">Бойков </span>Руслан Борисович
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-350,<br>
			 &nbsp;<span style="font-size: 13px;">ул.Д.Донского, </span><span style="font-size: small;">к.314</span>&nbsp; &nbsp;
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Ведущий консультант
		</p>
		<p>
		</p>
		<p>
		</p>
	</td>
	<td>
		<p>
		</p>
		<p>
 <b style="font-weight: bold;">Бирюкова&nbsp;</b>Наталья Вячеславовна
		</p>
	</td>
	<td>
		<p align="right" style="text-align: center;">
			 599-553
		</p>
		<p align="right" style="text-align: center;">
		</p>
	</td>
</tr>
</tbody>
</table>
 <br>
<h2></h2><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>