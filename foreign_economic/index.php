<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Внешнеэкономическая деятельность");
?> 
<h2><font size="4">Внешнеторговый оборот</font></h2>
 
<h2> 
  <p align="justify"><font size="3" style="font-weight: normal;">По данным таможенной статистики, внешнеторговый оборот Калининградской области за 2013 год составил 13 619,2 млн долларов США, 13,2% от внешнеторгового оборота СЗФО за этот период. </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">На экспорт Калининградской области приходится 3,1% от стоимости экспорта СЗФО, а импорт Калининградской области составил 21,8% от стоимости импорта СЗФО. </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Внешнеторговый оборот Калининградской области за 2013 год по сравнению с 2012 годом не изменился, причем стоимостной объем экспорта увеличился на 1%, а объем импорта почти не изменился. </font></p>
 
  <p> </p>
 
  <p align="center"><i><font size="3" style="font-weight: normal;">Итоги внешней торговли Калининградской области (тыс. долл. США) </font></i></p>
 
  <table width="482" cellspacing="0" cellpadding="0" border="1" align="center" style="border-collapse: collapse; border: medium none;"> 
    <tbody> 
      <tr style="height: 53pt;"> <td width="95" valign="top" style="width: 70.9pt; border: 1pt solid black; padding: 0cm 5.4pt; height: 53pt;"> 
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"> </p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">Показатели<o:p></o:p></p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"> </p>
         </td> <td valign="top">
          <br />
        </td><td width="75" valign="top" style="width: 56.45pt; border-style: solid solid solid none; border-top: 1pt solid black; border-right: 1pt solid black; border-bottom: 1pt solid black; padding: 0cm 5.4pt; height: 53pt;"> 
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"> </p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">2012<o:p></o:p></p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"> </p>
         </td> <td width="83" valign="top" style="width: 62.4pt; border-style: solid solid solid none; border-top: 1pt solid black; border-right: 1pt solid black; border-bottom: 1pt solid black; padding: 0cm 5.4pt; height: 53pt;"> 
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">Доля во внешнеторговом обороте<o:p></o:p></p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">2012 г.<o:p></o:p></p>
         </td> <td width="76" valign="top" style="width: 57.25pt; border-style: solid solid solid none; border-top: 1pt solid black; border-right: 1pt solid black; border-bottom: 1pt solid black; padding: 0cm 5.4pt; height: 53pt;"> 
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"> </p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">2013<o:p></o:p></p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"> </p>
         </td> <td width="84" valign="top" style="width: 62.75pt; border-style: solid solid solid none; border-top: 1pt solid black; border-right: 1pt solid black; border-bottom: 1pt solid black; padding: 0cm 5.4pt; height: 53pt;"> 
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">Доля во внешнеторговом обороте<o:p></o:p></p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">2013<o:p></o:p></p>
         </td> <td width="69" valign="top" style="width: 51.75pt; border-style: solid solid solid none; border-top: 1pt solid black; border-right: 1pt solid black; border-bottom: 1pt solid black; padding: 0cm 5.4pt; height: 53pt;"> 
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"> </p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">2013 к<o:p></o:p></p>
         
          <p align="center" class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">2012<o:p></o:p></p>
         </td> </tr>
     
      <tr style="height: 10.2pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right: 1pt solid black; border-bottom: 1pt solid black; border-left: 1pt solid black; padding: 0cm 5.4pt; height: 10.2pt;"> 
          <p class="MsoNormal" style="margin-bottom: 0.0001pt;">Товарооборот<b><o:p></o:p></b></p>
         </td> <td valign="top">
          <br />
        </td><td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 10.2pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">13596608,5</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 10.2pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">1,00</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 10.2pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">13619459,6</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 10.2pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">1,00</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 10.2pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">100%</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> </tr>
     
      <tr style="height: 30pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right: 1pt solid black; border-bottom: 1pt solid black; border-left: 1pt solid black; padding: 0cm 5.4pt; height: 30pt;"> 
          <p class="MsoNormal" style="margin-bottom: 0.0001pt;">Экспорт<o:p></o:p></p>
         </td> <td valign="top">
          <br />
        </td><td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 30pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">1473655,6<o:p></o:p></span></p>
         </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 30pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">0,11</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 30pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">1494913,0<o:p></o:p></span></p>
         </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 30pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">0,11</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 30pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">101%</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> </tr>
     
      <tr style="height: 31.05pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right: 1pt solid black; border-bottom: 1pt solid black; border-left: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p class="MsoNormal" style="margin-bottom: 0.0001pt;">Импорт<o:p></o:p></p>
         </td> <td valign="top">
          <br />
        </td><td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">12122952,9<o:p></o:p></span></p>
         </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">0,89</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">12124546,6<o:p></o:p></span></p>
         </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">0,89</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">100%</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> </tr>
     
      <tr style="height: 31.05pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right: 1pt solid black; border-bottom: 1pt solid black; border-left: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p class="MsoNormal" style="margin-bottom: 0.0001pt;">Сальдо<o:p></o:p></p>
         </td> <td valign="top">
          <br />
        </td><td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">-10649297,3</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;"> </span></p>
         </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;">-10629633,6</span><span style="line-height: 115%;"><o:p></o:p></span></p>
         </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;"> </span></p>
         </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom: 1pt solid black; border-right: 1pt solid black; padding: 0cm 5.4pt; height: 31.05pt;"> 
          <p align="center" class="MsoNormal" style="text-align: center;"><span style="line-height: 115%;"> </span></p>
         </td> </tr>
     </tbody>
   </table>
 
  <p align="right"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Калининградская область является субъектом Российской Федерации, ориентированным на ввоз товаров &ndash; импорт товаров составляет 89% товарооборота Калининградской области. </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Географическая направленность товарооборота остается прежней, а именно, наиболее активны связи Калининградской области со странами дальнего зарубежья. Доля стран дальнего зарубежья в товарообороте составила 97%. </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Участники ВЭД Калининградской области осуществляли торговлю со 150 странами мира. Доля ведущих стран-партнеров, представленных на рисунке, составила 67,1%. За 2013 год в первую &laquo;тройку&raquo; стран-партнеров вошли Германия, Республика Корея и Словакия, их суммарный удельный вес составил 39,3% внешнеторгового оборота региона. </font></p>
 
  <p><font size="3"><img src="/upload/medialibrary/bd6/isutobdustaaujkfzvodpqqkji bktxbamzpifejxavnepmxnwhhbmdqc qjnbcyudrkznqx elfs ghuztoxzysifiy 2013.JPG" title="распределение внешнеторгового оборота по странам 2013.JPG" border="0" alt="распределение внешнеторгового оборота по странам 2013.JPG" width="378" height="299"  /></font></p>
 <font size="3" style="font-weight: normal;">В 2013 году 4294 участника ВЭД, зарегистрированных на территории Калининградской области, осуществляли внешнеторговые операции (экспортировали товары 367, импортировали &ndash; 4156 субъектов ВЭД).</font> 
  <p><span style="font-size: medium; font-weight: normal;"> </span><font size="4">Экспорт товаров из Калининградской области</font></p>
 </h2>
 
<h2> 
  <p> </p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Экспортные операции Калининградской области ориентированы на внешний рынок стран дальнего зарубежья. За 2013 год стоимостные объемы экспортных поставок в страны дальнего зарубежья составили 95,2% от общей стоимости экспорта.</font></p>
 
  <p align="center"><span style="font-weight: normal;"><i><font size="3">Распределение стоимостных объемов экспорта Калининградской области</font></i></span></p>
 
  <p align="center"><font size="3"><i><font style="font-weight: normal;">по странам - контрагентам за 2013 год</font> </i> 
      <br />
     </font></p>
 
  <div style="text-align: left;"> 
    <table width="513" cellspacing="0" cellpadding="0" border="1" align="center" style="width: 384.75pt;"> 
      <tbody> 
        <tr align="center"> <td width="180" style="width: 135pt; padding: 0cm;"> 
            <p style="text-align: center;">Страна-контрагент<o:p></o:p></p>
           </td> <td width="132" style="width: 99pt; padding: 0cm;"> 
            <p style="text-align: center;">Экспорт,тыс.долл. США<o:p></o:p></p>
           </td> <td width="95" style="width: 71.25pt; padding: 0cm;"> 
            <p style="text-align: center;">Удельный вес в экспорте<span class="apple-converted-space"> </span><o:p></o:p></p>
           </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> 
            <p style="text-align: center;">Основные товары<o:p></o:p></p>
           </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Всего экспорт, в том числе:<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">1494913,1</span> </td> <td width="95" style="width: 71.25pt; padding: 0cm;"> 
            <p style="text-align: right;">100,0%<o:p></o:p></p>
           </td> <td width="217" style="width: 162.75pt; padding: 0cm;"></td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Страны<span class="apple-converted-space"> </span>СНГ (8 стран)<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">71889,0</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">4,8%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"></td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Украина<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">34581,3</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">2,3%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> 
            <p>готовые продукты из рыбы<o:p></o:p></p>
           </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p style="text-align: left;">Прочие страны СНГ<o:p></o:p></p>
           </td> <td width="132" valign="bottom" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">37307,7</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">2,5%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"></td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Страны дальнего зарубежья<span class="apple-converted-space"> </span>(77 стран)<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">1423024,0</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">95,2%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"></td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Индия<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">398243,6</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">26,6%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> 
            <p>суда и плавучие конструкции<o:p></o:p></p>
           </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Литва<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">183699,2</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">12,3%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> 
            <p>удобрения<o:p></o:p></p>
           </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Норвегия<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">132243,9</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">8,8%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> 
            <p>масло рапсовое<o:p></o:p></p>
           </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Германия<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">91365,8</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">6,1%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> 
            <p>корма для животных<o:p></o:p></p>
           </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Польша<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">76530,4</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">5,1%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> корма для животных </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Алжир<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">66220,7</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">4,4%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> масло рапсовое </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Дания <o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"><span style="line-height: 115%;">47912,8</span> </td> <td width="95" valign="bottom" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">3,2%</span> </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> корма для животных </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Финляндия<o:p></o:p></p>
           </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">47403,0</span> </td> <td width="95" valign="bottom" style="width: 71.25pt; padding: 0cm;"> 
            <p style="text-align: right;">3,2%<o:p></o:p></p>
           </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> корма для животных </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> Нидерланды </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">43666,8</span> </td> <td width="95" valign="bottom" style="width: 71.25pt; padding: 0cm;"> 
            <p style="text-align: right;">2,9%<o:p></o:p></p>
           </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> масло рапсовое </td> </tr>
       
        <tr align="center"> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> Франция </td> <td width="132" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">40133,5</span> </td> <td width="95" valign="bottom" style="width: 71.25pt; padding: 0cm;"> 
            <p style="text-align: right;">2,7%<o:p></o:p></p>
           </td> <td width="217" style="width: 162.75pt; padding: 0cm;"> масло рапсовое </td> </tr>
       
        <tr> <td width="180" align="left" style="width: 135pt; padding: 0cm;"> 
            <p>Прочие страны дальнего зарубежья<o:p></o:p></p>
           </td> <td width="132" valign="bottom" align="center" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">295604,3</span> </td> <td width="95" valign="bottom" align="center" style="width: 71.25pt; padding: 0cm;"> 
            <p style="text-align: right;">19,9%<o:p></o:p></p>
           </td> <td width="217" style="width: 162.75pt; padding: 0cm;"></td> </tr>
       </tbody>
     </table>
   </div>
 </h2>
 
<h2> 
  <p></p>
 
  <p align="justify" style="text-align: start;"><span style="font-weight: normal;"><font size="3">В товарной структуре экспорта Калининградской области в 2013 году по сравнению с 2012 годом произошли существенные структурные сдвиги.</font></span></p>
 
  <p align="justify"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 
  <p align="justify"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 
  <p align="justify"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 </h2>
 
<h2> 
  <p align="justify"> </p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Главными статьями экспорта являются продовольственные товары и машиностроительная продукция. Стоимостной объем продовольственной продукции увеличился на 34%, при этом доля этой группы товаров увеличилась на 10,7 процентных пункта и составила 43,9% от экспорта области. Стоимостной объем продукции машиностроения увеличился на 11%, при этом доля этой группы товаров увеличилась на 2,8 процентных пункта и составила 32,5%. </font></p>
 
  <p align="justify"> </p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Крупнейшие предприятия-экспортеры области за 2013 год: </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ОАО &laquo;Прибалтийский судостроительный завод &laquo;Янтарь&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО Торговый дом &laquo;Содружество&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ЗАО &laquo;Содружество-Соя&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;АРВИ НПК&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;ГАЗПРОМНЕФТЬ МАРИН БУНКЕР&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;Вичюнай-Русь&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;Металлстиль&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ЗАО &laquo;Терминал&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ОАО &laquo;Научно-производственное объединение &laquo;Цифровые телевизионные системы&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;Балтийская табачная фабрика&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Стоимость экспортных поставок перечисленных выше предприятий составила 81,1% от экспорта Калининградской области за 2013 год. </font></p>
 
  <p> <font size="3" style="font-weight: normal;"> </font></p>
 </h2>
 
<h2><font size="3"> </font><font size="4">Импорт товаров в Калининградскую область </font></h2>
 
<h2> 
  <p><font size="3" style="font-weight: normal;"> </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Импорт товаров в Калининградскую область осуществляется преимущественно из стран дальнего зарубежья. </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">За 2013 год стоимостные объемы импортных поставок из стран дальнего зарубежья составили 97,2% от общей стоимости импорта. </font></p>
 
  <p align="center"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 
  <p align="center"><i><font size="3" style="font-weight: normal;">Распределение стоимостных объемов импорта Калининградской области </font></i></p>
 
  <p align="center"><i><font size="3" style="font-weight: normal;">по странам &ndash; контрагентам за 2012 год </font></i></p>
 
  <div align="center"> 
    <div align="center"> 
      <table width="514" cellspacing="0" cellpadding="0" border="1" class="MsoNormalTable" style="width: 385.5pt;"> 
        <tbody> 
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p align="center" style="text-align: center;">Страна-контрагент<o:p></o:p></p>
             </td> <td width="132" style="width: 99pt; padding: 0cm;"> 
              <p align="center" style="text-align: center;">Импорт,<span class="apple-converted-space"> </span>тыс. долл.США<o:p></o:p></p>
             </td> <td width="95" style="width: 71.25pt; padding: 0cm;"> 
              <p align="center" style="text-align: center;">Удельный вес в<span class="apple-converted-space"> </span>импорте<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> 
              <p align="center" style="text-align: center;">Основные товары<o:p></o:p></p>
             </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Всего импорт<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">12124546,6</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">100,0%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"></td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Страны СНГ (7 стран)<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">325910,0</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">2,7%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"></td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Украина<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">324480,5</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">2,7%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> 
              <p>железнодорожное оборудование<o:p></o:p></p>
             </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Прочие страны СНГ<o:p></o:p></p>
             </td> <td width="132" valign="bottom" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">1429,5</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">0,0%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"></td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Страны дальнего зарубежья (135 стран)<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">11798636,5</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">97,3%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"></td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Германия<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">2184853,5</span> </td> <td width="95" align="right" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">18,0%</span> </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> 
              <p>комплектующие для сборки автомобилей<o:p></o:p></p>
             </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Республика Корея<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">1711358,5</span> </td> <td width="95" align="right" style="text-align: right; width: 71.25pt; padding: 0cm;"> <span style="line-height: 115%;">14,1%</span> </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> 
              <p>комплектующие для сборки автомобилей<o:p></o:p></p>
             </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Словакия</p>
             </td><td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"><span style="line-height: 115%;">1356223,6</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">11,2%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> <span style="line-height: 115%;">комплектующие для сборки автомобилей</span> </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Китай<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">988807,2</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">8,2%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> 
              <p>электрооборудование<o:p></o:p></p>
             </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Польша<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">920262,8</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">7,6%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> <span style="line-height: 115%;">мясо</span> </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>США<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">845028,1</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">7,0%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> 
              <p>комплектующие для сборки автомобилей<o:p></o:p></p>
             </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Парагвай<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">489915,9</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">4,0 %<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> 
              <p>семена рапса<o:p></o:p></p>
             </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> Великобритания </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">355859,3</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">2,9%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> электрооборудование </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Бразилия<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">315329,9</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">2,6%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> мясо </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Литва<o:p></o:p></p>
             </td> <td width="132" align="right" style="text-align: right; width: 99pt; padding: 0cm;"> <span style="line-height: 115%;">272185,6</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">2,2%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"> <span style="line-height: 115%;">фосфаты калия</span> </td> </tr>
         
          <tr> <td width="179" style="width: 134.25pt; padding: 0cm;"> 
              <p>Прочие страны дальнего зарубежья<o:p></o:p></p>
             </td> <td width="132" valign="bottom" align="right" style="text-align: right; width: 99pt; padding: 0cm;"><span style="line-height: 115%;">2358812,1</span> </td> <td width="95" align="right" style="width: 71.25pt; padding: 0cm;"> 
              <p style="text-align: right;">19,5%<o:p></o:p></p>
             </td> <td width="222" style="width: 166.5pt; padding: 0cm;"></td> </tr>
         </tbody>
       </table>
     </div>
   </div>
 </h2>
 
<h2> 
  <p align="right"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">В товарной структуре импорта Калининградской области произошли несущественные структурные сдвиги. </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Главными статьями импорта являются машиностроительная продукция и продовольственные товары. Стоимостной объем машиностроительной продукции уменьшился на 5%, при этом ее доля по сравнению с прошлым годом уменьшилась на 2,7 процентных пункта и составила 56% от общего объема импорта. Стоимостной объем импорта продовольствия увеличился на 13%, при этом доля продовольственных товаров увеличилась на 2,6 процентных пункта и составила 22,5% от общего импорта Калининградской области. </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Крупнейшие предприятия-импортеры за 2013 год: </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;АВТОТОР-Терминал&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ЗАО &laquo;ДВ Транспорт&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ЗАО &laquo;АВТОТОР&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;Балтсервис&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;Эллада Интертрейд&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ЗАО &laquo;АВТОТОР-Менеджмент&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО Торговый дом &laquo;Содружество&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ЗАО &laquo;Содружество-Соя&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ООО &laquo;АВТОТОРТехобслуживание&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- ЗАО &laquo;Алко-Нафта&raquo; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">Стоимость импортных поставок перечисленных предприятий составила 48,9% от импорта Калининградской области за 2013 год. </font></p>
 
  <p align="justify"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 </h2>
 
<h2 align="justify"><font size="3">Краткие итоги внешней торговли Калининградской области за 2013 год </font></h2>
 
<h2> 
  <p align="justify"><i><font size="3" style="font-weight: normal;"> </font></i></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- Внешнеторговый оборот за 2013 год составил 13,6 млрд дол. США, стоимостной объем экспорта вырос на 1%, объем импорта сопоставим с показателем за 2012 год; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- Импорт составляет 89% от товарооборота области; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- На страны дальнего зарубежья приходится 97% стоимостного объема товарооборота; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- Внешнеторговые операции осуществляли 4294 участника ВЭД; </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- Существенные структурные сдвиги в экспорте. Профилирующие товары экспорта: продовольственные товары (43,9%), продукция машиностроения (32,5%); </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- Несущественные сдвиги в товарной структуре импорта. Главными статьями импорта являются машиностроительная продукция (56%) и продовольственные товары (22,5%); </font></p>
 
  <p align="justify"><font size="3" style="font-weight: normal;">- Основные страны-партнеры: Германия, Республика Корея, Словакия. </font></p>
 </h2>
 
<h2> </h2>
 
<p></p>
 
<p></p>
 
<p></p>
 
<p></p>
 
<p></p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>