<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Внешнеэкономическая деятельность");
?> 
<p> 
<a name="_Toc352947660">
 Внешнеэкономическая деятельность в Калининградской области</p>
 
<p><b> </b></p>
 
<h2> 
<a name="_Toc388882220">
 <i>Внешнеторговый оборот</i><i> </i></h2>
 
<p> 
<a name="_Toc385867601"> </a>
 </p>
 
<p>По данным таможенной статистики, внешнеторговый оборот Калининградской области за 2013 год составил 13 619,2 млн долларов США, 13,2% от внешнеторгового оборота СЗФО за этот период. </p>
 
<p>На экспорт Калининградской области приходится 3,1% от стоимости экспорта СЗФО, а импорт Калининградской области составил 21,8% от стоимости импорта СЗФО. </p>
 
<p>Внешнеторговый оборот Калининградской области за 2013 год по сравнению с 2012 годом не изменился, причем стоимостной объем экспорта увеличился на 1%, а объем импорта почти не изменился. </p>
 
<p> </p>
 
<p align="center"><i>Итоги внешней торговли Калининградской области (тыс. долл. США) </i></p>
 
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="482" style="border-collapse: collapse; border: none;"> 
  <tbody> 
    <tr style="height: 53pt;"> <td width="95" valign="top" style="width: 70.9pt; border: 1pt solid black; padding: 0cm 5.4pt; height: 53pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;"> </span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">Показатели<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;"> </span></p>
       </td> <td width="75" valign="top" style="width: 56.45pt; border-style: solid solid solid none; border-top-color: black; border-right-color: black; border-bottom-color: black; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 53pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;"> </span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">2012<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;"> </span></p>
       </td> <td width="83" valign="top" style="width: 62.4pt; border-style: solid solid solid none; border-top-color: black; border-right-color: black; border-bottom-color: black; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 53pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">Доля во внешнеторговом обороте<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">2012 г.<o:p></o:p></span></p>
       </td> <td width="76" valign="top" style="width: 57.25pt; border-style: solid solid solid none; border-top-color: black; border-right-color: black; border-bottom-color: black; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 53pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;"> </span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">2013<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;"> </span></p>
       </td> <td width="84" valign="top" style="width: 62.75pt; border-style: solid solid solid none; border-top-color: black; border-right-color: black; border-bottom-color: black; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 53pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">Доля во внешнеторговом обороте<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">2013<o:p></o:p></span></p>
       </td> <td width="69" valign="top" style="width: 51.75pt; border-style: solid solid solid none; border-top-color: black; border-right-color: black; border-bottom-color: black; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 53pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;"> </span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">2013 к<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">2012<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 10.2pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right-color: black; border-bottom-color: black; border-left-color: black; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 10.2pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"> 
<a name="_Toc352947663"></a>
 
<a name="_Toc352945334"></a>
 
<a name="_Toc352863438"></a>
 
<a name="_Toc352863353"></a>
 
<a name="_Toc352863219"></a>
 
<a name="_Toc352862637">
 <span style="font-size: 9pt; font-family: 'Times New Roman', serif; color: black;">Товарооборот</span><b><span style="font-size: 9pt;"><o:p></o:p></span></b></p>
       </td> <td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 10.2pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">13596608,5</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 10.2pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">1,00</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 10.2pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">13619459,6</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 10.2pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">1,00</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 10.2pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">100%</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 30pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right-color: black; border-bottom-color: black; border-left-color: black; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 30pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">Экспорт<o:p></o:p></span></p>
       </td> <td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 30pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">1473655,6<o:p></o:p></span></p>
       </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 30pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">0,11</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 30pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">1494913,0<o:p></o:p></span></p>
       </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 30pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">0,11</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 30pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">101%</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 31.05pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right-color: black; border-bottom-color: black; border-left-color: black; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">Импорт<o:p></o:p></span></p>
       </td> <td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">12122952,9<o:p></o:p></span></p>
       </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">0,89</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">12124546,6<o:p></o:p></span></p>
       </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">0,89</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">100%</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 31.05pt;"> <td width="95" style="width: 70.9pt; border-style: none solid solid; border-right-color: black; border-bottom-color: black; border-left-color: black; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-size: 9pt; font-family: 'Times New Roman', serif;">Сальдо<o:p></o:p></span></p>
       </td> <td width="75" style="width: 56.45pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">-10649297,3</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="83" style="width: 62.4pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"> </span></p>
       </td> <td width="76" style="width: 57.25pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;">-10629633,6</span><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"><o:p></o:p></span></p>
       </td> <td width="84" style="width: 62.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"> </span></p>
       </td> <td width="69" style="width: 51.75pt; border-style: none solid solid none; border-bottom-color: black; border-bottom-width: 1pt; border-right-color: black; border-right-width: 1pt; padding: 0cm 5.4pt; height: 31.05pt;"> 
        <p class="MsoNormal" align="center" style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: 'Times New Roman', serif;"> </span></p>
       </td> </tr>
   </tbody>
 </table>
 
<p align="right"><i> </i></p>
 
<p>Калининградская область является субъектом Российской Федерации, ориентированным на ввоз товаров &ndash; импорт товаров составляет 89% товарооборота Калининградской области. </p>
 
<p>Географическая направленность товарооборота остается прежней, а именно, наиболее активны связи Калининградской области со странами дальнего зарубежья. Доля стран дальнего зарубежья в товарообороте составила 97%. </p>
 
<p>Участники ВЭД Калининградской области осуществляли торговлю со 150 странами мира. Доля ведущих стран-партнеров, представленных на рисунке, составила 67,1%. За 2013 год в первую &laquo;тройку&raquo; стран-партнеров вошли Германия, Республика Корея и Словакия, их суммарный удельный вес составил 39,3% внешнеторгового оборота региона. </p>
 
<p align="center"><img src="/upload/medialibrary/bd6/isutobdustaaujkfzvodpqqkji bktxbamzpifejxavnepmxnwhhbmdqc qjnbcyudrkznqx elfs ghuztoxzysifiy 2013.JPG" title="распределение внешнеторгового оборота по странам 2013.JPG" border="0" alt="распределение внешнеторгового оборота по странам 2013.JPG" width="378" height="299"  /></p>
 
<p align="center"><i>Распределение внешнеторгового оборота по странам-контрагентам </i></p>
 
<p align="center"><i>за 2013 год </i></p>
 
<p align="center"> </p>
 
<p>В 2013 году 4294 участника ВЭД, зарегистрированных на территории Калининградской области, осуществляли внешнеторговые операции (экспортировали товары 367, импортировали – 4156 субъектов ВЭД). </p>
 
<p><i> </i></p>
 
<h2> 
<a name="_Toc352862639">
 <i>Экспорт товаров из Калининградской области</i><i> </i></h2>
 
<p> </p>
 
<p>Экспортные операции Калининградской области ориентированы на внешний рынок стран дальнего зарубежья. За 2013 год стоимостные объемы экспортных поставок в страны дальнего зарубежья составили 95,2% от общей стоимости экспорта. </p>
 
<p> </p>
 
<p> 
<a name="_Toc352863358">
 Распределение стоимостных объемов экспорта Калининградской области </p>
 
<p align="center"><i>по странам - контрагентам за 2013 год </i></p>
 
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" align="left" width="521" style="border-collapse: collapse; border: none; margin-left: 6.75pt; margin-right: 6.75pt;"> 
  <tbody> 
    <tr style="height: 23.2pt;"> <td width="158" style="width: 118.4pt; border-style: solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-left-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Страна-контрагент<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: solid solid none none; border-top-color: windowtext; border-top-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Экспорт,<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">тыс.долл. США<o:p></o:p></span></p>
       </td> <td width="79" style="width: 59.15pt; border-style: solid solid none none; border-top-color: windowtext; border-top-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Удельный вес - в<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">экспорте<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: solid solid none none; border-top-color: windowtext; border-top-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Основные товары<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">Всего экспорт<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">1494913,1<o:p></o:p></span></p>
       </td> <td width="79" style="width: 59.15pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">100,0%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;"> </span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">Страны СНГ (8 стран)<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">71889,0<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">4,8%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;"> </span></p>
       </td> </tr>
   
    <tr style="height: 15.45pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">УКРАИНА<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">34581,3<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,3%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">готовые продукты из рыбы<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ПРОЧИЕ СТРАНЫ СНГ<o:p></o:p></span></p>
       </td> <td width="110" valign="bottom" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">37307,7<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,5%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;"> </span></p>
       </td> </tr>
   
    <tr style="height: 6.95pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">Страны ДЗ (91 страна)<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">1423024,0<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">95,2%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <h1 style="margin: 0cm 0cm 0.0001pt;"><span style="font-size: 11pt; font-family: 'Times New Roman', serif; font-weight: normal;"> </span></h1>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ИНДИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">398243,6<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">26,6%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">суда и плавучие механизмы<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ЛИТВА<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">183699,2<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">12,3%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">удобрения<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">НОРВЕГИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">132243,9<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">8,8%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ГЕРМАНИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">91365,8<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">6,1%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ПОЛЬША<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">76530,4<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">5,1%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">АЛЖИР<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">66220,7<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">4,4%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ДАНИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">47912,8<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">3,2%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ФИНЛЯНДИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">47403,0<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">3,2%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">НИДЕРЛАНДЫ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">43666,8<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,9%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ФРАНЦИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">40133,5<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,7%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ПРОЧИЕ СТРАНЫ ДЗ<o:p></o:p></span></p>
       </td> <td width="110" valign="bottom" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">295604,3<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">19,9%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;"> <o:p></o:p></span></p>
       </td> </tr>
   </tbody>
 </table>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p>В товарной структуре экспорта Калининградской области в 2013 году по сравнению с 2012 годом произошли существенные структурные сдвиги. </p>
 
<p>Главными статьями экспорта являются продовольственные товары и машиностроительная продукция. Стоимостной объем продовольственной продукции увеличился на 34%, при этом доля этой группы товаров увеличилась на 10,7 процентных пункта и составила 43,9% от экспорта области. Стоимостной объем продукции машиностроения увеличился на 11%, при этом доля этой группы товаров увеличилась на 2,8 процентных пункта и составила 32,5%. </p>
 
<p> </p>
 
<p>Крупнейшие предприятия-экспортеры области за 2013 год: </p>
 
<p>- ОАО «Прибалтийский судостроительный завод «Янтарь» </p>
 
<p>- ООО Торговый дом «Содружество» </p>
 
<p>- ЗАО «Содружество-Соя» </p>
 
<p>- ООО «АРВИ НПК» </p>
 
<p>- ООО «ГАЗПРОМНЕФТЬ МАРИН БУНКЕР» </p>
 
<p>- ООО «Вичюнай-Русь» </p>
 
<p>- ООО «Металлстиль» </p>
 
<p>- ЗАО «Терминал» </p>
 
<p>- ОАО «Научно-производственное объединение «Цифровые телевизионные системы» </p>
 
<p>- ООО «Балтийская табачная фабрика» </p>
 
<p>Стоимость экспортных поставок перечисленных выше предприятий составила 81,1% от экспорта Калининградской области за 2013 год. </p>
 
<p> <b> </b></p>
 
<h2> 
<a name="_Toc352947667">
 <i>Импорт товаров в Калининградскую область</i><i> </i></h2>
 
<p><b> </b></p>
 
<p>Импорт товаров в Калининградскую область осуществляется преимущественно из стран дальнего зарубежья. </p>
 
<p>За 2013 год стоимостные объемы импортных поставок из стран дальнего зарубежья составили 97,2% от общей стоимости импорта. </p>
 
<p align="center"><i> </i></p>
 
<p align="center"><i>Распределение стоимостных объемов импорта Калининградской области </i></p>
 
<p align="center"><i>по странам – контрагентам за 2012 год</i></p>
 
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" align="left" width="521" style="border-collapse: collapse; border: none; margin-left: 6.75pt; margin-right: 6.75pt;"> 
  <tbody> 
    <tr style="height: 23.2pt;"> <td width="158" style="width: 118.4pt; border-style: solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-left-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Страна-контрагент<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: solid solid none none; border-top-color: windowtext; border-top-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Экспорт,<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">тыс.долл. США<o:p></o:p></span></p>
       </td> <td width="79" style="width: 59.15pt; border-style: solid solid none none; border-top-color: windowtext; border-top-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Удельный вес - в<o:p></o:p></span></p>
       
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">экспорте<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: solid solid none none; border-top-color: windowtext; border-top-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 23.2pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">Основные товары<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">Всего экспорт<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">1494913,1<o:p></o:p></span></p>
       </td> <td width="79" style="width: 59.15pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">100,0%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: solid solid solid none; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;"> </span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">Страны СНГ (8 стран)<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">71889,0<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">4,8%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;"> </span></p>
       </td> </tr>
   
    <tr style="height: 15.45pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">УКРАИНА<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">34581,3<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,3%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 15.45pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">готовые продукты из рыбы<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ПРОЧИЕ СТРАНЫ СНГ<o:p></o:p></span></p>
       </td> <td width="110" valign="bottom" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">37307,7<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,5%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;"> </span></p>
       </td> </tr>
   
    <tr style="height: 6.95pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">Страны ДЗ (91 страна)<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">1423024,0<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">95,2%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 6.95pt;"> 
        <h1 style="margin: 0cm 0cm 0.0001pt;"><span style="font-size: 11pt; font-family: 'Times New Roman', serif; font-weight: normal;"> </span></h1>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ИНДИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">398243,6<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">26,6%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">суда и плавучие механизмы<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ЛИТВА<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">183699,2<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">12,3%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">удобрения<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">НОРВЕГИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">132243,9<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">8,8%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ГЕРМАНИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">91365,8<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">6,1%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ПОЛЬША<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">76530,4<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">5,1%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">АЛЖИР<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">66220,7<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">4,4%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ДАНИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">47912,8<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">3,2%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ФИНЛЯНДИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">47403,0<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">3,2%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">корма для животных<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">НИДЕРЛАНДЫ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">43666,8<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,9%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 13.4pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ФРАНЦИЯ<o:p></o:p></span></p>
       </td> <td width="110" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">40133,5<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">2,7%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 13.4pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">масло рапсовое<o:p></o:p></span></p>
       </td> </tr>
   
    <tr style="height: 14.25pt;"> <td width="158" style="width: 118.4pt; border-style: none solid solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="font-family: 'Times New Roman', serif;">ПРОЧИЕ СТРАНЫ ДЗ<o:p></o:p></span></p>
       </td> <td width="110" valign="bottom" style="width: 82.85pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">295604,3<o:p></o:p></span></p>
       </td> <td width="79" valign="bottom" style="width: 59.15pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;">19,9%<o:p></o:p></span></p>
       </td> <td width="174" style="width: 130.25pt; border-style: none solid solid none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt; height: 14.25pt;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-family: 'Times New Roman', serif;"> <o:p></o:p></span></p>
       </td> </tr>
   </tbody>
 </table>
 
<p align="right"><i> </i></p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p> 
  <br />
 </p>
 
<p>В товарной структуре импорта Калининградской области произошли несущественные структурные сдвиги. </p>
 
<p>Главными статьями импорта являются машиностроительная продукция и продовольственные товары. Стоимостной объем машиностроительной продукции уменьшился на 5%, при этом ее доля по сравнению с прошлым годом уменьшилась на 2,7 процентных пункта и составила 56% от общего объема импорта. Стоимостной объем импорта продовольствия увеличился на 13%, при этом доля продовольственных товаров увеличилась на 2,6 процентных пункта и составила 22,5% от общего импорта Калининградской области. </p>
 
<p>Крупнейшие предприятия-импортеры за 2013 год: </p>
 
<p>- ООО «АВТОТОР-Терминал» </p>
 
<p>- ЗАО «ДВ Транспорт» </p>
 
<p>- ЗАО «АВТОТОР» </p>
 
<p>- ООО «Балтсервис» </p>
 
<p>- ООО «Эллада Интертрейд» </p>
 
<p>- ЗАО «АВТОТОР-Менеджмент» </p>
 
<p>- ООО Торговый дом «Содружество» </p>
 
<p>- ЗАО «Содружество-Соя» </p>
 
<p>- ООО «АВТОТОРТехобслуживание» </p>
 
<p>- ЗАО «Алко-Нафта» </p>
 
<p>Стоимость импортных поставок перечисленных предприятий составила 48,9% от импорта Калининградской области за 2013 год. </p>
 
<p><i> </i></p>
 
<h2> 
<a name="_Toc352947668">
 <i>Краткие итоги внешней торговли</i><i> 
<a name="_Toc352947669">
 Калининградской области за 2013 год </i></h2>
 
<p><b><i> </i></b></p>
 
<p>- Внешнеторговый оборот за 2013 год составил 13,6 млрд дол. США, стоимостной объем экспорта вырос на 1%, объем импорта сопоставим с показателем за 2012 год; </p>
 
<p>- Импорт составляет 89% от товарооборота области; </p>
 
<p>- На страны дальнего зарубежья приходится 97% стоимостного объема товарооборота; </p>
 
<p>- Внешнеторговые операции осуществляли 4294 участника ВЭД; </p>
 
<p>- Существенные структурные сдвиги в экспорте. Профилирующие товары экспорта: продовольственные товары (43,9%), продукция машиностроения (32,5%); </p>
 
<p>- Несущественные сдвиги в товарной структуре импорта. Главными статьями импорта являются машиностроительная продукция (56%) и продовольственные товары (22,5%); </p>
 
<p> </p>
 
<p>- Основные страны-партнеры: Германия, Республика Корея, Словакия. </p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>