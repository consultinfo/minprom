<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Импортозамещение");
?><p style="text-align: justify;">
	 «Импортозамещение само по себе не является панацеей. Но оно означает переоборудование производственной части нашей промышленности, сельского хозяйства, а значит - повышение производительности труда» Президент России Владимир Путин.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Целью политики импортозамещения Калининградской области является создание современных высокотехнологичных предприятий, продукция которых будет востребована не только на российском, но и на зарубежных рынках.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 СПРАВОЧНО: На федеральном уровне, в том числе при участии Правительства Калининградской области, уже утверждены 20 отраслевых планов импортозамещения в системообразующих отраслях. В 2015 году вступил в силу Федеральный закон № 488-ФЗ «О промышленной политике», который создал рамочные условия для обновления российской промышленности, в том числе реализации политики импортозамещения, созданы ряд институтов поддержки российских производителей, такие как Фонд развития промышленности и Российских экспортный центр.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Распоряжением Правительства Российской Федерации от 30 сентября 2014 года № 1936-р был утвержден «План содействия импортозамещению в промышленности», в соответствии с которыми федеральными органами исполнительной власти сформированы отраслевые планы мероприятий по импортозамещению.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 В части обрабатывающей промышленности соответствующие планы утверждены приказами Минпромторга России от 31.03.2015 №№ 645-663.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 В соответствии с методическими рекомендациями Минэкономразвития России, планами импортозамещения Минпромторга России, а также с учетом приоритетов, установленных в документах стратегического планирования <br>
	 в сфере промышленной политики Российской Федерации и Калининградской области, приказом Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области утвержден региональный план по импортозамещению в сфере промышленности Калининградской области от 18 апреля 2016 года № 28 (далее – региональный план импортозамещения).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Цель регионального плана импортозамещения - обеспечение координации действий региональных и муниципальных органов власти, делового сообщества, научных и экспертных организаций, направленных на:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; организацию производств импортозамещающей продукции;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; освоение предприятиями обрабатывающей промышленности выпуска новых видов продукции;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; увеличение доли отечественных поставщиков в цепочках поставщиков регионального уровня, повышение конкурентоспособности производимых в Калининградской области товаров, работ и услуг
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; интеграцию производственных цепочек региональных производителей и производителей государств-членов Евразийского экономического союза (ЕАЭС).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Для эффективной коммуникации с бизнесом сообществом региона и научными организациями и транспорентности промышленной политики при Министерстве по промышленной политике, развитию предпринимательства и торговли создан создан экспертный совет по импортозамещению, функциями которого являются.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 К компетенции совета отнесен широкий крут вопросов в сфере промышленной политики региона и реализации политики импортозмещения, а именно:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 - разработка мероприятий, направленных на снижение зависимости промышленности от импорта продукции, оборудования, комплектующих и запасных частей иностранных компаний, а также на социально-экономическое развитие Калининградской области;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 - разработка предложений по мерам поддержки инвестиционных проектов, реализуемых в рамках региональных планов по импортозамещению;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 - подготовка рекомендаций о дополнительном включении инвестиционных проектов в соответствующие региональные планы по импортозамещению;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 - подготовка рекомендаций об исключении инвестиционных проектов из соответствующих региональных планов по импортозамещению.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Ответственный сотрудник министерства за деятельность экспертного совета по импортозамещению – заместитель начальника отдела промышленности и ОЭЗ – Гонобоблев Н.Е.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Полезные ссылки: <br>
</p>
<div>
	<a href="/importozameshchenie/Приказ №28 от 18.04__.pdf">Региональный план по импортозамещению в сфере промышленности Калининградской области</a>
</div>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<div>
	Правительственная комиссия по импортозамещению<br>
	 (<a href="http://government.ru/department/314/about/">http://government.ru/department/314/about/</a>)
</div>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<div>
	Отраслевые планы импортозамещения Минпромторга России<br>
	 (<a href="http://minpromtorg.gov.ru/docs/orders/">http://minpromtorg.gov.ru/docs/orders/</a>)
</div>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<div>
	Международная специализированная выставка<br>
	 «Импортозамещение» (<a href="http://www.imzam-expo.ru/import/">http://www.imzam-expo.ru/import/</a>)
</div>
<p style="text-align: justify;">
</p>
<p>
</p>
<p>
</p>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>