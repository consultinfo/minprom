<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/interviews/([0-9]+).php#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/interviews/detail.php",
	),
	array(
		"CONDITION" => "#^/calendar/([0-9]+).php#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/calendar/detail.php",
	),
	array(
		"CONDITION" => "#^/experts/([0-9]+).php#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/experts/detail.php",
	),
	array(
		"CONDITION" => "#^/opinion/([0-9]+).php#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/opinion/detail.php",
	),
	array(
		"CONDITION" => "#^/documents/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/documents/index.php",
	),
	array(
		"CONDITION" => "#^/support/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/support/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
);

?>