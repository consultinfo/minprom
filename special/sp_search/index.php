<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("Поиск по сайту");
?>
<?/*$APPLICATION->IncludeComponent("bitrix:search.page", "main", array(
	"RESTART" => "N",
	"NO_WORD_LOGIC" => "N",
	"CHECK_DATES" => "N",
	"USE_TITLE_RANK" => "N",
	"DEFAULT_SORT" => "rank",
	"FILTER_NAME" => "",
	"arrFILTER" => array(
		0 => "no",
	),
	"SHOW_WHERE" => "N",
	"SHOW_WHEN" => "N",
	"PAGE_RESULT_COUNT" => "10",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Результаты поиска",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"USE_LANGUAGE_GUESS" => "Y",
	"USE_SUGGEST" => "N",
	"SHOW_RATING" => "",
	"RATING_TYPE" => "",
	"PATH_TO_USER_PROFILE" => "",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);*/?>

<?$APPLICATION->IncludeComponent("bitrix:search.title", "search", array(
	"NUM_CATEGORIES" => "3",
	"TOP_COUNT" => "5",
	"ORDER" => "date",
	"USE_LANGUAGE_GUESS" => "Y",
	"CHECK_DATES" => "N",
	"SHOW_OTHERS" => "Y",
	"PAGE" => "#SITE_DIR#search/",
	"CATEGORY_OTHERS_TITLE" => GetMessage("OTHER_TITLE"),
	"CATEGORY_0_TITLE" => GetMessage("NEWS_TITLE"),
	"CATEGORY_0" => array(
		0 => "iblock_news",
	),
	"CATEGORY_0_iblock_news" => array(
	),
	"CATEGORY_1_TITLE" => "",
	"CATEGORY_1" => array(
		0 => "no",
	),
	"CATEGORY_2_TITLE" => GetMessage("JOB_TITLE"),
	"CATEGORY_2" => array(
	),
	"SHOW_INPUT" => "Y",
	"INPUT_ID" => "title-search-input",
	"CONTAINER_ID" => "title-search"
	),
	false
);?>


<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>