<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("              Торговый реестр");
?> 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>Приказ Минпромторга РФ <a href="/trade_and_market/Prikaz_MPT_602.rtf" >от 16 июля 2010 № 602</a> &quot;Об утверждении формы торгового реестра, порядка формирования торгового реестра и порядка предоставления информации, содержащейся в торговом реестре&quot;</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <p align="center"><b>Уважаемые субъекты предпринимательской деятельности!</b></p>
 
  <p align="center"> </p>
 
  <p>Министерство по промышленной политике, развитию предпринимательства и торговли Калининградской области информирует, что частью 4 статьи 20 Федерального закона от 25.12.2009 № 381-ФЗ &laquo;Об основах государственного регулирования торговой деятельности в Российской Федерации&raquo; предусмотрено формирование торгового реестра. Торговый реестр включает в себя сведения о хозяйствующих субъектах, осуществляющих торговую деятельность, о хозяйствующих субъектах, осуществляющих поставки товаров (за исключением производителей товаров).</p>
 
  <p>Порядок формирования торгового реестра утвержден приказом Министерства промышленности и торговли Российской Федерации от 16.07.2010 № 602.</p>
 
  <p>Внесение сведений о хозяйствующих субъектах, осуществляющих торговую деятельность, и внесение сведений о хозяйствующих субъектах, осуществляющих поставки товаров (за исключением производителей товаров), производится по заявлению хозяйствующего субъекта о внесении сведений в торговый реестр с приложением необходимой информации, указанной в <a href="/trade_and_market/Pril_1.pdf" >приложениях 1</a>,   <a href="/trade_and_market/Pril_2.xlsx" >2</a>,   <a href="/trade_and_market/Pril_3.xlsx" >3</a>, а также следующих документов:</p>
 
  <p>а) заверенной в установленном порядке копии свидетельства о государственной регистрации юридического лица или индивидуального предпринимателя;</p>
 
  <p>б) заверенной в установленном порядке копии свидетельства о постановке на учет в налоговом органе по месту нахождения на территории Российской Федерации (кроме случаев, когда заверенная в установленном порядке отметка о постановке на учет в налоговой инспекции сделана на свидетельстве о государственной регистрации, с указанием идентификационного номера налогоплательщика (ИНН);</p>
 
  <p><a name="Par2"></a>в) документов или их копий, подтверждающих информацию хозяйствующего субъекта, осуществляющего торговую деятельность, о принадлежащих ему торговых объектах;</p>
 
  <p> 
<a name="Par3"></a>
 г) документов или их копий, подтверждающих информацию хозяйствующего субъекта об объектах хозяйствующего субъекта, осуществляющего поставки товаров.</p>
 
  <p>Документы, указанные в подпунктах &quot;в&quot; и &quot;г&quot;, предоставляются на каждый объект, вносимый в торговый реестр. После внесения сведений документы возвращаются хозяйствующему субъекту.</p>
 Заявление хозяйствующего субъекта о внесении сведений в торговый реестр необходимо подать в Министерство по промышленной политике, развитию предпринимательства и торговли Калининградской области (г. Калининград, ул. Дм. Донского, д. 1, кабинет 328м или 401, тел. 599-349, 599-246)</div>
 
<div> 
  <br />
 </div>
 
<br />
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>