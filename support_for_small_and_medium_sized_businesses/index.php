<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гид предпринимателя");
?><p style="text-align: center;">
 <span style="font-size: 9px;"><b>Поддержка малого и среднего предпринимательства&nbsp; </b></span>
</p>
<p style="text-align: justify;">
	 Условия и порядок оказания поддержки субъектам малого и среднего предпринимательства и организациям, образующим инфраструктуру поддержки субъектов малого и среднего предпринимательства, установлены&nbsp;<a href="/support_for_small_and_medium_sized_businesses/programma.pdf" target="_blank">постановлением Правительства Калининградской области от 25 марта 2014 года № 144 «Государственная программа «Развитие промышленности и предпринимательства»</a>&nbsp;(далее - Программа).&nbsp;
</p>
<p style="text-align: justify;">
	 Программа направлена на развитие&nbsp;субъектов малого и среднего предпринимательства (далее – СМСП),&nbsp;осуществляющим деятельность по приоритетным направлениям, установленным стратегическими программными нормативными правовыми актами Калининградской области. Общий объем финансирования Программы на семилетний период составляет 2 446 533,12 тыс. руб., в том числе за счет средств областного бюджета 484 702,00 тыс. руб., за счет средств федерального бюджета – 1 961 831,12 тыс. руб. В 2014 году по Программе предусмотрено финансирование в объеме 577 528,861 тыс. рублей.
</p>
<p style="text-align: justify;">
	 Статистика:&nbsp;<a href="/trade_and_market/СТАТИСТИКА МСП для сайта.pdf" target="_blank">развитие малого и среднего предпринимательства в Калининградской области в 2014 году</a>
</p>
<p style="text-align: justify;">
 <br>
</p>
<p>
 <b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Уважаемые предприниматели!</b>
</p>
<blockquote>
</blockquote>
<blockquote>
</blockquote>
 <b>Автономная некоммерческая организация «Агентство стратегических инициатив по продвижению новых проектов» (далее – Агентство) начала реализацию Национальной предпринимательской инициативы (НПИ)</b>, призванной радикально улучшить условия ведения бизнеса в стране. Впервые в новейшей истории России предприниматели работали над «дорожными картами» (планами действий по улучшению делового климата РФ) вместе с органами власти и экспертным сообществом. В рамках НПИ было разработано и одобрено Правительством РФ 11 «дорожных карт».<br>
<p>
	 Они включают в себя конкретные нормативно-правовые акты, созданные для упрощения ведения бизнеса в нашей стране. Нововведения в рамках «дорожных карт» коснулись практически всех важных для ведения бизнеса сфер, включая ускорение и удешевление процедур подключения к энергосетям, регистрации юридических лиц и прав собственности, строительства, таможенного регулирования, доступа к закупкам госкомпаний и т.д. По «дорожным картам» принято уже более 90% требуемых нормативно-правовых актов, работа по подготовке оставшихся документов активно продолжается.
</p>
<p>
	 Агентство в соответствии с перечнем поручений по реализации Послания Президента РФ организует мониторинг применения в субъектах Российской Федерации нормативных правовых актов, изданных в целях реализации «дорожных карт».
</p>
<p>
	 Реальную пользу вступивших в силу законопроектов и проведенных мероприятий, или отсутствие таковой, способен в полной мере оценить только предприниматель, который ежедневно в рамках своей работы сталкивается с административными барьерами и видит, действительно ли сейчас стало легче, дешевле и быстрее вести бизнес.
</p>
<p>
	 В этой связи Агентство приглашает предпринимателей принять участие в анализе эффективности принимаемых мер по улучшению инвестклимата.
</p>
<p>
	 Для проведения опроса в сети «Интернет» в режиме онлайн подготовлены электронные анкеты по следующим направлениям дорожных карт:
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Повышение доступности энергетической инфраструктуры»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Развитие конкуренции и совершенствование антимонопольной политики»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Поддержка доступа на рынки зарубежных стран и поддержка экспорта»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Совершенствование таможенного администрирования»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Повышение качества государственных услуг в сфере государственного кадастрового учета недвижимого имущества и государственной регистрации прав на недвижимое имущество и сделок с ним»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Расширение доступа субъектов малого и среднего предпринимательства к закупкам инфраструктурных монополий и компаний с государственным участием»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Оптимизация процедур регистрации юридических лиц и индивидуальных предпринимателей»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Повышение качества регуляторной среды для бизнеса»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Совершенствование правового регулирования градостроительной деятельности и улучшение предпринимательского климата в сфере строительства»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Совершенствование налогового администрирования»;
</p>
<p>
	 ·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; «Совершенствование оценочной деятельности».
</p>
<p>
	 Заполнить анкету можно по следующему адресу: <a href="http://xn--80adjkclhjd6blf.xn--p1ai/">http://xn--80adjkclhjd6blf.xn--p1ai/</a>
</p>
<p>
 <b>Опрос продлится с 15 февраля по 15 марта 2016 года.</b>
</p>
<p>
</p>
<p>
	 Уважаемые предприниматели, для нас очень важна Ваша оценка работы органов власти на федеральном и региональном уровнях по сокращению количества и стоимости административных процедур.
</p>
<p>
	 Опрос носит строго анонимный характер. Его результаты будут использованы в обобщенном виде и размещены в открытом доступе. Аналитические итоговые материалы будут переданы в Правительство Российской Федерации&nbsp; и использованы для улучшения существующей ситуации.
</p>
<p style="text-align: justify;">
</p>
<p>
	 Ваше мнение очень важно для нас!
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>