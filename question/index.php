<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Задать вопрос");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN","Y");
?>

<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "obrashenie", Array(
    "COMPONENT_TEMPLATE" => ".default",
    "WEB_FORM_ID" => "1",	// ID веб-формы
    "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
    "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
    "SEF_MODE" => "Y",	// Включить поддержку ЧПУ
    "SEF_FOLDER" => "/question/",	// Каталог ЧПУ (относительно корня сайта)
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
    "LIST_URL" => "",	// Страница со списком результатов
    "EDIT_URL" => "",	// Страница редактирования результата
    "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
    "AJAX_MODE" =>           "Y", // режим AJAX
    "AJAX_OPTION_SHADOW" =>  "N", // затемнять область
    "AJAX_OPTION_JUMP" =>    "Y", // скроллить страницу до компонента
    "AJAX_OPTION_STYLE" =>   "Y", // подключать стили
    "AJAX_OPTION_HISTORY" => "N", // эмулировать переход по страницам
    "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
),
    false
);?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>