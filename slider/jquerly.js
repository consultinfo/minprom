<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("jquerly<script language=\"javascript\" src=\"/bitrix/templates/.default/js/*.js\"></script>");
?> 
<div>/*!</div>
 
<div> * jQuery JavaScript Library v2.1.2-pre c18c6229c84cd2f0c9fe9f6fc3749e2c93608cc7</div>
 
<div> * http://jquery.com/</div>
 
<div> *</div>
 
<div> * Includes Sizzle.js</div>
 
<div> * http://sizzlejs.com/</div>
 
<div> *</div>
 
<div> * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors</div>
 
<div> * Released under the MIT license</div>
 
<div> * http://jquery.org/license</div>
 
<div> *</div>
 
<div> * Date: 2014-06-02T21:06Z</div>
 
<div> */</div>
 
<div> 
  <br />
 </div>
 
<div>(function( global, factory ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( typeof module === &quot;object&quot; &amp;&amp; typeof module.exports === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// For CommonJS and CommonJS-like environments where a proper window is present,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// execute the factory and get jQuery</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// For environments that do not inherently posses a window with a document</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// (such as Node.js), expose a jQuery-making factory as module.exports</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// This accentuates the need for the creation of a real window</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// e.g. var jQuery = require(&quot;jquery&quot;)(window);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// See ticket #14549 for more info</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>module.exports = global.document ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>factory( global, true ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>function( w ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !w.document ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>throw new Error( &quot;jQuery requires a window with a document&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return factory( w );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>factory( global );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Pass this if window is not defined yet</div>
 
<div>}(typeof window !== &quot;undefined&quot; ? window : this, function( window, noGlobal ) {</div>
 
<div> 
  <br />
 </div>
 
<div>// Can't do this because several apps including ASP.NET trace</div>
 
<div>// the stack via arguments.caller.callee and Firefox dies if</div>
 
<div>// you try to trace through &quot;use strict&quot; call chains. (#13335)</div>
 
<div>// Support: Firefox 18+</div>
 
<div>//</div>
 
<div> 
  <br />
 </div>
 
<div>var arr = [];</div>
 
<div> 
  <br />
 </div>
 
<div>var slice = arr.slice;</div>
 
<div> 
  <br />
 </div>
 
<div>var concat = arr.concat;</div>
 
<div> 
  <br />
 </div>
 
<div>var push = arr.push;</div>
 
<div> 
  <br />
 </div>
 
<div>var indexOf = arr.indexOf;</div>
 
<div> 
  <br />
 </div>
 
<div>var class2type = {};</div>
 
<div> 
  <br />
 </div>
 
<div>var toString = class2type.toString;</div>
 
<div> 
  <br />
 </div>
 
<div>var hasOwn = class2type.hasOwnProperty;</div>
 
<div> 
  <br />
 </div>
 
<div>var support = {};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Use the correct document accordingly with window argument (sandbox)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>document = window.document,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>version = &quot;2.1.2-pre c18c6229c84cd2f0c9fe9f6fc3749e2c93608cc7&quot;,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Define a local copy of jQuery</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery = function( selector, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// The jQuery object is actually just the init constructor 'enhanced'</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Need init if jQuery is called (just allow error to be thrown if not included)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return new jQuery.fn.init( selector, context );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: Android&lt;4.1</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Make sure we trim BOM and NBSP</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Matches dashed string for camelizing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rmsPrefix = /^-ms-/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rdashAlpha = /-([\da-z])/gi,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Used by jQuery.camelCase as callback to replace()</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fcamelCase = function( all, letter ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return letter.toUpperCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn = jQuery.prototype = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// The current version of jQuery being used</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jquery: version,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>constructor: jQuery,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Start with an empty selector</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>selector: &quot;&quot;,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// The default length of a jQuery object is 0</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>length: 0,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>toArray: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return slice.call( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Get the Nth element in the matched element set OR</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Get the whole matched element set as a clean array</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>get: function( num ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return num != null ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Return just the one element from the set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>( num &lt; 0 ? this[ num + this.length ] : this[ num ] ) :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Return all the elements in a clean array</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>slice.call( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Take an array of elements and push it onto the stack</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// (returning the new matched element set)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>pushStack: function( elems ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Build a new jQuery matched element set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var ret = jQuery.merge( this.constructor(), elems );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Add the old object onto the stack (as a reference)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret.prevObject = this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret.context = this.context;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Return the newly-formed element set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Execute a callback for every element in the matched set.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// (You can seed the arguments with an array of args, but this is</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// only used internally.)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>each: function( callback, args ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.each( this, callback, args );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>map: function( callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( jQuery.map(this, function( elem, i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return callback.call( elem, i, elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}));</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>slice: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( slice.apply( this, arguments ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>first: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.eq( 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>last: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.eq( -1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>eq: function( i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var len = this.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>j = +i + ( i &lt; 0 ? len : 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( j &gt;= 0 &amp;&amp; j &lt; len ? [ this[j] ] : [] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>end: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.prevObject || this.constructor(null);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// For internal use only.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Behaves like an Array's method, not like a jQuery method.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>push: push,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>sort: arr.sort,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>splice: arr.splice</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend = jQuery.fn.extend = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var options, name, src, copy, copyIsArray, clone,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>target = arguments[0] || {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 1,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>length = arguments.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>deep = false;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Handle a deep copy situation</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( typeof target === &quot;boolean&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>deep = target;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// skip the boolean and the target</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>target = arguments[ i ] || {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i++;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Handle case when target is a string or something (possible in deep copy)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( typeof target !== &quot;object&quot; &amp;&amp; !jQuery.isFunction(target) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>target = {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// extend jQuery itself if only one argument is passed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( i === length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>target = this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; length; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Only deal with non-null/undefined values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( (options = arguments[ i ]) != null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Extend the base object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( name in options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>src = target[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>copy = options[ name ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Prevent never-ending loop</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( target === copy ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>continue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Recurse if we're merging plain objects or arrays</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( deep &amp;&amp; copy &amp;&amp; ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( copyIsArray ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>copyIsArray = false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>clone = src &amp;&amp; jQuery.isArray(src) ? src : [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>clone = src &amp;&amp; jQuery.isPlainObject(src) ? src : {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Never move original objects, clone them</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>target[ name ] = jQuery.extend( deep, clone, copy );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Don't bring in undefined values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else if ( copy !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>target[ name ] = copy;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Return the modified object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return target;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Unique for each copy of jQuery on the page</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>expando: &quot;jQuery&quot; + ( version + Math.random() ).replace( /\D/g, &quot;&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Assume jQuery is ready without the ready module</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isReady: true,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>error: function( msg ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>throw new Error( msg );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>noop: function() {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// See test/unit/core.js for details concerning isFunction.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Since version 1.3, DOM methods and functions like alert</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// aren't supported. They return false on IE (#2968).</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isFunction: function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.type(obj) === &quot;function&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isArray: Array.isArray,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isWindow: function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return obj != null &amp;&amp; obj === obj.window;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isNumeric: function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// parseFloat NaNs numeric-cast false positives (null|true|false|&quot;&quot;)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// ...but misinterprets leading-number strings, particularly hex literals (&quot;0x...&quot;)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// subtraction forces infinities to NaN</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return !jQuery.isArray( obj ) &amp;&amp; obj - parseFloat( obj ) &gt;= 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isPlainObject: function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Not plain objects:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// - Any object or value whose internal [[Class]] property is not &quot;[object Object]&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// - DOM nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// - window</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.type( obj ) !== &quot;object&quot; || obj.nodeType || jQuery.isWindow( obj ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( obj.constructor &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>!hasOwn.call( obj.constructor.prototype, &quot;isPrototypeOf&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If the function hasn't returned already, we're confident that</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// |obj| is a plain object, created by {} or constructed with new Object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isEmptyObject: function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var name;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( name in obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>type: function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( obj == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return obj + &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: Android &lt; 4.0, iOS &lt; 6 (functionish RegExp)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return typeof obj === &quot;object&quot; || typeof obj === &quot;function&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>class2type[ toString.call(obj) ] || &quot;object&quot; :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>typeof obj;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Evaluates a script in a global context</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>globalEval: function( code ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var script,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>indirect = eval;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>code = jQuery.trim( code );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( code ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If the code includes a valid, prologue position</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// strict mode pragma, execute code by injecting a</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// script tag into the document.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( code.indexOf(&quot;use strict&quot;) === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>script = document.createElement(&quot;script&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>script.text = code;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>document.head.appendChild( script ).parentNode.removeChild( script );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Otherwise, avoid the DOM node creation, insertion</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// and removal by using an indirect global eval</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>indirect( code );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Convert dashed to camelCase; used by the css and data modules</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Microsoft forgot to hump their vendor prefix (#9572)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>camelCase: function( string ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return string.replace( rmsPrefix, &quot;ms-&quot; ).replace( rdashAlpha, fcamelCase );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>nodeName: function( elem, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return elem.nodeName &amp;&amp; elem.nodeName.toLowerCase() === name.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// args is for internal usage only</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>each: function( obj, callback, args ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var value,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>length = obj.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>isArray = isArraylike( obj );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( args ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( isArray ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( ; i &lt; length; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>value = callback.apply( obj[ i ], args );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( value === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( i in obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>value = callback.apply( obj[ i ], args );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( value === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// A special, fast, case for the most common use of each</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( isArray ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( ; i &lt; length; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>value = callback.call( obj[ i ], i, obj[ i ] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( value === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( i in obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>value = callback.call( obj[ i ], i, obj[ i ] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( value === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return obj;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: Android&lt;4.1</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>trim: function( text ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return text == null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;&quot; :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>( text + &quot;&quot; ).replace( rtrim, &quot;&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// results is for internal usage only</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>makeArray: function( arr, results ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var ret = results || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( arr != null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( isArraylike( Object(arr) ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.merge( ret,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>typeof arr === &quot;string&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>[ arr ] : arr</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>push.call( ret, arr );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>inArray: function( elem, arr, i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return arr == null ? -1 : indexOf.call( arr, elem, i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>merge: function( first, second ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var len = +second.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>j = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = first.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; j &lt; len; j++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>first[ i++ ] = second[ j ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>first.length = i;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return first;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>grep: function( elems, callback, invert ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var callbackInverse,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matches = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>length = elems.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>callbackExpect = !invert;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Go through the array, only saving the items</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// that pass the validator function</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; i &lt; length; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>callbackInverse = !callback( elems[ i ], i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( callbackInverse !== callbackExpect ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matches.push( elems[ i ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return matches;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// arg is for internal usage only</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>map: function( elems, callback, arg ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var value,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>length = elems.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>isArray = isArraylike( elems ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret = [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Go through the array, translating each of the items to their new values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( isArray ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; length; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>value = callback( elems[ i ], i, arg );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( value != null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>ret.push( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Go through every key on the object,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( i in elems ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>value = callback( elems[ i ], i, arg );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( value != null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>ret.push( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Flatten any nested arrays</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return concat.apply( [], ret );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// A global GUID counter for objects</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>guid: 1,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Bind a function to a context, optionally partially applying any</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// arguments.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>proxy: function( fn, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var tmp, args, proxy;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof context === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tmp = fn[ context ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>context = fn;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = tmp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Quick check to determine if target is callable, in the spec</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// this throws a TypeError, but we will just return undefined.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !jQuery.isFunction( fn ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Simulated bind</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>args = slice.call( arguments, 2 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>proxy = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return fn.apply( context || this, args.concat( slice.call( arguments ) ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Set the guid of unique handler to the same of original handler, so it can be removed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>proxy.guid = fn.guid = fn.guid || jQuery.guid++;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return proxy;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>now: Date.now,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// jQuery.support is not used in Core but other projects attach their</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// properties to it so it needs to exist.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support: support</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Populate the class2type map</div>
 
<div>jQuery.each(&quot;Boolean Number String Function Array Date RegExp Object Error&quot;.split(&quot; &quot;), function(i, name) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>class2type[ &quot;[object &quot; + name + &quot;]&quot; ] = name.toLowerCase();</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>function isArraylike( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var length = obj.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>type = jQuery.type( obj );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( type === &quot;function&quot; || jQuery.isWindow( obj ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( obj.nodeType === 1 &amp;&amp; length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return type === &quot;array&quot; || length === 0 ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>typeof length === &quot;number&quot; &amp;&amp; length &gt; 0 &amp;&amp; ( length - 1 ) in obj;</div>
 
<div>}</div>
 
<div>var Sizzle =</div>
 
<div>/*!</div>
 
<div> * Sizzle CSS Selector Engine v1.10.19</div>
 
<div> * http://sizzlejs.com/</div>
 
<div> *</div>
 
<div> * Copyright 2013 jQuery Foundation, Inc. and other contributors</div>
 
<div> * Released under the MIT license</div>
 
<div> * http://jquery.org/license</div>
 
<div> *</div>
 
<div> * Date: 2014-04-18</div>
 
<div> */</div>
 
<div>(function( window ) {</div>
 
<div> 
  <br />
 </div>
 
<div>var i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Expr,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>getText,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isXML,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>tokenize,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>compile,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>select,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>outermostContext,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>sortInput,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hasDuplicate,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Local document vars</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>setDocument,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>document,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>docElem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>documentIsHTML,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rbuggyQSA,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rbuggyMatches,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>matches,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>contains,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Instance-specific data</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>expando = &quot;sizzle&quot; + -(new Date()),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>preferredDoc = window.document,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>dirruns = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>done = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>classCache = createCache(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>tokenCache = createCache(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>compilerCache = createCache(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>sortOrder = function( a, b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( a === b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hasDuplicate = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// General-purpose constants</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>strundefined = typeof undefined,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>MAX_NEGATIVE = 1 &lt;&lt; 31,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Instance methods</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hasOwn = ({}).hasOwnProperty,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>arr = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>pop = arr.pop,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>push_native = arr.push,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>push = arr.push,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>slice = arr.slice,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Use a stripped-down indexOf if we can't use a native one</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>indexOf = arr.indexOf || function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>len = this.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this[i] === elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return i;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return -1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>booleans = &quot;checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped&quot;,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Regular expressions</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>whitespace = &quot;[\\x20\\t\\r\\n\\f]&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// http://www.w3.org/TR/css3-syntax/#characters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>characterEncoding = &quot;(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+&quot;,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Loosely modeled on CSS identifier characters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>identifier = characterEncoding.replace( &quot;w&quot;, &quot;w#&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>attributes = &quot;\\[&quot; + whitespace + &quot;*(&quot; + characterEncoding + &quot;)(?:&quot; + whitespace +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Operator (capture 2)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;*([*^$|!~]?=)&quot; + whitespace +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// &quot;Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;*(?:'((?:\\\\.|[^\\\\'])*)'|\&quot;((?:\\\\.|[^\\\\\&quot;])*)\&quot;|(&quot; + identifier + &quot;))|)&quot; + whitespace +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;*\\]&quot;,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>pseudos = &quot;:(&quot; + characterEncoding + &quot;)(?:\\((&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// 1. quoted (capture 3; capture 4 or capture 5)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;('((?:\\\\.|[^\\\\'])*)'|\&quot;((?:\\\\.|[^\\\\\&quot;])*)\&quot;)|&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// 2. simple (capture 6)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;((?:\\\\.|[^\\\\()[\\]]|&quot; + attributes + &quot;)*)|&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// 3. anything else (capture 2)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;.*&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;)\\)|)&quot;,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rtrim = new RegExp( &quot;^&quot; + whitespace + &quot;+|((?:^|[^\\\\])(?:\\\\.)*)&quot; + whitespace + &quot;+$&quot;, &quot;g&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rcomma = new RegExp( &quot;^&quot; + whitespace + &quot;*,&quot; + whitespace + &quot;*&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rcombinators = new RegExp( &quot;^&quot; + whitespace + &quot;*([&gt;+~]|&quot; + whitespace + &quot;)&quot; + whitespace + &quot;*&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rattributeQuotes = new RegExp( &quot;=&quot; + whitespace + &quot;*([^\\]'\&quot;]*?)&quot; + whitespace + &quot;*\\]&quot;, &quot;g&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rpseudo = new RegExp( pseudos ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ridentifier = new RegExp( &quot;^&quot; + identifier + &quot;$&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>matchExpr = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;ID&quot;: new RegExp( &quot;^#(&quot; + characterEncoding + &quot;)&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;CLASS&quot;: new RegExp( &quot;^\\.(&quot; + characterEncoding + &quot;)&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;TAG&quot;: new RegExp( &quot;^(&quot; + characterEncoding.replace( &quot;w&quot;, &quot;w*&quot; ) + &quot;)&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;ATTR&quot;: new RegExp( &quot;^&quot; + attributes ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;PSEUDO&quot;: new RegExp( &quot;^&quot; + pseudos ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;CHILD&quot;: new RegExp( &quot;^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(&quot; + whitespace +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;*(even|odd|(([+-]|)(\\d*)n|)&quot; + whitespace + &quot;*(?:([+-]|)&quot; + whitespace +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;*(\\d+)|))&quot; + whitespace + &quot;*\\)|)&quot;, &quot;i&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;bool&quot;: new RegExp( &quot;^(?:&quot; + booleans + &quot;)$&quot;, &quot;i&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// For use in libraries implementing .is()</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// We use this for POS matching in `select`</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;needsContext&quot;: new RegExp( &quot;^&quot; + whitespace + &quot;*[&gt;+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>whitespace + &quot;*((?:-\\d)?\\d*)&quot; + whitespace + &quot;*\\)|)(?=[^-]|$)&quot;, &quot;i&quot; )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rinputs = /^(?:input|select|textarea|button)$/i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rheader = /^h\d$/i,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rnative = /^[^{]+\{\s*\[native \w/,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Easily-parseable/retrievable ID or TAG or CLASS selectors</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rsibling = /[+~]/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rescape = /'|\\/g,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>runescape = new RegExp( &quot;\\\\([\\da-f]{1,6}&quot; + whitespace + &quot;?|(&quot; + whitespace + &quot;)|.)&quot;, &quot;ig&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>funescape = function( _, escaped, escapedWhitespace ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var high = &quot;0x&quot; + escaped - 0x10000;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// NaN means non-codepoint</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: Firefox&lt;24</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Workaround erroneous numeric interpretation of +&quot;0x&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return high !== high || escapedWhitespace ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>escaped :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>high &lt; 0 ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// BMP codepoint</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>String.fromCharCode( high + 0x10000 ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Supplemental Plane codepoint (surrogate pair)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>String.fromCharCode( high &gt;&gt; 10 | 0xD800, high &amp; 0x3FF | 0xDC00 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Optimize for push.apply( _, NodeList )</div>
 
<div>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>push.apply(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>(arr = slice.call( preferredDoc.childNodes )),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>preferredDoc.childNodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: Android&lt;4.0</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Detect silently failing push.apply</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>arr[ preferredDoc.childNodes.length ].nodeType;</div>
 
<div>} catch ( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>push = { apply: arr.length ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Leverage slice if possible</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( target, els ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>push_native.apply( target, slice.call(els) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: IE&lt;9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Otherwise append directly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( target, els ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var j = target.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Can't trust NodeList.length</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( (target[j++] = els[i++]) ) {}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>target.length = j - 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function Sizzle( selector, context, results, seed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var match, elem, m, nodeType,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// QSA vars</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i, groups, old, nid, newContext, newSelector;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>setDocument( context );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>context = context || document;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>results = results || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !selector || typeof selector !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( (nodeType = context.nodeType) !== 1 &amp;&amp; nodeType !== 9 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( documentIsHTML &amp;&amp; !seed ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Shortcuts</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( (match = rquickExpr.exec( selector )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Speed-up: Sizzle(&quot;#ID&quot;)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( (m = match[1]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( nodeType === 9 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem = context.getElementById( m );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Check parentNode to catch when Blackberry 4.6 returns</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// nodes that are no longer in the document (jQuery #6963)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem &amp;&amp; elem.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Handle the case where IE, Opera, and Webkit return items</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// by name instead of ID</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( elem.id === m ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>results.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Context is not a document</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( context.ownerDocument &amp;&amp; (elem = context.ownerDocument.getElementById( m )) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>contains( context, elem ) &amp;&amp; elem.id === m ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>results.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Speed-up: Sizzle(&quot;TAG&quot;)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( match[2] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>push.apply( results, context.getElementsByTagName( selector ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return results;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Speed-up: Sizzle(&quot;.CLASS&quot;)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( (m = match[3]) &amp;&amp; support.getElementsByClassName &amp;&amp; context.getElementsByClassName ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>push.apply( results, context.getElementsByClassName( m ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// QSA path</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( support.qsa &amp;&amp; (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>nid = old = expando;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>newContext = context;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>newSelector = nodeType === 9 &amp;&amp; selector;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// qSA works strangely on Element-rooted queries</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// We can work around this by specifying an extra ID on the root</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// and working up from there (Thanks to Andrew Dupont for the technique)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// IE 8 doesn't work on object elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( nodeType === 1 &amp;&amp; context.nodeName.toLowerCase() !== &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>groups = tokenize( selector );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( (old = context.getAttribute(&quot;id&quot;)) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>nid = old.replace( rescape, &quot;\\$&amp;&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>context.setAttribute( &quot;id&quot;, nid );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>nid = &quot;[id='&quot; + nid + &quot;'] &quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = groups.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>groups[i] = nid + toSelector( groups[i] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>newContext = rsibling.test( selector ) &amp;&amp; testContext( context.parentNode ) || context;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>newSelector = groups.join(&quot;,&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( newSelector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>push.apply( results,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>newContext.querySelectorAll( newSelector )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} catch(qsaError) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} finally {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( !old ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>context.removeAttribute(&quot;id&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// All others</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return select( selector.replace( rtrim, &quot;$1&quot; ), context, results, seed );</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Create key-value caches of limited size</div>
 
<div> * @returns {Function(string, Object)} Returns the Object data after storing it on itself with</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">	</span>property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">	</span>deleting the oldest entry</div>
 
<div> */</div>
 
<div>function createCache() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var keys = [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>function cache( key, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Use (key + &quot; &quot;) to avoid collision with native prototype properties (see Issue #157)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( keys.push( key + &quot; &quot; ) &gt; Expr.cacheLength ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Only keep the most recent entries</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete cache[ keys.shift() ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return (cache[ key + &quot; &quot; ] = value);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return cache;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Mark a function for special use by Sizzle</div>
 
<div> * @param {Function} fn The function to mark</div>
 
<div> */</div>
 
<div>function markFunction( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fn[ expando ] = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return fn;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Support testing using an element</div>
 
<div> * @param {Function} fn Passed the created div and expects a boolean result</div>
 
<div> */</div>
 
<div>function assert( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var div = document.createElement(&quot;div&quot;);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return !!fn( div );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} catch (e) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} finally {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Remove from its parent by default</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( div.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>div.parentNode.removeChild( div );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// release memory in IE</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div = null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Adds the same handler for all of the specified attrs</div>
 
<div> * @param {String} attrs Pipe-separated list of attributes</div>
 
<div> * @param {Function} handler The method that will be applied</div>
 
<div> */</div>
 
<div>function addHandle( attrs, handler ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var arr = attrs.split(&quot;|&quot;),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = attrs.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>Expr.attrHandle[ arr[i] ] = handler;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Checks document order of two siblings</div>
 
<div> * @param {Element} a</div>
 
<div> * @param {Element} b</div>
 
<div> * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b</div>
 
<div> */</div>
 
<div>function siblingCheck( a, b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var cur = b &amp;&amp; a,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>diff = cur &amp;&amp; a.nodeType === 1 &amp;&amp; b.nodeType === 1 &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>( ~b.sourceIndex || MAX_NEGATIVE ) -</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>( ~a.sourceIndex || MAX_NEGATIVE );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Use IE sourceIndex if available on both nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( diff ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return diff;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check if b follows a</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( cur ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (cur = cur.nextSibling) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( cur === b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return -1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return a ? 1 : -1;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Returns a function to use in pseudos for input types</div>
 
<div> * @param {String} type</div>
 
<div> */</div>
 
<div>function createInputPseudo( type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var name = elem.nodeName.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return name === &quot;input&quot; &amp;&amp; elem.type === type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Returns a function to use in pseudos for buttons</div>
 
<div> * @param {String} type</div>
 
<div> */</div>
 
<div>function createButtonPseudo( type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var name = elem.nodeName.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return (name === &quot;input&quot; || name === &quot;button&quot;) &amp;&amp; elem.type === type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Returns a function to use in pseudos for positionals</div>
 
<div> * @param {Function} fn</div>
 
<div> */</div>
 
<div>function createPositionalPseudo( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return markFunction(function( argument ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>argument = +argument;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return markFunction(function( seed, matches ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var j,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matchIndexes = fn( [], seed.length, argument ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = matchIndexes.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Match elements found at the specified indexes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( seed[ (j = matchIndexes[i]) ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>seed[j] = !(matches[j] = seed[j]);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Checks a node for validity as a Sizzle context</div>
 
<div> * @param {Element|Object=} context</div>
 
<div> * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value</div>
 
<div> */</div>
 
<div>function testContext( context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return context &amp;&amp; typeof context.getElementsByTagName !== strundefined &amp;&amp; context;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Expose support vars for convenience</div>
 
<div>support = Sizzle.support = {};</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Detects XML nodes</div>
 
<div> * @param {Element|Object} elem An element or a document</div>
 
<div> * @returns {Boolean} True iff elem is a non-HTML XML node</div>
 
<div> */</div>
 
<div>isXML = Sizzle.isXML = function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// documentElement is verified for cases where it doesn't yet exist</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// (such as loading iframes in IE - #4833)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var documentElement = elem &amp;&amp; (elem.ownerDocument || elem).documentElement;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return documentElement ? documentElement.nodeName !== &quot;HTML&quot; : false;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Sets document-related variables once based on the current document</div>
 
<div> * @param {Element|Object} [doc] An element or document object to use to set the document</div>
 
<div> * @returns {Object} Returns the current document</div>
 
<div> */</div>
 
<div>setDocument = Sizzle.setDocument = function( node ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var hasCompare,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>doc = node ? node.ownerDocument || node : preferredDoc,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>parent = doc.defaultView;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If no document and documentElement is available, return</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return document;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Set our document</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>document = doc;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>docElem = doc.documentElement;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support tests</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>documentIsHTML = !isXML( doc );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE&gt;8</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If iframe document is assigned to &quot;document&quot; variable and if iframe has been reloaded,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// IE will throw &quot;permission denied&quot; error when accessing &quot;document&quot; variable, see jQuery #13936</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// IE6-8 do not support the defaultView property so parent will be undefined</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( parent &amp;&amp; parent !== parent.top ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// IE11 does not have attachEvent, so all must suffer</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( parent.addEventListener ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>parent.addEventListener( &quot;unload&quot;, function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>setDocument();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}, false );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( parent.attachEvent ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>parent.attachEvent( &quot;onunload&quot;, function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>setDocument();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* Attributes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>---------------------------------------------------------------------- */</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE&lt;8</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Verify that getAttribute really returns attributes and not properties (excepting IE8 booleans)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.attributes = assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div.className = &quot;i&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return !div.getAttribute(&quot;className&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* getElement(s)By*</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>---------------------------------------------------------------------- */</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check if getElementsByTagName(&quot;*&quot;) returns only elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.getElementsByTagName = assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div.appendChild( doc.createComment(&quot;&quot;) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return !div.getElementsByTagName(&quot;*&quot;).length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check if getElementsByClassName can be trusted</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.getElementsByClassName = rnative.test( doc.getElementsByClassName ) &amp;&amp; assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div.innerHTML = &quot;&lt;div class='a'&gt;&lt;/div&gt;&lt;div class='a i'&gt;&lt;/div&gt;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: Safari&lt;4</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Catch class over-caching</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div.firstChild.className = &quot;i&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: Opera&lt;10</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Catch gEBCN failure to find non-leading classes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return div.getElementsByClassName(&quot;i&quot;).length === 2;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE&lt;10</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check if getElementById returns elements by name</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// The broken getElementById methods don't pick up programatically-set names,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// so use a roundabout getElementsByName test</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.getById = assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem.appendChild( div ).id = expando;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return !doc.getElementsByName || !doc.getElementsByName( expando ).length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// ID find and filter</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( support.getById ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>Expr.find[&quot;ID&quot;] = function( id, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( typeof context.getElementById !== strundefined &amp;&amp; documentIsHTML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var m = context.getElementById( id );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Check parentNode to catch when Blackberry 4.6 returns</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// nodes that are no longer in the document #6963</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return m &amp;&amp; m.parentNode ? [ m ] : [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>Expr.filter[&quot;ID&quot;] = function( id ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var attrId = id.replace( runescape, funescape );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return elem.getAttribute(&quot;id&quot;) === attrId;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: IE6/7</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// getElementById is not reliable as a find shortcut</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>delete Expr.find[&quot;ID&quot;];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>Expr.filter[&quot;ID&quot;] =  function( id ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var attrId = id.replace( runescape, funescape );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var node = typeof elem.getAttributeNode !== strundefined &amp;&amp; elem.getAttributeNode(&quot;id&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return node &amp;&amp; node.value === attrId;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Tag</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Expr.find[&quot;TAG&quot;] = support.getElementsByTagName ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( tag, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( typeof context.getElementsByTagName !== strundefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return context.getElementsByTagName( tag );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( tag, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>tmp = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>results = context.getElementsByTagName( tag );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Filter out possible comments</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( tag === &quot;*&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( (elem = results[i++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>tmp.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return tmp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Class</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Expr.find[&quot;CLASS&quot;] = support.getElementsByClassName &amp;&amp; function( className, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof context.getElementsByClassName !== strundefined &amp;&amp; documentIsHTML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return context.getElementsByClassName( className );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* QSA/matchesSelector</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>---------------------------------------------------------------------- */</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// QSA and matchesSelector support</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// matchesSelector(:active) reports false when true (IE9/Opera 11.5)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rbuggyMatches = [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// qSa(:focus) reports false when true (Chrome 21)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// We allow this because of a bug in IE8/9 that throws an error</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// whenever `document.activeElement` is accessed on an iframe</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// So, we allow :focus to pass through QSA all the time to avoid the IE error</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// See http://bugs.jquery.com/ticket/13378</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rbuggyQSA = [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Build QSA regex</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Regex strategy adopted from Diego Perini</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Select is set to empty string on purpose</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// This is to test IE's treatment of not explicitly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// setting a boolean content attribute,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// since its presence should be enough</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// http://bugs.jquery.com/ticket/12359</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>div.innerHTML = &quot;&lt;select msallowclip=''&gt;&lt;option selected=''&gt;&lt;/option&gt;&lt;/select&gt;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: IE8, Opera 11-12.16</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Nothing should be selected when empty strings follow ^= or $= or *=</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// The test attribute must be unknown in Opera but &quot;safe&quot; for WinRT</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( div.querySelectorAll(&quot;[msallowclip^='']&quot;).length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rbuggyQSA.push( &quot;[*^$]=&quot; + whitespace + &quot;*(?:''|\&quot;\&quot;)&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: IE8</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Boolean attributes and &quot;value&quot; are not treated correctly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !div.querySelectorAll(&quot;[selected]&quot;).length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rbuggyQSA.push( &quot;\\[&quot; + whitespace + &quot;*(?:value|&quot; + booleans + &quot;)&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Webkit/Opera - :checked should return selected option elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// IE8 throws error here and will not see later tests</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !div.querySelectorAll(&quot;:checked&quot;).length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rbuggyQSA.push(&quot;:checked&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: Windows 8 Native Apps</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// The type and name attributes are restricted during .innerHTML assignment</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var input = doc.createElement(&quot;input&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>input.setAttribute( &quot;type&quot;, &quot;hidden&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>div.appendChild( input ).setAttribute( &quot;name&quot;, &quot;D&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: IE8</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Enforce case-sensitivity of name attribute</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( div.querySelectorAll(&quot;[name=d]&quot;).length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rbuggyQSA.push( &quot;name&quot; + whitespace + &quot;*[*^$|!~]?=&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// IE8 throws error here and will not see later tests</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !div.querySelectorAll(&quot;:enabled&quot;).length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rbuggyQSA.push( &quot;:enabled&quot;, &quot;:disabled&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Opera 10-11 does not throw on post-comma invalid pseudos</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>div.querySelectorAll(&quot;*,:x&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>rbuggyQSA.push(&quot;,.*:&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem.webkitMatchesSelector ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem.mozMatchesSelector ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem.oMatchesSelector ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem.msMatchesSelector) )) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Check to see if it's possible to do matchesSelector</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// on a disconnected node (IE 9)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>support.disconnectedMatch = matches.call( div, &quot;div&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// This should fail with an exception</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Gecko does not error, returns false instead</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matches.call( div, &quot;[s!='']:x&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>rbuggyMatches.push( &quot;!=&quot;, pseudos );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rbuggyQSA = rbuggyQSA.length &amp;&amp; new RegExp( rbuggyQSA.join(&quot;|&quot;) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rbuggyMatches = rbuggyMatches.length &amp;&amp; new RegExp( rbuggyMatches.join(&quot;|&quot;) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* Contains</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>---------------------------------------------------------------------- */</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hasCompare = rnative.test( docElem.compareDocumentPosition );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Element contains another</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Purposefully does not implement inclusive descendent</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// As in, an element does not contain itself</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>contains = hasCompare || rnative.test( docElem.contains ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( a, b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var adown = a.nodeType === 9 ? a.documentElement : a,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>bup = b &amp;&amp; b.parentNode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return a === bup || !!( bup &amp;&amp; bup.nodeType === 1 &amp;&amp; (</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>adown.contains ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>adown.contains( bup ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>a.compareDocumentPosition &amp;&amp; a.compareDocumentPosition( bup ) &amp; 16</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>));</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( a, b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( (b = b.parentNode) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( b === a ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* Sorting</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>---------------------------------------------------------------------- */</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Document order sorting</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>sortOrder = hasCompare ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>function( a, b ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Flag for duplicate removal</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( a === b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hasDuplicate = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Sort on method existence if only one input has compareDocumentPosition</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( compare ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return compare;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Calculate position if both inputs belong to the same document</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>a.compareDocumentPosition( b ) :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Otherwise we know they are disconnected</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>1;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Disconnected nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( compare &amp; 1 ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>(!support.sortDetached &amp;&amp; b.compareDocumentPosition( a ) === compare) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Choose the first element that is related to our preferred document</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( a === doc || a.ownerDocument === preferredDoc &amp;&amp; contains(preferredDoc, a) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return -1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( b === doc || b.ownerDocument === preferredDoc &amp;&amp; contains(preferredDoc, b) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Maintain original order</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return sortInput ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return compare &amp; 4 ? -1 : 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>function( a, b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Exit early if the nodes are identical</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( a === b ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hasDuplicate = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var cur,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>aup = a.parentNode,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>bup = b.parentNode,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ap = [ a ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>bp = [ b ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Parentless nodes are either documents or disconnected</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !aup || !bup ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return a === doc ? -1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>b === doc ? 1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>aup ? -1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>bup ? 1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>sortInput ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If the nodes are siblings, we can do a quick check</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( aup === bup ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return siblingCheck( a, b );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Otherwise we need full lists of their ancestors for comparison</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cur = a;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (cur = cur.parentNode) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ap.unshift( cur );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cur = b;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (cur = cur.parentNode) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>bp.unshift( cur );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Walk down the tree looking for a discrepancy</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( ap[i] === bp[i] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i++;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return i ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Do a sibling check if the nodes have a common ancestor</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>siblingCheck( ap[i], bp[i] ) :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Otherwise nodes in our document sort first</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ap[i] === preferredDoc ? -1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>bp[i] === preferredDoc ? 1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return doc;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Sizzle.matches = function( expr, elements ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return Sizzle( expr, null, null, elements );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Sizzle.matchesSelector = function( elem, expr ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Set document vars if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( ( elem.ownerDocument || elem ) !== document ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>setDocument( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Make sure that attribute selectors are quoted</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>expr = expr.replace( rattributeQuotes, &quot;='$1']&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( support.matchesSelector &amp;&amp; documentIsHTML &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var ret = matches.call( elem, expr );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// IE 9's matchesSelector returns false on disconnected nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( ret || support.disconnectedMatch ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// As well, disconnected nodes are said to be in a document</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// fragment in IE 9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem.document &amp;&amp; elem.document.nodeType !== 11 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} catch(e) {}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return Sizzle( expr, document, null, [ elem ] ).length &gt; 0;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Sizzle.contains = function( context, elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Set document vars if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( ( context.ownerDocument || context ) !== document ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>setDocument( context );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return contains( context, elem );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Sizzle.attr = function( elem, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Set document vars if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( ( elem.ownerDocument || elem ) !== document ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>setDocument( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var fn = Expr.attrHandle[ name.toLowerCase() ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Don't get fooled by Object.prototype properties (jQuery #13807)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>val = fn &amp;&amp; hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn( elem, name, !documentIsHTML ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>undefined;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return val !== undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>val :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>support.attributes || !documentIsHTML ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem.getAttribute( name ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>(val = elem.getAttributeNode(name)) &amp;&amp; val.specified ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val.value :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>null;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Sizzle.error = function( msg ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>throw new Error( &quot;Syntax error, unrecognized expression: &quot; + msg );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Document sorting and removing duplicates</div>
 
<div> * @param {ArrayLike} results</div>
 
<div> */</div>
 
<div>Sizzle.uniqueSort = function( results ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>duplicates = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>j = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Unless we *know* we can detect duplicates, assume their presence</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hasDuplicate = !support.detectDuplicates;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>sortInput = !support.sortStable &amp;&amp; results.slice( 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>results.sort( sortOrder );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( hasDuplicate ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (elem = results[i++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem === results[ i ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>j = duplicates.push( i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( j-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>results.splice( duplicates[ j ], 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Clear input after sorting to release objects</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// See https://github.com/jquery/sizzle/pull/225</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>sortInput = null;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return results;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Utility function for retrieving the text value of an array of DOM nodes</div>
 
<div> * @param {Array|Element} elem</div>
 
<div> */</div>
 
<div>getText = Sizzle.getText = function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var node,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret = &quot;&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>nodeType = elem.nodeType;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !nodeType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If no nodeType, this is expected to be an array</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (node = elem[i++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Do not traverse comment nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret += getText( node );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Use textContent for elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// innerText usage removed for consistency of new lines (jQuery #11153)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof elem.textContent === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.textContent;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Traverse its children</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ret += getText( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else if ( nodeType === 3 || nodeType === 4 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return elem.nodeValue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Do not include comment or processing instruction nodes</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return ret;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Expr = Sizzle.selectors = {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Can be adjusted by the user</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cacheLength: 50,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>createPseudo: markFunction,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>match: matchExpr,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>attrHandle: {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>find: {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>relative: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;&gt;&quot;: { dir: &quot;parentNode&quot;, first: true },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot; &quot;: { dir: &quot;parentNode&quot; },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;+&quot;: { dir: &quot;previousSibling&quot;, first: true },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;~&quot;: { dir: &quot;previousSibling&quot; }</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>preFilter: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;ATTR&quot;: function( match ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>match[1] = match[1].replace( runescape, funescape );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Move the given value to match[3] whether quoted or unquoted</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>match[3] = ( match[3] || match[4] || match[5] || &quot;&quot; ).replace( runescape, funescape );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( match[2] === &quot;~=&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match[3] = &quot; &quot; + match[3] + &quot; &quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return match.slice( 0, 4 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;CHILD&quot;: function( match ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>/* matches from matchExpr[&quot;CHILD&quot;]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>1 type (only|nth|...)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>2 what (child|of-type)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>4 xn-component of xn+y argument ([+-]?\d*n|)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>5 sign of xn-component</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>6 x of xn-component</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>7 sign of y-component</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>8 y of y-component</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>*/</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>match[1] = match[1].toLowerCase();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( match[1].slice( 0, 3 ) === &quot;nth&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// nth-* requires argument</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !match[3] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>Sizzle.error( match[0] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// numeric x and y parameters for Expr.filter.CHILD</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// remember that false/true cast respectively to 0/1</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === &quot;even&quot; || match[3] === &quot;odd&quot; ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match[5] = +( ( match[7] + match[8] ) || match[3] === &quot;odd&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// other types prohibit arguments</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( match[3] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>Sizzle.error( match[0] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return match;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;PSEUDO&quot;: function( match ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var excess,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>unquoted = !match[6] &amp;&amp; match[2];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( matchExpr[&quot;CHILD&quot;].test( match[0] ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Accept quoted arguments as-is</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( match[3] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match[2] = match[4] || match[5] || &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Strip excess characters from unquoted arguments</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( unquoted &amp;&amp; rpseudo.test( unquoted ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Get excess from tokenize (recursively)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>(excess = tokenize( unquoted, true )) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// advance to the next closing parenthesis</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>(excess = unquoted.indexOf( &quot;)&quot;, unquoted.length - excess ) - unquoted.length) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// excess is a negative index</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match[0] = match[0].slice( 0, excess );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match[2] = unquoted.slice( 0, excess );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Return only captures needed by the pseudo filter method (type and argument)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return match.slice( 0, 3 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>filter: {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;TAG&quot;: function( nodeNameSelector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return nodeNameSelector === &quot;*&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>function() { return true; } :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return elem.nodeName &amp;&amp; elem.nodeName.toLowerCase() === nodeName;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;CLASS&quot;: function( className ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var pattern = classCache[ className + &quot; &quot; ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return pattern ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>(pattern = new RegExp( &quot;(^|&quot; + whitespace + &quot;)&quot; + className + &quot;(&quot; + whitespace + &quot;|$)&quot; )) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>classCache( className, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return pattern.test( typeof elem.className === &quot;string&quot; &amp;&amp; elem.className || typeof elem.getAttribute !== strundefined &amp;&amp; elem.getAttribute(&quot;class&quot;) || &quot;&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;ATTR&quot;: function( name, operator, check ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var result = Sizzle.attr( elem, name );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( result == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return operator === &quot;!=&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !operator ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>result += &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return operator === &quot;=&quot; ? result === check :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>operator === &quot;!=&quot; ? result !== check :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>operator === &quot;^=&quot; ? check &amp;&amp; result.indexOf( check ) === 0 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>operator === &quot;*=&quot; ? check &amp;&amp; result.indexOf( check ) &gt; -1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>operator === &quot;$=&quot; ? check &amp;&amp; result.slice( -check.length ) === check :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>operator === &quot;~=&quot; ? ( &quot; &quot; + result + &quot; &quot; ).indexOf( check ) &gt; -1 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>operator === &quot;|=&quot; ? result === check || result.slice( 0, check.length + 1 ) === check + &quot;-&quot; :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;CHILD&quot;: function( type, what, argument, first, last ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var simple = type.slice( 0, 3 ) !== &quot;nth&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>forward = type.slice( -4 ) !== &quot;last&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ofType = what === &quot;of-type&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return first === 1 &amp;&amp; last === 0 ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Shortcut for :nth-*(n)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return !!elem.parentNode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>function( elem, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var cache, outerCache, node, diff, nodeIndex, start,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>dir = simple !== forward ? &quot;nextSibling&quot; : &quot;previousSibling&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>parent = elem.parentNode,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>name = ofType &amp;&amp; elem.nodeName.toLowerCase(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>useCache = !xml &amp;&amp; !ofType;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( parent ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// :(first|last|only)-(child|of-type)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( simple ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>while ( dir ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>node = elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>while ( (node = node[ dir ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">										</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// Reverse direction for :only-* (if we haven't yet done so)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>start = dir = type === &quot;only&quot; &amp;&amp; !start &amp;&amp; &quot;nextSibling&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>start = [ forward ? parent.firstChild : parent.lastChild ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// non-xml :nth-child(...) stores cache data on `parent`</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( forward &amp;&amp; useCache ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Seek `elem` from a previously-cached index</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>outerCache = parent[ expando ] || (parent[ expando ] = {});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>cache = outerCache[ type ] || [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>nodeIndex = cache[0] === dirruns &amp;&amp; cache[1];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>diff = cache[0] === dirruns &amp;&amp; cache[2];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>node = nodeIndex &amp;&amp; parent.childNodes[ nodeIndex ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>while ( (node = ++nodeIndex &amp;&amp; node &amp;&amp; node[ dir ] ||</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// Fallback to seeking `elem` from the start</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>(diff = nodeIndex = 0) || start.pop()) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// When found, cache indexes on `parent` and break</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( node.nodeType === 1 &amp;&amp; ++diff &amp;&amp; node === elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>outerCache[ type ] = [ dirruns, nodeIndex, diff ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Use previously-cached element index if available</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>} else if ( useCache &amp;&amp; (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) &amp;&amp; cache[0] === dirruns ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>diff = cache[1];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Use the same loop as above to seek `elem` from the start</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>while ( (node = ++nodeIndex &amp;&amp; node &amp;&amp; node[ dir ] ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>(diff = nodeIndex = 0) || start.pop()) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) &amp;&amp; ++diff ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>// Cache the index of each encountered element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>if ( useCache ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">										</span>(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>if ( node === elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">										</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Incorporate the offset, then check against cycle size</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>diff -= last;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return diff === first || ( diff % first === 0 &amp;&amp; diff / first &gt;= 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;PSEUDO&quot;: function( pseudo, argument ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// pseudo-class names are case-insensitive</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// http://www.w3.org/TR/selectors/#pseudo-classes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remember that setFilters inherits from pseudos</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var args,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>Sizzle.error( &quot;unsupported pseudo: &quot; + pseudo );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// The user may use createPseudo to indicate that</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// arguments are needed to create the filter function</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// just as Sizzle does</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( fn[ expando ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return fn( argument );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// But maintain support for old signatures</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( fn.length &gt; 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>args = [ pseudo, pseudo, &quot;&quot;, argument ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>markFunction(function( seed, matches ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>var idx,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>matched = fn( seed, argument ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>i = matched.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>idx = indexOf.call( seed, matched[i] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>seed[ idx ] = !( matches[ idx ] = matched[i] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return fn( elem, 0, args );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return fn;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>pseudos: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Potentially complex pseudos</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;not&quot;: markFunction(function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Trim the selector passed to compile</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// to avoid treating leading and trailing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// spaces as combinators</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var input = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>results = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matcher = compile( selector.replace( rtrim, &quot;$1&quot; ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return matcher[ expando ] ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>markFunction(function( seed, matches, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>unmatched = matcher( seed, null, xml, [] ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>i = seed.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Match elements unmatched by `matcher`</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( (elem = unmatched[i]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>seed[i] = !(matches[i] = elem);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>function( elem, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>input[0] = elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matcher( input, null, xml, results );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return !results.pop();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;has&quot;: markFunction(function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return Sizzle( selector, elem ).length &gt; 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;contains&quot;: markFunction(function( text ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) &gt; -1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// &quot;Whether an element is represented by a :lang() selector</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// is based solely on the element's language value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// being equal to the identifier C,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// or beginning with the identifier C immediately followed by &quot;-&quot;.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// The matching of C against the element's language value is performed case-insensitively.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// The identifier C does not have to be a valid language name.&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// http://www.w3.org/TR/selectors/#lang-pseudo</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;lang&quot;: markFunction( function( lang ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// lang value must be a valid identifier</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !ridentifier.test(lang || &quot;&quot;) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>Sizzle.error( &quot;unsupported lang: &quot; + lang );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>lang = lang.replace( runescape, funescape ).toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var elemLang;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>do {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( (elemLang = documentIsHTML ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.lang :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.getAttribute(&quot;xml:lang&quot;) || elem.getAttribute(&quot;lang&quot;)) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elemLang = elemLang.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return elemLang === lang || elemLang.indexOf( lang + &quot;-&quot; ) === 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} while ( (elem = elem.parentNode) &amp;&amp; elem.nodeType === 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Miscellaneous</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;target&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var hash = window.location &amp;&amp; window.location.hash;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return hash &amp;&amp; hash.slice( 1 ) === elem.id;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;root&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem === docElem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;focus&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem === document.activeElement &amp;&amp; (!document.hasFocus || document.hasFocus()) &amp;&amp; !!(elem.type || elem.href || ~elem.tabIndex);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Boolean properties</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;enabled&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.disabled === false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;disabled&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.disabled === true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;checked&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// In CSS3, :checked should return both checked and selected elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var nodeName = elem.nodeName.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return (nodeName === &quot;input&quot; &amp;&amp; !!elem.checked) || (nodeName === &quot;option&quot; &amp;&amp; !!elem.selected);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;selected&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Accessing this property makes selected-by-default</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// options in Safari work properly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem.parentNode.selectedIndex;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.selected === true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Contents</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;empty&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// http://www.w3.org/TR/selectors/#empty-pseudo</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>//   but not by others (comment: 8; processing instruction: 7; etc.)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// nodeType &lt; 6 works because attributes (2) do not appear as children</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( elem.nodeType &lt; 6 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;parent&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return !Expr.pseudos[&quot;empty&quot;]( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Element/input types</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;header&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return rheader.test( elem.nodeName );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;input&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return rinputs.test( elem.nodeName );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;button&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var name = elem.nodeName.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return name === &quot;input&quot; &amp;&amp; elem.type === &quot;button&quot; || name === &quot;button&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;text&quot;: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var attr;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.nodeName.toLowerCase() === &quot;input&quot; &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem.type === &quot;text&quot; &amp;&amp;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Support: IE&lt;8</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// New HTML5 attribute values (e.g., &quot;search&quot;) appear with elem.type === &quot;text&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( (attr = elem.getAttribute(&quot;type&quot;)) == null || attr.toLowerCase() === &quot;text&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Position-in-collection</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;first&quot;: createPositionalPseudo(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return [ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;last&quot;: createPositionalPseudo(function( matchIndexes, length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return [ length - 1 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;eq&quot;: createPositionalPseudo(function( matchIndexes, length, argument ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return [ argument &lt; 0 ? argument + length : argument ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;even&quot;: createPositionalPseudo(function( matchIndexes, length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var i = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; length; i += 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matchIndexes.push( i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return matchIndexes;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;odd&quot;: createPositionalPseudo(function( matchIndexes, length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var i = 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; length; i += 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matchIndexes.push( i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return matchIndexes;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;lt&quot;: createPositionalPseudo(function( matchIndexes, length, argument ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var i = argument &lt; 0 ? argument + length : argument;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; --i &gt;= 0; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matchIndexes.push( i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return matchIndexes;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;gt&quot;: createPositionalPseudo(function( matchIndexes, length, argument ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var i = argument &lt; 0 ? argument + length : argument;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; ++i &lt; length; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matchIndexes.push( i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return matchIndexes;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>})</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Expr.pseudos[&quot;nth&quot;] = Expr.pseudos[&quot;eq&quot;];</div>
 
<div> 
  <br />
 </div>
 
<div>// Add button/input type pseudos</div>
 
<div>for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Expr.pseudos[ i ] = createInputPseudo( i );</div>
 
<div>}</div>
 
<div>for ( i in { submit: true, reset: true } ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Expr.pseudos[ i ] = createButtonPseudo( i );</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Easy API for creating new setFilters</div>
 
<div>function setFilters() {}</div>
 
<div>setFilters.prototype = Expr.filters = Expr.pseudos;</div>
 
<div>Expr.setFilters = new setFilters();</div>
 
<div> 
  <br />
 </div>
 
<div>tokenize = Sizzle.tokenize = function( selector, parseOnly ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var matched, match, tokens, type,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>soFar, groups, preFilters,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cached = tokenCache[ selector + &quot; &quot; ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( cached ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return parseOnly ? 0 : cached.slice( 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>soFar = selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>groups = [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>preFilters = Expr.preFilter;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>while ( soFar ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Comma and first run</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !matched || (match = rcomma.exec( soFar )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( match ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Don't consume trailing commas as valid</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>soFar = soFar.slice( match[0].length ) || soFar;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>groups.push( (tokens = []) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>matched = false;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Combinators</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( (match = rcombinators.exec( soFar )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matched = match.shift();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tokens.push({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>value: matched,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Cast descendant combinators to space</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>type: match[0].replace( rtrim, &quot; &quot; )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>soFar = soFar.slice( matched.length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Filters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( type in Expr.filter ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( (match = matchExpr[ type ].exec( soFar )) &amp;&amp; (!preFilters[ type ] ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>(match = preFilters[ type ]( match ))) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matched = match.shift();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>tokens.push({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>value: matched,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>type: type,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matches: match</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>soFar = soFar.slice( matched.length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !matched ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Return the length of the invalid excess</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// if we're just parsing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Otherwise, throw an error or return tokens</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return parseOnly ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>soFar.length :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>soFar ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>Sizzle.error( selector ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Cache the tokens</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tokenCache( selector, groups ).slice( 0 );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>function toSelector( tokens ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>len = tokens.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>selector = &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>selector += tokens[i].value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return selector;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function addCombinator( matcher, combinator, base ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var dir = combinator.dir,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>checkNonElements = base &amp;&amp; dir === &quot;parentNode&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>doneName = done++;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return combinator.first ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Check against closest ancestor/preceding element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( elem, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( (elem = elem[ dir ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( elem.nodeType === 1 || checkNonElements ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return matcher( elem, context, xml );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Check against all ancestor/preceding elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( elem, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var oldCache, outerCache,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>newCache = [ dirruns, doneName ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( (elem = elem[ dir ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem.nodeType === 1 || checkNonElements ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( matcher( elem, context, xml ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( (elem = elem[ dir ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem.nodeType === 1 || checkNonElements ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>outerCache = elem[ expando ] || (elem[ expando ] = {});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( (oldCache = outerCache[ dir ]) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>oldCache[ 0 ] === dirruns &amp;&amp; oldCache[ 1 ] === doneName ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Assign to newCache so results back-propagate to previous elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>return (newCache[ 2 ] = oldCache[ 2 ]);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Reuse newcache so results back-propagate to previous elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>outerCache[ dir ] = newCache;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// A match means we're done; a fail means we have to keep checking</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function elementMatcher( matchers ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return matchers.length &gt; 1 ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( elem, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var i = matchers.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !matchers[i]( elem, context, xml ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>matchers[0];</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function multipleContexts( selector, contexts, results ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>len = contexts.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>Sizzle( selector, contexts[i], results );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return results;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function condense( unmatched, map, filter, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>newUnmatched = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>len = unmatched.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>mapped = map != null;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( (elem = unmatched[i]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !filter || filter( elem, context, xml ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>newUnmatched.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( mapped ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>map.push( i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return newUnmatched;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( postFilter &amp;&amp; !postFilter[ expando ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>postFilter = setMatcher( postFilter );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( postFinder &amp;&amp; !postFinder[ expando ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>postFinder = setMatcher( postFinder, postSelector );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return markFunction(function( seed, results, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var temp, i, elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>preMap = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>postMap = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>preexisting = results.length,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Get initial elements from seed or context</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elems = seed || multipleContexts( selector || &quot;*&quot;, context.nodeType ? [ context ] : context, [] ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Prefilter to get matcher input, preserving a map for seed-results synchronization</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matcherIn = preFilter &amp;&amp; ( seed || !selector ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>condense( elems, preMap, preFilter, context, xml ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elems,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matcherOut = matcher ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>postFinder || ( seed ? preFilter : preexisting || postFilter ) ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// ...intermediate processing is necessary</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>[] :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// ...otherwise use results directly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>results :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matcherIn;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Find primary matches</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( matcher ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matcher( matcherIn, matcherOut, context, xml );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Apply postFilter</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( postFilter ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>temp = condense( matcherOut, postMap );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>postFilter( temp, [], context, xml );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Un-match failing elements by moving them back to matcherIn</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = temp.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( (elem = temp[i]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( seed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( postFinder || preFilter ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( postFinder ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Get the final matcherOut by condensing this intermediate into postFinder contexts</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>temp = [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i = matcherOut.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( (elem = matcherOut[i]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Restore matcherIn since elem is not yet a final match</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>temp.push( (matcherIn[i] = elem) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>postFinder( null, (matcherOut = []), temp, xml );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Move matched elements from seed to results to keep them synchronized</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = matcherOut.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( (elem = matcherOut[i]) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>(temp = postFinder ? indexOf.call( seed, elem ) : preMap[i]) &gt; -1 ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>seed[temp] = !(results[temp] = elem);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Add elements to results, through postFinder if defined</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matcherOut = condense(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matcherOut === results ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matcherOut.splice( preexisting, matcherOut.length ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matcherOut</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( postFinder ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>postFinder( null, results, matcherOut, xml );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>push.apply( results, matcherOut );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function matcherFromTokens( tokens ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var checkContext, matcher, j,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>len = tokens.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>leadingRelative = Expr.relative[ tokens[0].type ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>implicitRelative = leadingRelative || Expr.relative[&quot; &quot;],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = leadingRelative ? 1 : 0,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// The foundational matcher ensures that elements are reachable from top-level context(s)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>matchContext = addCombinator( function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem === checkContext;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}, implicitRelative, true ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>matchAnyContext = addCombinator( function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return indexOf.call( checkContext, elem ) &gt; -1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}, implicitRelative, true ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>matchers = [ function( elem, context, xml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return ( !leadingRelative &amp;&amp; ( xml || context !== outermostContext ) ) || (</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>(checkContext = context).nodeType ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matchContext( elem, context, xml ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matchAnyContext( elem, context, xml ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( (matcher = Expr.relative[ tokens[i].type ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Return special upon seeing a positional matcher</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( matcher[ expando ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Find the next relative operator (if any) for proper handling</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>j = ++i;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( ; j &lt; len; j++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( Expr.relative[ tokens[j].type ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return setMatcher(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i &gt; 1 &amp;&amp; elementMatcher( matchers ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i &gt; 1 &amp;&amp; toSelector(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// If the preceding token was a descendant combinator, insert an implicit any-element `*`</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === &quot; &quot; ? &quot;*&quot; : &quot;&quot; })</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>).replace( rtrim, &quot;$1&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matcher,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i &lt; j &amp;&amp; matcherFromTokens( tokens.slice( i, j ) ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>j &lt; len &amp;&amp; matcherFromTokens( (tokens = tokens.slice( j )) ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>j &lt; len &amp;&amp; toSelector( tokens )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matchers.push( matcher );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return elementMatcher( matchers );</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function matcherFromGroupMatchers( elementMatchers, setMatchers ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var bySet = setMatchers.length &gt; 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>byElement = elementMatchers.length &gt; 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>superMatcher = function( seed, context, xml, results, outermost ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var elem, j, matcher,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matchedCount = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = &quot;0&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>unmatched = seed &amp;&amp; [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>setMatched = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>contextBackup = outermostContext,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// We must always have either seed elements or outermost context</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elems = seed || byElement &amp;&amp; Expr.find[&quot;TAG&quot;]( &quot;*&quot;, outermost ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Use integer dirruns iff this is the outermost matcher</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>len = elems.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( outermost ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>outermostContext = context !== document &amp;&amp; context;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add elements passing elementMatchers directly to results</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Keep `i` a string if there are no elements so `matchedCount` will be &quot;00&quot; below</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: IE&lt;9, Safari</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Tolerate NodeList properties (IE: &quot;length&quot;; Safari: &lt;number&gt;) matching elements by id</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i !== len &amp;&amp; (elem = elems[i]) != null; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( byElement &amp;&amp; elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>j = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>while ( (matcher = elementMatchers[j++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( matcher( elem, context, xml ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>results.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( outermost ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>dirruns = dirrunsUnique;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Track unmatched elements for set filters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( bySet ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// They will have gone through all possible matchers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( (elem = !matcher &amp;&amp; elem) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>matchedCount--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Lengthen the array for every element, matched or not</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( seed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>unmatched.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Apply set filters to unmatched elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matchedCount += i;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( bySet &amp;&amp; i !== matchedCount ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>j = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( (matcher = setMatchers[j++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matcher( unmatched, setMatched, context, xml );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( seed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Reintegrate element matches to eliminate the need for sorting</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( matchedCount &gt; 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( !(unmatched[i] || setMatched[i]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>setMatched[i] = pop.call( results );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Discard index placeholder values to get only actual matches</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>setMatched = condense( setMatched );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Add matches to results</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>push.apply( results, setMatched );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Seedless set matches succeeding multiple successful matchers stipulate sorting</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( outermost &amp;&amp; !seed &amp;&amp; setMatched.length &gt; 0 &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>( matchedCount + setMatchers.length ) &gt; 1 ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>Sizzle.uniqueSort( results );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Override manipulation of globals by nested matchers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( outermost ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>dirruns = dirrunsUnique;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>outermostContext = contextBackup;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return unmatched;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return bySet ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>markFunction( superMatcher ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>superMatcher;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>setMatchers = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elementMatchers = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cached = compilerCache[ selector + &quot; &quot; ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !cached ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Generate a function of recursive functions that can be used to check each element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !match ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>match = tokenize( selector );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = match.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cached = matcherFromTokens( match[i] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( cached[ expando ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>setMatchers.push( cached );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elementMatchers.push( cached );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Cache the compiled function</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Save selector and tokenization</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cached.selector = selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return cached;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * A low-level selection function that works with Sizzle's compiled</div>
 
<div> *  selector functions</div>
 
<div> * @param {String|Function} selector A selector or a pre-compiled</div>
 
<div> *  selector function built with Sizzle.compile</div>
 
<div> * @param {Element} context</div>
 
<div> * @param {Array} [results]</div>
 
<div> * @param {Array} [seed] A set of elements to match against</div>
 
<div> */</div>
 
<div>select = Sizzle.select = function( selector, context, results, seed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i, tokens, token, type, find,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>compiled = typeof selector === &quot;function&quot; &amp;&amp; selector,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>match = !seed &amp;&amp; tokenize( (selector = compiled.selector || selector) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>results = results || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Try to minimize operations if there is no seed and only one group</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( match.length === 1 ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Take a shortcut and set the context if the root selector is an ID</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>tokens = match[0] = match[0].slice( 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( tokens.length &gt; 2 &amp;&amp; (token = tokens[0]).type === &quot;ID&quot; &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>support.getById &amp;&amp; context.nodeType === 9 &amp;&amp; documentIsHTML &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>Expr.relative[ tokens[1].type ] ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>context = ( Expr.find[&quot;ID&quot;]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return results;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Precompiled matchers will still verify ancestry, so step up a level</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( compiled ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>context = context.parentNode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>selector = selector.slice( tokens.shift().value.length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fetch a seed set for right-to-left matching</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = matchExpr[&quot;needsContext&quot;].test( selector ) ? 0 : tokens.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>token = tokens[i];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Abort if we hit a combinator</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( Expr.relative[ (type = token.type) ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( (find = Expr.find[ type ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Search, expanding context for leading sibling combinators</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( (seed = find(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>token.matches[0].replace( runescape, funescape ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>rsibling.test( tokens[0].type ) &amp;&amp; testContext( context.parentNode ) || context</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>)) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// If seed is empty or no tokens remain, we can return early</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tokens.splice( i, 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>selector = seed.length &amp;&amp; toSelector( tokens );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( !selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>push.apply( results, seed );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return results;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Compile and execute a filtering function if one is not provided</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Provide `match` to avoid retokenization if we modified the selector above</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>( compiled || compile( selector, match ) )(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>seed,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>context,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>!documentIsHTML,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>results,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>rsibling.test( selector ) &amp;&amp; testContext( context.parentNode ) || context</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return results;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>// One-time assignments</div>
 
<div> 
  <br />
 </div>
 
<div>// Sort stability</div>
 
<div>support.sortStable = expando.split(&quot;&quot;).sort( sortOrder ).join(&quot;&quot;) === expando;</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: Chrome&lt;14</div>
 
<div>// Always assume duplicates if they aren't passed to the comparison function</div>
 
<div>support.detectDuplicates = !!hasDuplicate;</div>
 
<div> 
  <br />
 </div>
 
<div>// Initialize against the default document</div>
 
<div>setDocument();</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: Webkit&lt;537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)</div>
 
<div>// Detached nodes confoundingly follow *each other*</div>
 
<div>support.sortDetached = assert(function( div1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Should return 1, but returns 4 (following)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return div1.compareDocumentPosition( document.createElement(&quot;div&quot;) ) &amp; 1;</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE&lt;8</div>
 
<div>// Prevent attribute/property &quot;interpolation&quot;</div>
 
<div>// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx</div>
 
<div>if ( !assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>div.innerHTML = &quot;&lt;a href='#'&gt;&lt;/a&gt;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return div.firstChild.getAttribute(&quot;href&quot;) === &quot;#&quot; ;</div>
 
<div>}) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>addHandle( &quot;type|href|height|width&quot;, function( elem, name, isXML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !isXML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.getAttribute( name, name.toLowerCase() === &quot;type&quot; ? 1 : 2 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE&lt;9</div>
 
<div>// Use defaultValue in place of getAttribute(&quot;value&quot;)</div>
 
<div>if ( !support.attributes || !assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>div.innerHTML = &quot;&lt;input/&gt;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>div.firstChild.setAttribute( &quot;value&quot;, &quot;&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return div.firstChild.getAttribute( &quot;value&quot; ) === &quot;&quot;;</div>
 
<div>}) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>addHandle( &quot;value&quot;, function( elem, name, isXML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !isXML &amp;&amp; elem.nodeName.toLowerCase() === &quot;input&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.defaultValue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE&lt;9</div>
 
<div>// Use getAttributeNode to fetch booleans when getAttribute lies</div>
 
<div>if ( !assert(function( div ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return div.getAttribute(&quot;disabled&quot;) == null;</div>
 
<div>}) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>addHandle( booleans, function( elem, name, isXML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var val;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !isXML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem[ name ] === true ? name.toLowerCase() :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>(val = elem.getAttributeNode( name )) &amp;&amp; val.specified ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>val.value :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>return Sizzle;</div>
 
<div> 
  <br />
 </div>
 
<div>})( window );</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.find = Sizzle;</div>
 
<div>jQuery.expr = Sizzle.selectors;</div>
 
<div>jQuery.expr[&quot;:&quot;] = jQuery.expr.pseudos;</div>
 
<div>jQuery.unique = Sizzle.uniqueSort;</div>
 
<div>jQuery.text = Sizzle.getText;</div>
 
<div>jQuery.isXMLDoc = Sizzle.isXML;</div>
 
<div>jQuery.contains = Sizzle.contains;</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var rneedsContext = jQuery.expr.match.needsContext;</div>
 
<div> 
  <br />
 </div>
 
<div>var rsingleTag = (/^&lt;(\w+)\s*\/?&gt;(?:&lt;\/\1&gt;|)$/);</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var risSimple = /^.[^:#\[\.,]*$/;</div>
 
<div> 
  <br />
 </div>
 
<div>// Implement the identical functionality for filter and not</div>
 
<div>function winnow( elements, qualifier, not ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( jQuery.isFunction( qualifier ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.grep( elements, function( elem, i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>/* jshint -W018 */</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return !!qualifier.call( elem, i, elem ) !== not;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( qualifier.nodeType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.grep( elements, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return ( elem === qualifier ) !== not;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( typeof qualifier === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( risSimple.test( qualifier ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jQuery.filter( qualifier, elements, not );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>qualifier = jQuery.filter( qualifier, elements );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return jQuery.grep( elements, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return ( indexOf.call( qualifier, elem ) &gt;= 0 ) !== not;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.filter = function( expr, elems, not ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var elem = elems[ 0 ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( not ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>expr = &quot;:not(&quot; + expr + &quot;)&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return elems.length === 1 &amp;&amp; elem.nodeType === 1 ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.nodeType === 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}));</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>find: function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>len = this.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>self = this;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof selector !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.pushStack( jQuery( selector ).filter(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( i = 0; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( jQuery.contains( self[ i ], this ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( i = 0; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.find( selector, self[ i ], ret );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Needed because $( selector, context ) becomes $( context ).find( selector )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret = this.pushStack( len &gt; 1 ? jQuery.unique( ret ) : ret );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret.selector = this.selector ? this.selector + &quot; &quot; + selector : selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>filter: function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( winnow(this, selector || [], false) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>not: function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( winnow(this, selector || [], true) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>is: function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return !!winnow(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If this is a positional/relative selector, check membership in the returned set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// so $(&quot;p:first&quot;).is(&quot;p:last&quot;) won't return true for a doc with two &quot;p&quot;.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>typeof selector === &quot;string&quot; &amp;&amp; rneedsContext.test( selector ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( selector ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>selector || [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>false</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>).length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Initialize a jQuery object</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// A central reference to the root jQuery(document)</div>
 
<div>var rootjQuery,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// A simple way to check for HTML strings</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Prioritize #id over &lt;tag&gt; to avoid XSS via location.hash (#9521)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Strict HTML recognition (#11290: must start with &lt;)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rquickExpr = /^(?:\s*(&lt;[\w\W]+&gt;)[^&gt;]*|#([\w-]*))$/,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>init = jQuery.fn.init = function( selector, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var match, elem;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// HANDLE: $(&quot;&quot;), $(null), $(undefined), $(false)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Handle HTML strings</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof selector === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( selector[0] === &quot;&lt;&quot; &amp;&amp; selector[ selector.length - 1 ] === &quot;&gt;&quot; &amp;&amp; selector.length &gt;= 3 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Assume that strings that start and end with &lt;&gt; are HTML and skip the regex check</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match = [ null, selector, null ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>match = rquickExpr.exec( selector );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Match html or make sure no context is specified for #id</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( match &amp;&amp; (match[1] || !context) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// HANDLE: $(html) -&gt; $(array)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( match[1] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>context = context instanceof jQuery ? context[0] : context;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// scripts is true for back-compat</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Intentionally let the error be thrown if parseHTML is not present</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.merge( this, jQuery.parseHTML(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>match[1],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>context &amp;&amp; context.nodeType ? context.ownerDocument || context : document,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>true</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// HANDLE: $(html, props)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( rsingleTag.test( match[1] ) &amp;&amp; jQuery.isPlainObject( context ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>for ( match in context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Properties of context are called as methods if possible</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( jQuery.isFunction( this[ match ] ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>this[ match ]( context[ match ] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// ...and otherwise set as attributes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>this.attr( match, context[ match ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// HANDLE: $(#id)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem = document.getElementById( match[2] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Check parentNode to catch when Blackberry 4.6 returns</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// nodes that are no longer in the document #6963</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem &amp;&amp; elem.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Inject the element directly into the jQuery object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>this.length = 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>this[0] = elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>this.context = document;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>this.selector = selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// HANDLE: $(expr, $(...))</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( !context || context.jquery ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return ( context || rootjQuery ).find( selector );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// HANDLE: $(expr, context)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// (which is just equivalent to: $(context).find(expr)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this.constructor( context ).find( selector );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// HANDLE: $(DOMElement)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( selector.nodeType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.context = this[0] = selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.length = 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// HANDLE: $(function)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Shortcut for document ready</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( jQuery.isFunction( selector ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return typeof rootjQuery.ready !== &quot;undefined&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rootjQuery.ready( selector ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Execute immediately if ready is not present</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>selector( jQuery );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( selector.selector !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.selector = selector.selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.context = selector.context;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.makeArray( selector, this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Give the init function the jQuery prototype for later instantiation</div>
 
<div>init.prototype = jQuery.fn;</div>
 
<div> 
  <br />
 </div>
 
<div>// Initialize central reference</div>
 
<div>rootjQuery = jQuery( document );</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var rparentsprev = /^(?:parents|prev(?:Until|All))/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// methods guaranteed to produce a unique set when starting from a unique set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>guaranteedUnique = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>children: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>contents: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>next: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>prev: true</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>dir: function( elem, dir, until ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var matched = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>truncate = until !== undefined;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (elem = elem[ dir ]) &amp;&amp; elem.nodeType !== 9 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( truncate &amp;&amp; jQuery( elem ).is( until ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matched.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return matched;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>sibling: function( n, elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var matched = [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; n; n = n.nextSibling ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( n.nodeType === 1 &amp;&amp; n !== elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matched.push( n );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return matched;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>has: function( target ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var targets = jQuery( target, this ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>l = targets.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.filter(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var i = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( jQuery.contains( this, targets[i] ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>closest: function( selectors, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var cur,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>l = this.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matched = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>pos = rneedsContext.test( selectors ) || typeof selectors !== &quot;string&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( selectors, context || this.context ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( cur = this[i]; cur &amp;&amp; cur !== context; cur = cur.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Always skip document fragments</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( cur.nodeType &lt; 11 &amp;&amp; (pos ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>pos.index(cur) &gt; -1 :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Don't pass non-elements to Sizzle</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>cur.nodeType === 1 &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>jQuery.find.matchesSelector(cur, selectors)) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matched.push( cur );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( matched.length &gt; 1 ? jQuery.unique( matched ) : matched );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Determine the position of an element within</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// the matched set of elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>index: function( elem ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// No argument, return index in parent</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return ( this[ 0 ] &amp;&amp; this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// index in selector</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof elem === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return indexOf.call( jQuery( elem ), this[ 0 ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Locate the position of the desired element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return indexOf.call( this,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If it receives a jQuery object, the first element is used</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem.jquery ? elem[ 0 ] : elem</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>add: function( selector, context ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.unique(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.merge( this.get(), jQuery( selector, context ) )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>addBack: function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.add( selector == null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.prevObject : this.prevObject.filter(selector)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>function sibling( cur, dir ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>while ( (cur = cur[dir]) &amp;&amp; cur.nodeType !== 1 ) {}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return cur;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.each({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>parent: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var parent = elem.parentNode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return parent &amp;&amp; parent.nodeType !== 11 ? parent : null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>parents: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.dir( elem, &quot;parentNode&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>parentsUntil: function( elem, i, until ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.dir( elem, &quot;parentNode&quot;, until );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>next: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return sibling( elem, &quot;nextSibling&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prev: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return sibling( elem, &quot;previousSibling&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>nextAll: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.dir( elem, &quot;nextSibling&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prevAll: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.dir( elem, &quot;previousSibling&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>nextUntil: function( elem, i, until ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.dir( elem, &quot;nextSibling&quot;, until );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prevUntil: function( elem, i, until ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.dir( elem, &quot;previousSibling&quot;, until );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>siblings: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>children: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.sibling( elem.firstChild );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>contents: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return elem.contentDocument || jQuery.merge( [], elem.childNodes );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}, function( name, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fn[ name ] = function( until, selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var matched = jQuery.map( this, fn, until );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( name.slice( -5 ) !== &quot;Until&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>selector = until;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( selector &amp;&amp; typeof selector === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>matched = jQuery.filter( selector, matched );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( this.length &gt; 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remove duplicates</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !guaranteedUnique[ name ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.unique( matched );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Reverse order for parents* and prev-derivatives</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( rparentsprev.test( name ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>matched.reverse();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( matched );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div>var rnotwhite = (/\S+/g);</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// String to Object options format cache</div>
 
<div>var optionsCache = {};</div>
 
<div> 
  <br />
 </div>
 
<div>// Convert String-formatted options into Object-formatted ones and store in cache</div>
 
<div>function createOptions( options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var object = optionsCache[ options ] = {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>object[ flag ] = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return object;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/*</div>
 
<div> * Create a callback list using the following parameters:</div>
 
<div> *</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">	</span>options: an optional list of space-separated options that will change how</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">			</span>the callback list behaves or a more traditional option object</div>
 
<div> *</div>
 
<div> * By default a callback list will act like an event callback list and can be</div>
 
<div> * &quot;fired&quot; multiple times.</div>
 
<div> *</div>
 
<div> * Possible options:</div>
 
<div> *</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">	</span>once:<span class="Apple-tab-span" style="white-space: pre;">			</span>will ensure the callback list can only be fired once (like a Deferred)</div>
 
<div> *</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">	</span>memory:<span class="Apple-tab-span" style="white-space: pre;">			</span>will keep track of previous values and will call any callback added</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">					</span>after the list has been fired right away with the latest &quot;memorized&quot;</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">					</span>values (like a Deferred)</div>
 
<div> *</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">	</span>unique:<span class="Apple-tab-span" style="white-space: pre;">			</span>will ensure a callback can only be added once (no duplicate in the list)</div>
 
<div> *</div>
 
<div> *<span class="Apple-tab-span" style="white-space: pre;">	</span>stopOnFalse:<span class="Apple-tab-span" style="white-space: pre;">	</span>interrupt callings when a callback returns false</div>
 
<div> *</div>
 
<div> */</div>
 
<div>jQuery.Callbacks = function( options ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Convert options from String-formatted to Object-formatted if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// (we check in cache first)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>options = typeof options === &quot;string&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>( optionsCache[ options ] || createOptions( options ) ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.extend( {}, options );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var // Last fire value (for non-forgettable lists)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>memory,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Flag to know if list was already fired</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>fired,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Flag to know if list is currently firing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>firing,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// First callback to fire (used internally by add and fireWith)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>firingStart,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// End of the loop when firing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>firingLength,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Index of currently firing callback (modified by remove if needed)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>firingIndex,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Actual callback list</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>list = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Stack of fire calls for repeatable lists</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>stack = !options.once &amp;&amp; [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fire callbacks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>fire = function( data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>memory = options.memory &amp;&amp; data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fired = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>firingIndex = firingStart || 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>firingStart = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>firingLength = list.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>firing = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; list &amp;&amp; firingIndex &lt; firingLength; firingIndex++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false &amp;&amp; options.stopOnFalse ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>memory = false; // To prevent further calls using add</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>firing = false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( list ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( stack ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( stack.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>fire( stack.shift() );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else if ( memory ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>list = [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>self.disable();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Actual Callbacks object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>self = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add a callback or a collection of callbacks to the list</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>add: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( list ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// First, we save the current length</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var start = list.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>(function add( args ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>jQuery.each( args, function( _, arg ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>var type = jQuery.type( arg );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( type === &quot;function&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( !options.unique || !self.has( arg ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>list.push( arg );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>} else if ( arg &amp;&amp; arg.length &amp;&amp; type !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// Inspect recursively</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>add( arg );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>})( arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Do we need to add the callbacks to the</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// current firing batch?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( firing ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>firingLength = list.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// With memory, if we're not firing then</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// we should call right away</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>} else if ( memory ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>firingStart = start;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>fire( memory );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remove a callback from the list</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>remove: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( list ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.each( arguments, function( _, arg ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>var index;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>while ( ( index = jQuery.inArray( arg, list, index ) ) &gt; -1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>list.splice( index, 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Handle firing indexes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( firing ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( index &lt;= firingLength ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>firingLength--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( index &lt;= firingIndex ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>firingIndex--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Check if a given callback is in the list.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If no argument is given, return whether or not list has callbacks attached.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>has: function( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return fn ? jQuery.inArray( fn, list ) &gt; -1 : !!( list &amp;&amp; list.length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remove all callbacks from the list</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>empty: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>list = [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>firingLength = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Have the list do nothing anymore</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>disable: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>list = stack = memory = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Is it disabled?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>disabled: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return !list;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Lock the list in its current state</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>lock: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>stack = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !memory ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>self.disable();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Is it locked?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>locked: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return !stack;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Call all callbacks with the given context and arguments</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fireWith: function( context, args ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( list &amp;&amp; ( !fired || stack ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>args = args || [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>args = [ context, args.slice ? args.slice() : args ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( firing ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>stack.push( args );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>fire( args );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Call all the callbacks with the given arguments</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fire: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>self.fireWith( this, arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// To know if the callbacks have already been called at least once</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fired: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return !!fired;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return self;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Deferred: function( func ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var tuples = [</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// action, add listener, listener list, final state</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>[ &quot;resolve&quot;, &quot;done&quot;, jQuery.Callbacks(&quot;once memory&quot;), &quot;resolved&quot; ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>[ &quot;reject&quot;, &quot;fail&quot;, jQuery.Callbacks(&quot;once memory&quot;), &quot;rejected&quot; ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>[ &quot;notify&quot;, &quot;progress&quot;, jQuery.Callbacks(&quot;memory&quot;) ]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>state = &quot;pending&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>promise = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>state: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return state;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>always: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>deferred.done( arguments ).fail( arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>then: function( /* fnDone, fnFail, fnProgress */ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var fns = arguments;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return jQuery.Deferred(function( newDefer ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>jQuery.each( tuples, function( i, tuple ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>var fn = jQuery.isFunction( fns[ i ] ) &amp;&amp; fns[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// deferred[ done | fail | progress ] for forwarding actions to newDefer</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>deferred[ tuple[1] ](function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>var returned = fn &amp;&amp; fn.apply( this, arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( returned &amp;&amp; jQuery.isFunction( returned.promise ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>returned.promise()</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">										</span>.done( newDefer.resolve )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">										</span>.fail( newDefer.reject )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">										</span>.progress( newDefer.notify );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>newDefer[ tuple[ 0 ] + &quot;With&quot; ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>fns = null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}).promise();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Get a promise for this deferred</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If obj is provided, the promise aspect is added to the object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>promise: function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return obj != null ? jQuery.extend( obj, promise ) : promise;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>deferred = {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Keep pipe for back-compat</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>promise.pipe = promise.then;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Add list-specific methods</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.each( tuples, function( i, tuple ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var list = tuple[ 2 ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>stateString = tuple[ 3 ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// promise[ done | fail | progress ] = list.add</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>promise[ tuple[1] ] = list.add;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Handle state</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( stateString ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>list.add(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// state = [ resolved | rejected ]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>state = stateString;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// [ reject_list | resolve_list ].disable; progress_list.lock</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// deferred[ resolve | reject | notify ]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>deferred[ tuple[0] ] = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>deferred[ tuple[0] + &quot;With&quot; ]( this === deferred ? promise : this, arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>deferred[ tuple[0] + &quot;With&quot; ] = list.fireWith;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make the deferred a promise</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>promise.promise( deferred );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Call given func if any</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( func ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>func.call( deferred, deferred );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// All done!</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return deferred;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Deferred helper</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>when: function( subordinate /* , ..., subordinateN */ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>resolveValues = slice.call( arguments ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>length = resolveValues.length,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// the count of uncompleted subordinates</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>remaining = length !== 1 || ( subordinate &amp;&amp; jQuery.isFunction( subordinate.promise ) ) ? length : 0,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// the master Deferred. If resolveValues consist of only a single Deferred, just use that.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>deferred = remaining === 1 ? subordinate : jQuery.Deferred(),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Update function for both resolve and progress values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>updateFunc = function( i, contexts, values ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>contexts[ i ] = this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>values[ i ] = arguments.length &gt; 1 ? slice.call( arguments ) : value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( values === progressValues ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>deferred.notifyWith( contexts, values );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>} else if ( !( --remaining ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>deferred.resolveWith( contexts, values );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>progressValues, progressContexts, resolveContexts;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// add listeners to Deferred subordinates; treat others as resolved</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( length &gt; 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>progressValues = new Array( length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>progressContexts = new Array( length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>resolveContexts = new Array( length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; length; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( resolveValues[ i ] &amp;&amp; jQuery.isFunction( resolveValues[ i ].promise ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>resolveValues[ i ].promise()</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>.done( updateFunc( i, resolveContexts, resolveValues ) )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>.fail( deferred.reject )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>.progress( updateFunc( i, progressContexts, progressValues ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>--remaining;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// if we're not waiting on anything, resolve the master</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !remaining ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>deferred.resolveWith( resolveContexts, resolveValues );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return deferred.promise();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// The deferred used on DOM ready</div>
 
<div>var readyList;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.ready = function( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Add the callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.ready.promise().done( fn );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return this;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Is the DOM ready to be used? Set to true once it occurs.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isReady: false,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// A counter to track how many items to wait for before</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// the ready event fires. See #6781</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>readyWait: 1,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Hold (or release) the ready event</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>holdReady: function( hold ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( hold ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.readyWait++;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.ready( true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Handle when the DOM is ready</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ready: function( wait ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Abort if there are pending holds or we're already ready</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Remember that the DOM is ready</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.isReady = true;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If a normal DOM Ready event fired, decrement, and wait if need be</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( wait !== true &amp;&amp; --jQuery.readyWait &gt; 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If there are functions bound, to execute</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>readyList.resolveWith( document, [ jQuery ] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Trigger any bound ready events</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.fn.triggerHandler ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery( document ).triggerHandler( &quot;ready&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery( document ).off( &quot;ready&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * The ready event handler and self cleanup method</div>
 
<div> */</div>
 
<div>function completed() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>document.removeEventListener( &quot;DOMContentLoaded&quot;, completed, false );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>window.removeEventListener( &quot;load&quot;, completed, false );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.ready();</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.ready.promise = function( obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !readyList ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>readyList = jQuery.Deferred();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Catch cases where $(document).ready() is called after the browser event has already occurred.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// we once tried to use readyState &quot;interactive&quot; here, but it caused issues like the one</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( document.readyState === &quot;complete&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Handle it asynchronously to allow scripts the opportunity to delay ready</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>setTimeout( jQuery.ready );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Use the handy event callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>document.addEventListener( &quot;DOMContentLoaded&quot;, completed, false );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// A fallback to window.onload, that will always work</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>window.addEventListener( &quot;load&quot;, completed, false );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return readyList.promise( obj );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Kick off the DOM ready check even if the user does not</div>
 
<div>jQuery.ready.promise();</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Multifunctional method to get and set values of a collection</div>
 
<div>// The value/s can optionally be executed if it's a function</div>
 
<div>var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>len = elems.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>bulk = key == null;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Sets many values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( jQuery.type( key ) === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>chainable = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( i in key ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Sets one value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else if ( value !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>chainable = true;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !jQuery.isFunction( value ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>raw = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( bulk ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Bulk operations run against the entire set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( raw ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fn.call( elems, value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fn = null;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// ...except when executing function values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>bulk = fn;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fn = function( elem, key, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return bulk.call( jQuery( elem ), value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return chainable ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elems :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Gets</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>bulk ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn.call( elems ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>len ? fn( elems[0], key ) : emptyGet;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Determines whether an object can have data</div>
 
<div> */</div>
 
<div>jQuery.acceptData = function( owner ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Accepts only:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>//  - Node</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>//    - Node.ELEMENT_NODE</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>//    - Node.DOCUMENT_NODE</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>//  - Object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>//    - Any</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* jshint -W018 */</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>function Data() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: Android &lt; 4,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Old WebKit does not have Object.preventExtensions/freeze method,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// return new empty object instead with no [[set]] accessor</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Object.defineProperty( this.cache = {}, 0, {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>get: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>this.expando = jQuery.expando + Math.random();</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>Data.uid = 1;</div>
 
<div>Data.accepts = jQuery.acceptData;</div>
 
<div> 
  <br />
 </div>
 
<div>Data.prototype = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>key: function( owner ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// We can accept data for non-element nodes in modern browsers,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// but we should not, see #8335.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Always return the key for a frozen object.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !Data.accepts( owner ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var descriptor = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Check if the owner object already has a cache key</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>unlock = owner[ this.expando ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If not, create one</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !unlock ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>unlock = Data.uid++;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Secure it in a non-enumerable, non-writable property</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>descriptor[ this.expando ] = { value: unlock };</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>Object.defineProperties( owner, descriptor );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: Android &lt; 4</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Fallback to a less secure definition</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} catch ( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>descriptor[ this.expando ] = unlock;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.extend( owner, descriptor );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Ensure the cache object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !this.cache[ unlock ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.cache[ unlock ] = {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return unlock;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>set: function( owner, data, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var prop,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// There may be an unlock assigned to this node,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// if there is no entry for this &quot;owner&quot;, create one inline</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// and set the unlock as though an owner entry had always existed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>unlock = this.key( owner ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cache = this.cache[ unlock ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Handle: [ owner, key, value ] args</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof data === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cache[ data ] = value;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Handle: [ owner, { properties } ] args</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Fresh assignments by object are shallow copied</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.isEmptyObject( cache ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.extend( this.cache[ unlock ], data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Otherwise, copy the properties one-by-one to the cache object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( prop in data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>cache[ prop ] = data[ prop ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return cache;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>get: function( owner, key ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Either a valid cache is found, or will be created.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// New caches will be created and the unlock returned,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// allowing direct access to the newly created</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// empty data object. A valid owner object must be provided.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var cache = this.cache[ this.key( owner ) ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return key === undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cache : cache[ key ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>access: function( owner, key, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var stored;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// In cases where either:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//   1. No key was specified</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//   2. A string key was specified, but no value provided</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Take the &quot;read&quot; path and allow the get method to determine</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// which value to return, respectively either:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//   1. The entire cache object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//   2. The data stored at the key</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( key === undefined ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>((key &amp;&amp; typeof key === &quot;string&quot;) &amp;&amp; value === undefined) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>stored = this.get( owner, key );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return stored !== undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>stored : this.get( owner, jQuery.camelCase(key) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// [*]When the key is not a string, or both a key and value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// are specified, set or extend (existing objects) with either:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//   1. An object of properties</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//   2. A key and value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.set( owner, key, value );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Since the &quot;set&quot; path can have two possible entry points</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// return the expected data based on which path was taken[*]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return value !== undefined ? value : key;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>remove: function( owner, key ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i, name, camel,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>unlock = this.key( owner ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cache = this.cache[ unlock ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( key === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.cache[ unlock ] = {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support array or space separated string of keys</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.isArray( key ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If &quot;name&quot; is an array of keys...</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// When data is initially created, via (&quot;key&quot;, &quot;val&quot;) signature,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// keys will be converted to camelCase.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Since there is no way to tell _how_ a key was added, remove</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// both plain key and camelCase key. #12786</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// This will only penalize the array argument path.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>name = key.concat( key.map( jQuery.camelCase ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>camel = jQuery.camelCase( key );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Try the string as a key before any manipulation</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( key in cache ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>name = [ key, camel ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// If a key with the spaces exists, use it.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Otherwise, create an array by matching non-whitespace</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>name = camel;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>name = name in cache ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>[ name ] : ( name.match( rnotwhite ) || [] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = name.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>delete cache[ name[ i ] ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hasData: function( owner ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return !jQuery.isEmptyObject(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.cache[ owner[ this.expando ] ] || {}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>discard: function( owner ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( owner[ this.expando ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete this.cache[ owner[ this.expando ] ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div>var data_priv = new Data();</div>
 
<div> 
  <br />
 </div>
 
<div>var data_user = new Data();</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>/*</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>Implementation Summary</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>1. Enforce API surface and semantic compatibility with 1.9.x branch</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>2. Improve the module's maintainability by reducing the storage</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>paths to a single mechanism.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>3. Use the same single mechanism to support &quot;private&quot; and &quot;user&quot; data.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>4. _Never_ expose &quot;private&quot; data to user code (TODO: Drop _data, _removeData)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>5. Avoid exposing implementation details on user objects (eg. expando properties)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>6. Provide a clear path for implementation upgrade to WeakMap in 2014</div>
 
<div>*/</div>
 
<div>var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rmultiDash = /([A-Z])/g;</div>
 
<div> 
  <br />
 </div>
 
<div>function dataAttr( elem, key, data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var name;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If nothing was found internally, try to fetch any</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// data from the HTML5 data-* attribute</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( data === undefined &amp;&amp; elem.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>name = &quot;data-&quot; + key.replace( rmultiDash, &quot;-$1&quot; ).toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>data = elem.getAttribute( name );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof data === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = data === &quot;true&quot; ? true :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data === &quot;false&quot; ? false :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data === &quot;null&quot; ? null :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Only convert to a number if it doesn't change the string</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>+data + &quot;&quot; === data ? +data :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>rbrace.test( data ) ? jQuery.parseJSON( data ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} catch( e ) {}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Make sure we set the data so it isn't changed later</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data_user.set( elem, key, data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return data;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hasData: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return data_user.hasData( elem ) || data_priv.hasData( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>data: function( elem, name, data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return data_user.access( elem, name, data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>removeData: function( elem, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>data_user.remove( elem, name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// TODO: Now that all calls to _data and _removeData have been replaced</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// with direct calls to data_priv methods, these can be deprecated.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>_data: function( elem, name, data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return data_priv.access( elem, name, data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>_removeData: function( elem, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>data_priv.remove( elem, name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>data: function( key, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i, name, data,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem = this[ 0 ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>attrs = elem &amp;&amp; elem.attributes;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Gets all values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( key === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = data_user.get( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( elem.nodeType === 1 &amp;&amp; !data_priv.get( elem, &quot;hasDataAttrs&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i = attrs.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>while ( i-- ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Support: IE11+</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// The attrs elements can be null (#14894)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( attrs[ i ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>name = attrs[ i ].name;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( name.indexOf( &quot;data-&quot; ) === 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>name = jQuery.camelCase( name.slice(5) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>dataAttr( elem, name, data[ name ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data_priv.set( elem, &quot;hasDataAttrs&quot;, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Sets multiple values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof key === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data_user.set( this, key );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return access( this, function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var data,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>camelKey = jQuery.camelCase( key );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// The calling jQuery object (element matches) is not empty</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// (and therefore has an element appears at this[ 0 ]) and the</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// `value` parameter was not undefined. An empty jQuery object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// will result in `undefined` for elem = this[ 0 ] which will</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// throw an exception if an attempt to read a data cache is made.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem &amp;&amp; value === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Attempt to get data from the cache</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// with the key as-is</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = data_user.get( elem, key );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( data !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Attempt to get data from the cache</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// with the key camelized</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = data_user.get( elem, camelKey );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( data !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Attempt to &quot;discover&quot; the data in</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// HTML5 custom data-* attrs</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = dataAttr( elem, camelKey, undefined );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( data !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// We tried really hard, but the data doesn't exist.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Set the data...</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// First, attempt to store a copy or reference of any</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// data that might've been store with a camelCased key.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var data = data_user.get( this, camelKey );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// For HTML5 data-* attribute interop, we have to</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// store property names with dashes in a camelCase form.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// This might not apply to all properties...*</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data_user.set( this, camelKey, value );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// *... In the case of properties that might _actually_</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// have dashes, we need to also store a copy of that</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// unchanged property.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( key.indexOf(&quot;-&quot;) !== -1 &amp;&amp; data !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data_user.set( this, key, value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}, null, value, arguments.length &gt; 1, null, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>removeData: function( key ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data_user.remove( this, key );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>queue: function( elem, type, data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var queue;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = ( type || &quot;fx&quot; ) + &quot;queue&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>queue = data_priv.get( elem, type );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Speed up dequeue by getting out quickly if this is just a lookup</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !queue || jQuery.isArray( data ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>queue = data_priv.access( elem, type, jQuery.makeArray(data) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>queue.push( data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return queue || [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>dequeue: function( elem, type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>type = type || &quot;fx&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var queue = jQuery.queue( elem, type ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>startLength = queue.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = queue.shift(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks = jQuery._queueHooks( elem, type ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>next = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.dequeue( elem, type );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If the fx queue is dequeued, always remove the progress sentinel</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( fn === &quot;inprogress&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = queue.shift();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>startLength--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( fn ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add a progress sentinel to prevent the fx queue from being</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// automatically dequeued</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( type === &quot;fx&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>queue.unshift( &quot;inprogress&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// clear up the last queue stop function</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete hooks.stop;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn.call( elem, next, hooks );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !startLength &amp;&amp; hooks ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks.empty.fire();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// not intended for public consumption - generates a queueHooks object, or returns the current one</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>_queueHooks: function( elem, type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var key = type + &quot;queueHooks&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return data_priv.get( elem, key ) || data_priv.access( elem, key, {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>empty: jQuery.Callbacks(&quot;once memory&quot;).add(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data_priv.remove( elem, [ type + &quot;queue&quot;, key ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>})</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>queue: function( type, data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var setter = 2;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof type !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = &quot;fx&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>setter--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( arguments.length &lt; setter ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jQuery.queue( this[0], type );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return data === undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var queue = jQuery.queue( this, type, data );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// ensure a hooks for this queue</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery._queueHooks( this, type );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( type === &quot;fx&quot; &amp;&amp; queue[0] !== &quot;inprogress&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.dequeue( this, type );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>dequeue: function( type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.dequeue( this, type );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>clearQueue: function( type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.queue( type || &quot;fx&quot;, [] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Get a promise resolved when queues of a certain type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// are emptied (fx is the type by default)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>promise: function( type, obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var tmp,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>count = 1,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>defer = jQuery.Deferred(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elements = this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = this.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>resolve = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !( --count ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>defer.resolveWith( elements, [ elements ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof type !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>obj = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>type = type || &quot;fx&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tmp = data_priv.get( elements[ i ], type + &quot;queueHooks&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( tmp &amp;&amp; tmp.empty ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>count++;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>tmp.empty.add( resolve );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>resolve();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return defer.promise( obj );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div>var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;</div>
 
<div> 
  <br />
 </div>
 
<div>var cssExpand = [ &quot;Top&quot;, &quot;Right&quot;, &quot;Bottom&quot;, &quot;Left&quot; ];</div>
 
<div> 
  <br />
 </div>
 
<div>var isHidden = function( elem, el ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// isHidden might be called from jQuery#filter function;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// in that case, element will be second argument</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem = el || elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.css( elem, &quot;display&quot; ) === &quot;none&quot; || !jQuery.contains( elem.ownerDocument, elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div>var rcheckableType = (/^(?:checkbox|radio)$/i);</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var fragment = document.createDocumentFragment(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div = fragment.appendChild( document.createElement( &quot;div&quot; ) ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>input = document.createElement( &quot;input&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// #11217 - WebKit loses check when the name is after the checked attribute</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: Windows Web Apps (WWA)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// `name` and `type` need .setAttribute for WWA</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>input.setAttribute( &quot;type&quot;, &quot;radio&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>input.setAttribute( &quot;checked&quot;, &quot;checked&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>input.setAttribute( &quot;name&quot;, &quot;t&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>div.appendChild( input );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: Safari 5.1, iOS 5.1, Android 4.x, Android 2.3</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// old WebKit doesn't clone checked state correctly in fragments</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Make sure textarea (and checkbox) defaultValue is properly cloned</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE9-IE11+</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>div.innerHTML = &quot;&lt;textarea&gt;x&lt;/textarea&gt;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;</div>
 
<div>})();</div>
 
<div>var strundefined = typeof undefined;</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>support.focusinBubbles = &quot;onfocusin&quot; in window;</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rkeyEvent = /^key/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rmouseEvent = /^(?:mouse|pointer|contextmenu)|click/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;</div>
 
<div> 
  <br />
 </div>
 
<div>function returnTrue() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return true;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function returnFalse() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return false;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function safeActiveElement() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return document.activeElement;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} catch ( err ) { }</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/*</div>
 
<div> * Helper functions for managing events -- not part of the public interface.</div>
 
<div> * Props to Dean Edwards' addEvent library for many of the ideas.</div>
 
<div> */</div>
 
<div>jQuery.event = {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>global: {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>add: function( elem, types, handler, data, selector ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var handleObjIn, eventHandle, tmp,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>events, t, handleObj,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special, handlers, type, namespaces, origType,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elemData = data_priv.get( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Don't attach events to noData or text/comment nodes (but allow plain objects)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elemData ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Caller can pass in an object of custom data in lieu of the handler</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( handler.handler ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handleObjIn = handler;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handler = handleObjIn.handler;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>selector = handleObjIn.selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make sure that the handler has a unique ID, used to find/remove it later</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !handler.guid ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handler.guid = jQuery.guid++;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Init the element's event structure and main handler, if this is the first</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !(events = elemData.events) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>events = elemData.events = {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !(eventHandle = elemData.handle) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>eventHandle = elemData.handle = function( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Discard the second event of a jQuery.event.trigger() and</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// when an event is called after a page has unloaded</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return typeof jQuery !== strundefined &amp;&amp; jQuery.event.triggered !== e.type ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.event.dispatch.apply( elem, arguments ) : undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Handle multiple events separated by a space</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>types = ( types || &quot;&quot; ).match( rnotwhite ) || [ &quot;&quot; ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>t = types.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( t-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tmp = rtypenamespace.exec( types[t] ) || [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = origType = tmp[1];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>namespaces = ( tmp[2] || &quot;&quot; ).split( &quot;.&quot; ).sort();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// There *must* be a type, no attaching namespace-only handlers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>continue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If event changes its type, use the special event handlers for the changed type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special = jQuery.event.special[ type ] || {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If selector defined, determine special event api type, otherwise given type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = ( selector ? special.delegateType : special.bindType ) || type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Update special based on newly reset type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special = jQuery.event.special[ type ] || {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// handleObj is passed to all event handlers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handleObj = jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>type: type,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>origType: origType,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data: data,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handler: handler,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>guid: handler.guid,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>selector: selector,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>needsContext: selector &amp;&amp; jQuery.expr.match.needsContext.test( selector ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>namespace: namespaces.join(&quot;.&quot;)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}, handleObjIn );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Init the event handler queue if we're the first</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !(handlers = events[ type ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handlers = events[ type ] = [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handlers.delegateCount = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Only use addEventListener if the special events handler returns false</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem.addEventListener ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.addEventListener( type, eventHandle, false );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( special.add ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>special.add.call( elem, handleObj );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !handleObj.handler.guid ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>handleObj.handler.guid = handler.guid;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add to the element's handler list, delegates in front</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handlers.splice( handlers.delegateCount++, 0, handleObj );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handlers.push( handleObj );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Keep track of which events have ever been used, for event optimization</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.event.global[ type ] = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Detach an event or set of events from an element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>remove: function( elem, types, handler, selector, mappedTypes ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var j, origCount, tmp,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>events, t, handleObj,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special, handlers, type, namespaces, origType,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elemData = data_priv.hasData( elem ) &amp;&amp; data_priv.get( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elemData || !(events = elemData.events) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Once for each type.namespace in types; type may be omitted</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>types = ( types || &quot;&quot; ).match( rnotwhite ) || [ &quot;&quot; ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>t = types.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( t-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tmp = rtypenamespace.exec( types[t] ) || [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = origType = tmp[1];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>namespaces = ( tmp[2] || &quot;&quot; ).split( &quot;.&quot; ).sort();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Unbind all events (on this namespace, if provided) for the element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( type in events ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.event.remove( elem, type + types[ t ], handler, selector, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>continue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special = jQuery.event.special[ type ] || {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = ( selector ? special.delegateType : special.bindType ) || type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handlers = events[ type ] || [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tmp = tmp[2] &amp;&amp; new RegExp( &quot;(^|\\.)&quot; + namespaces.join(&quot;\\.(?:.*\\.|)&quot;) + &quot;(\\.|$)&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remove matching events</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>origCount = j = handlers.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( j-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handleObj = handlers[ j ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( ( mappedTypes || origType === handleObj.origType ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>( !handler || handler.guid === handleObj.guid ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>( !tmp || tmp.test( handleObj.namespace ) ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>( !selector || selector === handleObj.selector || selector === &quot;**&quot; &amp;&amp; handleObj.selector ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>handlers.splice( j, 1 );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( handleObj.selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>handlers.delegateCount--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( special.remove ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>special.remove.call( elem, handleObj );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remove generic event handler if we removed something and no more handlers exist</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// (avoids potential for endless recursion during removal of special event handlers)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( origCount &amp;&amp; !handlers.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.removeEvent( elem, type, elemData.handle );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>delete events[ type ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Remove the expando if it's no longer used</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isEmptyObject( events ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete elemData.handle;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data_priv.remove( elem, &quot;events&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>trigger: function( event, data, elem, onlyHandlers ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i, cur, tmp, bubbleType, ontype, handle, special,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>eventPath = [ elem || document ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = hasOwn.call( event, &quot;type&quot; ) ? event.type : event,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>namespaces = hasOwn.call( event, &quot;namespace&quot; ) ? event.namespace.split(&quot;.&quot;) : [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cur = tmp = elem = elem || document;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Don't do events on text and comment nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( elem.nodeType === 3 || elem.nodeType === 8 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// focus/blur morphs to focusin/out; ensure we're not firing them right now</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( type.indexOf(&quot;.&quot;) &gt;= 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Namespaced trigger; create a regexp to match event type in handle()</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>namespaces = type.split(&quot;.&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = namespaces.shift();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>namespaces.sort();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ontype = type.indexOf(&quot;:&quot;) &lt; 0 &amp;&amp; &quot;on&quot; + type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Caller can pass in a jQuery.Event object, Object, or just an event type string</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event = event[ jQuery.expando ] ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>new jQuery.Event( type, typeof event === &quot;object&quot; &amp;&amp; event );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Trigger bitmask: &amp; 1 for native handlers; &amp; 2 for jQuery (always true)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event.isTrigger = onlyHandlers ? 2 : 3;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event.namespace = namespaces.join(&quot;.&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event.namespace_re = event.namespace ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>new RegExp( &quot;(^|\\.)&quot; + namespaces.join(&quot;\\.(?:.*\\.|)&quot;) + &quot;(\\.|$)&quot; ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>null;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Clean up the event in case it is being reused</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event.result = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !event.target ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event.target = elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Clone any incoming data and prepend the event, creating the handler arg list</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>data = data == null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>[ event ] :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.makeArray( data, [ event ] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Allow special events to draw outside the lines</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>special = jQuery.event.special[ type ] || {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !onlyHandlers &amp;&amp; special.trigger &amp;&amp; special.trigger.apply( elem, data ) === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Determine event propagation path in advance, per W3C events spec (#9951)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !onlyHandlers &amp;&amp; !special.noBubble &amp;&amp; !jQuery.isWindow( elem ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>bubbleType = special.delegateType || type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !rfocusMorph.test( bubbleType + type ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>cur = cur.parentNode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; cur; cur = cur.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>eventPath.push( cur );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>tmp = cur;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Only add window if we got to document (e.g., not plain obj or detached DOM)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( tmp === (elem.ownerDocument || document) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>eventPath.push( tmp.defaultView || tmp.parentWindow || window );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fire handlers on the event path</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (cur = eventPath[i++]) &amp;&amp; !event.isPropagationStopped() ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event.type = i &gt; 1 ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>bubbleType :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>special.bindType || type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// jQuery handler</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handle = ( data_priv.get( cur, &quot;events&quot; ) || {} )[ event.type ] &amp;&amp; data_priv.get( cur, &quot;handle&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( handle ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handle.apply( cur, data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Native handler</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handle = ontype &amp;&amp; cur[ ontype ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( handle &amp;&amp; handle.apply &amp;&amp; jQuery.acceptData( cur ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>event.result = handle.apply( cur, data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( event.result === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>event.preventDefault();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event.type = type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If nobody prevented the default action, do it now</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !onlyHandlers &amp;&amp; !event.isDefaultPrevented() ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.acceptData( elem ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Call a native DOM method on the target with the same name name as the event.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Don't do default actions on window, that's where global variables be (#6170)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( ontype &amp;&amp; jQuery.isFunction( elem[ type ] ) &amp;&amp; !jQuery.isWindow( elem ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Don't re-trigger an onFOO event when we call its FOO() method</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tmp = elem[ ontype ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( tmp ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem[ ontype ] = null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Prevent re-triggering of the same event, since we already bubbled it above</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.event.triggered = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem[ type ]();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.event.triggered = undefined;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( tmp ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem[ ontype ] = tmp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return event.result;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>dispatch: function( event ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make a writable jQuery.Event from the native event object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event = jQuery.event.fix( event );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i, j, ret, matched, handleObj,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handlerQueue = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>args = slice.call( arguments ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handlers = ( data_priv.get( this, &quot;events&quot; ) || {} )[ event.type ] || [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special = jQuery.event.special[ event.type ] || {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Use the fix-ed jQuery.Event rather than the (read-only) native event</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>args[0] = event;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event.delegateTarget = this;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Call the preDispatch hook for the mapped type, and let it bail if desired</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( special.preDispatch &amp;&amp; special.preDispatch.call( this, event ) === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Determine handlers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>handlerQueue = jQuery.event.handlers.call( this, event, handlers );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Run delegates first; they may want to stop propagation beneath us</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (matched = handlerQueue[ i++ ]) &amp;&amp; !event.isPropagationStopped() ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event.currentTarget = matched.elem;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>j = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( (handleObj = matched.handlers[ j++ ]) &amp;&amp; !event.isImmediatePropagationStopped() ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Triggered event must either 1) have no namespace, or</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>event.handleObj = handleObj;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>event.data = handleObj.data;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>.apply( matched.elem, args );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( ret !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( (event.result = ret) === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>event.preventDefault();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>event.stopPropagation();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Call the postDispatch hook for the mapped type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( special.postDispatch ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special.postDispatch.call( this, event );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return event.result;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>handlers: function( event, handlers ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i, matches, sel, handleObj,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handlerQueue = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delegateCount = handlers.delegateCount,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cur = event.target;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Find delegate handlers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Black-hole SVG &lt;use&gt; instance trees (#13180)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Avoid non-left-click bubbling in Firefox (#3861)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( delegateCount &amp;&amp; cur.nodeType &amp;&amp; (!event.button || event.type !== &quot;click&quot;) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; cur !== this; cur = cur.parentNode || this ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( cur.disabled !== true || event.type !== &quot;click&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>matches = [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>for ( i = 0; i &lt; delegateCount; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>handleObj = handlers[ i ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Don't conflict with Object.prototype properties (#13203)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>sel = handleObj.selector + &quot; &quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( matches[ sel ] === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>matches[ sel ] = handleObj.needsContext ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>jQuery( sel, this ).index( cur ) &gt;= 0 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>jQuery.find( sel, this, null, [ cur ] ).length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( matches[ sel ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>matches.push( handleObj );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( matches.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>handlerQueue.push({ elem: cur, handlers: matches });</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Add the remaining (directly-bound) handlers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( delegateCount &lt; handlers.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return handlerQueue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Includes some event props shared by KeyEvent and MouseEvent</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>props: &quot;altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which&quot;.split(&quot; &quot;),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fixHooks: {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>keyHooks: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>props: &quot;char charCode key keyCode&quot;.split(&quot; &quot;),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>filter: function( event, original ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add which for key events</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( event.which == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>event.which = original.charCode != null ? original.charCode : original.keyCode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return event;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>mouseHooks: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>props: &quot;button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement&quot;.split(&quot; &quot;),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>filter: function( event, original ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var eventDoc, doc, body,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>button = original.button;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Calculate pageX/Y if missing and clientX/Y available</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( event.pageX == null &amp;&amp; original.clientX != null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>eventDoc = event.target.ownerDocument || document;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>doc = eventDoc.documentElement;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>body = eventDoc.body;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>event.pageX = original.clientX + ( doc &amp;&amp; doc.scrollLeft || body &amp;&amp; body.scrollLeft || 0 ) - ( doc &amp;&amp; doc.clientLeft || body &amp;&amp; body.clientLeft || 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>event.pageY = original.clientY + ( doc &amp;&amp; doc.scrollTop  || body &amp;&amp; body.scrollTop  || 0 ) - ( doc &amp;&amp; doc.clientTop  || body &amp;&amp; body.clientTop  || 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add which for click: 1 === left; 2 === middle; 3 === right</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Note: button is not normalized, so don't use it</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !event.which &amp;&amp; button !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>event.which = ( button &amp; 1 ? 1 : ( button &amp; 2 ? 3 : ( button &amp; 4 ? 2 : 0 ) ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return event;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fix: function( event ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( event[ jQuery.expando ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return event;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Create a writable copy of the event object and normalize some properties</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i, prop, copy,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = event.type,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>originalEvent = event,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fixHook = this.fixHooks[ type ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !fixHook ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.fixHooks[ type ] = fixHook =</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rmouseEvent.test( type ) ? this.mouseHooks :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rkeyEvent.test( type ) ? this.keyHooks :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>{};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>event = new jQuery.Event( originalEvent );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = copy.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>prop = copy[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event[ prop ] = originalEvent[ prop ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: Cordova 2.5 (WebKit) (#13255)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// All events should have a target; Cordova deviceready doesn't</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !event.target ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event.target = document;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: Safari 6.0+, Chrome &lt; 28</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Target should not be a text node (#504, #13143)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( event.target.nodeType === 3 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event.target = event.target.parentNode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>special: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>load: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Prevent triggered image.load events from bubbling to window.load</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>noBubble: true</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>focus: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Fire native event if possible so blur/focus sequence is correct</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>trigger: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( this !== safeActiveElement() &amp;&amp; this.focus ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>this.focus();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delegateType: &quot;focusin&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>blur: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>trigger: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( this === safeActiveElement() &amp;&amp; this.blur ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>this.blur();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delegateType: &quot;focusout&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>click: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// For checkbox, fire native event so checked state will be right</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>trigger: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( this.type === &quot;checkbox&quot; &amp;&amp; this.click &amp;&amp; jQuery.nodeName( this, &quot;input&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>this.click();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// For cross-browser consistency, don't fire native .click() on links</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>_default: function( event ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return jQuery.nodeName( event.target, &quot;a&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>beforeunload: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>postDispatch: function( event ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Support: Firefox 20+</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Firefox doesn't alert if the returnValue field is not set.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( event.result !== undefined &amp;&amp; event.originalEvent ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>event.originalEvent.returnValue = event.result;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>simulate: function( type, elem, event, bubble ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Piggyback on a donor event to simulate a different one.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fake originalEvent to avoid donor's stopPropagation, but if the</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// simulated event prevents default then we do the same on the donor.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var e = jQuery.extend(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>new jQuery.Event(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>{</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>type: type,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>isSimulated: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>originalEvent: {}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( bubble ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.event.trigger( e, null, elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.event.dispatch.call( elem, e );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( e.isDefaultPrevented() ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>event.preventDefault();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.removeEvent = function( elem, type, handle ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( elem.removeEventListener ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem.removeEventListener( type, handle, false );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.Event = function( src, props ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Allow instantiation without the 'new' keyword</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !(this instanceof jQuery.Event) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return new jQuery.Event( src, props );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Event object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( src &amp;&amp; src.type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.originalEvent = src;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.type = src.type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Events bubbling up the document may have been marked as prevented</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// by a handler lower down the tree; reflect the correct value.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.isDefaultPrevented = src.defaultPrevented ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>src.defaultPrevented === undefined &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Support: Android &lt; 4.0</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>src.returnValue === false ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>returnTrue :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>returnFalse;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Event type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.type = src;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Put explicitly provided properties onto the event object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( props ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.extend( this, props );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Create a timestamp if incoming event doesn't have one</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>this.timeStamp = src &amp;&amp; src.timeStamp || jQuery.now();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Mark it as fixed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>this[ jQuery.expando ] = true;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding</div>
 
<div>// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html</div>
 
<div>jQuery.Event.prototype = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isDefaultPrevented: returnFalse,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isPropagationStopped: returnFalse,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>isImmediatePropagationStopped: returnFalse,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>preventDefault: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var e = this.originalEvent;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.isDefaultPrevented = returnTrue;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( e &amp;&amp; e.preventDefault ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>e.preventDefault();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>stopPropagation: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var e = this.originalEvent;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.isPropagationStopped = returnTrue;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( e &amp;&amp; e.stopPropagation ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>e.stopPropagation();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>stopImmediatePropagation: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var e = this.originalEvent;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.isImmediatePropagationStopped = returnTrue;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( e &amp;&amp; e.stopImmediatePropagation ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>e.stopImmediatePropagation();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.stopPropagation();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Create mouseenter/leave events using mouseover/out and event-time checks</div>
 
<div>// Support: Chrome 15+</div>
 
<div>jQuery.each({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>mouseenter: &quot;mouseover&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>mouseleave: &quot;mouseout&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>pointerenter: &quot;pointerover&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>pointerleave: &quot;pointerout&quot;</div>
 
<div>}, function( orig, fix ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.event.special[ orig ] = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>delegateType: fix,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>bindType: fix,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>handle: function( event ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var ret,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>target = this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>related = event.relatedTarget,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handleObj = event.handleObj;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// For mousenter/leave call the handler if related is outside the target.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// NB: No relatedTarget if the mouse left/entered the browser window</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !related || (related !== target &amp;&amp; !jQuery.contains( target, related )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>event.type = handleObj.origType;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ret = handleObj.handler.apply( this, arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>event.type = fix;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Create &quot;bubbling&quot; focus and blur events</div>
 
<div>// Support: Firefox, Chrome, Safari</div>
 
<div>if ( !support.focusinBubbles ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.each({ focus: &quot;focusin&quot;, blur: &quot;focusout&quot; }, function( orig, fix ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Attach a single capturing handler on the document while someone wants focusin/focusout</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var handler = function( event ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.event.special[ fix ] = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>setup: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var doc = this.ownerDocument || this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>attaches = data_priv.access( doc, fix );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !attaches ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>doc.addEventListener( orig, handler, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data_priv.access( doc, fix, ( attaches || 0 ) + 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>teardown: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var doc = this.ownerDocument || this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>attaches = data_priv.access( doc, fix ) - 1;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !attaches ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>doc.removeEventListener( orig, handler, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data_priv.remove( doc, fix );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data_priv.access( doc, fix, attaches );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>on: function( types, selector, data, fn, /*INTERNAL*/ one ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var origFn, type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Types can be a map of types/handlers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof types === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// ( types-Object, selector, data )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( typeof selector !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// ( types-Object, data )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = data || selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>selector = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( type in types ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.on( type, selector, data, types[ type ], one );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( data == null &amp;&amp; fn == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// ( types, fn )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data = selector = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( fn == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( typeof selector === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// ( types, selector, fn )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fn = data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// ( types, data, fn )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fn = data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>selector = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( fn === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = returnFalse;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( !fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( one === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>origFn = fn;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = function( event ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Can use an empty set, since event contains the info</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery().off( event );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return origFn.apply( this, arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Use same guid so caller can remove using origFn</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each( function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.event.add( this, types, fn, data, selector );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>one: function( types, selector, data, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.on( types, selector, data, fn, 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>off: function( types, selector, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var handleObj, type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( types &amp;&amp; types.preventDefault &amp;&amp; types.handleObj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// ( event )  dispatched jQuery.Event</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handleObj = types.handleObj;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery( types.delegateTarget ).off(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handleObj.namespace ? handleObj.origType + &quot;.&quot; + handleObj.namespace : handleObj.origType,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handleObj.selector,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>handleObj.handler</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof types === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// ( types-object [, selector] )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( type in types ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.off( type, selector, types[ type ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( selector === false || typeof selector === &quot;function&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// ( types [, fn] )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = selector;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>selector = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( fn === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fn = returnFalse;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.event.remove( this, types, fn, selector );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>trigger: function( type, data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.event.trigger( type, data, this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>triggerHandler: function( type, data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var elem = this[0];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jQuery.event.trigger( type, data, elem, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rxhtmlTag = /&lt;(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^&gt;]*)\/&gt;/gi,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rtagName = /&lt;([\w:]+)/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rhtml = /&lt;|&amp;#?\w+;/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rnoInnerhtml = /&lt;(?:script|style|link)/i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// checked=&quot;checked&quot; or checked</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rscriptType = /^$|\/(?:java|ecma)script/i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rscriptTypeMasked = /^true\/(.*)/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rcleanScript = /^\s*&lt;!(?:\[CDATA\[|--)|(?:\]\]|--)&gt;\s*$/g,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// We have to close these tags to support XHTML (#13200)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>wrapMap = {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: IE 9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>option: [ 1, &quot;&lt;select multiple='multiple'&gt;&quot;, &quot;&lt;/select&gt;&quot; ],</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>thead: [ 1, &quot;&lt;table&gt;&quot;, &quot;&lt;/table&gt;&quot; ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>col: [ 2, &quot;&lt;table&gt;&lt;colgroup&gt;&quot;, &quot;&lt;/colgroup&gt;&lt;/table&gt;&quot; ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>tr: [ 2, &quot;&lt;table&gt;&lt;tbody&gt;&quot;, &quot;&lt;/tbody&gt;&lt;/table&gt;&quot; ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>td: [ 3, &quot;&lt;table&gt;&lt;tbody&gt;&lt;tr&gt;&quot;, &quot;&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&quot; ],</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>_default: [ 0, &quot;&quot;, &quot;&quot; ]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE 9</div>
 
<div>wrapMap.optgroup = wrapMap.option;</div>
 
<div> 
  <br />
 </div>
 
<div>wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;</div>
 
<div>wrapMap.th = wrapMap.td;</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: 1.x compatibility</div>
 
<div>// Manipulating tables requires a tbody</div>
 
<div>function manipulationTarget( elem, content ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return jQuery.nodeName( elem, &quot;table&quot; ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, &quot;tr&quot; ) ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem.getElementsByTagName(&quot;tbody&quot;)[0] ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem.appendChild( elem.ownerDocument.createElement(&quot;tbody&quot;) ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Replace/restore the type attribute of script elements for safe DOM manipulation</div>
 
<div>function disableScript( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>elem.type = (elem.getAttribute(&quot;type&quot;) !== null) + &quot;/&quot; + elem.type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return elem;</div>
 
<div>}</div>
 
<div>function restoreScript( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var match = rscriptTypeMasked.exec( elem.type );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( match ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem.type = match[ 1 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem.removeAttribute(&quot;type&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return elem;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Mark scripts as having already been evaluated</div>
 
<div>function setGlobalEval( elems, refElements ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>l = elems.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>data_priv.set(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elems[ i ], &quot;globalEval&quot;, !refElements || data_priv.get( refElements[ i ], &quot;globalEval&quot; )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function cloneCopyEvent( src, dest ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( dest.nodeType !== 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// 1. Copy private data: events, handlers, etc.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( data_priv.hasData( src ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>pdataOld = data_priv.access( src );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>pdataCur = data_priv.set( dest, pdataOld );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>events = pdataOld.events;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( events ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete pdataCur.handle;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>pdataCur.events = {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( type in events ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( i = 0, l = events[ type ].length; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.event.add( dest, type, events[ type ][ i ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// 2. Copy user data</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( data_user.hasData( src ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>udataOld = data_user.access( src );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>udataCur = jQuery.extend( {}, udataOld );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>data_user.set( dest, udataCur );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function getAll( context, tag ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var ret = context.getElementsByTagName ? context.getElementsByTagName( tag || &quot;*&quot; ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>context.querySelectorAll ? context.querySelectorAll( tag || &quot;*&quot; ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>[];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return tag === undefined || tag &amp;&amp; jQuery.nodeName( context, tag ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.merge( [ context ], ret ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE &gt;= 9</div>
 
<div>function fixInput( src, dest ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var nodeName = dest.nodeName.toLowerCase();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Fails to persist the checked state of a cloned checkbox or radio button.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( nodeName === &quot;input&quot; &amp;&amp; rcheckableType.test( src.type ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dest.checked = src.checked;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Fails to return the selected option to the default selected state when cloning options</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else if ( nodeName === &quot;input&quot; || nodeName === &quot;textarea&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dest.defaultValue = src.defaultValue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>clone: function( elem, dataAndEvents, deepDataAndEvents ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var i, l, srcElements, destElements,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>clone = elem.cloneNode( true ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>inPage = jQuery.contains( elem.ownerDocument, elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: IE &gt;= 9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fix Cloning issues</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !support.noCloneChecked &amp;&amp; ( elem.nodeType === 1 || elem.nodeType === 11 ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>!jQuery.isXMLDoc( elem ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>destElements = getAll( clone );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>srcElements = getAll( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( i = 0, l = srcElements.length; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fixInput( srcElements[ i ], destElements[ i ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Copy the events from the original to the clone</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( dataAndEvents ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( deepDataAndEvents ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>srcElements = srcElements || getAll( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>destElements = destElements || getAll( clone );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( i = 0, l = srcElements.length; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>cloneCopyEvent( srcElements[ i ], destElements[ i ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>cloneCopyEvent( elem, clone );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Preserve script evaluation history</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>destElements = getAll( clone, &quot;script&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( destElements.length &gt; 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>setGlobalEval( destElements, !inPage &amp;&amp; getAll( elem, &quot;script&quot; ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Return the cloned set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return clone;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>buildFragment: function( elems, context, scripts, selection ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var elem, tmp, tag, wrap, contains, j,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fragment = context.createDocumentFragment(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>nodes = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>l = elems.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem = elems[ i ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem || elem === 0 ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Add nodes directly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( jQuery.type( elem ) === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Support: QtWebKit</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// jQuery.merge because push.apply(_, arraylike) throws</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Convert non-html into a text node</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else if ( !rhtml.test( elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>nodes.push( context.createTextNode( elem ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Convert html into DOM nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tmp = tmp || fragment.appendChild( context.createElement(&quot;div&quot;) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Deserialize a standard representation</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tag = ( rtagName.exec( elem ) || [ &quot;&quot;, &quot;&quot; ] )[ 1 ].toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>wrap = wrapMap[ tag ] || wrapMap._default;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tmp.innerHTML = wrap[ 1 ] + elem.replace( rxhtmlTag, &quot;&lt;$1&gt;&lt;/$2&gt;&quot; ) + wrap[ 2 ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Descend through wrappers to the right content</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>j = wrap[ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>while ( j-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>tmp = tmp.lastChild;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Support: QtWebKit</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// jQuery.merge because push.apply(_, arraylike) throws</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.merge( nodes, tmp.childNodes );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Remember the top-level container</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tmp = fragment.firstChild;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Fixes #12346</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Support: Webkit, IE</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tmp.textContent = &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Remove wrapper from fragment</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>fragment.textContent = &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>while ( (elem = nodes[ i++ ]) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// #4087 - If origin and destination elements are the same, and this is</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// that element, do not do anything</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( selection &amp;&amp; jQuery.inArray( elem, selection ) !== -1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>continue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>contains = jQuery.contains( elem.ownerDocument, elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Append to fragment</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tmp = getAll( fragment.appendChild( elem ), &quot;script&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Preserve script evaluation history</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( contains ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>setGlobalEval( tmp );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Capture executables</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( scripts ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>j = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( (elem = tmp[ j++ ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( rscriptType.test( elem.type || &quot;&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>scripts.push( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return fragment;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cleanData: function( elems ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var data, elem, type, key,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>special = jQuery.event.special,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; (elem = elems[ i ]) !== undefined; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.acceptData( elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>key = elem[ data_priv.expando ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( key &amp;&amp; (data = data_priv.cache[ key ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( data.events ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>for ( type in data.events ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( special[ type ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>jQuery.event.remove( elem, type );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// This is a shortcut to avoid jQuery.event.remove's overhead</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>jQuery.removeEvent( elem, type, data.handle );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( data_priv.cache[ key ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Discard any remaining `private` data</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>delete data_priv.cache[ key ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Discard any remaining `user` data</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete data_user.cache[ elem[ data_user.expando ] ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>text: function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return access( this, function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return value === undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.text( this ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.empty().each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>this.textContent = value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}, null, value, arguments.length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>append: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.domManip( arguments, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var target = manipulationTarget( this, elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>target.appendChild( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prepend: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.domManip( arguments, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var target = manipulationTarget( this, elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>target.insertBefore( elem, target.firstChild );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>before: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.domManip( arguments, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.parentNode.insertBefore( elem, this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>after: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.domManip( arguments, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.parentNode.insertBefore( elem, this.nextSibling );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>remove: function( selector, keepData /* Internal Use Only */ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elems = selector ? jQuery.filter( selector, this ) : this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; (elem = elems[i]) != null; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !keepData &amp;&amp; elem.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.cleanData( getAll( elem ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( keepData &amp;&amp; jQuery.contains( elem.ownerDocument, elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>setGlobalEval( getAll( elem, &quot;script&quot; ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem.parentNode.removeChild( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>empty: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; (elem = this[i]) != null; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem.nodeType === 1 ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Prevent memory leaks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.cleanData( getAll( elem, false ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Remove any remaining nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem.textContent = &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>clone: function( dataAndEvents, deepDataAndEvents ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dataAndEvents = dataAndEvents == null ? false : dataAndEvents;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.map(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jQuery.clone( this, dataAndEvents, deepDataAndEvents );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>html: function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return access( this, function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var elem = this[ 0 ] || {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>l = this.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( value === undefined &amp;&amp; elem.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return elem.innerHTML;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// See if we can take a shortcut and just use innerHTML</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( typeof value === &quot;string&quot; &amp;&amp; !rnoInnerhtml.test( value ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>!wrapMap[ ( rtagName.exec( value ) || [ &quot;&quot;, &quot;&quot; ] )[ 1 ].toLowerCase() ] ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>value = value.replace( rxhtmlTag, &quot;&lt;$1&gt;&lt;/$2&gt;&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>for ( ; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem = this[ i ] || {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Remove element nodes and prevent memory leaks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( elem.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>jQuery.cleanData( getAll( elem, false ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>elem.innerHTML = value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If using innerHTML throws an exception, use the fallback method</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} catch( e ) {}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.empty().append( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}, null, value, arguments.length );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>replaceWith: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var arg = arguments[ 0 ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make the changes, replacing each context element with the new content</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.domManip( arguments, function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>arg = this.parentNode;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.cleanData( getAll( this ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( arg ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>arg.replaceChild( elem, this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Force removal if there was no new content (e.g., from empty arguments)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return arg &amp;&amp; (arg.length || arg.nodeType) ? this : this.remove();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>detach: function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.remove( selector, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>domManip: function( args, callback ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Flatten any nested arrays</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>args = concat.apply( [], args );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var fragment, first, scripts, hasScripts, node, doc,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>l = this.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>set = this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>iNoClone = l - 1,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>value = args[ 0 ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>isFunction = jQuery.isFunction( value );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// We can't cloneNode fragments that contain checked, in WebKit</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( isFunction ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( l &gt; 1 &amp;&amp; typeof value === &quot;string&quot; &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>!support.checkClone &amp;&amp; rchecked.test( value ) ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.each(function( index ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var self = set.eq( index );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( isFunction ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>args[ 0 ] = value.call( this, index, self.html() );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>self.domManip( args, callback );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( l ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>first = fragment.firstChild;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( fragment.childNodes.length === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>fragment = first;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( first ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>scripts = jQuery.map( getAll( fragment, &quot;script&quot; ), disableScript );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>hasScripts = scripts.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Use the original fragment for the last item instead of the first because it can end up</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// being emptied incorrectly in certain situations (#8070).</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( ; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>node = fragment;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( i !== iNoClone ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>node = jQuery.clone( node, true, true );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Keep references to cloned scripts for later restoration</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( hasScripts ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Support: QtWebKit</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// jQuery.merge because push.apply(_, arraylike) throws</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>jQuery.merge( scripts, getAll( node, &quot;script&quot; ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>callback.call( this[ i ], node, i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( hasScripts ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>doc = scripts[ scripts.length - 1 ].ownerDocument;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Reenable scripts</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.map( scripts, restoreScript );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Evaluate executable scripts on first document insertion</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>for ( i = 0; i &lt; hasScripts; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>node = scripts[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( rscriptType.test( node.type || &quot;&quot; ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>!data_priv.access( node, &quot;globalEval&quot; ) &amp;&amp; jQuery.contains( doc, node ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( node.src ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// Optional AJAX dependency, but won't run scripts if not present</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( jQuery._evalUrl ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>jQuery._evalUrl( node.src );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>jQuery.globalEval( node.textContent.replace( rcleanScript, &quot;&quot; ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.each({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>appendTo: &quot;append&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prependTo: &quot;prepend&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>insertBefore: &quot;before&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>insertAfter: &quot;after&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>replaceAll: &quot;replaceWith&quot;</div>
 
<div>}, function( name, original ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fn[ name ] = function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var elems,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>insert = jQuery( selector ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>last = insert.length - 1,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; i &lt;= last; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elems = i === last ? this : this.clone( true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery( insert[ i ] )[ original ]( elems );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: QtWebKit</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// .get() because push.apply(_, arraylike) throws</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>push.apply( ret, elems.get() );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.pushStack( ret );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var iframe,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>elemdisplay = {};</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Retrieve the actual display of a element</div>
 
<div> * @param {String} name nodeName of the element</div>
 
<div> * @param {Object} doc Document object</div>
 
<div> */</div>
 
<div>// Called only from within defaultDisplay</div>
 
<div>function actualDisplay( name, doc ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var style,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// getDefaultComputedStyle might be reliably used only on attached element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>display = window.getDefaultComputedStyle &amp;&amp; ( style = window.getDefaultComputedStyle( elem[ 0 ] ) ) ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Use of this method is a temporary fix (more like optmization) until something better comes along,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// since it was removed from specification and supported only in FF</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.display : jQuery.css( elem[ 0 ], &quot;display&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// We don't have any data stored on the element,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// so use &quot;detach&quot; method as fast way to get rid of the element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>elem.detach();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return display;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Try to determine the default display value of an element</div>
 
<div> * @param {String} nodeName</div>
 
<div> */</div>
 
<div>function defaultDisplay( nodeName ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var doc = document,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>display = elemdisplay[ nodeName ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !display ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>display = actualDisplay( nodeName, doc );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If the simple way fails, read from inside an iframe</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( display === &quot;none&quot; || !display ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Use the already-created iframe if possible</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>iframe = (iframe || jQuery( &quot;&lt;iframe frameborder='0' width='0' height='0'/&gt;&quot; )).appendTo( doc.documentElement );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>doc = iframe[ 0 ].contentDocument;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: IE</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>doc.write();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>doc.close();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>display = actualDisplay( nodeName, doc );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>iframe.detach();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Store the correct default display</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elemdisplay[ nodeName ] = display;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return display;</div>
 
<div>}</div>
 
<div>var rmargin = (/^margin/);</div>
 
<div> 
  <br />
 </div>
 
<div>var rnumnonpx = new RegExp( &quot;^(&quot; + pnum + &quot;)(?!px)[a-z%]+$&quot;, &quot;i&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div>var getStyles = function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return elem.ownerDocument.defaultView.getComputedStyle( elem, null );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>function curCSS( elem, name, computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var width, minWidth, maxWidth, ret,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>style = elem.style;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>computed = computed || getStyles( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// getPropertyValue is only needed for .css('filter') in IE9, see #12537</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret = computed.getPropertyValue( name ) || computed[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( computed ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( ret === &quot;&quot; &amp;&amp; !jQuery.contains( elem.ownerDocument, elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret = jQuery.style( elem, name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: iOS &lt; 6</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// A tribute to the &quot;awesome hack by Dean Edwards&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// iOS &lt; 6 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( rnumnonpx.test( ret ) &amp;&amp; rmargin.test( name ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remember the original values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>width = style.width;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>minWidth = style.minWidth;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>maxWidth = style.maxWidth;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Put in the new values to get a computed value out</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.minWidth = style.maxWidth = style.width = ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret = computed.width;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Revert the changed values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.width = width;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.minWidth = minWidth;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.maxWidth = maxWidth;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return ret !== undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: IE</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// IE returns zIndex value as an integer.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret + &quot;&quot; :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>ret;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>function addGetHookIf( conditionFn, hookFn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Define the hook, we'll check on the first run if it's really needed.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>get: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( conditionFn() ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Hook not needed (or it's not possible to use it due to missing dependency),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// remove it.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Since there are no other hooks for marginRight, remove the whole object.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>delete this.get;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Hook needed; redefine it so that the support test is not executed again.</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return (this.get = hookFn).apply( this, arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var pixelPositionVal, boxSizingReliableVal,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem = document.documentElement,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>container = document.createElement( &quot;div&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div = document.createElement( &quot;div&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !div.style ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>div.style.backgroundClip = &quot;content-box&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>div.cloneNode( true ).style.backgroundClip = &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.clearCloneStyle = div.style.backgroundClip === &quot;content-box&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>container.style.cssText = &quot;border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;position:absolute&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>container.appendChild( div );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Executing both pixelPosition &amp; boxSizingReliable tests require only one layout</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// so they're executed at the same time to save the second computation.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>function computePixelPositionAndBoxSizingReliable() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div.style.cssText =</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: Firefox&lt;29, Android 2.3</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Vendor-prefix box-sizing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;box-sizing:border-box;display:block;margin-top:1%;top:1%;&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;border:1px;padding:1px;width:4px;position:absolute&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>div.innerHTML = &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem.appendChild( container );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var divStyle = window.getComputedStyle( div, null );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>pixelPositionVal = divStyle.top !== &quot;1%&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>boxSizingReliableVal = divStyle.width === &quot;4px&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem.removeChild( container );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: node.js jsdom</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Don't assume that getComputedStyle is a property of the global object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( window.getComputedStyle ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.extend( support, {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>pixelPosition: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// This test is executed only once but we still do memoizing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// since we can use the boxSizingReliable pre-computing.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// No need to check if the test was already performed, though.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>computePixelPositionAndBoxSizingReliable();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return pixelPositionVal;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>boxSizingReliable: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( boxSizingReliableVal == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>computePixelPositionAndBoxSizingReliable();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return boxSizingReliableVal;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>reliableMarginRight: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Support: Android 2.3</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Check if div with explicit width and no margin-right incorrectly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// gets computed margin-right based on width of container. (#3333)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// This support function is only executed once so no memoizing is needed.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var ret,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>marginDiv = div.appendChild( document.createElement( &quot;div&quot; ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Reset CSS: box-sizing; display; margin; border; padding</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>marginDiv.style.cssText = div.style.cssText =</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Support: Firefox&lt;29, Android 2.3</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Vendor-prefix box-sizing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>&quot;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;&quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>&quot;box-sizing:content-box;display:block;margin:0;border:0;padding:0&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>marginDiv.style.marginRight = marginDiv.style.width = &quot;0&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>div.style.width = &quot;1px&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>docElem.appendChild( container );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ret = !parseFloat( window.getComputedStyle( marginDiv, null ).marginRight );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>docElem.removeChild( container );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>})();</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// A method for quickly swapping in/out CSS properties to get correct calculations.</div>
 
<div>jQuery.swap = function( elem, options, callback, args ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var ret, name,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>old = {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Remember the old values, and insert the new ones</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( name in options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>old[ name ] = elem.style[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem.style[ name ] = options[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ret = callback.apply( elem, args || [] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Revert the old values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( name in options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem.style[ name ] = old[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return ret;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// swappable if display is none or starts with table except &quot;table&quot;, &quot;table-cell&quot;, or &quot;table-caption&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// see here for display values: https://developer.mozilla.org/en-US/docs/CSS/display</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rdisplayswap = /^(none|table(?!-c[ea]).+)/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rnumsplit = new RegExp( &quot;^(&quot; + pnum + &quot;)(.*)$&quot;, &quot;i&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rrelNum = new RegExp( &quot;^([+-])=(&quot; + pnum + &quot;)&quot;, &quot;i&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cssShow = { position: &quot;absolute&quot;, visibility: &quot;hidden&quot;, display: &quot;block&quot; },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cssNormalTransform = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>letterSpacing: &quot;0&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>fontWeight: &quot;400&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cssPrefixes = [ &quot;Webkit&quot;, &quot;O&quot;, &quot;Moz&quot;, &quot;ms&quot; ];</div>
 
<div> 
  <br />
 </div>
 
<div>// return a css property mapped to a potentially vendor prefixed property</div>
 
<div>function vendorPropName( style, name ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// shortcut for names that are not vendor prefixed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( name in style ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return name;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// check for vendor prefixed names</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var capName = name[0].toUpperCase() + name.slice(1),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>origName = name,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = cssPrefixes.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>name = cssPrefixes[ i ] + capName;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( name in style ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return name;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return origName;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function setPositiveNumber( elem, value, subtract ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var matches = rnumsplit.exec( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return matches ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Guard against undefined &quot;subtract&quot;, e.g., when used as in cssHooks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || &quot;px&quot; ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>value;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var i = extra === ( isBorderBox ? &quot;border&quot; : &quot;content&quot; ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If we already have the right measurement, avoid augmentation</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>4 :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Otherwise initialize for horizontal or vertical properties</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>name === &quot;width&quot; ? 1 : 0,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>val = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; 4; i += 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// both box models exclude margin, so add it if we want it</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( extra === &quot;margin&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( isBorderBox ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// border-box includes padding, so remove it if we want content</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( extra === &quot;content&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val -= jQuery.css( elem, &quot;padding&quot; + cssExpand[ i ], true, styles );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// at this point, extra isn't border nor margin, so remove border</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( extra !== &quot;margin&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val -= jQuery.css( elem, &quot;border&quot; + cssExpand[ i ] + &quot;Width&quot;, true, styles );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// at this point, extra isn't content, so add padding</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>val += jQuery.css( elem, &quot;padding&quot; + cssExpand[ i ], true, styles );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// at this point, extra isn't content nor padding, so add border</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( extra !== &quot;padding&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val += jQuery.css( elem, &quot;border&quot; + cssExpand[ i ] + &quot;Width&quot;, true, styles );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return val;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function getWidthOrHeight( elem, name, extra ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Start with offset property, which is equivalent to the border-box value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var valueIsBorderBox = true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>val = name === &quot;width&quot; ? elem.offsetWidth : elem.offsetHeight,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>styles = getStyles( elem ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>isBorderBox = jQuery.css( elem, &quot;boxSizing&quot;, false, styles ) === &quot;border-box&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// some non-html elements return undefined for offsetWidth, so check for null/undefined</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( val &lt;= 0 || val == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fall back to computed then uncomputed css if necessary</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>val = curCSS( elem, name, styles );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( val &lt; 0 || val == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>val = elem.style[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Computed unit is not pixels. Stop here and return.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( rnumnonpx.test(val) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return val;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// we need the check for style in case a browser which returns unreliable values</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// for getComputedStyle silently falls back to the reliable elem.style</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>valueIsBorderBox = isBorderBox &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>( support.boxSizingReliable() || val === elem.style[ name ] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Normalize &quot;&quot;, auto, and prepare for extra</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>val = parseFloat( val ) || 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// use the active box-sizing model to add/subtract irrelevant styles</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return ( val +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>augmentWidthOrHeight(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>name,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>extra || ( isBorderBox ? &quot;border&quot; : &quot;content&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>valueIsBorderBox,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>styles</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>) + &quot;px&quot;;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function showHide( elements, show ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var display, elem, hidden,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>values = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>index = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>length = elements.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; index &lt; length; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem = elements[ index ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elem.style ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>continue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>values[ index ] = data_priv.get( elem, &quot;olddisplay&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>display = elem.style.display;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( show ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Reset the inline display of this element to learn if it is</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// being hidden by cascaded rules or not</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !values[ index ] &amp;&amp; display === &quot;none&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem.style.display = &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Set elements which have been overridden with display: none</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// in a stylesheet to whatever the default browser style is</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// for such an element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem.style.display === &quot;&quot; &amp;&amp; isHidden( elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>values[ index ] = data_priv.access( elem, &quot;olddisplay&quot;, defaultDisplay(elem.nodeName) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hidden = isHidden( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( display !== &quot;none&quot; || !hidden ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data_priv.set( elem, &quot;olddisplay&quot;, hidden ? display : jQuery.css( elem, &quot;display&quot; ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Set the display of most of the elements in a second loop</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// to avoid the constant reflow</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( index = 0; index &lt; length; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>elem = elements[ index ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elem.style ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>continue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !show || elem.style.display === &quot;none&quot; || elem.style.display === &quot;&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem.style.display = show ? values[ index ] || &quot;&quot; : &quot;none&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return elements;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Add in style property hooks for overriding the default</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// behavior of getting and setting a style property</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cssHooks: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>opacity: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>get: function( elem, computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// We should always get a number back from opacity</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var ret = curCSS( elem, &quot;opacity&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return ret === &quot;&quot; ? &quot;1&quot; : ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Don't automatically add &quot;px&quot; to these possibly-unitless properties</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cssNumber: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;columnCount&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;fillOpacity&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;flexGrow&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;flexShrink&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;fontWeight&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;lineHeight&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;opacity&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;order&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;orphans&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;widows&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;zIndex&quot;: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;zoom&quot;: true</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Add in properties whose names you wish to fix before</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// setting or getting the value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cssProps: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// normalize float css property</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;float&quot;: &quot;cssFloat&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Get and set the style property on a DOM Node</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>style: function( elem, name, value, extra ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Don't set styles on text and comment nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make sure that we're working with the right name</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var ret, type, hooks,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>origName = jQuery.camelCase( name ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style = elem.style;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// gets hook for the prefixed version</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// followed by the unprefixed version</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Check if we're setting a value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( value !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = typeof value;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// convert relative number strings (+= or -=) to relative numbers. #7345</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( type === &quot;string&quot; &amp;&amp; (ret = rrelNum.exec( value )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Fixes bug #9237</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>type = &quot;number&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Make sure that null and NaN values aren't set. See: #7116</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( value == null || value !== value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If a number was passed in, add 'px' to the (except for certain CSS properties)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( type === &quot;number&quot; &amp;&amp; !jQuery.cssNumber[ origName ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>value += &quot;px&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Fixes #8908, it can be done more correctly by specifying setters in cssHooks,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// but it would mean to define eight (for every problematic property) identical functions</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !support.clearCloneStyle &amp;&amp; value === &quot;&quot; &amp;&amp; name.indexOf( &quot;background&quot; ) === 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>style[ name ] = &quot;inherit&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If a hook was provided, use that value, otherwise just set the specified value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !hooks || !(&quot;set&quot; in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>style[ name ] = value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If a hook was provided get the non-computed value from there</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( hooks &amp;&amp; &quot;get&quot; in hooks &amp;&amp; (ret = hooks.get( elem, false, extra )) !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Otherwise just get the value from the style object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return style[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>css: function( elem, name, extra, styles ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var val, num, hooks,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>origName = jQuery.camelCase( name );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make sure that we're working with the right name</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// gets hook for the prefixed version</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// followed by the unprefixed version</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If a hook was provided get the computed value from there</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( hooks &amp;&amp; &quot;get&quot; in hooks ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>val = hooks.get( elem, true, extra );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Otherwise, if a way to get the computed value exists, use that</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( val === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>val = curCSS( elem, name, styles );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>//convert &quot;normal&quot; to computed value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( val === &quot;normal&quot; &amp;&amp; name in cssNormalTransform ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>val = cssNormalTransform[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Return, converting to number if forced or a qualifier was provided and val looks numeric</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( extra === &quot;&quot; || extra ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>num = parseFloat( val );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return val;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.each([ &quot;height&quot;, &quot;width&quot; ], function( i, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.cssHooks[ name ] = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>get: function( elem, computed, extra ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// certain elements can have dimension info if we invisibly show them</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// however, it must have a current display style that would benefit from this</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return rdisplayswap.test( jQuery.css( elem, &quot;display&quot; ) ) &amp;&amp; elem.offsetWidth === 0 ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.swap( elem, cssShow, function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return getWidthOrHeight( elem, name, extra );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>getWidthOrHeight( elem, name, extra );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>set: function( elem, value, extra ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var styles = extra &amp;&amp; getStyles( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return setPositiveNumber( elem, value, extra ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>augmentWidthOrHeight(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>name,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>extra,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.css( elem, &quot;boxSizing&quot;, false, styles ) === &quot;border-box&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>styles</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>) : 0</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: Android 2.3</div>
 
<div>jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>function( elem, computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Work around by temporarily setting element display to inline-block</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jQuery.swap( elem, { &quot;display&quot;: &quot;inline-block&quot; },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>curCSS, [ elem, &quot;marginRight&quot; ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>);</div>
 
<div> 
  <br />
 </div>
 
<div>// These hooks are used by animate to expand properties</div>
 
<div>jQuery.each({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>margin: &quot;&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>padding: &quot;&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>border: &quot;Width&quot;</div>
 
<div>}, function( prefix, suffix ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.cssHooks[ prefix + suffix ] = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>expand: function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>expanded = {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// assumes a single number if not a string</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>parts = typeof value === &quot;string&quot; ? value.split(&quot; &quot;) : [ value ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; 4; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>expanded[ prefix + cssExpand[ i ] + suffix ] =</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>parts[ i ] || parts[ i - 2 ] || parts[ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return expanded;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !rmargin.test( prefix ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>css: function( name, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return access( this, function( elem, name, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var styles, len,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>map = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>i = 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.isArray( name ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>styles = getStyles( elem );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>len = name.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return map;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return value !== undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.style( elem, name, value ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.css( elem, name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}, name, value, arguments.length &gt; 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>show: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return showHide( this, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hide: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return showHide( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>toggle: function( state ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof state === &quot;boolean&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return state ? this.show() : this.hide();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( isHidden( this ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).show();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).hide();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>function Tween( elem, options, prop, end, easing ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return new Tween.prototype.init( elem, options, prop, end, easing );</div>
 
<div>}</div>
 
<div>jQuery.Tween = Tween;</div>
 
<div> 
  <br />
 </div>
 
<div>Tween.prototype = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>constructor: Tween,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>init: function( elem, options, prop, end, easing, unit ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.elem = elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.prop = prop;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.easing = easing || &quot;swing&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.options = options;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.start = this.now = this.cur();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.end = end;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.unit = unit || ( jQuery.cssNumber[ prop ] ? &quot;&quot; : &quot;px&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>cur: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var hooks = Tween.propHooks[ this.prop ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return hooks &amp;&amp; hooks.get ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks.get( this ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>Tween.propHooks._default.get( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>run: function( percent ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var eased,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks = Tween.propHooks[ this.prop ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( this.options.duration ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.pos = eased = jQuery.easing[ this.easing ](</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>percent, this.options.duration * percent, 0, 1, this.options.duration</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.pos = eased = percent;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this.now = ( this.end - this.start ) * eased + this.start;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( this.options.step ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.options.step.call( this.elem, this.now, this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( hooks &amp;&amp; hooks.set ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks.set( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>Tween.propHooks._default.set( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>Tween.prototype.init.prototype = Tween.prototype;</div>
 
<div> 
  <br />
 </div>
 
<div>Tween.propHooks = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>_default: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>get: function( tween ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var result;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( tween.elem[ tween.prop ] != null &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return tween.elem[ tween.prop ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// passing an empty string as a 3rd parameter to .css will automatically</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// attempt a parseFloat and fallback to a string if the parse fails</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// so, simple values such as &quot;10px&quot; are parsed to Float.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// complex values such as &quot;rotate(1rad)&quot; are returned as is.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>result = jQuery.css( tween.elem, tween.prop, &quot;&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Empty strings, null, undefined and &quot;auto&quot; are converted to 0.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return !result || result === &quot;auto&quot; ? 0 : result;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>set: function( tween ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// use step hook for back compat - use cssHook if its there - use .style if its</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// available and use plain properties where available</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.fx.step[ tween.prop ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.fx.step[ tween.prop ]( tween );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( tween.elem.style &amp;&amp; ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>tween.elem[ tween.prop ] = tween.now;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE9</div>
 
<div>// Panic based approach to setting things on disconnected nodes</div>
 
<div> 
  <br />
 </div>
 
<div>Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>set: function( tween ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( tween.elem.nodeType &amp;&amp; tween.elem.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tween.elem[ tween.prop ] = tween.now;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.easing = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>linear: function( p ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return p;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>swing: function( p ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return 0.5 - Math.cos( p * Math.PI ) / 2;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fx = Tween.prototype.init;</div>
 
<div> 
  <br />
 </div>
 
<div>// Back Compat &lt;1.8 extension point</div>
 
<div>jQuery.fx.step = {};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fxNow, timerId,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rfxtypes = /^(?:toggle|show|hide)$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rfxnum = new RegExp( &quot;^(?:([+-])=|)(&quot; + pnum + &quot;)([a-z%]*)$&quot;, &quot;i&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rrun = /queueHooks$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>animationPrefilters = [ defaultPrefilter ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>tweeners = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;*&quot;: [ function( prop, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var tween = this.createTween( prop, value ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>target = tween.cur(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>parts = rfxnum.exec( value ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>unit = parts &amp;&amp; parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? &quot;&quot; : &quot;px&quot; ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Starting value computation is required for potential unit mismatches</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>start = ( jQuery.cssNumber[ prop ] || unit !== &quot;px&quot; &amp;&amp; +target ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>rfxnum.exec( jQuery.css( tween.elem, prop ) ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>scale = 1,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>maxIterations = 20;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( start &amp;&amp; start[ 3 ] !== unit ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Trust units reported by jQuery.css</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>unit = unit || start[ 3 ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Make sure we update the tween properties later on</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>parts = parts || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Iteratively approximate from a nonzero starting point</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>start = +target || 1;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>do {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// If previous iteration zeroed out, double until we get *something*</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Use a string for doubling factor so we don't accidentally see scale as unchanged below</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>scale = scale || &quot;.5&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Adjust and apply</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>start = start / scale;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.style( tween.elem, prop, start + unit );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Update scale, tolerating zero or NaN from tween.cur()</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// And breaking the loop if scale is unchanged or perfect, or if we've just had enough</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} while ( scale !== (scale = tween.cur() / target) &amp;&amp; scale !== 1 &amp;&amp; --maxIterations );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Update tween properties</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( parts ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>start = tween.start = +start || +target || 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>tween.unit = unit;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If a +=/-= token was provided, we're doing a relative animation</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>tween.end = parts[ 1 ] ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>+parts[ 2 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return tween;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} ]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Animations created synchronously will run synchronously</div>
 
<div>function createFxNow() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>setTimeout(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>fxNow = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return ( fxNow = jQuery.now() );</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Generate parameters to create a standard animation</div>
 
<div>function genFx( type, includeWidth ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var which,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>attrs = { height: type };</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// if we include width, step value is 1 to do all cssExpand values,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// if we don't include width, step value is 2 to skip over Left and Right</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>includeWidth = includeWidth ? 1 : 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; 4 ; i += 2 - includeWidth ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>which = cssExpand[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>attrs[ &quot;margin&quot; + which ] = attrs[ &quot;padding&quot; + which ] = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( includeWidth ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>attrs.opacity = attrs.width = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return attrs;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function createTween( value, prop, animation ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var tween,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>collection = ( tweeners[ prop ] || [] ).concat( tweeners[ &quot;*&quot; ] ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>index = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>length = collection.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; index &lt; length; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( (tween = collection[ index ].call( animation, prop, value )) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// we're done with this property</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return tween;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function defaultPrefilter( elem, props, opts ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* jshint validthis: true */</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>anim = this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>orig = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>style = elem.style,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>hidden = elem.nodeType &amp;&amp; isHidden( elem ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dataShow = data_priv.get( elem, &quot;fxshow&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// handle queue: false promises</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !opts.queue ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>hooks = jQuery._queueHooks( elem, &quot;fx&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( hooks.unqueued == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks.unqueued = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>oldfire = hooks.empty.fire;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks.empty.fire = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !hooks.unqueued ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>oldfire();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>hooks.unqueued++;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>anim.always(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// doing this makes sure that the complete handler will be called</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// before this completes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>anim.always(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>hooks.unqueued--;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !jQuery.queue( elem, &quot;fx&quot; ).length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>hooks.empty.fire();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// height/width overflow pass</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( elem.nodeType === 1 &amp;&amp; ( &quot;height&quot; in props || &quot;width&quot; in props ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make sure that nothing sneaks out</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Record all 3 overflow attributes because IE9-10 do not</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// change the overflow attribute when overflowX and</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// overflowY are set to the same value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Set display property to inline-block for height/width</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// animations on inline elements that are having width/height animated</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>display = jQuery.css( elem, &quot;display&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Test default display if display is currently &quot;none&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>checkDisplay = display === &quot;none&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data_priv.get( elem, &quot;olddisplay&quot; ) || defaultDisplay( elem.nodeName ) : display;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( checkDisplay === &quot;inline&quot; &amp;&amp; jQuery.css( elem, &quot;float&quot; ) === &quot;none&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.display = &quot;inline-block&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( opts.overflow ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>style.overflow = &quot;hidden&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>anim.always(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.overflow = opts.overflow[ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.overflowX = opts.overflow[ 1 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>style.overflowY = opts.overflow[ 2 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// show/hide pass</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( prop in props ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>value = props[ prop ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( rfxtypes.exec( value ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete props[ prop ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>toggle = toggle || value === &quot;toggle&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( value === ( hidden ? &quot;hide&quot; : &quot;show&quot; ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( value === &quot;show&quot; &amp;&amp; dataShow &amp;&amp; dataShow[ prop ] !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>hidden = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>continue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>orig[ prop ] = dataShow &amp;&amp; dataShow[ prop ] || jQuery.style( elem, prop );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Any non-fx value stops us from restoring the original display value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>display = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !jQuery.isEmptyObject( orig ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( dataShow ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( &quot;hidden&quot; in dataShow ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>hidden = dataShow.hidden;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>dataShow = data_priv.access( elem, &quot;fxshow&quot;, {} );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// store state if its toggle - enables .stop().toggle() to &quot;reverse&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( toggle ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>dataShow.hidden = !hidden;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( hidden ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery( elem ).show();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>anim.done(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( elem ).hide();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>anim.done(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var prop;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data_priv.remove( elem, &quot;fxshow&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( prop in orig ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.style( elem, prop, orig[ prop ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( prop in orig ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !( prop in dataShow ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>dataShow[ prop ] = tween.start;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( hidden ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tween.end = tween.start;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>tween.start = prop === &quot;width&quot; || prop === &quot;height&quot; ? 1 : 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If this is a noop like .hide().hide(), restore an overwritten display value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else if ( (display === &quot;none&quot; ? defaultDisplay( elem.nodeName ) : display) === &quot;inline&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>style.display = display;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function propFilter( props, specialEasing ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var index, name, easing, value, hooks;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// camelCase, specialEasing and expand cssHook pass</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( index in props ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>name = jQuery.camelCase( index );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>easing = specialEasing[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>value = props[ index ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isArray( value ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>easing = value[ 1 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>value = props[ index ] = value[ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( index !== name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>props[ name ] = value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete props[ index ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>hooks = jQuery.cssHooks[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( hooks &amp;&amp; &quot;expand&quot; in hooks ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>value = hooks.expand( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete props[ name ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// not quite $.extend, this wont overwrite keys already present.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// also - reusing 'index' from above because we have the correct &quot;name&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( index in value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !( index in props ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>props[ index ] = value[ index ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>specialEasing[ index ] = easing;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>specialEasing[ name ] = easing;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>function Animation( elem, properties, options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var result,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>stopped,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>index = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>length = animationPrefilters.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>deferred = jQuery.Deferred().always( function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// don't match elem in the :animated selector</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete tick.elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>tick = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( stopped ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var currentTime = fxNow || createFxNow(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// archaic crash bug won't allow us to use 1 - ( 0.5 || 0 ) (#12497)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>temp = remaining / animation.duration || 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>percent = 1 - temp,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>index = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>length = animation.tweens.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; index &lt; length ; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>animation.tweens[ index ].run( percent );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>deferred.notifyWith( elem, [ animation, percent, remaining ]);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( percent &lt; 1 &amp;&amp; length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return remaining;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>deferred.resolveWith( elem, [ animation ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>animation = deferred.promise({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem: elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>props: jQuery.extend( {}, properties ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>opts: jQuery.extend( true, { specialEasing: {} }, options ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>originalProperties: properties,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>originalOptions: options,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>startTime: fxNow || createFxNow(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>duration: options.duration,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tweens: [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>createTween: function( prop, end ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var tween = jQuery.Tween( elem, animation.opts, prop, end,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>animation.opts.specialEasing[ prop ] || animation.opts.easing );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>animation.tweens.push( tween );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return tween;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>stop: function( gotoEnd ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var index = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// if we are going to the end, we want to run all the tweens</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// otherwise we skip this part</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>length = gotoEnd ? animation.tweens.length : 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( stopped ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>stopped = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( ; index &lt; length ; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>animation.tweens[ index ].run( 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// resolve when we played the last frame</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// otherwise, reject</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( gotoEnd ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>deferred.resolveWith( elem, [ animation, gotoEnd ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>deferred.rejectWith( elem, [ animation, gotoEnd ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>props = animation.props;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>propFilter( props, animation.opts.specialEasing );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; index &lt; length ; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( result ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return result;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.map( props, createTween, animation );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( jQuery.isFunction( animation.opts.start ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>animation.opts.start.call( elem, animation );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fx.timer(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.extend( tick, {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem: elem,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>anim: animation,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>queue: animation.opts.queue</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>})</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// attach callbacks from options</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return animation.progress( animation.opts.progress )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>.done( animation.opts.done, animation.opts.complete )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>.fail( animation.opts.fail )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>.always( animation.opts.always );</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.Animation = jQuery.extend( Animation, {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>tweener: function( props, callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( props ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>callback = props;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>props = [ &quot;*&quot; ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>props = props.split(&quot; &quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var prop,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>index = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>length = props.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; index &lt; length ; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>prop = props[ index ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tweeners[ prop ] = tweeners[ prop ] || [];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>tweeners[ prop ].unshift( callback );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prefilter: function( callback, prepend ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( prepend ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>animationPrefilters.unshift( callback );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>animationPrefilters.push( callback );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.speed = function( speed, easing, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var opt = speed &amp;&amp; typeof speed === &quot;object&quot; ? jQuery.extend( {}, speed ) : {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>complete: fn || !fn &amp;&amp; easing ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.isFunction( speed ) &amp;&amp; speed,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>duration: speed,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>easing: fn &amp;&amp; easing || easing &amp;&amp; !jQuery.isFunction( easing ) &amp;&amp; easing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === &quot;number&quot; ? opt.duration :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// normalize opt.queue - true/undefined/null -&gt; &quot;fx&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( opt.queue == null || opt.queue === true ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>opt.queue = &quot;fx&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Queueing</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>opt.old = opt.complete;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>opt.complete = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( opt.old ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>opt.old.call( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( opt.queue ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.dequeue( this, opt.queue );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return opt;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fadeTo: function( speed, to, easing, callback ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// show any hidden elements after setting opacity to 0</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.filter( isHidden ).css( &quot;opacity&quot;, 0 ).show()</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// animate to the value specified</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>.end().animate({ opacity: to }, speed, easing, callback );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>animate: function( prop, speed, easing, callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var empty = jQuery.isEmptyObject( prop ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>optall = jQuery.speed( speed, easing, callback ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>doAnimation = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Operate on a copy of prop so per-property easing won't be lost</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var anim = Animation( this, jQuery.extend( {}, prop ), optall );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Empty animations, or finishing resolves immediately</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( empty || data_priv.get( this, &quot;finish&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>anim.stop( true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>doAnimation.finish = doAnimation;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return empty || optall.queue === false ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.each( doAnimation ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.queue( optall.queue, doAnimation );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>stop: function( type, clearQueue, gotoEnd ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var stopQueue = function( hooks ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var stop = hooks.stop;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete hooks.stop;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>stop( gotoEnd );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof type !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>gotoEnd = clearQueue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>clearQueue = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( clearQueue &amp;&amp; type !== false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.queue( type || &quot;fx&quot;, [] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var dequeue = true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>index = type != null &amp;&amp; type + &quot;queueHooks&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>timers = jQuery.timers,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = data_priv.get( this );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( index ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( data[ index ] &amp;&amp; data[ index ].stop ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>stopQueue( data[ index ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( index in data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( data[ index ] &amp;&amp; data[ index ].stop &amp;&amp; rrun.test( index ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>stopQueue( data[ index ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( index = timers.length; index--; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( timers[ index ].elem === this &amp;&amp; (type == null || timers[ index ].queue === type) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>timers[ index ].anim.stop( gotoEnd );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>dequeue = false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>timers.splice( index, 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// start the next in the queue if the last step wasn't forced</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// timers currently will call their complete callbacks, which will dequeue</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// but only if they were gotoEnd</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( dequeue || !gotoEnd ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.dequeue( this, type );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>finish: function( type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( type !== false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = type || &quot;fx&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var index,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>data = data_priv.get( this ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>queue = data[ type + &quot;queue&quot; ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>hooks = data[ type + &quot;queueHooks&quot; ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>timers = jQuery.timers,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>length = queue ? queue.length : 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// enable finishing flag on private data</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data.finish = true;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// empty the queue first</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.queue( this, type, [] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( hooks &amp;&amp; hooks.stop ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>hooks.stop.call( this, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// look for any active animations, and finish them</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( index = timers.length; index--; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( timers[ index ].elem === this &amp;&amp; timers[ index ].queue === type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>timers[ index ].anim.stop( true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>timers.splice( index, 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// look for any animations in the old queue and finish them</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( index = 0; index &lt; length; index++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( queue[ index ] &amp;&amp; queue[ index ].finish ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>queue[ index ].finish.call( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// turn off finishing flag</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete data.finish;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.each([ &quot;toggle&quot;, &quot;show&quot;, &quot;hide&quot; ], function( i, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var cssFn = jQuery.fn[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fn[ name ] = function( speed, easing, callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return speed == null || typeof speed === &quot;boolean&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cssFn.apply( this, arguments ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.animate( genFx( name, true ), speed, easing, callback );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Generate shortcuts for custom animations</div>
 
<div>jQuery.each({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>slideDown: genFx(&quot;show&quot;),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>slideUp: genFx(&quot;hide&quot;),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>slideToggle: genFx(&quot;toggle&quot;),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fadeIn: { opacity: &quot;show&quot; },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fadeOut: { opacity: &quot;hide&quot; },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fadeToggle: { opacity: &quot;toggle&quot; }</div>
 
<div>}, function( name, props ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fn[ name ] = function( speed, easing, callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.animate( props, speed, easing, callback );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.timers = [];</div>
 
<div>jQuery.fx.tick = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var timer,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>timers = jQuery.timers;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fxNow = jQuery.now();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( ; i &lt; timers.length; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>timer = timers[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Checks the timer has not already been removed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !timer() &amp;&amp; timers[ i ] === timer ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>timers.splice( i--, 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !timers.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.fx.stop();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fxNow = undefined;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fx.timer = function( timer ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.timers.push( timer );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( timer() ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.fx.start();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.timers.pop();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fx.interval = 13;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fx.start = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !timerId ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fx.stop = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>clearInterval( timerId );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>timerId = null;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fx.speeds = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>slow: 600,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>fast: 200,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Default speed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>_default: 400</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Based off of the plugin by Clint Helfers, with permission.</div>
 
<div>// http://blindsignals.com/index.php/2009/07/jquery-delay/</div>
 
<div>jQuery.fn.delay = function( time, type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>type = type || &quot;fx&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return this.queue( type, function( next, hooks ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var timeout = setTimeout( next, time );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>hooks.stop = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>clearTimeout( timeout );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var input = document.createElement( &quot;input&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>select = document.createElement( &quot;select&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>opt = select.appendChild( document.createElement( &quot;option&quot; ) );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>input.type = &quot;checkbox&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: iOS 5.1, Android 4.x, Android 2.3</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check the default checkbox/radio value (&quot;&quot; on old WebKit; &quot;on&quot; elsewhere)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.checkOn = input.value !== &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Must access the parent to make an option select properly</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE9, IE10</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.optSelected = opt.selected;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Make sure that the options inside disabled selects aren't marked as disabled</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// (WebKit marks them as disabled)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>select.disabled = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.optDisabled = !opt.disabled;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check if an input maintains its value after becoming a radio</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE9, IE10</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>input = document.createElement( &quot;input&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>input.value = &quot;t&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>input.type = &quot;radio&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>support.radioValue = input.value === &quot;t&quot;;</div>
 
<div>})();</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var nodeHook, boolHook,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>attrHandle = jQuery.expr.attrHandle;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>attr: function( name, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return access( this, jQuery.attr, name, value, arguments.length &gt; 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>removeAttr: function( name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.removeAttr( this, name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>attr: function( elem, name, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var hooks, ret,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>nType = elem.nodeType;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// don't get/set attributes on text, comment and attribute nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fallback to prop when attributes are not supported</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof elem.getAttribute === strundefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jQuery.prop( elem, name, value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// All attributes are lowercase</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Grab necessary hook if one is defined</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>name = name.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks = jQuery.attrHooks[ name ] ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( value !== undefined ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( value === null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.removeAttr( elem, name );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( hooks &amp;&amp; &quot;set&quot; in hooks &amp;&amp; (ret = hooks.set( elem, value, name )) !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return ret;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem.setAttribute( name, value + &quot;&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( hooks &amp;&amp; &quot;get&quot; in hooks &amp;&amp; (ret = hooks.get( elem, name )) !== null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return ret;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret = jQuery.find.attr( elem, name );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Non-existent attributes return null, we normalize to undefined</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return ret == null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>undefined :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>removeAttr: function( elem, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var name, propName,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>attrNames = value &amp;&amp; value.match( rnotwhite );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( attrNames &amp;&amp; elem.nodeType === 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( (name = attrNames[i++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>propName = jQuery.propFix[ name ] || name;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Boolean attributes get special treatment (#10870)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( jQuery.expr.match.bool.test( name ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Set corresponding property to false</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem[ propName ] = false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem.removeAttribute( name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>attrHooks: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>type: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>set: function( elem, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !support.radioValue &amp;&amp; value === &quot;radio&quot; &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.nodeName( elem, &quot;input&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Setting the type on a radio button after the value resets the value in IE6-9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Reset value to default in case type is set after value during creation</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var val = elem.value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem.setAttribute( &quot;type&quot;, value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( val ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.value = val;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Hooks for boolean attributes</div>
 
<div>boolHook = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>set: function( elem, value, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( value === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Remove boolean attributes when set to false</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.removeAttr( elem, name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem.setAttribute( name, name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return name;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div>jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var getter = attrHandle[ name ] || jQuery.find.attr;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>attrHandle[ name ] = function( elem, name, isXML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var ret, handle;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !isXML ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Avoid an infinite loop by temporarily removing this function from the getter</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>handle = attrHandle[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>attrHandle[ name ] = ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ret = getter( elem, name, isXML ) != null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>name.toLowerCase() :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>attrHandle[ name ] = handle;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var rfocusable = /^(?:input|select|textarea|button)$/i;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prop: function( name, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return access( this, jQuery.prop, name, value, arguments.length &gt; 1 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>removeProp: function( name ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>delete this[ jQuery.propFix[ name ] || name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>propFix: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;for&quot;: &quot;htmlFor&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;class&quot;: &quot;className&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prop: function( elem, name, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var ret, hooks, notxml,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>nType = elem.nodeType;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// don't get/set properties on text, comment and attribute nodes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>notxml = nType !== 1 || !jQuery.isXMLDoc( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( notxml ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Fix name and attach hooks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>name = jQuery.propFix[ name ] || name;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks = jQuery.propHooks[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( value !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return hooks &amp;&amp; &quot;set&quot; in hooks &amp;&amp; (ret = hooks.set( elem, value, name )) !== undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ret :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( elem[ name ] = value );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return hooks &amp;&amp; &quot;get&quot; in hooks &amp;&amp; (ret = hooks.get( elem, name )) !== null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ret :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem[ name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>propHooks: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>tabIndex: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>get: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return elem.hasAttribute( &quot;tabindex&quot; ) || rfocusable.test( elem.nodeName ) || elem.href ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem.tabIndex :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>-1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE9+</div>
 
<div>// Selectedness for an option in an optgroup can be inaccurate</div>
 
<div>if ( !support.optSelected ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.propHooks.selected = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>get: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var parent = elem.parentNode;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( parent &amp;&amp; parent.parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>parent.parentNode.selectedIndex;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.each([</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;tabIndex&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;readOnly&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;maxLength&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;cellSpacing&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;cellPadding&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;rowSpan&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;colSpan&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;useMap&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;frameBorder&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;contentEditable&quot;</div>
 
<div>], function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.propFix[ this.toLowerCase() ] = this;</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var rclass = /[\t\r\n\f]/g;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>addClass: function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var classes, elem, cur, clazz, j, finalValue,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>proceed = typeof value === &quot;string&quot; &amp;&amp; value,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>len = this.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( value ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.each(function( j ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).addClass( value.call( this, j, this.className ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( proceed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// The disjunction here is for better compressibility (see removeClass)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>classes = ( value || &quot;&quot; ).match( rnotwhite ) || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem = this[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>cur = elem.nodeType === 1 &amp;&amp; ( elem.className ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>( &quot; &quot; + elem.className + &quot; &quot; ).replace( rclass, &quot; &quot; ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>&quot; &quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( cur ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>j = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>while ( (clazz = classes[j++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( cur.indexOf( &quot; &quot; + clazz + &quot; &quot; ) &lt; 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>cur += clazz + &quot; &quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// only assign if different to avoid unneeded rendering.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>finalValue = jQuery.trim( cur );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem.className !== finalValue ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.className = finalValue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>removeClass: function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var classes, elem, cur, clazz, j, finalValue,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>proceed = arguments.length === 0 || typeof value === &quot;string&quot; &amp;&amp; value,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>len = this.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( value ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.each(function( j ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).removeClass( value.call( this, j, this.className ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( proceed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>classes = ( value || &quot;&quot; ).match( rnotwhite ) || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>for ( ; i &lt; len; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem = this[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// This expression is here for better compressibility (see addClass)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>cur = elem.nodeType === 1 &amp;&amp; ( elem.className ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>( &quot; &quot; + elem.className + &quot; &quot; ).replace( rclass, &quot; &quot; ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>&quot;&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( cur ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>j = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>while ( (clazz = classes[j++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Remove *all* instances</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>while ( cur.indexOf( &quot; &quot; + clazz + &quot; &quot; ) &gt;= 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>cur = cur.replace( &quot; &quot; + clazz + &quot; &quot;, &quot; &quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// only assign if different to avoid unneeded rendering.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>finalValue = value ? jQuery.trim( cur ) : &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( elem.className !== finalValue ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.className = finalValue;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>toggleClass: function( value, stateVal ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var type = typeof value;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof stateVal === &quot;boolean&quot; &amp;&amp; type === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return stateVal ? this.addClass( value ) : this.removeClass( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( value ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.each(function( i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( type === &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// toggle individual class names</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var className,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>self = jQuery( this ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>classNames = value.match( rnotwhite ) || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( (className = classNames[ i++ ]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// check each className given, space separated list</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( self.hasClass( className ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>self.removeClass( className );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>self.addClass( className );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Toggle whole class name</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( type === strundefined || type === &quot;boolean&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( this.className ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// store className if set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>data_priv.set( this, &quot;__className__&quot;, this.className );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If the element has a class name or if we're passed &quot;false&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// then remove the whole classname (if there was one, the above saved it).</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Otherwise bring back whatever was previously saved (if anything),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// falling back to the empty string if nothing was stored.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.className = this.className || value === false ? &quot;&quot; : data_priv.get( this, &quot;__className__&quot; ) || &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hasClass: function( selector ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var className = &quot; &quot; + selector + &quot; &quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>l = this.length;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( ; i &lt; l; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this[i].nodeType === 1 &amp;&amp; (&quot; &quot; + this[i].className + &quot; &quot;).replace(rclass, &quot; &quot;).indexOf( className ) &gt;= 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var rreturn = /\r/g;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>val: function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var hooks, ret, isFunction,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem = this[0];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !arguments.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( hooks &amp;&amp; &quot;get&quot; in hooks &amp;&amp; (ret = hooks.get( elem, &quot;value&quot; )) !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>ret = elem.value;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return typeof ret === &quot;string&quot; ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// handle most common string cases</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>ret.replace(rreturn, &quot;&quot;) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// handle cases where value is null/undef or number</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>ret == null ? &quot;&quot; : ret;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>isFunction = jQuery.isFunction( value );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function( i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var val;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this.nodeType !== 1 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( isFunction ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val = value.call( this, i, jQuery( this ).val() );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val = value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Treat null/undefined as &quot;&quot;; convert numbers to string</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( val == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val = &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( typeof val === &quot;number&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val += &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( jQuery.isArray( val ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>val = jQuery.map( val, function( value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return value == null ? &quot;&quot; : value + &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If set returns undefined, fall back to normal setting</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !hooks || !(&quot;set&quot; in hooks) || hooks.set( this, val, &quot;value&quot; ) === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.value = val;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>valHooks: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>option: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>get: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var val = jQuery.find.attr( elem, &quot;value&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return val != null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>val :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Support: IE10-11+</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// option.text throws exceptions (#14686, #14858)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.trim( jQuery.text( elem ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>select: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>get: function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var value, option,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>options = elem.options,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>index = elem.selectedIndex,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>one = elem.type === &quot;select-one&quot; || index &lt; 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>values = one ? null : [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>max = one ? index + 1 : options.length,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i = index &lt; 0 ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>max :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>one ? index : 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Loop through all the selected options</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( ; i &lt; max; i++ ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>option = options[ i ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// IE6-9 doesn't update selected after form reset (#2551)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( ( option.selected || i === index ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Don't return options that are disabled or in a disabled optgroup</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>( support.optDisabled ? !option.disabled : option.getAttribute( &quot;disabled&quot; ) === null ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, &quot;optgroup&quot; ) ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Get the specific value for the option</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>value = jQuery( option ).val();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// We don't need an array for one selects</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( one ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>return value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// Multi-Selects return an array</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>values.push( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return values;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>set: function( elem, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var optionSet, option,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>options = elem.options,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>values = jQuery.makeArray( value ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>i = options.length;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( i-- ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>option = options[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( (option.selected = jQuery.inArray( option.value, values ) &gt;= 0) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>optionSet = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// force browsers to behave consistently when non-matching value is set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !optionSet ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem.selectedIndex = -1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return values;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Radios and checkboxes getter/setter</div>
 
<div>jQuery.each([ &quot;radio&quot;, &quot;checkbox&quot; ], function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.valHooks[ this ] = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>set: function( elem, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.isArray( value ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) &gt;= 0 );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !support.checkOn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.valHooks[ this ].get = function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Support: Webkit</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// &quot;&quot; is returned instead of &quot;on&quot; if a value isn't specified</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elem.getAttribute(&quot;value&quot;) === null ? &quot;on&quot; : elem.value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Return jQuery for attributes-only inclusion</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.each( (&quot;blur focus focusin focusout load resize scroll unload click dblclick &quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave &quot; +</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>&quot;change select submit keydown keypress keyup error contextmenu&quot;).split(&quot; &quot;), function( i, name ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Handle event binding</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fn[ name ] = function( data, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return arguments.length &gt; 0 ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.on( name, null, data, fn ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>this.trigger( name );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>hover: function( fnOver, fnOut ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>bind: function( types, data, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.on( types, null, data, fn );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>unbind: function( types, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.off( types, null, fn );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>delegate: function( selector, types, data, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.on( types, selector, data, fn );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>undelegate: function( selector, types, fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// ( namespace ) or ( selector, types [, fn] )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return arguments.length === 1 ? this.off( selector, &quot;**&quot; ) : this.off( types, selector || &quot;**&quot;, fn );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var nonce = jQuery.now();</div>
 
<div> 
  <br />
 </div>
 
<div>var rquery = (/\?/);</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Support: Android 2.3</div>
 
<div>// Workaround failure to string-cast null input</div>
 
<div>jQuery.parseJSON = function( data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return JSON.parse( data + &quot;&quot; );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Cross-browser xml parsing</div>
 
<div>jQuery.parseXML = function( data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var xml, tmp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !data || typeof data !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: IE9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>tmp = new DOMParser();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>xml = tmp.parseFromString( data, &quot;text/xml&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} catch ( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>xml = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !xml || xml.getElementsByTagName( &quot;parsererror&quot; ).length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.error( &quot;Invalid XML: &quot; + data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return xml;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Document location</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxLocParts,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxLocation,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rhash = /#.*$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rts = /([?&amp;])_=[^&amp;]*/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// #7653, #8125, #8152: local protocol detection</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rnoContent = /^(?:GET|HEAD)$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rprotocol = /^\/\//,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* Prefilters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 2) These are called:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> *    - BEFORE asking for a transport</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> *    - AFTER param serialization (s.data is a string if s.processData is true)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 3) key is the dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 4) the catchall symbol &quot;*&quot; can be used</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 5) execution will start with transport dataType and THEN continue down to &quot;*&quot; if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> */</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>prefilters = {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>/* Transports bindings</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 1) key is the dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 2) the catchall symbol &quot;*&quot; can be used</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> * 3) selection will start with transport dataType and THEN go to &quot;*&quot; if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span> */</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>transports = {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>allTypes = &quot;*/&quot;.concat(&quot;*&quot;);</div>
 
<div> 
  <br />
 </div>
 
<div>// #8138, IE may throw an exception when accessing</div>
 
<div>// a field from window.location if document.domain has been set</div>
 
<div>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxLocation = location.href;</div>
 
<div>} catch( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Use the href attribute of an A element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// since IE will modify it given document.location</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxLocation = document.createElement( &quot;a&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxLocation.href = &quot;&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxLocation = ajaxLocation.href;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Segment location into parts</div>
 
<div>ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];</div>
 
<div> 
  <br />
 </div>
 
<div>// Base &quot;constructor&quot; for jQuery.ajaxPrefilter and jQuery.ajaxTransport</div>
 
<div>function addToPrefiltersOrTransports( structure ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// dataTypeExpression is optional and defaults to &quot;*&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return function( dataTypeExpression, func ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof dataTypeExpression !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>func = dataTypeExpression;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>dataTypeExpression = &quot;*&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var dataType,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( func ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// For each dataType in the dataTypeExpression</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( (dataType = dataTypes[i++]) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Prepend if requested</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( dataType[0] === &quot;+&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>dataType = dataType.slice( 1 ) || &quot;*&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>(structure[ dataType ] = structure[ dataType ] || []).unshift( func );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Otherwise append</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>(structure[ dataType ] = structure[ dataType ] || []).push( func );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Base inspection function for prefilters and transports</div>
 
<div>function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var inspected = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>seekingTransport = ( structure === transports );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>function inspect( dataType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var selected;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>inspected[ dataType ] = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( typeof dataTypeOrTransport === &quot;string&quot; &amp;&amp; !seekingTransport &amp;&amp; !inspected[ dataTypeOrTransport ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>options.dataTypes.unshift( dataTypeOrTransport );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>inspect( dataTypeOrTransport );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( seekingTransport ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return !( selected = dataTypeOrTransport );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return selected;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return inspect( options.dataTypes[ 0 ] ) || !inspected[ &quot;*&quot; ] &amp;&amp; inspect( &quot;*&quot; );</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// A special extend for ajax options</div>
 
<div>// that takes &quot;flat&quot; options (not to be deep extended)</div>
 
<div>// Fixes #9887</div>
 
<div>function ajaxExtend( target, src ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var key, deep,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>flatOptions = jQuery.ajaxSettings.flatOptions || {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>for ( key in src ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( src[ key ] !== undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( deep ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.extend( true, target, deep );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return target;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/* Handles responses to an ajax request:</div>
 
<div> * - finds the right dataType (mediates between content-type and expected dataType)</div>
 
<div> * - returns the corresponding response</div>
 
<div> */</div>
 
<div>function ajaxHandleResponses( s, jqXHR, responses ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var ct, type, finalDataType, firstDataType,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>contents = s.contents,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dataTypes = s.dataTypes;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Remove auto dataType and get content-type in the process</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>while ( dataTypes[ 0 ] === &quot;*&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dataTypes.shift();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( ct === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ct = s.mimeType || jqXHR.getResponseHeader(&quot;Content-Type&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check if we're dealing with a known content-type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( ct ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( type in contents ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( contents[ type ] &amp;&amp; contents[ type ].test( ct ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>dataTypes.unshift( type );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Check to see if we have a response for the expected dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( dataTypes[ 0 ] in responses ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>finalDataType = dataTypes[ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Try convertible dataTypes</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( type in responses ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !dataTypes[ 0 ] || s.converters[ type + &quot; &quot; + dataTypes[0] ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>finalDataType = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !firstDataType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>firstDataType = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Or just use first one</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>finalDataType = finalDataType || firstDataType;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If we found a dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// We add the dataType to the list if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// and return the corresponding response</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( finalDataType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( finalDataType !== dataTypes[ 0 ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>dataTypes.unshift( finalDataType );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return responses[ finalDataType ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>/* Chain conversions given the request and the original response</div>
 
<div> * Also sets the responseXXX fields on the jqXHR instance</div>
 
<div> */</div>
 
<div>function ajaxConvert( s, response, jqXHR, isSuccess ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var conv2, current, conv, tmp, prev,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>converters = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Work with a copy of dataTypes in case we need to modify it for conversion</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dataTypes = s.dataTypes.slice();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Create converters map with lowercased keys</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( dataTypes[ 1 ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( conv in s.converters ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>converters[ conv.toLowerCase() ] = s.converters[ conv ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>current = dataTypes.shift();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Convert to each sequential dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>while ( current ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( s.responseFields[ current ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR[ s.responseFields[ current ] ] = response;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Apply the dataFilter if provided</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !prev &amp;&amp; isSuccess &amp;&amp; s.dataFilter ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>response = s.dataFilter( response, s.dataType );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>prev = current;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>current = dataTypes.shift();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( current ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// There's only work to do if current dataType is non-auto</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( current === &quot;*&quot; ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>current = prev;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Convert response if prev dataType is non-auto and differs from current</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else if ( prev !== &quot;*&quot; &amp;&amp; prev !== current ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Seek a direct converter</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>conv = converters[ prev + &quot; &quot; + current ] || converters[ &quot;* &quot; + current ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If none found, seek a pair</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !conv ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>for ( conv2 in converters ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>// If conv2 outputs current</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>tmp = conv2.split( &quot; &quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( tmp[ 1 ] === current ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// If prev can be converted to accepted input</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>conv = converters[ prev + &quot; &quot; + tmp[ 0 ] ] ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>converters[ &quot;* &quot; + tmp[ 0 ] ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( conv ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// Condense equivalence converters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>if ( conv === true ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>conv = converters[ conv2 ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// Otherwise, insert the intermediate dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>} else if ( converters[ conv2 ] !== true ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>current = tmp[ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>dataTypes.unshift( tmp[ 1 ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>break;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Apply converter (if not an equivalence)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( conv !== true ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Unless errors are allowed to bubble, catch and return them</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( conv &amp;&amp; s[ &quot;throws&quot; ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>response = conv( response );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>response = conv( response );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>} catch ( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>return { state: &quot;parsererror&quot;, error: conv ? e : &quot;No conversion from &quot; + prev + &quot; to &quot; + current };</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return { state: &quot;success&quot;, data: response };</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.extend({</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Counter for holding the number of active queries</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>active: 0,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Last-Modified header cache for next request</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>lastModified: {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>etag: {},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxSettings: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>url: ajaxLocation,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>type: &quot;GET&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>global: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>processData: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>async: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>contentType: &quot;application/x-www-form-urlencoded; charset=UTF-8&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>/*</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>timeout: 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>data: null,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dataType: null,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>username: null,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>password: null,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cache: null,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>throws: false,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>traditional: false,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>headers: {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>*/</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>accepts: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;*&quot;: allTypes,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>text: &quot;text/plain&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>html: &quot;text/html&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>xml: &quot;application/xml, text/xml&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>json: &quot;application/json, text/javascript&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>contents: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>xml: /xml/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>html: /html/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>json: /json/</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>responseFields: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>xml: &quot;responseXML&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>text: &quot;responseText&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>json: &quot;responseJSON&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Data converters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Keys separate source (or catchall &quot;*&quot;) and destination types with a single space</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>converters: {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Convert anything to text</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;* text&quot;: String,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Text to html (true = no transformation)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;text html&quot;: true,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Evaluate text as a json expression</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;text json&quot;: jQuery.parseJSON,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Parse text as xml</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;text xml&quot;: jQuery.parseXML</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// For options that shouldn't be deep extended:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// you can add your own custom options here if</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// and when you create one that shouldn't be</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// deep extended (see ajaxExtend)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>flatOptions: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>url: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>context: true</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Creates a full fledged settings object into target</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// with both ajaxSettings and settings fields.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If target is omitted, writes into ajaxSettings.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxSetup: function( target, settings ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return settings ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Building a settings object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Extending ajaxSettings</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>ajaxExtend( jQuery.ajaxSettings, target );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajaxTransport: addToPrefiltersOrTransports( transports ),</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Main method</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>ajax: function( url, options ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If url is an object, simulate pre-1.5 signature</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof url === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>options = url;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>url = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Force options to be an object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>options = options || {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var transport,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// URL without anti-cache param</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>cacheURL,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Response headers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>responseHeadersString,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>responseHeaders,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// timeout handle</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>timeoutTimer,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Cross-domain detection vars</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>parts,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// To know if global events are to be dispatched</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>fireGlobals,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Loop variable</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Create the final options object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s = jQuery.ajaxSetup( {}, options ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Callbacks context</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>callbackContext = s.context || s,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Context for global events is callbackContext if it is a DOM node or jQuery collection</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>globalEventContext = s.context &amp;&amp; ( callbackContext.nodeType || callbackContext.jquery ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( callbackContext ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.event,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Deferreds</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>deferred = jQuery.Deferred(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>completeDeferred = jQuery.Callbacks(&quot;once memory&quot;),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Status-dependent callbacks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>statusCode = s.statusCode || {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Headers (they are sent all at once)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>requestHeaders = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>requestHeadersNames = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// The jqXHR state</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>state = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Default abort message</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>strAbort = &quot;canceled&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Fake xhr</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>readyState: 0,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Builds headers hashtable if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>getResponseHeader: function( key ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var match;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( state === 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( !responseHeaders ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>responseHeaders = {};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>while ( (match = rheaders.exec( responseHeadersString )) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>match = responseHeaders[ key.toLowerCase() ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return match == null ? null : match;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Raw string</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>getAllResponseHeaders: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return state === 2 ? responseHeadersString : null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Caches the header</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>setRequestHeader: function( name, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var lname = name.toLowerCase();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( !state ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>requestHeaders[ name ] = value;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Overrides response content-type header</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>overrideMimeType: function( type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( !state ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>s.mimeType = type;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Status-dependent callbacks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>statusCode: function( map ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var code;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( map ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( state &lt; 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>for ( code in map ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>// Lazy-add the new callback in a way that preserves old ones</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>statusCode[ code ] = [ statusCode[ code ], map[ code ] ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>// Execute the appropriate callbacks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>jqXHR.always( map[ jqXHR.status ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Cancel the request</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>abort: function( statusText ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>var finalText = statusText || strAbort;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( transport ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>transport.abort( finalText );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>done( 0, finalText );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Attach deferreds</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>deferred.promise( jqXHR ).complete = completeDeferred.add;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jqXHR.success = jqXHR.done;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jqXHR.error = jqXHR.fail;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Remove hash character (#7531: and string promotion)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Add protocol if not provided (prefilters might expect it)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Handle falsy url in the settings object (#10093: consistency with old signature)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// We also use the url parameter if available</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.url = ( ( url || s.url || ajaxLocation ) + &quot;&quot; ).replace( rhash, &quot;&quot; )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>.replace( rprotocol, ajaxLocParts[ 1 ] + &quot;//&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Alias method option to type as per ticket #12004</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.type = options.method || options.type || s.method || s.type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Extract dataTypes list</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.dataTypes = jQuery.trim( s.dataType || &quot;*&quot; ).toLowerCase().match( rnotwhite ) || [ &quot;&quot; ];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// A cross-domain request is in order when we have a protocol:host:port mismatch</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( s.crossDomain == null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>parts = rurl.exec( s.url.toLowerCase() );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s.crossDomain = !!( parts &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>( parts[ 3 ] || ( parts[ 1 ] === &quot;http:&quot; ? &quot;80&quot; : &quot;443&quot; ) ) !==</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === &quot;http:&quot; ? &quot;80&quot; : &quot;443&quot; ) ) )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Convert data if not already a string</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( s.data &amp;&amp; s.processData &amp;&amp; typeof s.data !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s.data = jQuery.param( s.data, s.traditional );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Apply prefilters</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If request was aborted inside a prefilter, stop there</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( state === 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jqXHR;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// We can fire global events as of now if asked to</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>fireGlobals = jQuery.event &amp;&amp; s.global;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Watch for a new set of requests</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( fireGlobals &amp;&amp; jQuery.active++ === 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.event.trigger(&quot;ajaxStart&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Uppercase the type</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.type = s.type.toUpperCase();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Determine if request has content</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.hasContent = !rnoContent.test( s.type );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Save the URL in case we're toying with the If-Modified-Since</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// and/or If-None-Match header later on</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>cacheURL = s.url;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// More options handling for requests with no content</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !s.hasContent ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If data is available, append data to url</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( s.data ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>cacheURL = ( s.url += ( rquery.test( cacheURL ) ? &quot;&amp;&quot; : &quot;?&quot; ) + s.data );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// #9682: remove data so that it's not used in an eventual retry</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>delete s.data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add anti-cache in url if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( s.cache === false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>s.url = rts.test( cacheURL ) ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// If there is already a '_' parameter, set its value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>cacheURL.replace( rts, &quot;$1_=&quot; + nonce++ ) :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Otherwise add one to the end</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>cacheURL + ( rquery.test( cacheURL ) ? &quot;&amp;&quot; : &quot;?&quot; ) + &quot;_=&quot; + nonce++;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( s.ifModified ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.lastModified[ cacheURL ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jqXHR.setRequestHeader( &quot;If-Modified-Since&quot;, jQuery.lastModified[ cacheURL ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( jQuery.etag[ cacheURL ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jqXHR.setRequestHeader( &quot;If-None-Match&quot;, jQuery.etag[ cacheURL ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Set the correct header, if data is being sent</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( s.data &amp;&amp; s.hasContent &amp;&amp; s.contentType !== false || options.contentType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR.setRequestHeader( &quot;Content-Type&quot;, s.contentType );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Set the Accepts header for the server, depending on the dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jqXHR.setRequestHeader(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;Accept&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s.dataTypes[ 0 ] &amp;&amp; s.accepts[ s.dataTypes[0] ] ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== &quot;*&quot; ? &quot;, &quot; + allTypes + &quot;; q=0.01&quot; : &quot;&quot; ) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>s.accepts[ &quot;*&quot; ]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Check for headers option</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( i in s.headers ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR.setRequestHeader( i, s.headers[ i ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Allow custom headers/mimetypes and early abort</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( s.beforeSend &amp;&amp; ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Abort if not done already and return</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return jqXHR.abort();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// aborting is no longer a cancellation</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>strAbort = &quot;abort&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Install callbacks on deferreds</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( i in { success: 1, error: 1, complete: 1 } ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR[ i ]( s[ i ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Get transport</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If no transport, we auto-abort</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !transport ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>done( -1, &quot;No Transport&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR.readyState = 1;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Send global event</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( fireGlobals ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>globalEventContext.trigger( &quot;ajaxSend&quot;, [ jqXHR, s ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Timeout</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( s.async &amp;&amp; s.timeout &gt; 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>timeoutTimer = setTimeout(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jqXHR.abort(&quot;timeout&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}, s.timeout );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>state = 1;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>transport.send( requestHeaders, done );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} catch ( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Propagate exception as error if not done</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( state &lt; 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>done( -1, e );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Simply rethrow otherwise</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>throw e;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Callback for when everything is done</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function done( status, nativeStatusText, responses, headers ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var isSuccess, success, error, response, modified,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>statusText = nativeStatusText;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Called once</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( state === 2 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// State is &quot;done&quot; now</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>state = 2;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Clear timeout if it exists</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( timeoutTimer ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>clearTimeout( timeoutTimer );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Dereference transport for early garbage collection</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// (no matter how long the jqXHR object will be used)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>transport = undefined;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Cache response headers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>responseHeadersString = headers || &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Set readyState</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR.readyState = status &gt; 0 ? 4 : 0;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Determine if successful</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>isSuccess = status &gt;= 200 &amp;&amp; status &lt; 300 || status === 304;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Get response data</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( responses ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>response = ajaxHandleResponses( s, jqXHR, responses );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Convert no matter what (that way responseXXX fields are always set)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>response = ajaxConvert( s, response, jqXHR, isSuccess );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If successful, handle type chaining</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( isSuccess ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( s.ifModified ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>modified = jqXHR.getResponseHeader(&quot;Last-Modified&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( modified ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>jQuery.lastModified[ cacheURL ] = modified;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>modified = jqXHR.getResponseHeader(&quot;etag&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( modified ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>jQuery.etag[ cacheURL ] = modified;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// if no content</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( status === 204 || s.type === &quot;HEAD&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>statusText = &quot;nocontent&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// if not modified</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else if ( status === 304 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>statusText = &quot;notmodified&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If we have data, let's convert it</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>statusText = response.state;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>success = response.data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>error = response.error;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>isSuccess = !error;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// We extract error from statusText</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// then normalize statusText and status for non-aborts</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>error = statusText;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( status || !statusText ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>statusText = &quot;error&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( status &lt; 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>status = 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Set data for the fake xhr object</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR.status = status;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR.statusText = ( nativeStatusText || statusText ) + &quot;&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Success/Error</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( isSuccess ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Status-dependent callbacks</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jqXHR.statusCode( statusCode );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>statusCode = undefined;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( fireGlobals ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>globalEventContext.trigger( isSuccess ? &quot;ajaxSuccess&quot; : &quot;ajaxError&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>[ jqXHR, s, isSuccess ? success : error ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Complete</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( fireGlobals ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>globalEventContext.trigger( &quot;ajaxComplete&quot;, [ jqXHR, s ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Handle the global AJAX counter</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !( --jQuery.active ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.event.trigger(&quot;ajaxStop&quot;);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jqXHR;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>getJSON: function( url, data, callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.get( url, data, callback, &quot;json&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>getScript: function( url, callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.get( url, undefined, callback, &quot;script&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.each( [ &quot;get&quot;, &quot;post&quot; ], function( i, method ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery[ method ] = function( url, data, callback, type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// shift arguments if data argument was omitted</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( data ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type = type || callback;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>callback = data;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.ajax({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>url: url,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type: method,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>dataType: type,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data: data,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>success: callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery._evalUrl = function( url ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return jQuery.ajax({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>url: url,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>type: &quot;GET&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>dataType: &quot;script&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>async: false,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>global: false,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;throws&quot;: true</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>wrapAll: function( html ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var wrap;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( html ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.each(function( i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).wrapAll( html.call(this, i) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( this[ 0 ] ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// The elements to wrap the target around</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( this[ 0 ].parentNode ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>wrap.insertBefore( this[ 0 ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>wrap.map(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var elem = this;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>while ( elem.firstElementChild ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>elem = elem.firstElementChild;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}).append( this );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>wrapInner: function( html ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( html ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.each(function( i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).wrapInner( html.call(this, i) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var self = jQuery( this ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>contents = self.contents();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( contents.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>contents.wrapAll( html );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>self.append( html );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>wrap: function( html ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var isFunction = jQuery.isFunction( html );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.each(function( i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>unwrap: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.parent().each(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !jQuery.nodeName( this, &quot;body&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery( this ).replaceWith( this.childNodes );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}).end();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.expr.filters.hidden = function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Support: Opera &lt;= 12.12</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Opera reports offsetWidths and offsetHeights less than zero on some elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return elem.offsetWidth &lt;= 0 &amp;&amp; elem.offsetHeight &lt;= 0;</div>
 
<div>};</div>
 
<div>jQuery.expr.filters.visible = function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return !jQuery.expr.filters.hidden( elem );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var r20 = /%20/g,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rbracket = /\[\]$/,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rCRLF = /\r?\n/g,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rsubmittable = /^(?:input|select|textarea|keygen)/i;</div>
 
<div> 
  <br />
 </div>
 
<div>function buildParams( prefix, obj, traditional, add ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var name;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( jQuery.isArray( obj ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Serialize array item.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.each( obj, function( i, v ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( traditional || rbracket.test( prefix ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Treat each array item as a scalar.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>add( prefix, v );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Item is non-scalar (array or object), encode its numeric index.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>buildParams( prefix + &quot;[&quot; + ( typeof v === &quot;object&quot; ? i : &quot;&quot; ) + &quot;]&quot;, v, traditional, add );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else if ( !traditional &amp;&amp; jQuery.type( obj ) === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Serialize object item.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( name in obj ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>buildParams( prefix + &quot;[&quot; + name + &quot;]&quot;, obj[ name ], traditional, add );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Serialize scalar item.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>add( prefix, obj );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>// Serialize an array of form elements or a set of</div>
 
<div>// key/values into a query string</div>
 
<div>jQuery.param = function( a, traditional ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var prefix,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>add = function( key, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// If value is a function, invoke it and return its value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>value = jQuery.isFunction( value ) ? value() : ( value == null ? &quot;&quot; : value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s[ s.length ] = encodeURIComponent( key ) + &quot;=&quot; + encodeURIComponent( value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Set traditional to true for jQuery &lt;= 1.3.2 behavior.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( traditional === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>traditional = jQuery.ajaxSettings &amp;&amp; jQuery.ajaxSettings.traditional;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If an array was passed in, assume that it is an array of form elements.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( jQuery.isArray( a ) || ( a.jquery &amp;&amp; !jQuery.isPlainObject( a ) ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Serialize the form elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.each( a, function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>add( this.name, this.value );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If traditional, encode the &quot;old&quot; way (the way 1.3.2 or older</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// did it), otherwise encode params recursively.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( prefix in a ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>buildParams( prefix, a[ prefix ], traditional, add );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Return the resulting serialization</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return s.join( &quot;&amp;&quot; ).replace( r20, &quot;+&quot; );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>serialize: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery.param( this.serializeArray() );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>serializeArray: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.map(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Can add propHook for &quot;elements&quot; to filter or add form elements</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var elements = jQuery.prop( this, &quot;elements&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return elements ? jQuery.makeArray( elements ) : this;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>})</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>.filter(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var type = this.type;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Use .is( &quot;:disabled&quot; ) so that fieldset[disabled] works</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return this.name &amp;&amp; !jQuery( this ).is( &quot;:disabled&quot; ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>rsubmittable.test( this.nodeName ) &amp;&amp; !rsubmitterTypes.test( type ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>( this.checked || !rcheckableType.test( type ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>})</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>.map(function( i, elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var val = jQuery( this ).val();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return val == null ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>null :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.isArray( val ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.map( val, function( val ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>return { name: elem.name, value: val.replace( rCRLF, &quot;\r\n&quot; ) };</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}) :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>{ name: elem.name, value: val.replace( rCRLF, &quot;\r\n&quot; ) };</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}).get();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.ajaxSettings.xhr = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return new XMLHttpRequest();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} catch( e ) {}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>var xhrId = 0,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>xhrCallbacks = {},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>xhrSuccessStatus = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// file protocol always yields status code 0, assume 200</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>0: 200,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Support: IE9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// #1450: sometimes IE returns 1223 when it should be 204</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>1223: 204</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>xhrSupported = jQuery.ajaxSettings.xhr();</div>
 
<div> 
  <br />
 </div>
 
<div>// Support: IE9</div>
 
<div>// Open requests must be manually aborted on unload (#5280)</div>
 
<div>if ( window.ActiveXObject ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery( window ).on( &quot;unload&quot;, function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>for ( var key in xhrCallbacks ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>xhrCallbacks[ key ]();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>support.cors = !!xhrSupported &amp;&amp; ( &quot;withCredentials&quot; in xhrSupported );</div>
 
<div>support.ajax = xhrSupported = !!xhrSupported;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.ajaxTransport(function( options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var callback;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Cross domain only allowed if supported through XMLHttpRequest</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( support.cors || xhrSupported &amp;&amp; !options.crossDomain ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>send: function( headers, complete ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var i,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>xhr = options.xhr(),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>id = ++xhrId;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>xhr.open( options.type, options.url, options.async, options.username, options.password );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Apply custom fields if provided</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( options.xhrFields ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>for ( i in options.xhrFields ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>xhr[ i ] = options.xhrFields[ i ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Override mime type if needed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( options.mimeType &amp;&amp; xhr.overrideMimeType ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>xhr.overrideMimeType( options.mimeType );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// X-Requested-With header</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// For cross-domain requests, seeing as conditions for a preflight are</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// akin to a jigsaw puzzle, we simply never set it to be sure.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// (it can always be set on a per-request basis or even using ajaxSetup)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// For same-domain requests, won't change header if already provided.</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( !options.crossDomain &amp;&amp; !headers[&quot;X-Requested-With&quot;] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>headers[&quot;X-Requested-With&quot;] = &quot;XMLHttpRequest&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Set headers</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>for ( i in headers ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>xhr.setRequestHeader( i, headers[ i ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>callback = function( type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>delete xhrCallbacks[ id ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>callback = xhr.onload = xhr.onerror = null;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>if ( type === &quot;abort&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>xhr.abort();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>} else if ( type === &quot;error&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>complete(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>// file: protocol always yields status 0; see #8605, #14207</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>xhr.status,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>xhr.statusText</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>complete(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>xhrSuccessStatus[ xhr.status ] || xhr.status,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>xhr.statusText,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>// Support: IE9</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>// Accessing binary-data responseText throws an exception</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>// (#11426)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>typeof xhr.responseText === &quot;string&quot; ? {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">										</span>text: xhr.responseText</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>} : undefined,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">									</span>xhr.getAllResponseHeaders()</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">								</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Listen to events</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>xhr.onload = callback();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>xhr.onerror = callback(&quot;error&quot;);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Create the abort callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>callback = xhrCallbacks[ id ] = callback(&quot;abort&quot;);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>try {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Do send the request (this may raise an exception)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>xhr.send( options.hasContent &amp;&amp; options.data || null );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>} catch ( e ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// #14683: Only rethrow if this hasn't been notified as an error yet</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>if ( callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>throw e;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>abort: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>callback();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Install script dataType</div>
 
<div>jQuery.ajaxSetup({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>accepts: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>script: &quot;text/javascript, application/javascript, application/ecmascript, application/x-ecmascript&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>contents: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>script: /(?:java|ecma)script/</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>converters: {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>&quot;text script&quot;: function( text ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>jQuery.globalEval( text );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return text;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Handle cache's special case and crossDomain</div>
 
<div>jQuery.ajaxPrefilter( &quot;script&quot;, function( s ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( s.cache === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.cache = false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( s.crossDomain ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.type = &quot;GET&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Bind script tag hack transport</div>
 
<div>jQuery.ajaxTransport( &quot;script&quot;, function( s ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// This transport only deals with cross domain requests</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( s.crossDomain ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var script, callback;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>send: function( _, complete ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>script = jQuery(&quot;&lt;script&gt;&quot;).prop({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>async: true,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>charset: s.scriptCharset,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>src: s.url</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}).on(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>&quot;load error&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>callback = function( evt ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>script.remove();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>callback = null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>if ( evt ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">							</span>complete( evt.type === &quot;error&quot; ? 404 : 200, evt.type );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>document.head.appendChild( script[ 0 ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>},</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>abort: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>callback();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var oldCallbacks = [],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>rjsonp = /(=)\?(?=&amp;|$)|\?\?/;</div>
 
<div> 
  <br />
 </div>
 
<div>// Default jsonp settings</div>
 
<div>jQuery.ajaxSetup({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jsonp: &quot;callback&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jsonpCallback: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var callback = oldCallbacks.pop() || ( jQuery.expando + &quot;_&quot; + ( nonce++ ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>this[ callback ] = true;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return callback;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Detect, normalize options and install callbacks for jsonp requests</div>
 
<div>jQuery.ajaxPrefilter( &quot;json jsonp&quot;, function( s, originalSettings, jqXHR ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var callbackName, overwritten, responseContainer,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jsonProp = s.jsonp !== false &amp;&amp; ( rjsonp.test( s.url ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>&quot;url&quot; :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>typeof s.data === &quot;string&quot; &amp;&amp; !( s.contentType || &quot;&quot; ).indexOf(&quot;application/x-www-form-urlencoded&quot;) &amp;&amp; rjsonp.test( s.data ) &amp;&amp; &quot;data&quot;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Handle iff the expected data type is &quot;jsonp&quot; or we have a parameter to set</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( jsonProp || s.dataTypes[ 0 ] === &quot;jsonp&quot; ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Get callback name, remembering preexisting value associated with it</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s.jsonpCallback() :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s.jsonpCallback;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Insert callback into url or form data</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jsonProp ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, &quot;$1&quot; + callbackName );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else if ( s.jsonp !== false ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>s.url += ( rquery.test( s.url ) ? &quot;&amp;&quot; : &quot;?&quot; ) + s.jsonp + &quot;=&quot; + callbackName;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Use data converter to retrieve json after script execution</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.converters[&quot;script json&quot;] = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !responseContainer ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery.error( callbackName + &quot; was not called&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return responseContainer[ 0 ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// force json dataType</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>s.dataTypes[ 0 ] = &quot;json&quot;;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Install callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>overwritten = window[ callbackName ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>window[ callbackName ] = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>responseContainer = arguments;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Clean-up function (fires after converters)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jqXHR.always(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Restore preexisting value</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>window[ callbackName ] = overwritten;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Save back as free</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( s[ callbackName ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// make sure that re-using the options doesn't screw things around</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>s.jsonpCallback = originalSettings.jsonpCallback;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// save the callback name for future use</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>oldCallbacks.push( callbackName );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Call if it was a function and we have a response</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( responseContainer &amp;&amp; jQuery.isFunction( overwritten ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>overwritten( responseContainer[ 0 ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>responseContainer = overwritten = undefined;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Delegate to script</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return &quot;script&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// data: string of html</div>
 
<div>// context (optional): If specified, the fragment will be created in this context, defaults to document</div>
 
<div>// keepScripts (optional): If true, will include scripts passed in the html string</div>
 
<div>jQuery.parseHTML = function( data, context, keepScripts ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( !data || typeof data !== &quot;string&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return null;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( typeof context === &quot;boolean&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>keepScripts = context;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>context = false;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>context = context || document;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var parsed = rsingleTag.exec( data ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>scripts = !keepScripts &amp;&amp; [];</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Single tag</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( parsed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return [ context.createElement( parsed[1] ) ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>parsed = jQuery.buildFragment( [ data ], context, scripts );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( scripts &amp;&amp; scripts.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery( scripts ).remove();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return jQuery.merge( [], parsed.childNodes );</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Keep a copy of the old load method</div>
 
<div>var _load = jQuery.fn.load;</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Load a url into a page</div>
 
<div> */</div>
 
<div>jQuery.fn.load = function( url, params, callback ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( typeof url !== &quot;string&quot; &amp;&amp; _load ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return _load.apply( this, arguments );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var selector, type, response,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>self = this,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>off = url.indexOf(&quot; &quot;);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( off &gt;= 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>selector = jQuery.trim( url.slice( off ) );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>url = url.slice( 0, off );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If it's a function</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( jQuery.isFunction( params ) ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// We assume that it's the callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>callback = params;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>params = undefined;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Otherwise, build a param string</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>} else if ( params &amp;&amp; typeof params === &quot;object&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>type = &quot;POST&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// If we have elements to modify, make the request</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( self.length &gt; 0 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.ajax({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>url: url,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// if &quot;type&quot; variable is undefined, then &quot;GET&quot; method will be used</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>type: type,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>dataType: &quot;html&quot;,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>data: params</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}).done(function( responseText ) {</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Save response for use in complete callback</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>response = arguments;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>self.html( selector ?</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// If a selector was specified, locate the right elements in a dummy div</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Exclude scripts to avoid IE 'Permission Denied' errors</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>jQuery(&quot;&lt;div&gt;&quot;).append( jQuery.parseHTML( responseText ) ).find( selector ) :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Otherwise use the full result</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>responseText );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}).complete( callback &amp;&amp; function( jqXHR, status ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return this;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Attach a bunch of functions for handling common AJAX events</div>
 
<div>jQuery.each( [ &quot;ajaxStart&quot;, &quot;ajaxStop&quot;, &quot;ajaxComplete&quot;, &quot;ajaxError&quot;, &quot;ajaxSuccess&quot;, &quot;ajaxSend&quot; ], function( i, type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fn[ type ] = function( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.on( type, fn );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.expr.filters.animated = function( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return jQuery.grep(jQuery.timers, function( fn ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return elem === fn.elem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}).length;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var docElem = window.document.documentElement;</div>
 
<div> 
  <br />
 </div>
 
<div>/**</div>
 
<div> * Gets a window from an element</div>
 
<div> */</div>
 
<div>function getWindow( elem ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 &amp;&amp; elem.defaultView;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.offset = {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>setOffset: function( elem, options, i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>position = jQuery.css( elem, &quot;position&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>curElem = jQuery( elem ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>props = {};</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Set position first, in-case top/left are set even on static elem</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( position === &quot;static&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem.style.position = &quot;relative&quot;;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>curOffset = curElem.offset();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>curCSSTop = jQuery.css( elem, &quot;top&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>curCSSLeft = jQuery.css( elem, &quot;left&quot; );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>calculatePosition = ( position === &quot;absolute&quot; || position === &quot;fixed&quot; ) &amp;&amp;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>( curCSSTop + curCSSLeft ).indexOf(&quot;auto&quot;) &gt; -1;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Need to be able to calculate position if either top or left is auto and position is either absolute or fixed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( calculatePosition ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>curPosition = curElem.position();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>curTop = curPosition.top;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>curLeft = curPosition.left;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>curTop = parseFloat( curCSSTop ) || 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>curLeft = parseFloat( curCSSLeft ) || 0;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.isFunction( options ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>options = options.call( elem, i, curOffset );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( options.top != null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>props.top = ( options.top - curOffset.top ) + curTop;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( options.left != null ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>props.left = ( options.left - curOffset.left ) + curLeft;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( &quot;using&quot; in options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>options.using.call( elem, props );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>curElem.css( props );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.extend({</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>offset: function( options ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( arguments.length ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return options === undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>this.each(function( i ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.offset.setOffset( this, options, i );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var docElem, win,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem = this[ 0 ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>box = { top: 0, left: 0 },</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>doc = elem &amp;&amp; elem.ownerDocument;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !doc ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>docElem = doc.documentElement;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Make sure it's not a disconnected DOM node</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !jQuery.contains( docElem, elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return box;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// If we don't have gBCR, just use 0,0 rather than error</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// BlackBerry 5, iOS 3 (original iPhone)</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( typeof elem.getBoundingClientRect !== strundefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>box = elem.getBoundingClientRect();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>win = getWindow( doc );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>top: box.top + win.pageYOffset - docElem.clientTop,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>left: box.left + win.pageXOffset - docElem.clientLeft</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>position: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( !this[ 0 ] ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>var offsetParent, offset,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>elem = this[ 0 ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>parentOffset = { top: 0, left: 0 };</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>if ( jQuery.css( elem, &quot;position&quot; ) === &quot;fixed&quot; ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// We assume that getBoundingClientRect is available when computed position is fixed</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>offset = elem.getBoundingClientRect();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Get *real* offsetParent</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>offsetParent = this.offsetParent();</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Get correct offsets</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>offset = this.offset();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( !jQuery.nodeName( offsetParent[ 0 ], &quot;html&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>parentOffset = offsetParent.offset();</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>// Add offsetParent borders</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>parentOffset.top += jQuery.css( offsetParent[ 0 ], &quot;borderTopWidth&quot;, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>parentOffset.left += jQuery.css( offsetParent[ 0 ], &quot;borderLeftWidth&quot;, true );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// Subtract parent offsets and element margins</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>top: offset.top - parentOffset.top - jQuery.css( elem, &quot;marginTop&quot;, true ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>left: offset.left - parentOffset.left - jQuery.css( elem, &quot;marginLeft&quot;, true )</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>},</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>offsetParent: function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return this.map(function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var offsetParent = this.offsetParent || docElem;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>while ( offsetParent &amp;&amp; ( !jQuery.nodeName( offsetParent, &quot;html&quot; ) &amp;&amp; jQuery.css( offsetParent, &quot;position&quot; ) === &quot;static&quot; ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>offsetParent = offsetParent.offsetParent;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return offsetParent || docElem;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>});</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Create scrollLeft and scrollTop methods</div>
 
<div>jQuery.each( { scrollLeft: &quot;pageXOffset&quot;, scrollTop: &quot;pageYOffset&quot; }, function( method, prop ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>var top = &quot;pageYOffset&quot; === prop;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.fn[ method ] = function( val ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return access( this, function( elem, method, val ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var win = getWindow( elem );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( val === undefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return win ? win[ prop ] : elem[ method ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( win ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>win.scrollTo(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>!top ? val : window.pageXOffset,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>top ? val : window.pageYOffset</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>);</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>} else {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>elem[ method ] = val;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}, method, val, arguments.length, null );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>};</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div>// Add the top/left cssHooks using jQuery.fn.position</div>
 
<div>// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084</div>
 
<div>// getComputedStyle returns percent when specified for top/left/bottom/right</div>
 
<div>// rather than make the css module depend on the offset module, we just check for it here</div>
 
<div>jQuery.each( [ &quot;top&quot;, &quot;left&quot; ], function( i, prop ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>function( elem, computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>if ( computed ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>computed = curCSS( elem, prop );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// if curCSS returns percentage, fallback to offset</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return rnumnonpx.test( computed ) ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery( elem ).position()[ prop ] + &quot;px&quot; :</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>computed;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>}</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>);</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods</div>
 
<div>jQuery.each( { Height: &quot;height&quot;, Width: &quot;width&quot; }, function( name, type ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>jQuery.each( { padding: &quot;inner&quot; + name, content: type, &quot;&quot;: &quot;outer&quot; + name }, function( defaultExtra, funcName ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>// margin is only for outerHeight, outerWidth</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>jQuery.fn[ funcName ] = function( margin, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>var chainable = arguments.length &amp;&amp; ( defaultExtra || typeof margin !== &quot;boolean&quot; ),</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>extra = defaultExtra || ( margin === true || value === true ? &quot;margin&quot; : &quot;border&quot; );</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>return access( this, function( elem, type, value ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>var doc;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( jQuery.isWindow( elem ) ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// isn't a whole lot we can do. See pull request at this URL for discussion:</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// https://github.com/jquery/jquery/pull/764</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return elem.document.documentElement[ &quot;client&quot; + name ];</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>// Get document width or height</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>if ( elem.nodeType === 9 ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>doc = elem.documentElement;</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// whichever is greatest</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>return Math.max(</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.body[ &quot;scroll&quot; + name ], doc[ &quot;scroll&quot; + name ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>elem.body[ &quot;offset&quot; + name ], doc[ &quot;offset&quot; + name ],</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">						</span>doc[ &quot;client&quot; + name ]</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>);</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">				</span>return value === undefined ?</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Get width or height on the element, requesting but not forcing parseFloat</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.css( elem, type, extra ) :</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>// Set width or height on the element</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">					</span>jQuery.style( elem, type, value, extra );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">			</span>}, type, chainable ? margin : undefined, chainable, null );</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>};</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>});</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// The number of elements contained in the matched element set</div>
 
<div>jQuery.fn.size = function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return this.length;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.fn.andSelf = jQuery.fn.addBack;</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>// Register as a named AMD module, since jQuery can be concatenated with other</div>
 
<div>// files that may use define, but not via a proper concatenation script that</div>
 
<div>// understands anonymous AMD modules. A named AMD is safest and most robust</div>
 
<div>// way to register. Lowercase jquery is used because AMD module names are</div>
 
<div>// derived from file names, and jQuery is normally delivered in a lowercase</div>
 
<div>// file name. Do this after creating the global so that if an AMD module wants</div>
 
<div>// to call noConflict to hide this version of jQuery, it will work.</div>
 
<div> 
  <br />
 </div>
 
<div>// Note that for maximum portability, libraries that are not jQuery should</div>
 
<div>// declare themselves as anonymous modules, and avoid setting a global if an</div>
 
<div>// AMD loader is present. jQuery is a special case. For more information, see</div>
 
<div>// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon</div>
 
<div> 
  <br />
 </div>
 
<div>if ( typeof define === &quot;function&quot; &amp;&amp; define.amd ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>define( &quot;jquery&quot;, [], function() {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>return jQuery;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>});</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>var</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Map over jQuery in case of overwrite</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>_jQuery = window.jQuery,</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>// Map over the $ in case of overwrite</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>_$ = window.$;</div>
 
<div> 
  <br />
 </div>
 
<div>jQuery.noConflict = function( deep ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( window.$ === jQuery ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>window.$ = _$;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>if ( deep &amp;&amp; window.jQuery === jQuery ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">		</span>window.jQuery = _jQuery;</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>}</div>
 
<div> 
  <br />
 </div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>return jQuery;</div>
 
<div>};</div>
 
<div> 
  <br />
 </div>
 
<div>// Expose jQuery and $ identifiers, even in</div>
 
<div>// AMD (#7102#comment:10, https://github.com/jquery/jquery/pull/557)</div>
 
<div>// and CommonJS for browser emulators (#13566)</div>
 
<div>if ( typeof noGlobal === strundefined ) {</div>
 
<div><span class="Apple-tab-span" style="white-space: pre;">	</span>window.jQuery = window.$ = jQuery;</div>
 
<div>}</div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div>return jQuery;</div>
 
<div> 
  <br />
 </div>
 
<div>}));</div>
 
<div> 
  <br />
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>