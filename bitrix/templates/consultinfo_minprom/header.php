<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

/** @global $APPLICATION */
?>
<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

$detect = new Mobile_Detect;

/** Основные константы*/
$page = $APPLICATION->GetCurPage(true);
define('MAIN', $page == '/index.php');
define('DOCS', $page == '/docs/index.php');
define('SUPPORT', $page == '/support/index.php');
define('QUESTION', $page == '/question/index.php');

if($detect->isMobile()){
    define('MOBILE', true);
} else{
    define('MOBILE', false);
}

if($detect->isTablet()){
    define('TABLET', true);
} else {
    define('TABLET', false);
}

/** Header h1*/
$header_h1 = array(
    "/documents/index.php",
    "/news/index.php",
    "/support/index.php",
    "/question/index.php",
    "/search/index.php"
);

?>
<!DOCTYPE HTML>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
<title><?=$APPLICATION->ShowTitle()?></title>
    <?
    /** fonts */
    Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700|Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet">');

    /** css */
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH .'/css/template_styles.css',true);

    /** js */
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH .'/js/scripts.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH .'/js/bundle.js',true);

    /** addString */
    Asset::getInstance()->addString('<link rel="icon" type="image/x-icon" href="'. SITE_TEMPLATE_PATH .'/favicon.ico"/>');
    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');

    $APPLICATION->ShowHead();
    
    ?>
</head>
<body>
<?if($USER->IsAuthorized()):?><div id="panel"><?if ($USER->IsAdmin())$APPLICATION->ShowPanel()?></div><?endif?>
<div class="mm_website-wrapper">
    <header id="mm_header">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-8">
                    <div class="mm_logotype" style="max-width: 520px;">
                        <a href="/" class="mm_logotype-link">
                                <span class="mm_logotype-icon">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/logotype.png" alt="">
                                </span>
                            <span class="mm_logotype-title"><?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/logotype-title.php", Array(), Array("MODE" => "text", "SHOW_BORDER" => true, "NAME" => "Логотип"));?></span>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                    <div class="mm_become-resident">
                        <a class="mm_button _red" type="button" href="/question/">Задать вопрос</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav id="mm_navigation">
        <div class="container">
            <div class="row">
                <? $APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top_menu_2d",
	array(
		"ROOT_MENU_TYPE" => "topmenu",
		"CHILD_MENU_TYPE" => "left",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"USE_EXT" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "top_menu_2d",
		"DELAY" => "N"
	),
	false
); ?>
                <div class="col-12 col-lg-3 _options">
                    <div class="mm_options">
                        <div class="mm_options-item">
                            <a href="http://minprom-cab.gov39.ru/" class="mm_options-link">
                                    <span class="mm_value">
                                        <span class="i _profile"></span>
                                    </span>
                            </a>
                        </div>
                        <?if(!MOBILE && !TABLET):?>
                            <div class="mm_options-item">
                                <a href="javascript:void(0);" class="mm_options-link" onclick="document.getElementById('target').submit(); return false;">
                                        <span class="mm_value">
                                            <span class="i _spec"></span>
                                        </span>
                                </a>
                            </div>
                        <?endif;?>
                        <div class="mm_options-item">
                            <a id="_search-button" href="javascript:void(0);" class="mm_options-link">
                                <span class="mm_value">
                                    <span class="i _zoom"></span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mm_search-bar">
                    <form action="/search/" class="mm_search-bar__form">
                        <input type="search" name="q" class="mm_search-bar__input" placeholder="Введите Ваш запрос">
                        <input type="hidden" name="how" value="d">
                        <button id="_search-cancel" class="mm_search-bar__button _cancel" type="button">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="mm_search-bar__button _submit" type="submit">
                            <span class="i _zoom"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </nav>
    <main id="mm_workarea">
        <?if(!MAIN):?>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?if(in_array($page, $header_h1)):?>
                            <header class="mm_workarea-header">
                                <h1 class="mm_workarea-heading"><?$APPLICATION->ShowTitle()?></h1>
                            </header>
                        <?endif;?>
                        <div class="mm_section">
                            <div class="mm_section-body">
                                <div class="mm_columns">
                                    <div class="mm_columns-item _left">
                                        <? $APPLICATION->IncludeComponent(
                                            "bitrix:breadcrumb", ".default",
                                            Array(
                                                "START_FROM" => "0",
                                                "PATH" => "",
                                                "SITE_ID" => "s1"
                                            )
                                        );?>
                                        <?if(!in_array($page, $header_h1)):?>
                                            <h1><?$APPLICATION->ShowTitle()?></h1>
                                        <?endif;?>
        <?endif;?>