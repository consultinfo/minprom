<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

/** @global $APPLICATION */
?>
<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?if(!MAIN):?>
                            </div>
                            <div class="mm_columns-item _right">
                                <?if(QUESTION){?>
                                    <div class="mm_section-body">
                                        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/podat_obrashenie.php", Array(), Array("MODE" => "html", "SHOW_BORDER" => true));?>
                                    </div>
                               <?} else {?>
                                <header class="mm_section-header">
                                    <h2 class="mm_section-heading">Разделы</h2>
                                </header>
                                <div class="mm_section-body">
                                    <div class="mm_aside">
                                        <div class="mm_aside-item">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:menu",
                                                "right_menu",
                                                array(
                                                    "ROOT_MENU_TYPE" => "left",
                                                    "CHILD_MENU_TYPE" => "",
                                                    "MENU_CACHE_TYPE" => "N",
                                                    "MENU_CACHE_TIME" => "3600",
                                                    "MENU_CACHE_USE_GROUPS" => "N",
                                                    "MENU_CACHE_GET_VARS" => array(
                                                    ),
                                                    "MAX_LEVEL" => "1",
                                                    "USE_EXT" => "N",
                                                    "ALLOW_MULTI_SELECT" => "N",
                                                    "COMPONENT_TEMPLATE" => "right_menu",
                                                    "DELAY" => "N"
                                                ),
                                                false
                                            ); ?>
                                        </div>
                                        <div class="mm_aside-item">
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:news.list",
                                                "events-list",
                                                array(
                                                    "DISPLAY_DATE" => "Y",
                                                    "DISPLAY_NAME" => "Y",
                                                    "DISPLAY_PICTURE" => "Y",
                                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                                    "AJAX_MODE" => "N",
                                                    "IBLOCK_TYPE" => "public",
                                                    "IBLOCK_ID" => "1",
                                                    "NEWS_COUNT" => "9",
                                                    "SORT_BY1" => "ACTIVE_FROM",
                                                    "SORT_ORDER1" => "DESC",
                                                    "SORT_BY2" => "SORT",
                                                    "SORT_ORDER2" => "DESC",
                                                    "FILTER_NAME" => "",
                                                    "FIELD_CODE" => array(
                                                        0 => "",
                                                        1 => "",
                                                    ),
                                                    "PROPERTY_CODE" => array(
                                                        0 => "",
                                                        1 => "",
                                                    ),
                                                    "CHECK_DATES" => "N",
                                                    "DETAIL_URL" => "",
                                                    "PREVIEW_TRUNCATE_LEN" => "",
                                                    "ACTIVE_DATE_FORMAT" => "j F",
                                                    "SET_TITLE" => "N",
                                                    "SET_BROWSER_TITLE" => "Y",
                                                    "SET_META_KEYWORDS" => "Y",
                                                    "SET_META_DESCRIPTION" => "Y",
                                                    "SET_LAST_MODIFIED" => "Y",
                                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                    "ADD_SECTIONS_CHAIN" => "N",
                                                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                                                    "PARENT_SECTION" => "",
                                                    "PARENT_SECTION_CODE" => "sobytiya",
                                                    "INCLUDE_SUBSECTIONS" => "N",
                                                    "CACHE_TYPE" => "A",
                                                    "CACHE_TIME" => "3600",
                                                    "CACHE_FILTER" => "Y",
                                                    "CACHE_GROUPS" => "Y",
                                                    "DISPLAY_TOP_PAGER" => "N",
                                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                                    "PAGER_TITLE" => "",
                                                    "PAGER_SHOW_ALWAYS" => "N",
                                                    "PAGER_TEMPLATE" => "",
                                                    "PAGER_DESC_NUMBERING" => "Y",
                                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                    "PAGER_SHOW_ALL" => "N",
                                                    "PAGER_BASE_LINK_ENABLE" => "Y",
                                                    "SET_STATUS_404" => "Y",
                                                    "SHOW_404" => "Y",
                                                    "MESSAGE_404" => "",
                                                    "PAGER_BASE_LINK" => "",
                                                    "PAGER_PARAMS_NAME" => "arrPager",
                                                    "AJAX_OPTION_JUMP" => "N",
                                                    "AJAX_OPTION_STYLE" => "Y",
                                                    "AJAX_OPTION_HISTORY" => "N",
                                                    "AJAX_OPTION_ADDITIONAL" => "",
                                                    "COMPONENT_TEMPLATE" => "events-list",
                                                    "FILE_404" => "",
                                                    "BLOCK_TITLE" => "События"
                                                ),
                                                false
                                            );?>
                                        </div>
                                    </div>
                                </div>
                                <?}?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?endif;?>
</main>
<footer id="mm_footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <?if(!MOBILE && !TABLET):?>
                    <a href="javascript:void(0);" class="mm_button _white" onclick="document.getElementById('target').submit(); return false;">Версия для слабовидящих</a>
                <?endif;?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bot_menu",
                    array(
                        "ROOT_MENU_TYPE" => "topmenu",
                        "CHILD_MENU_TYPE" => "",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "USE_EXT" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "bot_menu",
                        "DELAY" => "N"
                    ),
                    false
                ); ?>
                <div class="mm_government">
                    <div class="mm_government-image">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/coa.png" alt="">
                    </div>
                    <div class="mm_government-details">
                        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/government-details.php", Array(), Array("MODE" => "html", "SHOW_BORDER" => true));?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 _stats">
                <div class="mm_social_networks">
                    <h5 class="mm_social_networks__label"><?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/social_networks__label.php", Array(), Array("MODE" => "text", "SHOW_BORDER" => true));?></h5>
                    <div class="mm_social_networks__list">
                       <div class="mm_social_networks__item">
                            <a href="https://t.me/linkDD1" class="mm_social_networks__link">
                                        <span class="mm_value">
                                            <i class="fa fa-telegram" aria-hidden="true"></i>
                                        </span>
                            </a>
                        </div>
                        <div class="mm_social_networks__item">
                            <a href="https://vk.com/gov39" class="mm_social_networks__link">
                                        <span class="mm_value">
                                            <i class="fa fa-vk" aria-hidden="true"></i>
                                        </span>
                            </a>
                        </div>
                        <div class="mm_social_networks__item">
                            <a href="https://www.youtube.com/channel/UCM0BOq2sjvMgrlnkUjBgrIg" class="mm_social_networks__link">
                                        <span class="mm_value">
                                            <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                        </span>
                            </a>
                        </div>
                        <div class="mm_social_networks__item">
                            <a href="https://www.facebook.com/gov39/" class="mm_social_networks__link">
                                        <span class="mm_value">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="target"
          action="<?=MAIN ? "/special/" : $page; ?>"
          method="post">
        <input type="hidden" name="special" value="y">
    </form>
</footer>
    <div id="_modal" class="mm_modal _full">
        <div class="mm_modal-close">
            <button class="mm_modal-close__control b-close" type="button">
                <span class="i _cross"></span>
            </button>
        </div>
        <div class="mm_modal-body">
            <div class="mm_modal-body__inside" id="wrp">
            </div>
        </div>
    </div>
</div>
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/widgets/index.php", Array(), Array("MODE" => "html", "SHOW_BORDER" => true, "NAME" => "виджеты"));?>
</body>
</html>