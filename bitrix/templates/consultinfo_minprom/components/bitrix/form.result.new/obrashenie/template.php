<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if(!empty($arResult["FORM_NOTE"])):?>
    <?=$arResult["FORM_NOTE"]?>
    <script>
        $( document ).ready(function() {
            modal_alert("<?=preg_replace("/\r\n|\r|\n/",' ',$arResult["FORM_NOTE"]);?>");
        });
    </script>
<?endif;?>
<?if ($arResult["isFormNote"] != "Y")
{
?>
    <div class="mm_default-form">
        <?=$arResult["FORM_HEADER"]?>

            <div class="mm_default-form__item">
                <h6 class="mm_default-form__label">Фамилия</h6>
                <div class="mm_input <?if(isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_692"])):?>_error<?endif;?>">
                    <input type="text" name="form_text_1" value="<?=$_REQUEST["form_text_1"]?>">
                </div>
            </div>
            <div class="mm_default-form__item">
                <h6 class="mm_default-form__label">Имя</h6>
                <div class="mm_input <?if(isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_413"])):?>_error<?endif;?>">
                    <input type="text" name="form_text_2" value="<?=$_REQUEST["form_text_2"]?>">
                </div>
            </div>
            <div class="mm_default-form__item">
                <h6 class="mm_default-form__label">Отчество</h6>
                <div class="mm_input <?if(isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_744"])):?>_error<?endif;?>">
                    <input type="text" name="form_text_3" value="<?=$_REQUEST["form_text_3"]?>">
                </div>
            </div>
            <div class="mm_default-form__item">
                <h6 class="mm_default-form__label">Телефон</h6>
                <div class="mm_input <?if(isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_792"])):?>_error<?endif;?>">
                    <input type="tel" name="form_text_4" value="<?=$_REQUEST["form_text_4"]?>">
                </div>
            </div>
            <div class="mm_default-form__item">
                <h6 class="mm_default-form__label">Электронная почта</h6>
                <div class="mm_input <?if(isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_344"])):?>_error<?endif;?>">
                    <input type="email" name="form_email_5" value="<?=$_REQUEST["form_email_5"]?>">
                </div>
            </div>
            <div class="mm_default-form__item">
                <h6 class="mm_default-form__label">Обращение</h6>
                <div class="mm_input <?if(isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_785"])):?>_error<?endif;?>">
                    <textarea name="form_textarea_6"><?=$_REQUEST["form_textarea_6"]?></textarea>
                </div>
            </div>

            <div class="mm_default-form__item">
                <div class="mm_control <?if(isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_475"])):?>_error<?endif;?> ">
                    <label class="mm_control__label">
                        <input 
                            type="checkbox" 
                            class="mm_control__input" 
                            value="7"
                            name="form_checkbox_SIMPLE_QUESTION_475[]"
                            <?if($_REQUEST["form_checkbox_SIMPLE_QUESTION_475"][0] == "7"):?>
                                checked
                            <?endif;?>
                        >
                        <span class="mm_control__container">
                        <span class="mm_control__checkbox"></span>
                        <span class="mm_control__value">Я согласен на  обработку своих персональных данных, в соответствии с <a target="_blank" href="javascript:void(0);">Федеральным законом от 27.07.2006 N 152-ФЗ</a> в редакции от 22.02.2017 г.</span></span>
                    </label>
                </div>
            </div>

        <div class="mm_default-form__item">
            <input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" class="mm_button" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
            <input type="hidden" name="web_form_apply" value="Y" />
        </div>
        <?=$arResult["FORM_FOOTER"]?>
    </div>
    <?
} //endif (isFormNote)
?>






