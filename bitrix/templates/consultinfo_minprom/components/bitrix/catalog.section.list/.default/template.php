<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
    <div class="mm_tabs _480">
        <ul id="_tabs" class="mm_tabs-list">
            <?foreach ($arResult['SECTIONS'] as $key => $arSection):?>
                <li class="mm_tabs-list__item<?=$key == 0 ? " _active" : ""?>">
                    <a data-target="#_<?=$arSection["CODE"]?>" href="javascript:void(0);" class="mm_tabs-list__link<?=$key == 0 ? " _active" : ""?>"><?=$arSection["NAME"]?></a>
                </li>
            <?endforeach;?>
        </ul>
        <div class="mm_tabs-select">
            <div class="mm_select">
                <select id="_tabs-select">
                    <?foreach ($arResult["SECTIONS"] as $key => $arSection):?>
                        <option value="#_<?=$arSection["CODE"]?>" <?=$key == 0 ? "selected" : ""?>><?=$arSection["NAME"]?></option>
                    <?endforeach;?>
                </select>
            </div>
        </div>
    </div>