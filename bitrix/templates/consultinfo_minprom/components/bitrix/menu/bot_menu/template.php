<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="mm_footer_navigation">
        <?foreach($arResult as $arItem):?>
            <div class="mm_footer_navigation-item">
                <a href="<?=$arItem["LINK"]?>" class="mm_footer_navigation-link"><?=$arItem["TEXT"]?></a>
            </div>
        <?endforeach?>
    </div>
<?endif?>
