<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="col-12 col-lg-9 _navigation">
        <ul id="_nav" class="mm_navigation">
            <?
            $previousLevel = 0;
             foreach($arResult as $arItem):?>
        
                <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                    <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
                <?endif?>
            
                <?if ($arItem["IS_PARENT"]):?>
            
                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li class="mm_navigation-item">
                            <a href="<?=$arItem["LINK"]?>" class="mm_navigation-link">
                                <span class="mm_value"><?=$arItem["TEXT"]?></span>
                            </a>
                            <ul class="mm_navigation-sublevel _col-3">
                        <?else:?>
                            <li class="mm_navigation-sublevel__item">
                                <a href="<?=$arItem["LINK"]?>" class="mm_navigation-sublevel__link"><?=$arItem["TEXT"]?></a>
                            </li>
                    <?endif?>
            
                <?else:?>
            
                    <?if ($arItem["PERMISSION"] > "D"):?>
            
                        <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                            <li class="mm_navigation-item">
                                <a href="<?=$arItem["LINK"]?>" class="mm_navigation-link">
                                    <span class="mm_value"><?=$arItem["TEXT"]?></span>
                                </a>
                            </li>
                        <?else:?>
                            <li class="mm_navigation-sublevel__item">
                                <a href="<?=$arItem["LINK"]?>" class="mm_navigation-sublevel__link"><?=$arItem["TEXT"]?></a>
                            </li>
                        <?endif?>
            
                    <?else:?>
            
                        <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                            <li class="mm_navigation-sublevel__item">
                                <a href="<?=$arItem["LINK"]?>" class="mm_navigation-sublevel__link"><?=$arItem["TEXT"]?></a>
                            </li>
                        <?else:?>
                            <li class="mm_navigation-sublevel__item">
                                <a href="<?=$arItem["LINK"]?>" class="mm_navigation-sublevel__link"><?=$arItem["TEXT"]?></a>
                            </li>
                        <?endif?>
            
                    <?endif?>
            
                <?endif?>
            
                <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
        
             <?endforeach?>
        
            <?if ($previousLevel > 1)://close last item tags?>
                <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
            <?endif?>

        </ul>
    </div>
<?endif?>