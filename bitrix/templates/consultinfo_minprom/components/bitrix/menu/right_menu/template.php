<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="mm_docs_list">
        <?foreach($arResult as $arItem):?>
            <div class="mm_docs_list-item<?if($arItem["SELECTED"]):?> _current<?endif?>">
                <a href="<?=$arItem["LINK"]?>" class="mm_docs_list-item__link" <?=!empty($arItem["PARAMS"]["target"]) ? "target='".$arItem["PARAMS"]["target"]."'" : ""?>><?=$arItem["TEXT"]?></a>
            </div>
        <?endforeach?>
    </div>
<?endif?>