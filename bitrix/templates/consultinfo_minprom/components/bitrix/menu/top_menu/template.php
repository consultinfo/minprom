<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="col-12 col-lg-9 _navigation">
        <ul id="_nav" class="mm_navigation">
            <?foreach($arResult as $arItem):?>
                <li class="mm_navigation-item">
                    <a href="<?=$arItem["LINK"]?>" class="mm_navigation-link<?if($arItem["SELECTED"]):?> _current<?endif?>">
                        <span class="mm_value"><?=$arItem["TEXT"]?></span>
                    </a>
                </li>
            <?endforeach?>
        </ul>
    </div>
<?endif?>