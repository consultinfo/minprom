<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="mm_pagination">
    <div class="mm_pagination-unit">
        <button class="mm_button _theme" type="button" id="mm_button_add">Показать больше</button>
        <div class="mm_pager">
            <?if($arResult["NavPageNomer"] > 1):?>
                <a class="mm_pager-button _prev" href="<?=$arResult["sUrlPath"]. '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . ($arResult["NavPageNomer"]-1)?>"></a>
            <?endif;?>
            <div class="mm_pager-select">
                <div class="mm_select _pagination _pager scrollbar-dynamic">
                    <select id="_select">
                        <?for($i = 1; $i <= $arResult["NavPageCount"]; $i++):?>
                            <option value="<?=$i?>"<?=$arResult["NavPageNomer"] == $i ? "selected" : ""?>><?=$i?></option>
                        <?endfor;?>
                    </select>
                    <span class="mm_pager-select__pages">из <span id="_total-pages"><?=$arResult["NavPageCount"]?></span></span>
                </div>
            </div>
            <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
                <a class="mm_pager-button _next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>
            <?endif?>
        </div>
    </div>
</div>