<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
<div class="mm_filter" id="_anchor">
    <button data-goto="1" data-title="Фильтровать по" type="button" class="mm_filter-control _js-filter-control">
        <span class="mm_value">Фильтровать по</span>
    </button>
    <div class="mm_filter-dropdown">
        <div class="mm_filter-dropdown__inside _scroll-bar">
            <div data-level="1" class="mm_filter-options _js-filter-options">
                <?$goto = 2.1;?>
                <?foreach($arResult["FILTER"] as $key => $field):?>
                    <div class="mm_filter-option">
                        <a data-goto="<?=$goto?>" data-title="<?=$field["NAME"]?>" class="mm_filter-option__link _js-level-control" href="javascript:void(0);"><?=$field["NAME"]?></a>
                    </div>
                    <?$goto = $goto + 0.1;?>
                <?endforeach;?>
            </div>
            <?$goto = 2.1;?>
            <?foreach($arResult["FILTER"] as $key => $field):?>
                <div data-level="<?=$goto?>" class="mm_filter-options _js-filter-options">
                    <?foreach($field["IDS"] as $value):?>
                        <div class="mm_filter-option">
                            <a class="mm_filter-option__link _js-filter-option <?=!empty($arResult["CURRENT_FILTER"][$value["VALUE"]]) ? "_disabled" : ""?>" 
                               href="javascript:void(0);" 
                               onclick="ajax_Filter('<?=$field["FIELD"]?>','<?=$value["VALUE"]?>','add','<?=str_replace(array("'","\""), "", $value["NAME"])?>');"><?=$value["NAME"]?></a>
                        </div>
                    <?endforeach;?>
                </div>
                <?$goto = $goto + 0.1;?>
            <?endforeach;?>
        </div>
    </div>
    <div id="_filter-list" class="mm_filter-list">
        <?if(!empty($arResult["CURRENT_FILTER"])):?>
            <?foreach ($arResult["CURRENT_FILTER"] as $filter):?>
                <div class="mm_filter-element">
                    <span class="mm_value"><?=$filter["NAME"]?></span>
                    <button type="button"
                            class="mm_filter-element__remove _js-filter-element-remove"
                            onclick="ajax_Filter('SECTION_ID','<?=$filter["SECTION_ID"]?>','del');">
                        <span class="i _remove"></span>
                    </button>
                </div>
            <?endforeach;?>
        <?endif;?>
    </div>
</div>
<div class="mm_section-body" id="wrp">
    <?if ($_REQUEST["AJAX"] == "Y") $APPLICATION->RestartBuffer();?>
    <?if(!empty($arResult["ITEMS"])):?>
    <div class="mm_registry-list">
        <?foreach ($arResult["ITEMS"] as $key => $arItem):
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="mm_registry-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="mm_registry-item__no">
                    <span class="mm_value"><?=$arResult['NUMBER']?>.</span>
                </div>
                <div class="mm_registry-item__details">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="text-decoration: none;">
                        <h5 class="mm_registry-item__title"><?=$arItem["NAME"]?></h5>
                    </a>
                    <div class="mm_registry-item__props">
                        <p class="label">Администратор меры поддержки</p>
                        <p><?=$arResult["FILTER"]["SECTION_ID"]["IDS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"]?></p>
                    </div>
                    <?foreach ($arItem["DISPLAY_PROPERTIES"] as $prop):?>
                        <div class="mm_registry-item__props">
                            <p class="label"><?=$prop["NAME"]?></p>
                            <?if(is_array($prop["VALUE"])):?>
                                <p><?=$prop["VALUE"]["TEXT"]?></p>
                            <?else:?>
                                <p><?=$prop["VALUE"]?></p>
                            <?endif;?>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
            <?$arResult['NUMBER']++;?>
        <?endforeach;?>
    </div>
    <?else:?>
        <p><?=GetMessage('NO_ITEMS')?></p>
    <?endif;?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
    <script>
        var page_num = "<?=$arResult['NAV_RESULT']->NavPageNomer?>";
    </script>
    <?if ($_REQUEST["AJAX"] == "Y") die;?>
</div>
<script>
    var page = "<?=$APPLICATION->GetCurPage(false)?>";
    var news_count = <?=$arParams["NEWS_COUNT"]?>;
    var news_count_option = <?=$arParams["NEWS_COUNT"]?>;
</script>
<?endif;?>
