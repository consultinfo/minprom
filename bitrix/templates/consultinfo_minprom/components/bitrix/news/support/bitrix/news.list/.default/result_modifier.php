<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$rows = array();
$groups = array();
$arResult["SECTIONS"] = array();
$arResult["FILTER"] = array();
$arResult["CURRENT_FILTER"] = array();

$result = Bitrix\Iblock\SectionElementTable::getList(array(
    'order' => array('SORT' => 'ASC'),
    'filter' => array('IBLOCK_SECTION.IBLOCK_ID' => $arParams["IBLOCK_ID"] ,'IBLOCK_SECTION.ACTIVE' => "Y"),
    'select' => array('NAME' => 'IBLOCK_SECTION.NAME', 'CODE' => 'IBLOCK_SECTION.CODE', 'ELEMENT_ID' => 'IBLOCK_ELEMENT_ID', 'SECTION_ID' => 'IBLOCK_SECTION.ID' , 'SORT' => 'IBLOCK_SECTION.SORT')
));
while ($row = $result->fetch())
{
    $groups[$row["ELEMENT_ID"]][] = $row["SECTION_ID"];
    $arResult["SECTIONS"][$row["SECTION_ID"]] = $row;
    $arResult["FILTER"]["SECTION_ID"]["IDS"][$row["SECTION_ID"]] = $row;
    $arResult["FILTER"]["SECTION_ID"]["IDS"][$row["SECTION_ID"]]["VALUE"] = $row["SECTION_ID"];
}

$arResult["FILTER"]["SECTION_ID"]["FIELD"] = "SECTION_ID";
$arResult["FILTER"]["SECTION_ID"]["NAME"] = "Администратору";

/** Ищем сохраненный фильтр в сессии*/
if(!empty($_SESSION["NEWS_SUPPORT_FILTER"])){
    foreach ($_SESSION["NEWS_SUPPORT_FILTER"] as $field){
        foreach ($field as $value) {
            $arResult["CURRENT_FILTER"][$value] =  $arResult["SECTIONS"][$value];
        }
    }
}

$page_num = $arResult['NAV_RESULT']->NavPageNomer;
if($page_num > 1){
    $arResult['NUMBER'] = (int) (($page_num * $arParams["NEWS_COUNT"] - $arParams["NEWS_COUNT"]) + 1);
} else {
    $arResult['NUMBER'] = 1;
}
?>
