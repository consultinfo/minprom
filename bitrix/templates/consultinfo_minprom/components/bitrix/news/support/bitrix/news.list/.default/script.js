function mm_createElement(t) {
    return;
}

function ajax_Filter(filtered_field, filtered_value, action, name) {
    if(action == "add"){
        var html ='<div class="mm_filter-element"><span class="mm_value">' + name + '</span>' +
            '<button type="button" class="mm_filter-element__remove _js-filter-element-remove" onclick="ajax_Filter(\''+ filtered_field +'\',\'' + filtered_value + '\',\'del\');">' +
            '<span class="i _remove"></span></button>' +
            '</div>';
    }
    $.ajax({
        url: page,
        method: "post",
        data: {
            AJAX: "Y",
            filtered_field: filtered_field,
            filtered_value: filtered_value,
            action: action,
            PAGEN_1: page_num
        },
        beforeSend: function () {
            $('#wrp').fadeOut(200);
        },
        error: function () {
            alert("Ошибка запроса");
        },
        success: function (result) {
            $('#wrp').html(result).fadeIn(200);
            $(".mm_select select").styler();
            if(action == "add"){
                $('#_filter-list').append(html);
            }
        }
    })
}

$(document).ready(function() {

    $(document).on('change', '#_select', function (e) {
        var select_val;
        var offset = _anchor.offsetTop;
        select_val = $(this).val();

        ajax_section(news_count,select_val);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('click', '#mm_button_add', function (e) {
        news_count = news_count + news_count_option;

        ajax_section(news_count,page_num);
    });

    function ajax_section(news_count,page_num) {
        $.ajax({
            url: page,
            method: "post",
            data: {
                AJAX: "Y",
                NEWS_COUNT: news_count,
                PAGEN_1: page_num
            },
            beforeSend: function () {
                $('#wrp').fadeOut(200);
            },
            error: function () {
                alert("Ошибка запроса");
            },
            success: function (result) {
                $('#wrp').html(result).fadeIn(200);
                $(".mm_select select").styler();
                window.history.pushState(null, null, "/support/" + "?PAGEN_1=" + page_num);
            }
        })
    }
});