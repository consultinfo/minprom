<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
    <p class="mm_news-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></p>
<?endif;?>
<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
    <p>
        <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
             alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
             title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
        >
    </p>
<?endif?>
<?if(!empty($arResult["DETAIL_TEXT"])):?>
    <p>
        <?=$arResult["DETAIL_TEXT"]?>
    </p>
<?else:?>
    <p>
        <?=$arResult["PREVIEW_TEXT"]?>
    </p>
<?endif;?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]["DOCS"])):?>
    <?if($arResult["DISPLAY_PROPERTIES"]["DOCS"]["FILE_VALUE"][0]){?>
        <?foreach($arResult["DISPLAY_PROPERTIES"]["DOCS"]["FILE_VALUE"] as $arFile){?>
            <p>
                <a href="<?=$arFile["DISPLAY_PROPERTIES"]["DOCS"]["FILE_VALUE"]["SRC"]?>" class="mm_doc-link">
                    <span><?=$arFile["DISPLAY_PROPERTIES"]["DOCS"]["FILE_VALUE"]["FILE_NAME"]?></span>
                </a>
            </p>
        <?}?>
    <?} else {?>
        <p>
            <a href="<?=$arResult["DISPLAY_PROPERTIES"]["DOCS"]["FILE_VALUE"]["SRC"]?>" class="mm_doc-link">
                <span><?=$arResult["DISPLAY_PROPERTIES"]["DOCS"]["FILE_VALUE"]["FILE_NAME"]?></span>
            </a>
        </p>
    <?}?>
<?endif;?>
