<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
    <div class="mm_features">
        <div class="container">
            <div class="row">
                <div id="_features-slider" class="col-12 _arrows">
                <?foreach ($arResult["ITEMS"] as $arItem):
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div class="mm_features-item">
                        <a href="<?=$arItem["PROPERTIES"]["URL"]["VALUE"] ? $arItem["PROPERTIES"]["URL"]["VALUE"] : 'javascript:void(0);'?>" 
                           class="mm_features-item__inside" 
                           id="<?=$this->GetEditAreaId($arItem['ID']);?>" 
                           <?=$arItem["PROPERTIES"]["URL"]["VALUE"] ? 'target="_blank"' : ''?>>
                            <?if(!empty($arItem["PROPERTIES"]["ICON"]["VALUE"])):?>
                                <div class="mm_features-item__icon">
                                    <span class="i <?=$arItem["PROPERTIES"]["ICON"]["VALUE"]?>"></span>
                                </div>
                            <?endif;?>
                            <div class="mm_features-item__label">
                                <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                    <?=$arItem["PREVIEW_TEXT"]?>
                                <?else:?>
                                    <?=$arItem["NAME"]?>
                                <?endif;?>
                            </div>
                        </a>
                    </div>
                <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
<?endif;?>
