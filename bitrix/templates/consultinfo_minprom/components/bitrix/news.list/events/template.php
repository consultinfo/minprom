<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
    <section class="mm_section">
        <header class="mm_section-header">
            <h2 class="mm_section-heading"><?=$arParams["BLOCK_TITLE"]?></h2>
        </header>
        <div class="mm_section-body">
            <div class="mm_news-carousel _news-carousel _arrows">
            <?foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="mm_news-carousel-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="mm_news-carousel-item__icon">
                        <span class="i _calendar"></span>
                    </div>
                    <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                        <div class="mm_news-carousel-item__date">
                            <span class="mm_value"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                        </div>
                    <?endif;?>
                    <div class="mm_news-carousel-item__preview-text">
                        <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                            <p><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></p>
                        <?else:?>
                            <p><?=$arItem["NAME"]?></p>
                        <?endif;?>
                    </div>
                </div>
             <?endforeach;?>
            </div>
        </div>
    </section>
<?endif;?>