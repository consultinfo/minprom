<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
    <div class="mm_map">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="mm_section">
                        <header class="mm_section-header">
                            <h2 class="mm_section-heading">Промышленные парки <br>в калининградской области</h2>
                        </header>
                        <div class="mm_section-body">
                            <div class="mm_map-image">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/map.png" alt="">
                                <?foreach ($arResult["ITEMS"] as $key => $arItem):
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                    <div class="mm_map-item" style="left: <?=$arItem["PROPERTIES"]["POSITION_X"]["VALUE"]?>%; top: <?=$arItem["PROPERTIES"]["POSITION_Y"]["VALUE"]?>%;" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                        <a data-map-item="<?=$key?>" href="javascript:void(0);" class="mm_map-item__link <?=$key == "0" ? "_active" : ""?>">
                                            <span class="mm_value"><?=$arItem["NAME"]?></span>
                                        </a>
                                    </div>
                                <?endforeach;?>
                            </div>
                            <div id="_map-slider" class="mm_map-slider _arrows">
                                <?foreach ($arResult["ITEMS"] as $arItem):?>
                                    <div class="mm_map-slider__item">
                                    <h5 class="mm_map-slider__heading"><?=$arItem["PREVIEW_TEXT"]?></h5>
                                    <div class="mm_map-slider__props">
                                        <?foreach ($arItem["PROPERTIES"] as $arProp):?>
                                            <?if($arProp["PROPERTY_TYPE"] == "S" && $arProp["ACTIVE"] == "Y"):?>
                                                <div class="mm_map-slider__prop">
                                                    <span class="mm_value _heading"><?=$arProp["NAME"]?></span>
                                                    <span class="mm_value _value"><?=$arProp["VALUE"]?></span>
                                                </div>
                                            <?endif;?>
                                        <?endforeach;?>
                                        <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                                            <div class="mm_map-slider__prop">
                                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="mm_button _yellow">Подробнее</a>
                                            </div>
                                        <?endif;?>
                                    </div>
                                </div>
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?endif;?>