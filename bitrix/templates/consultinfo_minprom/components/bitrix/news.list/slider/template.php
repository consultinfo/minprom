<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
    <div class="mm_section">
        <div class="mm_section-body">
            <div class="mm_fullwidth-slider _js-fullwidth-slider _arrows _white">
                <?foreach ($arResult["ITEMS"] as $arItem):
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div class="mm_fullwidth-slider__item" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="mm_fullwidth-slider__container">
                            <h3 class="mm_fullwidth-slider__heading"><?=$arItem["PREVIEW_TEXT"] ? $arItem["PREVIEW_TEXT"] : $arItem["NAME"]?></h3>
                        </div>
                        <?if(!empty($arItem["PROPERTIES"]["URL"]["VALUE"])):?>
                            <div class="mm_fullwidth-slider__more">
                                <a href="<?=$arItem["PROPERTIES"]["URL"]["VALUE"]?>" <?if($arItem["PROPERTIES"]["TARGET"]["VALUE"] == "_blank"):?>target="_blank"<?endif;?> class="mm_button _white">Подробнее</a>
                            </div>
                        <?endif;?>
                    </div>
                <?endforeach;?>
            </div>
        </div>
    </div>
<?endif;?>