$(document).ready(function() {

    $(document).on('change', '#_select', function (e) {
        var select_val;
        var offset = _anchor.offsetTop;
        select_val = $(this).val();

        ajax_section(news_count,select_val);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('click', '#mm_button_add', function (e) {
        news_count = news_count + news_count_option;

        ajax_section(news_count,page_num);
    });

    function ajax_section(news_count,page_num) {
        $.ajax({
            url: page,
            method: "post",
            data: {
                AJAX: "Y",
                NEWS_COUNT: news_count,
                PAGEN_1: page_num
            },
            beforeSend: function () {
                $('#wrp').fadeOut(200);
            },
            error: function () {
                alert("Ошибка запроса");
            },
            success: function (result) {
                $('#wrp').html(result).fadeIn(200);
                $(".mm_select select").styler();
                window.history.pushState(null, null, "/map/" + "?PAGEN_1=" + page_num);
            }
        })
    }
});