<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
<div class="mm_section-body" id="wrp">
    <?if ($_REQUEST["AJAX"] == "Y") $APPLICATION->RestartBuffer();?>
    <div class="mm_tabs-container">
        <div class="mm_tabs-pane _active">
            <?if(!empty($arResult["ITEMS"])):?>
                <div class="mm_docs_list _important">
                    <?foreach ($arResult["ITEMS"] as $key => $arItem):
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="mm_docs_list-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                           <div class="mm_docs_list-item">
                               <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="mm_docs_list-item__link"><?=$arItem["NAME"]?></a>
                                <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                                    <div class="mm_docs_list-item__date">
                                        <span class="mm_value"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                                    </div>
                               <?endif;?>
                            </div>
                        </div>
                    <?endforeach;?>
                </div>
            <?else:?>
                <p><?=GetMessage('NO_ITEMS')?></p>
            <?endif;?>
        </div>
        <script>
            var page_num = "<?=$arResult['NAV_RESULT']->NavPageNomer?>";
        </script>
    </div>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
    <?if ($_REQUEST["AJAX"] == "Y") die;?>
</div>
<script>
    var page = "<?=$APPLICATION->GetCurPage(false)?>";
    var news_count = <?=$arParams["NEWS_COUNT"]?>;
    var news_count_option = <?=$arParams["NEWS_COUNT"]?>;
</script>
<?endif;?>
