<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
    <div class="mm_stats-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?foreach ($arResult["ITEMS"] as $arItem):
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="mm_stats-row" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <section class="mm_section">
                                <header class="mm_section-header">
                                    <h4 class="mm_section-heading"><?=$arItem['NAME']?></h4>
                                </header>
                                <div class="mm_section-body">
                                    <div class="mm_stats _arrows _js-stats-carousel">
                                        <?foreach ($arItem["PROPERTIES"]["INFO"]["~VALUE"] as $key => $arProp):?>
                                            <div class="mm_stats-item">
                                                <p class="mm_stats-item__label"><?=$arProp["TEXT"]?></p>
                                                <p class="mm_stats-item__value"><?=$arItem["PROPERTIES"]["INFO"]["DESCRIPTION"][$key]?></p>
                                            </div>
                                        <?endforeach;?>
                                    </div>
                                </div>
                            </section>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
<?endif;?>