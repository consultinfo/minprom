$(document).on('submit','.feedback form',function(e){
    var flag_complite = true;
    BX.closeWait();

    $.each($(this).find(".item"), function(){
        var item = $(this);
        if (item.find(".no_req").size())
        {
            return;
        }
        if(item.find("input,textarea").val().trim() == "")
        {
            flag_complite = false;
            item.addClass("error");
        }
        else if (item.hasClass("error")) {
            item.removeClass("error");
        }
    });

    if(!flag_complite)
    {
        alert("Заполните все обязательные поля!");
        e.preventDefault();
    }
});


function addFileToForm(id, number){
    var new_file = $("#file_"+id+"_"+number).find("[type='file']");
    if (new_file.size())
    {
        new_file.click();
    }
}

function AddFileName(id, val){
    val = val.split('\\');

    if (val == "") return false;

    var wrp = $("#input_file_"+id),
        btn = wrp.find(".fileBtn").find(".value"),
        number = btn.data("number")*1,
        next_number = number + 1,
        input_html = ''+
                '<div id="file_'+id+'_'+next_number+'">'+
                    '<input type="file" name="PROPERTY_FILE_'+id+'_'+next_number+'" onchange="AddFileName('+id+', this.value);">'+
                    '<input type="hidden" name="PROPERTY['+id+']['+next_number+']">'+
                '</div>',
        html = ''+
            '<div class="fileItem" id="fileItem_'+number+'">'+
                '<div class="uploaded">'+
                    '<span class="ico file"></span>'+
                    '<span class="value">' + val + '</span>'+
                '</div>'+
                '<div class="remove">'+
                    '<span class="ico del" onclick="removeFile('+id+','+number+');"></span>'+
                '</div>'+
            '</div>';

    btn.data("number", next_number);
    wrp.find(".fileName").append(html);
    wrp.find(".hide").append(input_html);
    btn.attr("onclick", 'addFileToForm('+id+', '+next_number+');');

    return false;
}

function removeFile(id, number) {
    $("#fileItem_"+number).remove();
    $("#file_"+id+"_"+number).remove();

    return false;
}


var bxajaxid_el,
    bxajaxid_catalog,
    cur_section_id = "0";

$(function() {
    bxajaxid_el = $("#bxajaxid");
    bxajaxid_catalog = bxajaxid_el.val();
    cur_section_id = $("select option:selected", bxajaxid_el);
});

var obSytlerOptions = {
    onSelectClosed: function() {
        $(this).addClass("load");
        var value = $(this).find("select option:selected").val();
        if (value == cur_section_id) return false;
        cur_section_id = value;

        $.ajax({
            url: "?bxajaxid="+ bxajaxid_catalog +"&SECTION_ID="+ value
        }).done(function(data) {
            $("#comp_"+ bxajaxid_catalog).html(data);
            initStyler();
        });
    }
};

function initStyler()
{
    $('.select_section select').styler(obSytlerOptions);
    $('.select_price select').styler();
}

$(function() {
    initStyler();
});


