<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="discussion">
		<div class="header_border">
			<div class="discussion_header" align="right">
				<a href="/discussion/" class="discussion_header_link">На обсуждение</a>
			</div>
		</div>
		<div class="discussion_line"><img src="/images/blind.gif" width="1" height="1"></div>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="discussion_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="discussion_bullet">
			<img src="/images/discussion_bullet.gif" class="discussion_bullet">
		</div>
		<div class="discussion_link">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="discussion"><?echo $arItem["NAME"];?></a><br>
			<?echo $arItem["PREVIEW_TEXT"];?>
		</div>
		<div class="clear"></div>
	</div>

<?endforeach;?>
	<div class="discussion_end"><img src="/images/blind.gif" width="1" height="1"></div>
</div>


