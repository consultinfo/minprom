<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="documents_list">

	<div class="documents_list_line_header"><img src="/images/blind.gif" width="1" height="1"></div>

<table width=350 cellpadding=0 cellspacing=1 border=0 bgcolor="#cccccc">
	<tr>
		<td bgcolor=#FFFFFF>
			<table width=500 cellpadding=5 cellspacing=0 border=0 class="base">
			<form method=GET action=index.php>
				<tr>
					<td colspan="2"><strong>Поиск документов:</strong></td>
				</tr>
				<tr>
					<td align="right" nowrap>Название документа:</td>
					<td width="100%"><INPUT class=typeinput maxLength=255  style="{width: 100%;}" name="docname" value="<?echo $_GET["docname"];?>"></td>
				</tr>				
				<tr>
					<td colspan="2" align="right"><INPUT class=inputbutton type=submit value='Искать'></td>
				</tr>					
				</form>
			</table>
		</td>
	</tr>
</table>

<br>
<table cellpadding="3" cellspacing="1" width="510" border="0" class="base" style="background-color: #ccc;">
	<tr bgcolor="#EEEEEE" align=center>

		<td width=50%><b>Название</b></td>
		<td width=15%><b>Размер</b></td>
		<td width=10%><b>Скачать</b></td>

	</tr>


<?foreach($arResult["ITEMS"] as $arItem):?>

	<tr bgcolor="#FFFFFF" align="center" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<?$parentArr = GetIblockSection($arItem["IBLOCK_SECTION_ID"]);?>
		<td align="left"><b><?echo $arItem["NAME"];?></b><br><div class="text"><?echo $arItem["PREVIEW_TEXT"];?></div></td>
		<td><?echo $arItem["PROPERTIES"]["SIZE"]["VALUE"];?></td>
		<td><a href="<?echo CFile::GetPath($arItem["PROPERTIES"]["DOC"]["VALUE"]); ?>">Скачать</a></td>

	</tr>
<?endforeach;?>

</table>
<br>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>
