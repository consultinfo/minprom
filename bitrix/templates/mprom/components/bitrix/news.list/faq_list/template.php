<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<!-- Блок "FAQ" -->

<div class="faq_list">
	<div class="header_border">
		<div class="faq_list_header" align="right">
			<a href="/faq/" class="faq_list_header_link">F.A.Q.</a> 
		</div>
	</div>
	<div class="faq_list_line"><img src="/images/blind.gif" width="1" height="1"></div>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="faq_list_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="faq_list_bullet">
			<img src="/images/faq_bullet2.gif" class="faq_list_bullet">
		</div>
		<div class="faq_list_text">
		<div class="faq_question"><b>Вопрос:</b> <?echo $arItem["PREVIEW_TEXT"];?></div>
		<div class="faq_ansver"><b>Ответ:</b> <?echo $arItem["DETAIL_TEXT"];?></div>




		</div>
		<div class="clear"></div>


</div>

<?endforeach;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>


