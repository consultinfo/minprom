<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="documents">
	<div class="header_border">
		<div class="documents_header" align="right">
			<a href="/documents/" class="documents_header_link">Документы</a>
		</div>
	</div>
	<div class="documents_line"><img src="/images/blind.gif" width="1" height="1"></div>


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="documents_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

		<div class="documents_bullet">
			<img src="/images/documents_bullet.png" class="documents_bullet">
		</div>

		<div class="documents_link">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="documents"><?=$arItem["NAME"]?></a><br>
		</div>
		<div class="clear"></div>
	</div>

<?endforeach;?>
	<div class="documents_end"><img src="/images/blind.gif" width="1" height="1"></div>
</div>