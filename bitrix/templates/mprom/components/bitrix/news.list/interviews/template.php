<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<!-- Блок "Интервью" -->

<div class="interviews">
	<div class="header_border">
		<div class="interviews_header" align="right">
			<a href="/interviews/" class="interviews_header_link">Интервью</a> 
		</div>
	</div>
	<div class="interviews_line"><img src="/images/blind.gif" width="1" height="1"></div>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="interviews_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="interviews_text">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="interviews_foto" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>">
			</a>
	<?endif?>


	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="interviews_link"><?=$arItem["NAME"]?></a>


	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<?echo $arItem["PREVIEW_TEXT"];?>
	<?endif;?>

		</div>
		<div class="clear"></div>


</div>

<?endforeach;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>


