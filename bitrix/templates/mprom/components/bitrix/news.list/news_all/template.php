<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<!-- Блок "Новости" -->
<div class="news">
	<div class="header_border">
		<div class="news_header" align="right">
			<a href="/news/" class="news_header_link">Новости</a>
		</div>
	</div>
	<div class="news_line"><img src="/images/blind.gif" width="1" height="1"></div>


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="news_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="news_text">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div class="news_foto">
				<div class="news_foto_top"><img src="/images/blind.gif" width="1" height="1"></div>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="news_foto" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"></a><div class="news_foto_bottom"><img src="/images/blind.gif" width="1" height="1"></div>
			</div>
	<?endif?>

	<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<div class="news_date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
	<?endif?>

	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news_link"><?=$arItem["NAME"]?></a>


	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<?echo $arItem["PREVIEW_TEXT"];?>
	<?endif;?>

		</div>
		<div class="clear"></div>

		<?$PARENT = GetIblockSection($arItem[IBLOCK_SECTION_ID]);?>
		<div class="news_section" align="right">/<a href="/news/<?=$PARENT[CODE];?>/" class="news_section"><?=$PARENT[NAME];?></a>/</div>
	</div>
	<div class="news_point"><img src="/images/blind.gif" width="1" height="1"></div>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>
