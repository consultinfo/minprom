<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<div class="faq">
	<div class="header_border">
		<div class="faq_header" align="right">
			<a href="/faq/" class="faq_header_link">F.A.Q.</a>
		</div>
	</div>
	<div class="faq_line"><img src="/images/blind.gif" width="1" height="1"></div>


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="faq_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="faq_foto">
			<img src="/images/faq_bullet.gif" class="faq_bullet">
		</div>
		<div class="faq_link">
			<a href="/faq/" class="faq"><?echo $arItem["PREVIEW_TEXT"];?></a>
		</div>
		<div class="clear"></div>
	</div>
<?endforeach;?>

</div>


