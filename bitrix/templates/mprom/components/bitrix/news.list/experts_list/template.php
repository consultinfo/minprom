<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<!-- Блок "Эксперты" -->
<div class="experts">
	<div class="header_border">
		<div class="experts_header" align="right">
			<a href="/experts/" class="experts_header_link">Эксперты</a>
		</div>
	</div>
	<div class="experts_line"><img src="/images/blind.gif" width="1" height="1"></div>


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="experts_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="experts_text">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div class="experts_foto">
				<div class="experts_foto_top"><img src="/images/blind.gif" width="1" height="1"></div>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="experts_foto" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"></a><div class="experts_foto_bottom"><img src="/images/blind.gif" width="1" height="1"></div>
			</div>
	<?endif?>

	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="experts_link"><?=$arItem["NAME"]?></a>

		</div>
		<div class="clear"></div>


	</div>


<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>

					
					


