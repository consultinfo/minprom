<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<!-- Блок "Календарь событий" -->
<div class="calendar_list">
	<div class="header_border">
		<div class="calendar_list_header" align="right">
			Календарь событий
		</div>
	</div>
	<div class="calendar_list_line"><img src="/images/blind.gif" width="1" height="1"></div>


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
<br>
	<div class="clendar_list_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="calendar_list_text">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div class="calendar_list_foto">
				<div class="calendar_list_foto_top"><img src="/images/blind.gif" width="1" height="1"></div>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="calendar_list_foto" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"></a><div class="calendar_list_foto_bottom"><img src="/images/blind.gif" width="1" height="1"></div>
			</div>
		<?endif?>

	<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<div class="calendar_list_date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
	<?endif?>

	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="calendar_list_link"><?=$arItem["NAME"]?></a>

	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<?echo $arItem["PREVIEW_TEXT"];?>
	<?endif;?>

		</div>
		<div class="clear"></div>


	</div>
	<div class="calendar_list_point"><img src="/images/blind.gif" width="1" height="1"></div>

<?endforeach;?>


</div>

					
					


