<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<!-- Блок "Рубрикатор новостей" -->	
		<div class="activity">
			<div class="header_border">
				<div class="activity_header" align="right">
					Рубрикатор новостей
				</div>
			</div>
			<div class="activity_line"><img src="/images/blind.gif" width="1" height="1"></div>
<?


foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
			<div class="activity_item">
				<div class="activity_bullet">
					<img src="/images/activity_bullet.gif" class="activity_bullet">
				</div>
				<div class="activity_link">
					<a href="<?=$arItem["LINK"]?>" class="activityact"><?=$arItem["TEXT"]?></a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="activity_point"><img src="/images/blind.gif" width="1" height="1"></div>

	<?else:?>
			<div class="activity_item">
				<div class="activity_bullet">
					<img src="/images/activity_bullet.gif" class="activity_bullet">
				</div>
				<div class="activity_link">
					<a href="<?=$arItem["LINK"]?>" class="activity"><?=$arItem["TEXT"]?></a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="activity_point"><img src="/images/blind.gif" width="1" height="1"></div>
	<?endif?>
	
<?endforeach?>

		</div>
<?endif?>
