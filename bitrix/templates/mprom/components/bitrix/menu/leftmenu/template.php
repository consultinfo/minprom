<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<!-- Блок "Деятельность" -->	
		<div class="activity">
			<div class="header_border">
				<div class="activity_header" align="right">
					О министерстве
				</div>
			</div>
			<div class="activity_line"><img src="/images/blind.gif" width="1" height="1"></div>
<?$sel=false;?>
<?foreach($arResult as $arItem):?>

	<?if($arItem[DEPTH_LEVEL] == "1"):?>
		<? if($arItem["ITEM_INDEX"]!="0"):?>
			<div class="activity_point"><img src="/images/blind.gif" width="1" height="1"></div>
		<?endif?>

		<?if($arItem["SELECTED"]):?>

			<div class="activity_item">
				<div class="activity_bullet">
					<img src="/images/activity_bullet.gif" class="activity_bullet">
				</div>
				<div class="activity_link">
					<a href="<?=$arItem["LINK"]?>" class="activityact"><?=$arItem["TEXT"]?></a>
				</div>
				<div class="clear"></div>
			</div>
			<?$sel=true;?>

		<?else:?>
			<div class="activity_item">
				<div class="activity_bullet">
					<img src="/images/activity_bullet.gif" class="activity_bullet">
				</div>
				<div class="activity_link">
					<a href="<?=$arItem["LINK"]?>" class="activity"><?=$arItem["TEXT"]?></a>
				</div>
				<div class="clear"></div>
			</div>
			<?$sel=false;?>

		<?endif?>
	<?endif?>

	<?if($arItem[DEPTH_LEVEL] == "2" AND $sel == true):?>

		<?if($arItem["SELECTED"]):?>
			<div class="activity_item2">
				<div class="activity_bullet2">
					<img src="/images/activity_bullet.gif" class="activity_bullet2">
				</div>
				<div class="activity_link2">
					<a href="<?=$arItem["LINK"]?>" class="activityact2"><?=$arItem["TEXT"]?></a>
				</div>
				<div class="clear"></div>
			</div>

		<?else:?>
			<div class="activity_item2">
				<div class="activity_bullet2">
					<img src="/images/activity_bullet.gif" class="activity_bullet2">
				</div>
				<div class="activity_link2">
					<a href="<?=$arItem["LINK"]?>" class="activity2"><?=$arItem["TEXT"]?></a>
				</div>
				<div class="clear"></div>
			</div>

		<?endif?>
		
	<?endif?>


<?endforeach?>

<div class="activity_point"><img src="/images/blind.gif" width="1" height="1"></div>

		</div>

<?endif?>