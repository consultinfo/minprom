<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if (!empty($arResult)):?>
	<div class="topmenu">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<? if($arItem["ITEM_INDEX"]!="0"):?>
		<div class="topmenusep"></div>
	<?endif?>

	<?if($arItem["SELECTED"]):?>
		<div class="topmenuitem">
			<a class="topmenuact" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		</div>
	<?else:?>
		<div class="topmenuitem">
			<a class="topmenu" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		</div>
	<?endif?>
<?endforeach?>
	</div>
<?endif?>


