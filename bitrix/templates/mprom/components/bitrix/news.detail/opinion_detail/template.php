<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- Календарь событий детально" -->


<div class="opinion_detail">
	<div class="header_border">
		<div class="opinion_detail_header" align="right">
			<a href="/opinion/" class="opinion_detail_header_link">Мнение</a> 
		</div>
	</div>
	<div class="opinion_detail_line"><img src="/images/blind.gif" width="1" height="1"></div>

	<div class="opinion_detail_item">
	<div class="opinion_detail_text">

		<div class="opinion_detail_foto">
			
			<? echo CFile::ShowImage($arResult[DOP][ID_EXPERT][$arResult[DISPLAY_PROPERTIES][EXPERT][VALUE]][DETAIL_PICTURE], 60, 0, "class=opinion_detail_foto", "", true);?>
			
		</div>


	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<div class="opinion_detail_date"><?echo $arResult["DISPLAY_ACTIVE_FROM"]?></div>
	<?endif?>

	<div class="opinion_detail_text_header"><?=$arResult["NAME"]?></div>

	<?echo $arResult["DETAIL_TEXT"];?>

	<div class="clear"></div>
	</div>

</div>
</div>
