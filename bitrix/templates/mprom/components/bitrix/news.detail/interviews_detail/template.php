<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- Интервью детально" -->


<div class="interviews">
	<div class="header_border">
		<div class="interviews_header" align="right">
			<a href="/interviews/" class="interviews_header_link">Интервью</a> 
		</div>
	</div>
	<div class="interviews_line"><img src="/images/blind.gif" width="1" height="1"></div>

	<div class="interviews_item">
	<div class="interviews_text">

	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img class="interviews_foto" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="news_foto" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["arResult"]?>" title="<?=$arResult["NAME"]?>">
	<?endif?>


	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<div class="interviews_date"><?echo $arResult["DISPLAY_ACTIVE_FROM"]?></div>
	<?endif?>

	<div class="interviews_link"><?=$arResult["NAME"]?></div><br>

	<?echo $arResult["DETAIL_TEXT"];?>

	<div class="clear"></div>
	</div>

</div>
</div>
