<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- Новости детально" -->


<div class="news">
	<div class="header_border">
		<div class="news_header" align="right">
			<a href="/news/" class="news_header_link">Новости</a> 
		</div>
	</div>
	<div class="news_line"><img src="/images/blind.gif" width="1" height="1"></div>

	<div class="news_item">
	<div class="news_text">

	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<div class="news_foto">
			<div class="news_foto_top"><img src="/images/blind.gif" width="1" height="1"></div>
			<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="news_foto" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["arResult"]?>" title="<?=$arResult["NAME"]?>"><div class="news_foto_bottom"><img src="/images/blind.gif" width="1" height="1"></div>
		</div>
	<?endif?>


	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<div class="news_date"><?echo $arResult["DISPLAY_ACTIVE_FROM"]?></div>
	<?endif?>

	<div class="news_link"><?=$arResult["NAME"]?></div>

	<?echo $arResult["DETAIL_TEXT"];?>

	<div class="clear"></div>
	</div>

</div>
</div>
