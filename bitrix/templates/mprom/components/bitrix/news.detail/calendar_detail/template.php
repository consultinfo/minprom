<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- Календарь событий детально" -->


<div class="calendar_detail">
	<div class="header_border">
		<div class="calendar_detail_header" align="right">
			<a href="/calendar/" class="calendar_detail_header_link">Календарь событий</a> 
		</div>
	</div>
	<div class="calendar_detail_line"><img src="/images/blind.gif" width="1" height="1"></div>

	<div class="calendar_detail_item">
	<div class="calendar_detail_text">

	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<div class="news_foto">
			<div class="news_foto_top"><img src="/images/blind.gif" width="1" height="1"></div>
			<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="news_foto" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["arResult"]?>" title="<?=$arResult["NAME"]?>"><div class="news_foto_bottom"><img src="/images/blind.gif" width="1" height="1"></div>
		</div>
	<?endif?>


	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<div class="calendar_detail_date"><?echo $arResult["DISPLAY_ACTIVE_FROM"]?></div>
	<?endif?>

	<div class="calendar_detail_text_header"><?=$arResult["NAME"]?></div>

	<?echo $arResult["DETAIL_TEXT"];?>

	<div class="clear"></div>
	</div>

</div>
</div>
