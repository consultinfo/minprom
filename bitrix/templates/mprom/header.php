<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<html>
<head><meta name = "sputnik-verification" content = "zqOhJok3rpdIQtpw"/>                              
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle(false)?> | Минпром</title>
<script type="text/javascript" src="/bitrix/templates/<?=SITE_TEMPLATE_ID?>/js/Autohyphen.js"></script>
</head>
<body onload="(new Autohyphen()).hyphenizeTree(document.body,true)">
<script language="javascript" src="http://gov39.ru/promo/jscode.php" type="text/javascript"></script>
<?$APPLICATION->ShowPanel();?>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="shadow_left">
</td>
<td width="990" style="background-color: #f9f8f3;">
  <div class="header" style="position: relative;">
    <a href="/"><img src="/images/header.jpg" width="990" height="164" alt="Министерство по промышленной политике, развитию предпринимательства и торговли Калининградской области"></a>
    <a href="special/?special=y" style="
    position: absolute;
    top: 140px;
    left: 770px;
    color: white;
    font-size: 19px;
    display: block;
    width: 224px;
">Версия для слабовидящих</a>

  </div>





<?$APPLICATION->IncludeComponent("bitrix:menu", "topmenu", array(
  "ROOT_MENU_TYPE" => "topmenu",
  "MENU_CACHE_TYPE" => "N",
  "MENU_CACHE_TIME" => "3600",
  "MENU_CACHE_USE_GROUPS" => "Y",
  "MENU_CACHE_GET_VARS" => array(
  ),
  "MAX_LEVEL" => "1",
  "CHILD_MENU_TYPE" => "left",
  "USE_EXT" => "N",
  "DELAY" => "N",
  "ALLOW_MULTI_SELECT" => "N"
  ),
  false
);?>

  <div class="clear"></div>

  <div class="left_column">



<!-- Блок "Рубрикатора новостей" -->

<?$APPLICATION->IncludeComponent("bitrix:menu", "leftmenu", array(
  "ROOT_MENU_TYPE" => "left",
  "MENU_CACHE_TYPE" => "N",
  "MENU_CACHE_TIME" => "0",
  "MENU_CACHE_USE_GROUPS" => "N",
  "MENU_CACHE_GET_VARS" => array(
  ),
  "MAX_LEVEL" => "1",
  "CHILD_MENU_TYPE" => "newsmenu",
  "USE_EXT" => "Y",
  "DELAY" => "N",
  "ALLOW_MULTI_SELECT" => "Y"
  ),
  false
);?>

<!-- Блок "Деятельность" -->
  
<?$APPLICATION->IncludeComponent("bitrix:menu", "template1", array(
  "ROOT_MENU_TYPE" => "leftmenu2",
  "MENU_CACHE_TYPE" => "N",
  "MENU_CACHE_TIME" => "3600",
  "MENU_CACHE_USE_GROUPS" => "Y",
  "MENU_CACHE_GET_VARS" => array(
  ),
  "MAX_LEVEL" => "3",
  "CHILD_MENU_TYPE" => "leftmenu2",
  "USE_EXT" => "Y",
  "DELAY" => "N",
  "ALLOW_MULTI_SELECT" => "N"
  ),
  false,
  array(
  "ACTIVE_COMPONENT" => "Y"
  )
);?>


<!-- Блок "На обсуждение" -->

<?$APPLICATION->IncludeComponent("bitrix:news.list", "table", array(
  "IBLOCK_TYPE" => "public",
  "IBLOCK_ID" => "5",
  "NEWS_COUNT" => "3",
  "SORT_BY1" => "ACTIVE_FROM",
  "SORT_ORDER1" => "DESC",
  "SORT_BY2" => "SORT",
  "SORT_ORDER2" => "ASC",
  "FILTER_NAME" => "",
  "FIELD_CODE" => array(
    0 => "",
    1 => "",
  ),
  "PROPERTY_CODE" => array(
    0 => "",
    1 => "EXPERT",
    2 => "",
  ),
  "CHECK_DATES" => "Y",
  "DETAIL_URL" => "/documents/#ID#.php",
  "AJAX_MODE" => "N",
  "AJAX_OPTION_SHADOW" => "N",
  "AJAX_OPTION_JUMP" => "N",
  "AJAX_OPTION_STYLE" => "N",
  "AJAX_OPTION_HISTORY" => "N",
  "CACHE_TYPE" => "A",
  "CACHE_TIME" => "36000000",
  "CACHE_FILTER" => "N",
  "CACHE_GROUPS" => "N",
  "PREVIEW_TRUNCATE_LEN" => "",
  "ACTIVE_DATE_FORMAT" => "d.m.Y",
  "SET_TITLE" => "N",
  "SET_STATUS_404" => "N",
  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
  "ADD_SECTIONS_CHAIN" => "N",
  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
  "PARENT_SECTION" => "14",
  "PARENT_SECTION_CODE" => $_REQUEST["CODE"],
  "DISPLAY_TOP_PAGER" => "N",
  "DISPLAY_BOTTOM_PAGER" => "N",
  "PAGER_TITLE" => "",
  "PAGER_SHOW_ALWAYS" => "N",
  "PAGER_TEMPLATE" => "",
  "PAGER_DESC_NUMBERING" => "N",
  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
  "PAGER_SHOW_ALL" => "N",
  "DISPLAY_DATE" => "Y",
  "DISPLAY_NAME" => "Y",
  "DISPLAY_PICTURE" => "Y",
  "DISPLAY_PREVIEW_TEXT" => "Y",
  "AJAX_OPTION_ADDITIONAL" => ""
  ),
  false
);?> 
    

<!-- Блок "Есть мнение" -->
<?$APPLICATION->IncludeComponent("bitrix:news.list", "opinion", Array(
  "IBLOCK_TYPE" => "public",  // Тип информационного блока (используется только для проверки)
  "IBLOCK_ID" => "2", // Код информационного блока
  "NEWS_COUNT" => "5",  // Количество новостей на странице
  "SORT_BY1" => "ACTIVE_FROM",  // Поле для первой сортировки новостей
  "SORT_ORDER1" => "DESC",  // Направление для первой сортировки новостей
  "SORT_BY2" => "SORT", // Поле для второй сортировки новостей
  "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
  "FILTER_NAME" => "",  // Фильтр
  "FIELD_CODE" => array(  // Поля
    0 => "",
    1 => "",
  ),
  "PROPERTY_CODE" => array( // Свойства
    0 => "",
    1 => "EXPERT",
    2 => "",
  ),
  "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
  "DETAIL_URL" => "/opinion/#ID#.php",  // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
  "AJAX_MODE" => "N", // Включить режим AJAX
  "AJAX_OPTION_SHADOW" => "N",  // Включить затенение
  "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
  "AJAX_OPTION_STYLE" => "N", // Включить подгрузку стилей
  "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
  "CACHE_TYPE" => "A",  // Тип кеширования
  "CACHE_TIME" => "36000000", // Время кеширования (сек.)
  "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
  "CACHE_GROUPS" => "N",  // Учитывать права доступа
  "PREVIEW_TRUNCATE_LEN" => "", // Максимальная длина анонса для вывода (только для типа текст)
  "ACTIVE_DATE_FORMAT" => "d.m.Y",  // Формат показа даты
  "SET_TITLE" => "N", // Устанавливать заголовок страницы
  "SET_STATUS_404" => "N",  // Устанавливать статус 404, если не найдены элемент или раздел
  "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
  "ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
  "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
  "PARENT_SECTION" => "", // ID раздела
  "PARENT_SECTION_CODE" => $_REQUEST["CODE"], // Код раздела
  "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
  "DISPLAY_BOTTOM_PAGER" => "N",  // Выводить под списком
  "PAGER_TITLE" => "",  // Название категорий
  "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
  "PAGER_TEMPLATE" => "", // Название шаблона
  "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
  "PAGER_SHOW_ALL" => "N",  // Показывать ссылку "Все"
  "DISPLAY_DATE" => "Y",  // Выводить дату элемента
  "DISPLAY_NAME" => "Y",  // Выводить название элемента
  "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
  "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
  "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
  ),
  false
);?> 

<!-- Блок "Задать вопрос" -->   
    <div class="question_ban">    
      <a href="/question/"><img src="/images/question.gif" border="0"></a>
    </div>
    
<!-- Блок "F.A.Q." -->

<?$APPLICATION->IncludeComponent("bitrix:news.list", "faq", array(
  "IBLOCK_TYPE" => "public",
  "IBLOCK_ID" => "6",
  "NEWS_COUNT" => "3",
  "SORT_BY1" => "ACTIVE_FROM",
  "SORT_ORDER1" => "DESC",
  "SORT_BY2" => "SORT",
  "SORT_ORDER2" => "ASC",
  "FILTER_NAME" => "",
  "FIELD_CODE" => array(
    0 => "",
    1 => "",
  ),
  "PROPERTY_CODE" => array(
    0 => "",
    1 => "",
    2 => "",
  ),
  "CHECK_DATES" => "Y",
  "DETAIL_URL" => "/faq/#ID#.php",
  "AJAX_MODE" => "N",
  "AJAX_OPTION_SHADOW" => "N",
  "AJAX_OPTION_JUMP" => "N",
  "AJAX_OPTION_STYLE" => "N",
  "AJAX_OPTION_HISTORY" => "N",
  "CACHE_TYPE" => "A",
  "CACHE_TIME" => "36000000",
  "CACHE_FILTER" => "N",
  "CACHE_GROUPS" => "N",
  "PREVIEW_TRUNCATE_LEN" => "",
  "ACTIVE_DATE_FORMAT" => "d.m.Y",
  "SET_TITLE" => "N",
  "SET_STATUS_404" => "N",
  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
  "ADD_SECTIONS_CHAIN" => "N",
  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
  "PARENT_SECTION" => "",
  "PARENT_SECTION_CODE" => $_REQUEST["CODE"],
  "DISPLAY_TOP_PAGER" => "N",
  "DISPLAY_BOTTOM_PAGER" => "N",
  "PAGER_TITLE" => "",
  "PAGER_SHOW_ALWAYS" => "N",
  "PAGER_TEMPLATE" => "",
  "PAGER_DESC_NUMBERING" => "N",
  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
  "PAGER_SHOW_ALL" => "N",
  "DISPLAY_DATE" => "Y",
  "DISPLAY_NAME" => "Y",
  "DISPLAY_PICTURE" => "Y",
  "DISPLAY_PREVIEW_TEXT" => "Y",
  "AJAX_OPTION_ADDITIONAL" => ""
  ),
  false
);?> 
  

  </div>
    
  <div class="center_column">
<?if ($APPLICATION->GetDirProperty("show_title")=="true"):?>
    <div class="content_title">
      <h1><?$APPLICATION->ShowTitle()?></h1>
    </div>  
<?endif;?>

