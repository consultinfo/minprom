
  </div>
  <div class="right_column">
    
  
<!-- Блок "Календарь событий" -->


<?
$show=$APPLICATION->GetDirProperty("show_calendar");
if ($show !="dont_show") {

$APPLICATION->IncludeComponent(
  "bitrix:news.list", 
  "calendar", 
  array(
    "IBLOCK_TYPE" => "public",
    "IBLOCK_ID" => "8",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => "DESC",
    "SORT_BY2" => "SORT",
    "SORT_ORDER2" => "ASC",
    "FILTER_NAME" => "",
    "FIELD_CODE" => array(
      0 => "",
      1 => "",
    ),
    "PROPERTY_CODE" => array(
      0 => "",
      1 => "",
    ),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "/calendar/#ID#.php",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_SHADOW" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "N",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "",
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "SET_TITLE" => "N",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => $_REQUEST["CODE"],
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "PAGER_TITLE" => "",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "COMPONENT_TEMPLATE" => "calendar",
    "SET_BROWSER_TITLE" => "Y",
    "SET_META_KEYWORDS" => "Y",
    "SET_META_DESCRIPTION" => "Y",
    "INCLUDE_SUBSECTIONS" => "Y"
  ),
  false
);
}
?> 

  
<!-- Блок "Документы" -->
<?$APPLICATION->IncludeComponent("bitrix:news.list", ".default", array(
  "IBLOCK_TYPE" => "-",
  "IBLOCK_ID" => "5",
  "NEWS_COUNT" => "1",
  "SORT_BY1" => "ACTIVE_FROM",
  "SORT_ORDER1" => "DESC",
  "SORT_BY2" => "SORT",
  "SORT_ORDER2" => "ASC",
  "FILTER_NAME" => "",
  "FIELD_CODE" => array(
    0 => "",
    1 => "",
  ),
  "PROPERTY_CODE" => array(
    0 => "",
    1 => "",
  ),
  "CHECK_DATES" => "N",
  "DETAIL_URL" => "/documents/#ID#.php",
  "AJAX_MODE" => "N",
  "AJAX_OPTION_SHADOW" => "N",
  "AJAX_OPTION_JUMP" => "N",
  "AJAX_OPTION_STYLE" => "N",
  "AJAX_OPTION_HISTORY" => "N",
  "CACHE_TYPE" => "A",
  "CACHE_TIME" => "36000000",
  "CACHE_FILTER" => "N",
  "CACHE_GROUPS" => "N",
  "PREVIEW_TRUNCATE_LEN" => "",
  "ACTIVE_DATE_FORMAT" => "d.m.Y",
  "SET_TITLE" => "N",
  "SET_STATUS_404" => "N",
  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
  "ADD_SECTIONS_CHAIN" => "N",
  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
  "PARENT_SECTION" => "",
  "PARENT_SECTION_CODE" => $_REQUEST["CODE"],
  "DISPLAY_TOP_PAGER" => "N",
  "DISPLAY_BOTTOM_PAGER" => "N",
  "PAGER_TITLE" => "",
  "PAGER_SHOW_ALWAYS" => "N",
  "PAGER_TEMPLATE" => "",
  "PAGER_DESC_NUMBERING" => "N",
  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
  "PAGER_SHOW_ALL" => "N",
  "DISPLAY_DATE" => "Y",
  "DISPLAY_NAME" => "Y",
  "DISPLAY_PICTURE" => "Y",
  "DISPLAY_PREVIEW_TEXT" => "Y",
  "AJAX_OPTION_ADDITIONAL" => ""
  ),
  false,
  array(
  "ACTIVE_COMPONENT" => "N"
  )
);?> 

  </div>

  
  <div class="clear"></div>
  <div class="footer">
    <div class="footer1">
      <img src="/images/blind.gif" width="1" height="1">
    </div>
    <div class="footer2">
      <div class="partners_banners">
        <a href="#"><img src="/images/banner1.gif" class="partners_banner"></a>

        <a href="#"><img src="/images/banner3.gif" class="partners_banner"></a>
        <a href="#"><img src="/images/banner4.gif" class="partners_banner"></a>
        <a href="#"><img src="/images/banner5.gif" class="partners_banner"></a>
        <a href="#"><img src="/images/banner6.gif" class="partners_banner"></a>

<a href="http://gosuslugi.ru"><img src="/images/banner8.jpg" class="partners_banner" width="100"></a>
        <a href="http://www.acgrf.ru"><img src="/images/banner7.jpg" class="partners_banner" width="350"></a>
<a href="http://corpmsp.ru"><img src="/images/banner9.jpg" class="partners_banner" width="50%"></a>
      </div>
      <div class="partners_text">
        Управление судебного департамента в Калининградской области <a href="usn.kld.sudrf.ru" class="partners_link">usn.kld.sudrf.ru</a>
        Министерство юстиции Российской федерации<a href="www.minjust.ru" class="partners_link">www.minjust.ru</a>
        Российский федеральный центр судебной экспертизы <a href="www.sudexpert.ru" class="partners_link">www.sudexpert.ru</a>
        Уполномоченный по защите прав предпринимателей в Калининградской области <a href="http://ombudsmanbiz39.ru/" class="partners_link">www.ombudsmanbiz39.ru</a>
      </div>
    </div>    
    <div class="footer3">
<?$APPLICATION->IncludeFile("/edit_copyright.php", "php");?>
<?$APPLICATION->IncludeFile("/edit_phone.php", "php");?>
      <div class="developed">
        <p>Обслуживание сайта</p><p> <a href="http://diez.io">#diez technologies</a> </p>
      <a href="http://cursormedia.info/"><img src="/images/cursormedia.gif"></a>

      </div>
    </div>
    
  </div>
</td>
<td class="shadow_right">
</td>
</tr>
</table>
<script type="text/javascript">
               (function(d, t, p) {
                   var j = d.createElement(t); j.async = true; j.type = "text/javascript";
                   j.src = ("https:" == p ? "https:" : "http:") + "//stat.sputnik.ru/cnt.js";;
                   var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
               })(document, "script", document.location.protocol);
           </script>

</body>
</html>
