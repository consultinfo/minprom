


$(document).ready(function () {

    var iblock_name = '';

    function change_iblock_name() {
        iblock_name =
            $('input[name="PROPERY[8][0]"]').val() + "_" + //тематика
            $('input[name="PROPERY[9][0]"]').val() + "_" + //фамилия
            $('input[name="PROPERY[10][0]"]').val();        //имя
            $('input[name="PROPERY[NAME][0]"]').val(iblock_name.trim());
    }

    function set_for_email(){
        $('.address_label').text('Адрес');
        $('.address_block  input').prop('required', false);
        $('input[name="PROPERY[13][0]"]').prop('required', true);
        $('div.n7 .val').text('Электронная почта*');
    }

    function set_fot_mail(){
        $('.address_label').text('Адрес*');
        $('.address_block input').prop('required', true);
        $('div.n7 input').prop('required', false);
        $('div.n7 .val').text('Электронная почта');
    }

    //При изменении тематики
    $('input[name="PROPERY[8][0]"], input[name="PROPERY[9][0]"]  ,input[name="PROPERY[10][0]"] ').on('keyup', function () {
        change_iblock_name();
    })

    $('div.n6 > select').on('mouseup', function () {
        var val = $('option:selected').val();
        if (val == 7) set_for_email();//в электронном
        if (val == 6) set_fot_mail();//в письменном
    })
});