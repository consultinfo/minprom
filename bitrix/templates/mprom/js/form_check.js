/*
Проверка данных формы
Для того, чтобы поле прошло проверку,
нужно добавить его в массив GN_CheckFields
формат: GN_CheckFields[поле_формы]=[название_поля,тип_проверки,минимальная длина поля,0|1];
если последний параметр равен 1 - значит, поле необязательное для заполнения, но если заполнено - произвести провеку
типы проверок:
not_empty - не пустое поле (по умолчанию)
nums_only - только числа
phone - проверка на телефонный номер (пробелы, цифры, дефис, запятые)
email - проверка на e-mail
*/

var GN_ErrorTemplateEmpty="Поле \"%err_title%\" не заполнено!";
var GN_ErrorTemplateNums="В поле \"%err_title%\" могут быть только цифры!";
var GN_ErrorTemplateEmail="Поле \"%err_title%\" заполнено с ошибкой!";
var GN_ErrorTemplatePhone="В поле \"%err_title%\" должны быть только цифры, запятые, пробелы и дефис!";

/* ******************************** */

Function.prototype.isFunction=true;

function GN_Error(obj,err_title,err_template)
{
	//if(!err_template) err_template=GN_ErrorTemplateEmpty;
	alert(err_template.replace("%err_title%",err_title));
	obj.focus();
}

function GN_Check_not_empty(obj,err_title)
{
	if(obj.value.length==0)
	{
		GN_Error(obj,err_title,GN_ErrorTemplateEmpty);
		return false;
	}
	return true;
}

function GN_Check_email(obj,err_title)
{
	if(GN_Check_not_empty(obj,err_title))
	{
		var re=new RegExp("^[\\w-\\.]+\\@[\\w\\.]+\\.[A-Za-z]{2,4}$");
		var re2=new RegExp("\\.{2,}");
		if(re.test(obj.value) && !re2.test(obj.value)){return true;}
		else{GN_Error(obj,err_title,GN_ErrorTemplateEmail);}
	}
	return false;
}

function GN_Check_nums_only(obj,err_title)
{
	if(GN_Check_not_empty(obj,err_title))
	{
		var re=new RegExp("[^0-9]+");
		if(!re.test(obj.value)){return true;}
		else{GN_Error(obj,err_title,GN_ErrorTemplateNums);}
	}
	return false;
}

function GN_Check_phone(obj,err_title)
{
	if(GN_Check_not_empty(obj,err_title))
	{
		var re=new RegExp("[^0-9\\s\\,\\-]+");
		if(!re.test(obj.value)){return true;}
		else{GN_Error(obj,err_title,GN_ErrorTemplatePhone);}
	}
	return false;
}

function GN_Check_length_exact(obj,err_title,ln)
{
	if(GN_Check_not_empty(obj,err_title))
	{
		if(obj.value.length==ln){return true;}
		GN_Error(obj,err_title,"Text field \"%err_title%\" must content more "+ln+" symbols!");
	}
	return false;
}

function GN_Check_length(obj,err_title,ln)
{
	ln=Math.abs(ln);
	if(GN_Check_not_empty(obj,err_title))
	{
		if(obj.value.length>=ln){return true;}
		GN_Error(obj,err_title,"Text field  \"%err_title%\" must content more "+ln+" symbols!");
	}
	return false;
}

function GN_CheckForm()
{
	var GN_Obj;
	var GN_Err=0;
	var GN_FieldRef;
	var GN_Func;
	for(var GN_Field in GN_CheckFields)
	{
		GN_FieldRef=GN_CheckFields[GN_Field];
		
		if(GN_FormReference[GN_Field]!=null)
		{
			GN_Obj=GN_FormReference[GN_Field];
			if((GN_FieldRef[3]==1 && GN_Obj.value.length>0) || !GN_FieldRef[3])
			{
				//check length
				if(GN_FieldRef[2] && (GN_FieldRef[2]>0 && !GN_Check_length_exact(GN_Obj,GN_FieldRef[0],GN_FieldRef[2])) || (GN_FieldRef[2]<0 && !GN_Check_length(GN_Obj,GN_FieldRef[0],GN_FieldRef[2]))) return false;
				
				//perform check
				if(!GN_FieldRef[1]) GN_FieldRef[1]="not_empty";
				GN_Func=eval("GN_Check_"+GN_FieldRef[1]);
				if(GN_Func.isFunction && !GN_Func(GN_Obj,GN_FieldRef[0])) return false;
			}
		}
	}
	return true;
}
