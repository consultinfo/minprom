<?
/*
You can place here your functions and event handlers

AddEventHandler("module", "EventName", "FunctionName");
function FunctionName(params)
{
	//code
}
*/

function setProp($prop){
    if (strlen(trim($prop))>0) return $prop;
        else return 'Отсутствует';
}


AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("MyAnswer", "OnAfterIBlockElementAddHandler"));
class MyAnswer
{

    function OnAfterIBlockElementAddHandler(&$arFields)
    {

        CModule::IncludeModule('iblock');

        if ($arFields["IBLOCK_ID"] == 9) // здесь ID инфоблока
        {


            $arEventFields = array(
                "AUTHOR" => $arFields['PROPERTY_VALUES'][9]." ".$arFields['PROPERTY_VALUES'][10]." ".$arFields['PROPERTY_VALUES'][11],
                "AUTHOR_EMAIL" => setProp($arFields['PROPERTY_VALUES'][13]),
                "TEXT" => $arFields['PROPERTY_VALUES'][16],
                "THEME" => $arFields['PROPERTY_VALUES'][8],
                "ADDRESS" => setProp($arFields['PROPERTY_VALUES'][17]." ".$arFields['PROPERTY_VALUES'][18]." ".$arFields['PROPERTY_VALUES'][19]),
                "EMAIL_FROM" => "noreply@ru",
                "PHONE" => setProp($arFields['PROPERTY_VALUES'][14]),
                "RESTYPE" => $arFields['PROPERTY_VALUES'][12]==7?"В электронном виде":"В письменном виде",
                "EMAIL_TO" => "",
                "ID" => $arFields['ID']

            );

            if(CModule::IncludeModule('iblock'))
            {
                $rsElement = CIBlockElement::GetList(
                    array(),
                    array("ID" => $arFields['ID'], "IBLOCK_ID" => $arFields["IBLOCK_ID"]),
                    false,
                    false,
                    array("PROPERTY_files")
                );
                $files = [];
                while($arElement = $rsElement->Fetch())
                {
                    $arFile = CFile::GetFileArray($arElement["PROPERTY_FILES_VALUE"]);
                    $files[] = $arFile["SRC"];
                }
            }


            CEvent::Send('FEEDBACK_FORM', 's1', $arEventFields, "N", "", $files); // s1 - ID сайта

        }
    }
}

?>