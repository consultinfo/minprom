$(document).ready(function() {
	$(document).on('click', '#_tabs a', function (e) {
        var id;
        id = $(this).data("id");
        if(id == 0){
            id = "main";
        }
        page_num = 1;
        
        ajax_section(id,news_count,page_num,true,true);
	});

    $(document).on('click', '.mm_pager-button._next', function (e) {
        e.preventDefault();
        var offset = _anchor.offsetTop;
        page_num++;
        
        ajax_section(cur_id,news_count,page_num,true,true);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('click', '.mm_pager-button._prev', function (e) {
        e.preventDefault();
        var offset = _anchor.offsetTop;
        page_num--;
        
        ajax_section(cur_id,news_count,page_num,true,true);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('change', '#_select', function (e) {
        var select_val;
        var offset = _anchor.offsetTop;
        select_val = $(this).val();
        if(cur_id == 0){
            cur_id = "main";
        }
        
        ajax_section(cur_id,news_count,select_val,true,true);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('change', '#_tabs-select', function (e) {
        var select_val;
        select_val = $(this).val();
        if(select_val == 0){
            select_val = "main";
        }

        ajax_section(select_val,news_count,page_num,true,true);
    });

    $(document).on('click', '#mm_button_add', function (e) {
        
        news_count = news_count + news_count_option;

        ajax_section(cur_id,news_count,page_num,true,true);
    });

    $(document).ready(function() {
        $('input[type=radio][name=_options]').change(function() {
            var how = this.value;
            if(cur_id == 0){
                cur_id = "main";
            }
            
            ajax_section(cur_id,news_count,page_num,true,true,how);
        });
    });
    
	function ajax_section(id, news_count, page_num, print_page, fast_request, how) {
        print_page = print_page || false;
        fast_request = fast_request || false;

        if(how === undefined){
            how = $("[name=_options]:checked").val();
        }
        
        var q =  $('#search').val();
        $.ajax({
            url: page,
            method: "post",
            data: {
                AJAX: "Y",
                ID_IBLOCK: id,
                NEWS_COUNT: news_count,
                PAGEN_1: page_num,
                q: q,
                F_REQUEST: fast_request,
                how: how
            },
            beforeSend: function () {
                $('#wrp').fadeOut(200);
            },
            error: function () {
                alert("Ошибка запроса");
            },
            success: function (result) {
                $('#wrp').html(result).fadeIn(200);
                $(".mm_select select").styler();
                
                if(id != "" && print_page === false){   
                    window.history.pushState(null, null, "/search/");
                }
                if(id != "" && print_page === true){
                    window.history.pushState(null, null, "/search/" + "?how=" + how + "&q=" + q + "&PAGEN_1=" + page_num + "&ID_IBLOCK=" + id);
                }
            }
        })
    }
    
});