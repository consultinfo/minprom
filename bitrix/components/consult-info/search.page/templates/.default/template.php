<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="mm_search">
    <div class="mm_search-header">
        <form action="" class="mm_default-form" method="get">
            <input type="hidden" name="tags" value="<?echo $arResult["REQUEST"]["TAGS"]?>" />
            <input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
            <div class="mm_search-header__result">
                <span class="mm_value">
                    <?if(!empty($arResult["REQUEST"]["QUERY"])):?>
                        Найдено <b><?if(is_object($arResult["NAV_RESULT"])):?><?=$arResult["ALL"]?><?else:?>0<?endif;?></b> результатов
                    <?endif;?>
                </span>
            </div>
            <div class="mm_search-header__bar">
                <div class="mm_input">
                    <input id="search" class="search-query" type="search" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" />
                </div>
                <button type="submit" class="mm_search-header__button">
                    <span class="i _zoom _dark"></span>
                </button>
            </div>
            <?if($arParams["SHOW_ORDER_BY"] != "N" && !empty($arResult["REQUEST"]["QUERY"])):?>
                 <div class="mm_search-sort">
                    <div class="mm_search-sort__item">
                        <div class="mm_control">
                            <label class="mm_control__label">
                                <input type="radio" class="mm_control__input" name="_options" value="d" <?if($arResult["REQUEST"]["HOW"]=="d" || $_REQUEST["how"] == "d"):?>checked<?endif;?>>
                                <span class="mm_control__container">
                                    <span class="mm_control__solid"><?=GetMessage("CT_BSP_ORDER_BY_DATE")?></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="mm_search-sort__item">
                        <div class="mm_control">
                            <label class="mm_control__label">
                                <input type="radio" class="mm_control__input" value="r" name="_options" <?if($arResult["REQUEST"]["HOW"]=="r" || $_REQUEST["how"] == "r"):?>checked<?endif;?>>
                                <span class="mm_control__container">
                                    <span class="mm_control__solid"><?=GetMessage("CT_BSP_ORDER_BY_RANK")?></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
		<?endif;?>
        </form>
    </div>
    <div class="mm_section" id="_anchor">
        <?if(!empty($arResult["REQUEST"]["QUERY"])):?>
            <?if(count($arResult["CATEGORIES"]["IBLOCK"])>0):?>
                <header class="mm_section-header">
                    <div class="mm_tabs _767">
                        <ul class="mm_tabs-list" id="_tabs">
                            <?foreach ($arResult["CATEGORIES"]["IBLOCK"] as $key => $arCat):?>
                                <li class="mm_tabs-list__item">
                                    <a href="javascript:void(0);" class="mm_tabs-list__link<?if($arCat["CURRENT"] == "Y"):?> _active<?endif;?>" data-id="<?=$arCat["ID"]?>"><?=$arCat["NAME"]?></a>
                                </li>
                           <?endforeach;?>
                        </ul>
                        <div class="mm_tabs-select">
                            <select id="_tabs-select">
                                <?foreach ($arResult["CATEGORIES"]["IBLOCK"] as $arCat):?>
                                    <option value="<?=$arCat["ID"]?>"<?if($arCat["CURRENT"] == "true"):?> selected<?endif;?>><?=$arCat["NAME"]?></option>
                               <?endforeach;?>
                            </select>
                        </div>
                    </div>
                </header>
            <?endif;?>
        <?endif;?>
        <div class="mm_section-body" id="wrp">
            <?if ($_REQUEST["AJAX"] == "Y") $APPLICATION->RestartBuffer();?>
            <?if(!empty($arResult["REQUEST"]["QUERY"])):?> 
                <?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
                    ?>
                    <?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
                <?endif;?>
                <?if(count($arResult["SEARCH"])>0 && $arResult["ALL"] != 0):?>
                    <div class="mm_tabs-container">
                        <div class="mm_tabs-pane _active">
                            <?foreach($arResult["SEARCH"] as $arItem):?>
                            <div class="mm_search-unit">
                                <div class="mm_search-unit__title">
                                    <a href="<?=$arItem["PARAM1"] == "public" ? $arItem["URL"]."&ext_news=y" : $arItem["URL"]?>"><?=$arItem["TITLE_FORMATED"]?></a>
                                </div>
                                <div class="mm_search-unit__preview">
                                    <p><?=$arItem["BODY_FORMATED"]?></p>
                                </div>
                                <div class="mm_search-unit__date">
                                    <span class="mm_value"><?=FormatDate("f j, Y",MakeTimeStamp($arItem["FULL_DATE_CHANGE"]))?></span>
                                </div>
                            </div>
                            <?endforeach;?>
                        </div>
                        <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
                    </div>
                <?else:?>
                    <p>По вашему запросу — <b><?=$arResult["REQUEST"]["QUERY"]?></b> — не соответствует никаким документам.</p>
                    <p>Попробуйте сделать следующее:</p>
                    <ul>
                        <li>Убедитесь, что все слова написаны правильно;</li>
                        <li>Попробуйте использовать разные ключевые слова;</li>
                        <li>Попробуйте использовать более общие ключевые слова.</li>
                    </ul>
                <?endif;?>
                <script>
                    var cur_id = <?=$arResult["CATEGORIES"]["CUR_ID"]?>;
                    var page_num = <?=(int) $arResult['NAV_RESULT']->NavPageNomer?>;
                </script>
                <?if ($_REQUEST["AJAX"] == "Y") die;?>
            <?endif;?>
        </div>
    </div>
</div>
<script>
    var page = "<?=$APPLICATION->GetCurPage(false)?>";
    var news_count = <?=(int) $arParams["PAGE_RESULT_COUNT"]?>;
    var news_count_option = <?=(int) $arParams["PAGE_RESULT_COUNT"]?>;
</script>