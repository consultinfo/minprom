<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<header class="mm_section-header">
    <div class="mm_tabs _480" id="_anchor">
        <ul id="_tabs" class="mm_tabs-list">
            <?foreach ($arResult["SECTIONS"] as $key => $sTabs):?>
                <li class="mm_tabs-list__item<?=$sTabs["CURRENT"] == "Y" ? " _active" : ""?>">
                    <a data-code="<?=$sTabs["CODE"]?>" href="javascript:void(0);" class="mm_tabs-list__link<?=$sTabs["CURRENT"] == "Y" ? " _active" : ""?>"><?=$sTabs["NAME"]?></a>
                </li>
            <?endforeach;?>
        </ul>
        <div class="mm_tabs-select">
            <select id="_tabs-select">
                <?foreach ($arResult["SECTIONS"] as $key => $sTabs):?>
                    <option value="<?=$sTabs["CODE"]?>" <?=$sTabs["CURRENT"] == "Y" ? "selected" : ""?>><?=$sTabs["NAME"]?></option>
                <?endforeach;?>
            </select>
        </div>
    </div>
</header>
<div class="mm_section-body" id="wrp">
    <?if ($_REQUEST["AJAX"] == "Y") $APPLICATION->RestartBuffer();?>
    <div class="mm_tabs-container">
        <div class="mm_tabs-pane _active">
            <?if(!empty($arResult["ITEMS"])):?>
                <div class="mm_docs_list _important">
                    <?foreach ($arResult["ITEMS"] as $arItem):
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="mm_docs_list-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                           <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="mm_docs_list-item__link"><?=$arItem["NAME"]?></a>
                            <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                                <div class="mm_docs_list-item__date">
                                    <span class="mm_value"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                                </div>
                           <?endif;?>
                        </div>
                    <?endforeach;?>
                </div>
            <?else:?>
                <p><?=GetMessage('NO_ITEMS')?></p>
            <?endif;?>
        </div>
    </div>
    <?=$arResult["NAV_STRING"]?>
    <script>
        var section_code = <?=CUtil::PhpToJSObject($arResult["SECTION"]["PATH"][0]["CODE"])?>;
        var page_num = <?=$arResult['NAV_RESULT']->NavPageNomer?>;
    </script>
    <?if ($_REQUEST["AJAX"] == "Y") die;?>
</div>
<script>
    var page = "<?=$APPLICATION->GetCurPage(false)?>";
    var news_count = <?=$arParams["NEWS_COUNT"]?>;
    var news_count_option = <?=$arParams["NEWS_COUNT"]?>;
    var list_page_url = "<?=$arResult["SECTION"]["PATH"][0]["LIST_PAGE_URL"]?>";
</script>