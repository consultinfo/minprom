$(document).ready(function() {
    $(document).on('click', '#_tabs a', function (e) {
        var section_code_select;
        section_code_select = $(this).data("code");

        ajax_section(section_code_select);
    });

    $(document).on('change', '#_tabs-select', function (e) {
        var select_val;
        select_val = $(this).val();

        ajax_section(select_val);
    });

    function ajax_section(section_code) {
        $.ajax({
            url: page,
            method: "post",
            data: {
                AJAX: "Y",
                sect: section_code,
                DISPLAY_BOT_PAGER: "N",
                PAGER_SHOW_ALL: "Y"
            },
            beforeSend: function () {
                $('#wrp').fadeOut(200);
            },
            error: function () {
                alert("Ошибка запроса");
            },
            success: function (result) {
                $('#wrp').html(result).fadeIn(200);
            }
        })
    }
});