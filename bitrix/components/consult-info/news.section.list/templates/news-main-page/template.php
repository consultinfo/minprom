<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<header class="mm_section-header">
    <div class="mm_tabs _767" id="_anchor">
        <ul class="mm_tabs-list" id="_tabs">
            <?foreach ($arResult["SECTIONS"] as $key => $sTabs):?>
                <li class="mm_tabs-list__item<?=$sTabs["CURRENT"] == "Y" ? " _active" : ""?>">
                    <a data-id="<?=$sTabs["ID"]?>" data-code="<?=$sTabs["CODE"]?>" href="javascript:void(0);" class="mm_tabs-list__link<?=$sTabs["CURRENT"] == "Y" ? " _active" : ""?>"><?=$sTabs["NAME"]?></a>
                </li>
            <?endforeach;?>
        </ul>
        <div class="mm_tabs-select">
            <select id="_tabs-select">
                <?foreach ($arResult["SECTIONS"] as $key => $sTabs):?>
                    <option value="<?=$sTabs["CODE"]?>" <?=$sTabs["CURRENT"] == "Y" ? "selected" : ""?>><?=$sTabs["NAME"]?></option>
                <?endforeach;?>
            </select>
        </div>
    </div>
</header>
<div class="mm_section-body" id="wrp">
    <div class="mm_news_list">
        <?foreach ($arResult["ITEMS"] as $key => $arItem):
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="mm_news_list-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="mm_news_list-item__link">
                    <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                        <span class="mm_news_list-item__image">
                               <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>">
                        </span> 
                    <?endif;?>
                    <span class="mm_news_list-item__details">
                       <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                           <?if(!empty($arParams["EVENT_SECTION_CODE"]) && $arParams["EVENT_SECTION_CODE"] == $arResult["SECTION"]["PATH"][0]["CODE"]):?>
                               <span class="mm_news_list-item__label"><?=FormatDate("d F",MakeTimeStamp($arItem["ACTIVE_FROM"]));?></span>
                               <span class="mm_value"><?=$arItem["NAME"]?></span>
                           <?else:?>
                               <span class="mm_value"><?=$arItem["NAME"]?></span>
                               <span class="mm_news_list-item__date"><?=$arItem["DISPLAY_ACTIVE_FROM"];?></span>
                           <?endif;?>
                       <?endif;?>
                    </span>
                </a>
            </div>
        <?endforeach;?>
    </div>
    <?if($arParams["PAGER_SHOW_ALL"] == "Y"):?>
        <div class="mm_all-entries">
            <a href="<?=$arResult["SECTION"]["PATH"][0]["LIST_PAGE_URL"]?>?sect=<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>">Все новости</a>
        </div>
    <?endif;?>
</div>
<script>
    var page = "<?=$arResult["SECTION"]["PATH"][0]["LIST_PAGE_URL"]?>";
</script>
