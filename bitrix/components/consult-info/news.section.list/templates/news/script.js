$(document).ready(function() {
    $(document).on('click', '#_tabs a', function (e) {
        var section_code_select;
        section_code_select = $(this).data("code");
        page_num = 1;

        ajax_section(section_code_select,news_count,page_num,true,list_page_url);
    });

    $(document).on('change', '#_select', function (e) {
        var select_val;
        var offset = _anchor.offsetTop;
        select_val = $(this).val();

        ajax_section(section_code,news_count,select_val,true,list_page_url);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('click', '#mm_button_add', function (e) {
        news_count = news_count + news_count_option;

        ajax_section(section_code,news_count,page_num,true,list_page_url);
    });

    $(document).on('click', '.mm_pager-button._next', function (e) {
        e.preventDefault();
        var offset = _anchor.offsetTop;
        page_num++;

        ajax_section(section_code,news_count,page_num,true,list_page_url);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('click', '.mm_pager-button._prev', function (e) {
        e.preventDefault();
        var offset = _anchor.offsetTop;
        page_num--;

        ajax_section(section_code,news_count,page_num,true,list_page_url);
        $(".mm_website-wrapper").animate({ scrollTop: offset}, "slow");
    });

    $(document).on('change', '#_tabs-select', function (e) {
        var select_val;
        select_val = $(this).val();

        ajax_section(select_val,news_count,page_num,true,list_page_url);
    });

    function ajax_section(section_code,news_count,page_num,print_page,list_page_url) {
        print_page = print_page || false;
        $.ajax({
            url: page,
            method: "post",
            data: {
                AJAX: "Y",
                sect: section_code,
                NEWS_COUNT: news_count,
                PAGEN_1: page_num
            },
            beforeSend: function () {
                $('#wrp').fadeOut(200);
            },
            error: function () {
                alert("Ошибка запроса");
            },
            success: function (result) {
                $('#wrp').html(result).fadeIn(200);
                $(".mm_select select").styler();

                if(section_code != "" && print_page === false){
                    window.history.pushState(null, null, list_page_url);
                }
                if(section_code != "" && print_page === true){
                    window.history.pushState(null, null, list_page_url + "?sect=" + section_code + "&PAGEN_1=" + page_num);
                }
            }
        })
    }

});