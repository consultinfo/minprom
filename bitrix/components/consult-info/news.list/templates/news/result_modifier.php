<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$rows = array();
$groups = array();
$arResult["SECTIONS"] = array();

$result = Bitrix\Iblock\SectionElementTable::getList(array(
    'filter' => array('IBLOCK_SECTION.IBLOCK_ID' => $arParams["IBLOCK_ID"] ,'IBLOCK_SECTION.ACTIVE' => "Y"),
    'select' => array('NAME' => 'IBLOCK_SECTION.NAME', 'CODE' => 'IBLOCK_SECTION.CODE', 'ELEMENT_ID' => 'IBLOCK_ELEMENT_ID', 'SECTION_ID' => 'IBLOCK_SECTION.ID' , 'SORT' => 'IBLOCK_SECTION.SORT')
));
while ($row = $result->fetch())
{
    $groups[$row["ELEMENT_ID"]][] = $row["SECTION_ID"];
    $arResult["SECTIONS"][$row["SECTION_ID"]] = $row;
}

foreach($arResult["ITEMS"] as $key => $item){
    
    if(!empty($groups[$item["ID"]])){
        $arResult["ITEMS"][$key]["GROUPS"] = $groups[$item["ID"]];
        $item["GROUPS"] = $groups[$item["ID"]];
    }
    
    if($item["IBLOCK_TYPE_ID"] == $arParams["IBLOCK_EXT_TYPE"]) {
        /**Если раздел Событие с сайта S1*/
        if ($item["IBLOCK_SECTION_ID"] == 40) {
            $item["GROUPS"][0] = 39;
        }
        /**Если раздел Общество или Экономика с сайта S1*/
        if ($item["IBLOCK_SECTION_ID"] == 2 || $item["IBLOCK_SECTION_ID"] == 3) {
            $item["GROUPS"][0] = 17;
        }
    }

    if(in_array(39, $item["GROUPS"]) || in_array(40, $item["GROUPS"])) {
        $item["EVENT"] = "true";
    }

    if(!empty($item["IBLOCK_SECTION_ID"]) && !empty($item["GROUPS"])){
        foreach ($item["GROUPS"] as $group){
            if(count($arResult["SECTIONS"][$group]["ITEMS"]) >= $arParams["NEWS_LIMIT"]){
                continue;
            }
            $arResult["SECTIONS"][$group]["ITEMS"][$item["ID"]] = $item;

            if($item["IBLOCK_TYPE_ID"] == $arParams["IBLOCK_EXT_TYPE"]){
                $arResult["SECTIONS"][$group]["ITEMS"][$item["ID"]]["DETAIL_PAGE_URL"] .= "?ext_news=y";
            }
        }
    }
    
}
$arResult["SECTIONS"] = array_values($arResult["SECTIONS"]);
$this->__component->SetResultCacheKeys(array("SECTIONS"));
?>