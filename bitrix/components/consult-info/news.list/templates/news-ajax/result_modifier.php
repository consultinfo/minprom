<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$rows = array();
$arResult["SECTIONS"] = array();

$result = Bitrix\Iblock\SectionElementTable::getList(array(
    'filter' => array('IBLOCK_SECTION.IBLOCK_ID' => $arParams["IBLOCK_ID"] ,'IBLOCK_SECTION.ACTIVE' => "Y"),
    'select' => array('NAME' => 'IBLOCK_SECTION.NAME', 'CODE' => 'IBLOCK_SECTION.CODE', 'SORT' => 'IBLOCK_SECTION.SORT', 'ID' => 'IBLOCK_SECTION.ID')
));
while ($row = $result->fetch())
{
    $arResult["SECTIONS"][$row["ID"]] = $row;
}
foreach($arResult["ITEMS"] as $key => &$item){
    if($item["IBLOCK_SECTION_ID"] == 39 || $item["IBLOCK_SECTION_ID"] == 40 ){
        $item["EVENT"] = "true";
    }
    if($item["IBLOCK_TYPE_ID"] == $arParams["IBLOCK_EXT_TYPE"]){
        $item["DETAIL_PAGE_URL"] .= "?ext_news=y";
    }
}
?>