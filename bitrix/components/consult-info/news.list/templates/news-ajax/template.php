<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(empty($_REQUEST["SECTION_CODE"])) $_REQUEST["SECTION_CODE"] = $arParams["PARENT_SECTION_CODE"];
?>
    <header class="mm_section-header">
        <div class="mm_tabs _767">
            <ul class="mm_tabs-list" id="_tabs">
                <?foreach ($arResult["SECTIONS"] as $key => $sTabs):?>
                    <li class="mm_tabs-list__item<?=$_REQUEST["SECTION_CODE"] == $sTabs["CODE"] ? " _active" : ""?>">
                        <a data-id="<?=$sTabs["ID"]?>" data-code="<?=$sTabs["CODE"]?>" href="javascript:void(0);" class="mm_tabs-list__link<?=$_REQUEST["SECTION_CODE"] == $sTabs["CODE"] ? " _active" : ""?>"><?=$sTabs["NAME"]?></a>
                    </li>
                <?endforeach;?>
            </ul>
            <div class="mm_tabs-select">
                <select id="_tabs-select">
                    <?foreach ($arResult["SECTIONS"] as $key => $sTabs):?>
                        <option value="<?=$sTabs["CODE"]?>" <?=$key == 0 ? "selected" : ""?>><?=$sTabs["NAME"]?></option>
                    <?endforeach;?>
                </select>
            </div>
        </div>
    </header>
    <div class="mm_section-body" id="wrp">
        <?if ($_REQUEST["AJAX"] == "Y") $APPLICATION->RestartBuffer();?>
            <div class="mm_news_list">
                <?foreach ($arResult["ITEMS"] as $key => $arItem):?>
                    <div class="mm_news_list-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="mm_news_list-item__link">
                            <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                                <span class="mm_news_list-item__image">
                                       <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>">
                                </span>
                            <?endif;?>
                            <span class="mm_news_list-item__details">
                               <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                                   <?if($arItem["EVENT"] == "true"):?>
                                       <span class="mm_news_list-item__label"><?=FormatDate("d F",MakeTimeStamp($arItem["ACTIVE_FROM"]));?></span>
                                       <span class="mm_value"><?=$arItem["NAME"]?></span>
                                   <?else:?>
                                       <span class="mm_value"><?=$arItem["NAME"]?></span>
                                       <span class="mm_news_list-item__date"><?=$arItem["DISPLAY_ACTIVE_FROM"];?></span>
                                   <?endif;?>
                               <?endif;?>
                            </span>
                        </a>
                    </div>
                <?endforeach;?>
            </div>  
        <?=$arResult["NAV_STRING"]?>
        <?if ($_REQUEST["AJAX"] == "Y") die;?>
    </div>
<script>
    var page = "<?=$APPLICATION->GetCurPage(false)?>";
    var news_count = <?=CUtil::PhpToJSObject($arParams["NEWS_COUNT"])?>;
    var section_code = <?=CUtil::PhpToJSObject($arResult["SECTION"]["PATH"][0]["CODE"])?>;
    var page_num = <?=CUtil::PhpToJSObject($arResult['NAV_RESULT']->NavPageNomer)?>;
    var list_page_url = <?=$arResult["LIST_PAGE_URL"]?>;
</script>