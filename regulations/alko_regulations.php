<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Административные регламенты, алкогольная продукция");
?><p class="MsoNormal" style="text-align: left; margin-bottom: 0.0001pt;" align="center">
</p>
<div>
	<div style="text-align: center;">
		<span style="font-size: 9px;"><b>&nbsp;Ад­ми­нист­ра­тив­ные рег­ла­мен­ты</b></span>
	</div>
	<div>
 <br>
	</div>
</div>
<div>
	<div>
		<div>
			<ul style="text-align: justify;">
				<li><a href="/documents/admreg_licenz_control_alk_5.docx"><span style="color: #006bc6;">Проект приказа министерства по промышленной политике, развитию предпринимательства и торговли </span><strong style="color: #006bc6;">Об утверждении Административного регламента Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области по исполнению государственной функции по осуществлению лицензионного контроля за розничной продажей алкогольной продукции</strong></a></li>
			</ul>
			<div style="text-align: justify;">
 <br>
			</div>
			<ul style="text-align: justify;">
				<li><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_prodlenie.doc" style="color: #006bc6;">Приказ министерства по промышленной политике, развитию предпринимательства и торговли от 27 марта 2013 года № 16&nbsp;</a><strong style="color: #006bc6;"><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_prodlenie.doc" style="color: #006bc6;">Об утверждении Административного регламента Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области по предоставлению государственной услуги по продлению срока действия лицензии на осуществление розничной продажи алкогольной продукции</a></strong></li>
			</ul>
			<div style="text-align: justify;">
 <br>
			</div>
			<ul style="text-align: justify;">
				<li><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_pereoformlenie.doc" style="color: #006bc6;">Приказ министерства по промышленной политике, развитию предпринимательства и торговли от 27 марта 2013 года № 15&nbsp;</a><strong style="color: #006bc6;"><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_pereoformlenie.doc" style="color: #006bc6;">Об утверждении Административного регламента Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области по предоставлению государственной услуги по переоформлению лицензии на осуществление розничной продажи алкогольной продукции</a></strong></li>
			</ul>
			<div style="text-align: justify;">
 <br>
			</div>
			<ul style="text-align: justify;">
				<li><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_vipiski_reestr.doc" style="color: #006bc6;"><span style="color: #006bc6;">Приказ министерства по промышленной политике, развитию предпринимательства и торговли от 27 марта 2013 года № 14</span><span style="color: #006bc6;">&nbsp;</span></a><strong style="color: #006bc6;"><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_vipiski_reestr.doc" style="color: #006bc6;">Об утверждении Административного регламента Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области по предоставлению государственной услуги по выдаче выписок из реестра выданных, приостановленных и аннулированных лицензий на осуществление розничной продажи алкогольной продукции</a></strong></li>
			</ul>
			<div style="text-align: justify;">
 <br>
			</div>
			<ul style="text-align: justify;">
				<li><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_vidacha.doc" style="color: #006bc6;">Приказ министерства по промышленной политике, развитию предпринимательства и торговли от 17 ноября 2012 года № 77&nbsp;</a><strong style="color: #006bc6;"><a href="http://gov39.ru/biznesu/reglamenty/areglament_alk_2013_vidacha.doc" style="color: #006bc6;">Об утверждении Административного регламента Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области по предоставлению государственной услуги по выдаче лицензии на осуществление розничной продажи алкогольной продукции</a></strong></li>
			</ul>
		</div>
		<div>
			<p>
			</p>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>