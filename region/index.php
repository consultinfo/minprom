<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Справочник инвестора");
?> 
<div> 
  <p class="a" style="margin-bottom: 0.0001pt;">Площадь Калининградской области &ndash; 15,1 тыс. <span lang="X-NONE">км2</span>. Она является одной из самых маленьких территорий России, но по плотности населения (63 человека на <span lang="X-NONE">1 км2</span>) среди краев и областей она занимает третье место.</p>
 
  <p class="a" style="margin-bottom: 0.0001pt;"><span style="text-indent: 35.4pt;">Калининградская область является самым западным регионом Российской Федерации, полностью отделенным от остальной территории страны сухопутными границами иностранных государств и международными морскими водами. На севере и востоке на протяжении 280,5 км регион граничит с Литовской Республикой, на юге на протяжении 231,98 км – с Республикой Польшей, на западе область омывает на протяжении более 183 км Балтийское море. Максимальная протяженность области с востока на запад составляет 205 км, с севера на юг – 108 км. От Калининграда до польской границы всего 35 км, до литовской – 70 км. Ближайший областной центр России – Псков расположен в 800 км, от Калининграда до Москвы – 1208 км.</span></p>
 
  <p class="a" style="margin-bottom: 0.0001pt;">До многих европейских столиц расстояния сравнительно небольшие:<o:p></o:p></p>
 
  <p class="a" style="margin: 0cm 0cm 0.0001pt 36pt;">&middot;         до Вильнюса – 350 км;<o:p></o:p></p>
 
  <p class="a" style="margin: 0cm 0cm 0.0001pt 36pt;">·         до Варшавы – 400 км;<o:p></o:p></p>
 
  <p class="a" style="margin: 0cm 0cm 0.0001pt 36pt;">·         до Берлина – 625 км;<o:p></o:p></p>
 
  <p class="a" style="margin: 0cm 0cm 0.0001pt 36pt;">·         до Стокгольма – 650 км;<o:p></o:p></p>
 
  <p class="a" style="margin: 0cm 0cm 0.0001pt 36pt;">·         до Копенгагена – 680 км. <o:p></o:p></p>
 
  <p class="a" style="margin-bottom: 0.0001pt;"> 
    <br />
   </p>
 </div>
 <img src="/upload/medialibrary/1b7/geograficheskoe-pologenie.jpg" title="geograficheskoe-pologenie.jpg" border="0" alt="geograficheskoe-pologenie.jpg" width="500" height="297"  /> 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <div><img src="/upload/medialibrary/cc9/geograficheskoe-pologenie2.jpg" title="geograficheskoe-pologenie2.jpg" border="0" alt="geograficheskoe-pologenie2.jpg" width="500" height="360"  /></div>
 
  <div><span style="text-indent: 47px;"> 
      <br />
     </span></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b><font size="4" color="#438ccb">Преимущества инвестирования в Калининградскую область</font></b></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b>Тестовая площадка для вхождения на российский рынок</b><o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Калининград – реальная возможность отработки ведения бизнеса в российских условиях с последующим выходом на рынки основной территории России И Таможенного Союза.<o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b>Близость европейских рынков</b><o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Калининградская область имеет общую границу с двумя государствами ЕС: на севере и востоке – с Литовской Республикой, на юге – с Республикой Польша. Динамика внешнеторгового оборота Калининградской области как нельзя лучше отражает приграничное местоположение региона – с 1999 года произошло 10-кратное увеличение объема внешней торговли.<o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b>Доступность балтийских и атлантических морских путей</b><o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Порт Калининград - единственный незамерзающий российский порт на Балтике с причалами для Ро-Ро и пассажирских судов, железнодорожных паромов, генеральных и контейнерных грузов, с возможностью осуществления таможенных, пограничных и других обязательных процедур.<o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b>Значительный туристско-рекреационный потенциал</b><o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Мягкий климат и прекрасные природные ландшафты, развитая инфраструктура туризма и уникальные природные ресурсы края солнечного камня привлекает многочисленных гостей из регионов России и зарубежных стран. Увеличиваясь на 10-12 % ежегодно последние несколько лет, поток туристов в 2008 впервые превысил полмиллиона человек. <o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b>Растущий потенциал транспортной системы</b><o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Объемы оборота грузов в портовом и железнодорожном комплексе региона неуклонно росли. В рамках Федеральной целевой программы предусмотрено финансирование целого ряда инфраструктурных проектов в сфере авиационного, железнодорожного, морского и речного транспорта, нацеленных на дальнейшее развитие транзитного потенциала Калининградской области.<o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b>Поддержка инвесторов со стороны регионального правительства</b><o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Политика открытых дверей, проводимая региональными властями, направлена на поддержку и развитие бизнеса, расширение межрегиональных и международных связей, создание благоприятного инвестиционного климата.<o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b>Стабильная среда для принятия долгосрочных решений</b><o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Приоритеты экономического развития региона закреплены в соответствующих программных документах, создавая условия стабильности и определенности, необходимые для долгосрочного прогнозирования и обоснования целесообразности тех или иных инвестиционных решений. <o:p></o:p></p>
   
    <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Калининград – регион возможностей. Инвесторы из более чем 70 стран уже сделали свой выбор в пользу Калининградской области и успешно реализуют здесь свои инвестиционные проекты. <font size="3" style="font-family: 'Times New Roman', serif; line-height: 150%; background-color: white;"><o:p></o:p></font></span></p>
   </div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>