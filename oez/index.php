<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ОЭЗ");
?><p style="text-align: center;">
</p>
<p>
</p>
<p style="text-align: center;">
</p>
<h3></h3>
<h2><b>Особая&nbsp;экономическая&nbsp;зона&nbsp;в Калининградской области функционирует до 1 апреля 2031 года&nbsp;</b> </h2>
 (ст. 21 гл. 7 Федерального закона от 10.01.2006 № 16-ФЗ)
<p>
</p>
<p>
</p>
<p>
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: justify;">
 <b>Руководитель&nbsp;Администрации&nbsp;Особой&nbsp;экономической зоны в&nbsp;Калининградской области -&nbsp;Антон Андреевич Алиханов </b>
</p>
 <b> </b>
<p style="text-align: justify;">
 <b> </b>Функции Администрации Особой экономической зоны в Калининградской области выполняет Министерство по промышленной политике, развитию предпринимательства и торговли Калининградской области
</p>
<p style="text-align: justify;">
 <b style="text-align: center;">Осо­бая эко­но­ми­чес­кая зо­на&nbsp; в&nbsp;Ка­ли­нинг­радс­кой об­лас­ти</b>
</p>
<p>
</p>
<p>
</p>
<p>
	 На 01 января 2017 года в&nbsp;единый реестр резидентов ОЭЗ в Калининградской области включено 126&nbsp;инвестиционных проектов,&nbsp;из них 17 инвестиционных проектов с объемом заявленных инвестиций от 50 млн рублей.
</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 35.4pt;">
</p>
<p>
</p>
<p style="text-align: justify;">
 <a href="/msp/Реестр резидентов ОЭЗ в КО.xls">Реестр резидентов Особой экономической зоны в Калининградской области</a><br>
</p>
<p style="text-align: justify;">
	 Общий объем заявленных инвестиций составляет порядка 95,3 млрд рублей, из них около 1,2 млрд рублей приходится на проекты от 50 млн рублей.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Численность сотрудников на предприятиях-резидентах ОЭЗ&nbsp;<span style="text-align: justify; text-indent: 35.4pt;">в Калининградской области при выходе на проектные мощности составит более 22,9 тыс. человек, в том числе 1,1 тыс. рабочих мест в рамках проектов с объемом инвестиций от 50 млн рублей.</span>
</p>
<p>
 <img width="510" alt="struktura.jpg" src="/upload/medialibrary/7e3/struktura.jpg" height="258" title="struktura.jpg" style="text-align: justify; text-indent: 35.4pt;" vspace="10" border="0" align="middle" hspace="0">
</p>
<p style="text-align: justify;">
	 Наибольшее количество организаций, зарегистрированных в качестве резидентов ОЭЗ, осуществляют инвестиционные проекты в обрабатывающих отраслях экономики, на транспорте, а также в сфере операций с недвижимым имуществом, аренды и предоставления услуг.
</p>
<p style="text-align: justify;">
	 По состоянию на 31 декабря 2014 года объем фактических инвестиций (с учетом стоимости земли и приобретенных зданий/сооружений), осуществленных с начала реализации инвестиционных проектов, достиг&nbsp;98 105 млн рублей, в том числе за 2014 год – 6&nbsp;528 млн рублей.&nbsp;На 31 декабря 2014 года фактически на предприятиях-резидентах занято 11 981 человек.
</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center; text-indent: 35.45pt;">
</p>
<p style="text-align: center;">
 <b>Сведения об уплаченных резидентами ОЭЗ <br>
	 в Калининградской области налогах&nbsp;</b>
</p>
 <img width="504" alt="nalogi.jpg" src="/upload/medialibrary/570/nalogi.jpg" height="310" title="nalogi.jpg" style="text-indent: 35.45pt;" border="0">
<p>
</p>
<div>
	<div>
		<p>
		</p>
		<p style="text-align: justify;">
			 До&nbsp;<span style="text-align: justify;">2012 года данные формировались по годовой отчетности резидентов, начиная с 2013 года - п</span><span style="text-align: justify;">о данным Управления Федеральной налоговой службы по Калининградской области.</span>
		</p>
		<p>
		</p>
		<p>
		</p>
		<p>
		</p>
		<p>
		</p>
		<div>
 <span style="color: #ffffff; background-color: #ffffff;"><?
$NAME = $_GET["docname"];
if ($NAME != '') { $arFilter["NAME"] = "%$NAME%"; }
?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doc",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "arFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "public",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"NEWS_COUNT" => "20",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "modern",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "8",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"SIZE",1=>"",),
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?></span>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>