<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Процедура свободной таможенной зоны в Калининградской области");
?><h2 align="center"> <b>Процедура свободной таможенной зоны</b> </h2>
<div>
	<p style="text-align: justify;">
		 После создания Таможенного союза между Российской Федерацией, Республикой Беларусь и Республикой Казахстан странами-участницами было принято&nbsp;<a href="/oez/Соглашение.docx">Соглашение по вопросам свободных (специальных, особых) экономических зон на таможенной территории Таможенного союза и таможенной процедуры свободной таможенной зоны от 18.06.2010 г.</a>
	</p>
	<p style="text-align: justify;">
		 Статья 10 Соглашения дает определение свободной таможенной зоны. «Свободная таможенная зона - таможенная процедура, при которой товары размещаются и используются в пределах территории СЭЗ или ее части без уплаты таможенных пошлин, налогов, а также без применения мер нетарифного регулирования в отношении иностранных товаров и без применения запретов и ограничений в отношении товаров таможенного союза».
	</p>
	<p>
	</p>
	<p style="text-align: justify;">
		 Иностранные товары, помещенные под таможенную процедуру свободной таможенной зоны, сохраняют статус иностранных товаров. Таким образом, юридическое лицо, государственная регистрация которого осуществлена в Калининградской области, ввозя на территорию ОЭЗ импортные товары и помещая их под таможенную процедуру свободной таможенной зоны, на основании статьи 9 Соглашения освобождается от уплаты импортных пошлин и НДС, уплачиваемого при ввозе импортных товаров. Кроме того, на такие товары не распространяются меры нетарифного регулирования. &nbsp; &nbsp;
	</p>
	<p>
 <span style="text-align: justify;">В соответствии со статьей 13 Соглашения, в отношении товаров, помещенных под таможенную процедуру свободной таможенной зоны, допускается совершение любых операций, в том числе:</span>
	</p>
</div>
<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
		</div>
	</blockquote>
	<div align="justify">
		<p>
			 - складирование (хранение, накопление, дробление) товаров;
		</p>
	</div>
</blockquote>
<div align="justify">
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
			<p>
				 - операции по погрузке (разгрузке) товаров и иные грузовые операции, связанные с хранением;
			</p>
		</div>
	</blockquote>
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
			<p>
				 - операции, необходимые для сохранности товаров, а также обычные операции по подготовке товаров к транспортировке, включая дробление партии, формирование отправок, сортировку, упаковку, переупаковку, маркировку, операции по улучшению товарных качеств;
			</p>
		</div>
	</blockquote>
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
			<p>
				 - операции, предусматривающие совершение сделок по передаче прав владения, пользования и (или) распоряжения этими товарами;
			</p>
		</div>
	</blockquote>
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
			<p>
				 - операции по переработке (обработке) товаров, в результате проведения которых товары теряют свои индивидуальные характеристики, и (или) по изготовлению товаров (включая сборку, разборку, монтаж, подгонку), а также операции по ремонту товаров;
			</p>
		</div>
	</blockquote>
</div>
<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
	<div align="justify">
		<p style="text-align: justify;">
			 - потребление товаров.&nbsp;
		</p>
	</div>
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
		</div>
	</blockquote>
</blockquote>
<div>
	<p>
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
		 Товары, помещенные под процедуру свободной таможенной зоны, сохраняют свой статус - иностранные товары остаются иностранными, а товары таможенного союза - товарами таможенного союза.&nbsp;Определение статуса товаров, изготовленных (полученных) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны, регулируются статьей 19 Соглашения. При этом, определение статуса товаров, изготовленных (полученных) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны&nbsp;резидентами, зарегистрированными до 1 января 2012 года,&nbsp;&nbsp;в соответствии со статьей 19 Соглашения&nbsp;&nbsp;осуществляется&nbsp;до 1 января 2017 года.&nbsp;Определение статуса товаров, изготовленных (полученных) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны лицами, государственная регистрация которых осуществлена в Калининградской области, и которые по состоянию на 1 апреля 2006 года осуществляли деятельность на основании Федерального закона Российской Федерации от 22 января 1996 года № 13-ФЗ «Об Особой экономической зоне в Калининградской области», в соответствии со статьей 19 Соглашения осуществляется&nbsp;до 1 апреля 2016 года.
	</p>
	<p style="text-align: justify;">
		 Товар, изготовленный (полученный) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны, признается товаром таможенного союза, если в результате осуществления операций по изготовлению (получению) товара выполняется одно из следующих условий:
	</p>
	<p>
	</p>
</div>
<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
			<p style="text-align: justify;">
			</p>
			<p>
			</p>
		</div>
	</blockquote>
	<div align="justify">
		<p>
			 - произошло изменение классификационного кода товара по единой Товарной номенклатуре внешнеэкономической деятельности таможенного союза на уровне любого из первых четырех знаков;
		</p>
	</div>
</blockquote>
<div align="justify">
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
			<p style="text-align: justify;">
			</p>
			<p>
			</p>
			<p>
				 - процентная доля стоимости иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны, не превышает фиксированной доли в цене конечной продукции, или добавленная стоимость достигает фиксированной доли в цене конечной продукции;
			</p>
		</div>
	</blockquote>
</div>
<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
	<div align="justify">
		<p style="text-align: justify;">
		</p>
		<p>
		</p>
		<p>
			 - в отношении товара выполнены условия, производственные и технологические операции, достаточные для признания товара, изготовленного (полученного) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны, товаром таможенного союза.
		</p>
	</div>
	<blockquote style="margin: 0px 0px 0px 40px; border: none; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; padding: 0px;">
		<div>
			<p>
			</p>
			<p>
			</p>
		</div>
	</blockquote>
</blockquote>
<div>
	<p style="text-align: justify;">
	</p>
	<p>
	</p>
	<p style="text-align: justify;">
		 Товар, изготовленный (полученный) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны, не признается товаром таможенного союза, если в отношении товара осуществлены только те операции, которые не отвечают критериям достаточной переработки, независимо от выполнения иных условий.
	</p>
	<p style="text-align: justify;">
		 Изменение классификационного кода товара по единой Товарной номенклатуре внешнеэкономической деятельности таможенного союза на уровне любого из первых четырех знаков не применяется в качестве критерия достаточной переработки товаров, изготовленных (полученных) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны, в случае если в отношении товаров установлен перечень условий, производственных и технологических операций, достаточных для признания товара, изготовленного (полученного) с использованием иностранных товаров, помещенных под таможенную процедуру свободной таможенной зоны, товаром таможенного союза.&nbsp;
	</p>
	<p>
	</p>
	<p style="text-align: justify;">
		 При вывозе иностранных товаров, помещенных под процедуру свободной таможенной зоны,&nbsp;товаров,&nbsp;получивших статус товаров, изготовленных (полученных) с использованием иностранных товаров,&nbsp;помещенных под таможенную процедуру свободной таможенной зоны,&nbsp;с территории ОЭЗ в Калининградской области импортные пошлины и НДС подлежат уплате в размерах, соответствующих суммам импортных пошлин и НДС, которые подлежали бы уплате при помещении этих товаров под таможенную процедуру выпуска для внутреннего потребления.
	</p>
	<p style="text-align: justify;">
		 Вывоз товаров, получивших статус товаров, изготовленных (полученных) с использованием иностранных товаров,&nbsp;помещенных под таможенную процедуру свободной таможенной зоны и получивший статус товаров таможенного союза, на территорию Таможенного союза&nbsp;резидентами, зарегистрированными до 1 января 2012 года, и юридическими&nbsp;лицами, государственная регистрация которых осуществлена в Калининградской области, и которые по состоянию на 1 апреля 2006 года осуществляли деятельность на основании Федерального закона Российской Федерации от 22 января 1996 года № 13-ФЗ «Об Особой экономической зоне в Калининградской области» производится без уплаты каких-либо таможенных пошлин и НДС.
	</p>
	<p>
	</p>
	<div style="text-align: justify;">
 <a href="/documents/perechen.doc">Перечень нормативных актов в развитие положений СОГЛАШЕНИЯ&nbsp;</a>
	</div>
	<div style="text-align: justify;">
 <a href="/documents/perechen.doc"></a><a href="/documents/perechen.doc">по вопросам свободных (специальных, особых) экономических зон на таможенной территории таможенного союза и таможенной процедуры свободной таможенной зоны&nbsp;</a>
	</div>
	<p>
	</p>
	<p>
 <br>
	</p>
	<p>
	</p>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>