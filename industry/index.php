<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Промышленность");
?> <span style="font-family: 'Times New Roman', Times, Tahoma; font-size: 14px; line-height: 19px; background-color: rgb(249, 248, 243);" class="Apple-style-span"> 
  <p align="center" style="color: rgb(86, 83, 71);"><img src="http://minprom.gov39.ru/upload/medialibrary/30d/obshij_vid.jpg" title="obshij_vid.jpg" border="0" alt="obshij_vid.jpg" width="500" height="299" style="border: 0px; "  /></p>
 
  <p style="text-align: justify;">На се­год­няш­ний день Ка­ли­нинг­радс­кая об­ласть вхо­дит в чис­ло на­ибо­лее ди­на­мич­но раз­ви­ва­ющих­ся субъ­ек­тов Рос­сий­ской Фе­де­ра­ции. Это ста­ло воз­мож­но не толь­ко бла­го­да­ря дос­ти­же­ни­ям тра­ди­ци­он­ных для об­лас­ти от­рас­лей про­мыш­лен­нос­ти, но и за счет раз­ви­тия сов­ре­мен­ных ин­но­ва­ци­он­ных тех­но­ло­гий и про­из­водств, ос­но­ван­ных на на­уч­ном и кад­ро­вом по­тен­ци­алах.  </p>
 </span> 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;">Промышленность является ведущей отраслью экономики Калининградской области. Доля промышленности в объёме ВРП в 2012 году составила 30,3%, в том числе: добыча полезных ископаемых &ndash; 4,8%; обрабатывающие производства – 22,3%; производство и распределение электроэнергии, газа и воды – 3,3%.<o:p></o:p></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">В 2013 году индекс промышленного производства по всем видам  деятельности составил 95,5% </span>(в РФ – 100,4%, по СЗФО – 99,2%)<span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">. <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Индексы промышленного производства по отдельным видам экономической деятельности по итогам 2013 года по отношению к предыдущему году составили:<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- добыча полезных ископаемых –  91,7%, <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- обрабатывающие производства – 96,2% , <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство и распределение электроэнергии, газа и воды – 95,3%. <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p> </o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Индекс обрабатывающих производств по итогам 2013 года по отношению к предыдущему году составил 96,2% (в РФ – 100,5%, по СЗФО – 99,5%). <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">На положительную динамику данного индекса оказали:<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство резиновых и пластмассовых изделий (109,8%);<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство транспортных средств и оборудования (108,4%);<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство машин и оборудования (108,4%);<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство пищевых продуктов, включая напитки, и табака (103,2%).<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Сокращение объемов производства продукции произошло:<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- в производстве электрооборудования, электронного и оптического оборудования на 29,1%;<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- в текстильном и швейном производстве на 13,3%;<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- в обработке древесины и производстве изделий из дерева на 13,0%; <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- в целюлозно-бумажном производстве, издательской и полиграфической деятельности на 9,6%;<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- в химическом производстве на 5,6%;<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- в производстве прочих неметаллических минеральных продуктов на 1,2%.<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Наиболее значимыми видами деятельности обрабатывающих производств по удельному весу в общем объеме выпуска продукции обрабатывающих производств являются: <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство транспортных средств и оборудования – 56,9%;<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство пищевых продуктов, включая напитки и табака – 23,0%;<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">- производство электрооборудования, электронного и оптического оборудования – 8,2 %.<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p> </o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Объем отгруженных товаров собственного производства, выполненных работ и услуг по полному кругу организаций по чистым видам экономической деятельности &laquo;добыча полезных ископаемых&raquo;, «обрабатывающие производства», «производство и распределение электроэнергии, газа и воды» в 2013 году составил 383 009,7 млн рублей. <o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p> </o:p></span></p>
 
<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="496" style="width: 371.85pt; margin-left: 5.15pt;"> 
  <tbody> 
    <tr style="page-break-inside: avoid; height: 15.05pt;"> <td width="342" nowrap="" rowspan="3" style="width: 256.5pt; padding: 0cm 5.4pt; height: 15.05pt; border-width: 1pt; border-style: solid; border-color: windowtext windowtext black;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 21.3pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Наименование показателя<o:p></o:p></span></p>
       </td> <td width="154" colspan="2" style="width: 115.35pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: solid solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext; border-top-width: 1pt; border-top-color: windowtext;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 21.3pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">2013 год<o:p></o:p></span></p>
       </td> <td style="height: 15.05pt; border: none;" width="0" height="20"></td> </tr>
   
    <tr style="height: 25.55pt;"> <td width="64" rowspan="2" style="width: 48.3pt; padding: 0cm 5.4pt; height: 25.55pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">млн. рублей<o:p></o:p></span></p>
       </td> <td width="89" rowspan="2" style="width: 67.05pt; padding: 0cm 5.4pt; height: 25.55pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: black; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">структура, в %  к  итогу<o:p></o:p></span></p>
       </td> <td style="height: 25.55pt; border: none;" width="0" height="34"></td> </tr>
   
    <tr style="height: 25.55pt;"> <td style="height: 25.55pt; border: none;" width="0" height="34"></td> </tr>
   
    <tr style="height: 29.35pt;"> <td width="342" valign="bottom" style="width: 256.5pt; padding: 0cm 5.4pt; height: 29.35pt; border-bottom-width: 1pt; border-style: none solid solid; border-bottom-color: windowtext; border-left-width: 1pt; border-left-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 21.3pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Объем отгруженных товаров собственного производства, выполненных работ и услуг<o:p></o:p></span></p>
       </td> <td width="64" style="width: 48.3pt; padding: 0cm 5.4pt; height: 29.35pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">383 009,7<o:p></o:p></span></p>
       </td> <td width="89" style="width: 67.05pt; padding: 0cm 5.4pt; height: 29.35pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">100<o:p></o:p></span></p>
       </td> <td style="height: 29.35pt; border: none;" width="0" height="39"></td> </tr>
   
    <tr style="height: 15.05pt;"> <td width="342" nowrap="" valign="bottom" style="width: 256.5pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid; border-bottom-color: windowtext; border-left-width: 1pt; border-left-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 21.3pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">в том числе:<o:p></o:p></span></p>
       </td> <td width="64" style="width: 48.3pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"></td> <td width="89" style="width: 67.05pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"></td> <td style="height: 15.05pt; border: none;" width="0" height="20"></td> </tr>
   
    <tr style="height: 15.05pt;"> <td width="342" nowrap="" valign="bottom" style="width: 256.5pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid; border-bottom-color: windowtext; border-left-width: 1pt; border-left-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 21.3pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Добыча полезных ископаемых<o:p></o:p></span></p>
       </td> <td width="64" style="width: 48.3pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">18 192,2<o:p></o:p></span></p>
       </td> <td width="89" style="width: 67.05pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">4,7<o:p></o:p></span></p>
       </td> <td style="height: 15.05pt; border: none;" width="0" height="20"></td> </tr>
   
    <tr style="height: 15.05pt;"> <td width="342" nowrap="" valign="bottom" style="width: 256.5pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid; border-bottom-color: windowtext; border-left-width: 1pt; border-left-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 21.3pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Обрабатывающие производства<o:p></o:p></span></p>
       </td> <td width="64" style="width: 48.3pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">343 890,6<o:p></o:p></span></p>
       </td> <td width="89" style="width: 67.05pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">89,8<o:p></o:p></span></p>
       </td> <td style="height: 15.05pt; border: none;" width="0" height="20"></td> </tr>
   
    <tr style="height: 15.05pt;"> <td width="342" nowrap="" valign="bottom" style="width: 256.5pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid; border-bottom-color: windowtext; border-left-width: 1pt; border-left-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 21.3pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Производство и распределение электроэнергии, газа и воды<o:p></o:p></span></p>
       </td> <td width="64" style="width: 48.3pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">20 926,9<o:p></o:p></span></p>
       </td> <td width="89" style="width: 67.05pt; padding: 0cm 5.4pt; height: 15.05pt; border-bottom-width: 1pt; border-style: none solid solid none; border-bottom-color: windowtext; border-right-width: 1pt; border-right-color: windowtext;"> 
        <p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">5,5<o:p></o:p></span></p>
       </td> <td style="height: 15.05pt; border: none;" width="0" height="20"></td> </tr>
   </tbody>
 </table>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p> </o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Лидирующее положение по темпам роста промышленного производства обеспечивается, главным образом, результатами деятельности обрабатывающих производств Калининградской области, доля которых в общем объеме отгрузки промышленной продукции составляет 89,8%.<o:p></o:p></span></p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify; text-indent: 35.45pt;"> </p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Объем отгруженных товаров обрабатывающих производств собственного производства, выполненных работ и услуг собственными силами, в фактических ценах в 2013 году составил 343 890,60 млн рублей.<o:p></o:p></span></p>
 
<p style="text-align: justify;"> </p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><o:p> </o:p></span></p>
 
<div align="justify">
  <br />
</div>
 
<p> 
  <br />
 </p>
 <?
$NAME = $_GET["docname"];
if ($NAME != '') { $arFilter["NAME"] = "%$NAME%"; }
?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doc",
	Array(
		"IBLOCK_TYPE" => "public",
		"IBLOCK_ID" => "5",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arFilter",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"SIZE",1=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "9",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "modern",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>