<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Перечень объектов недвижимости");
?><b><span style="font-size: 14pt; color: #0000ff;"><br>
 </span></b><br>
 <br>
 <b><span style="font-size: 14pt; color: #0000ff;">Министерство по промышленной политике, развитию предпринимательства и торговли Калининградской области определено уполномоченным органом исполнительной власти региона по формированию и передаче в налоговые органы перечня объектов недвижимого имущества, для которых налоговой базой является кадастровая стоимость.<br>
 </span></b><br>
 <br>
 <span style="color: #ee1d24;">Вышеуказанный ПЕРЕЧЕНЬ будет постоянно актуализироваться</span><br>
 <br>
 <a href="/Kadastr/Perechen2016_KO_kadast_2016_26_12_na_30_05_2017.xlsx">Перечень</a>&nbsp;объектов недвижимого имущества (по состоянию на 30.05.2017 г.)<br>
 <br>
 <a href="/Kadastr/doc00083320161226161231.pdf">Приказ</a> Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области от 26.12.2016 года № 111&nbsp;"Об определении перечня объектов недвижимого имущества, в отношении которых налоговая база определяется как кадастровая стоимость"<br>
 <br>
 <a href="/Kadastr/prikaz_30_05_17_nomer73.pdf">Приказ</a> Министерства по промышленной политике, развитию предпринимательства и торговли Калининградской области от 30.05.2017 года №73 "о внесении изменений в приказ от 26.12.2016 года № 111"<br>
 <br>
 <a href="/Kadastr/Statja_378_2_NKRF.rtf">Статья 378.2</a> Налогового кодекса Российской Федерации<br>
 <br>
 <a href="/Kadastr/ZakonKO_27112003_336.rtf">Закон</a> Калининградской области "О налоге на имущество организаций"<br>
 <br>
 <a href="/Kadastr/Post_GKR_18052016_254.rtf">Постановление</a> Правительства Калининградской области "Об определении уполномоченного органа исполнительной власти Калининградской области по определению перечня объектов недвижимого имущества, в отношении которых налоговая база определяется как кадастровая стоимость" <br>
 <br>
 <a href="/Kadastr/Информация о порядке исключения объекта нежилого фонда из перечня объектов недвижимого имущества.docx">Информация о порядке исключения объекта нежилого фонда из перечня объектов недвижимого имущества</a><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>