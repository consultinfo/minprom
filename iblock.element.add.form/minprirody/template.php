<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<div class="online_reception_wrp">
		<h3>правила</h3>
		<p><?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include_areas/regulations.php", Array(), Array("MODE" => "html", "SHOW_BORDER" => true));?></p>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
    <h3><?ShowNote($arResult["MESSAGE"])?></h3>
<?endif?>
	<div class="reception_wrp">
		<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data"/>
			<?=bitrix_sessid_post()?>
			<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
				<?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):?>
				<?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>
			<? if($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"]!="F"){?>
                         <?if ($propertyID!=65 && $propertyID!=66){?>
						    <div class="item">
									<div class="label"> <?}?>
										<?if ($propertyID==64){?>
											<span class=" val">Адрес:</span>
									</div>
										<?}?>
										<?if ($propertyID!=64 && $propertyID!=65 && $propertyID!=66)
											{?>
											<span class="val" id="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]?>">
												<?if (intval($propertyID) > 0):?><?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>
												<?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
													<span class="starrequired">*</span>
												<?endif?>
											</span>
									</div>
							              <?else:?><?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID)?>
										</span>
									</div>
							              <?endif?><?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
									<?endif;
											}
			}?>
								<?
								if (intval($propertyID) > 0)
								{
									if (
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
										&&
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
									)
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
									elseif (
										(
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
											||
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
										)
										&&
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
									)
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
								}
								elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

								if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
								{
									$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
									$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
								}
								else
								{
									$inputNum = 1;
								}

								if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
									$INPUT_TYPE = "USER_TYPE";
								else
									$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

								switch ($INPUT_TYPE):
									case "USER_TYPE":
										for ($i = 0; $i<$inputNum; $i++)
										{
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
												$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
												$description = "";
											}
											else
											{
												$value = "";
												$description = "";
											}
											echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
												array(
													$arResult["PROPERTY_LIST_FULL"][$propertyID],
													array(
														"VALUE" => $value,
														"DESCRIPTION" => $description,
													),
													array(
														"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
														"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
														"FORM_NAME"=>"iblock_add",
													),
												));
										?><br /><?
										}
									break;
									case "TAGS":
										$APPLICATION->IncludeComponent(
											"bitrix:search.tags.input",
											"",
											array(
												"VALUE" => $arResult["ELEMENT"][$propertyID],
												"NAME" => "PROPERTY[".$propertyID."][0]",
												"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
											), null, array("HIDE_ICONS"=>"Y")
										);
										break;
									case "HTML":
										$LHE = new CHTMLEditor;
										$LHE->Show(array(
											'name' => "PROPERTY[".$propertyID."][0]",
											'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
											'inputName' => "PROPERTY[".$propertyID."][0]",
											'content' => $arResult["ELEMENT"][$propertyID],
											'width' => '100%',
											'minBodyWidth' => 350,
											'normalBodyWidth' => 555,
											'height' => '200',
											'bAllowPhp' => false,
											'limitPhpAccess' => false,
											'autoResize' => true,
											'autoResizeOffset' => 40,
											'useFileDialogs' => false,
											'saveOnBlur' => true,
											'showTaskbars' => false,
											'showNodeNavi' => false,
											'askBeforeUnloadPage' => true,
											'bbCode' => false,
											'siteId' => SITE_ID,
											'controlsMap' => array(
												array('id' => 'Bold', 'compact' => true, 'sort' => 80),
												array('id' => 'Italic', 'compact' => true, 'sort' => 90),
												array('id' => 'Underline', 'compact' => true, 'sort' => 100),
												array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
												array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
												array('id' => 'Color', 'compact' => true, 'sort' => 130),
												array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
												array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
												array('separator' => true, 'compact' => false, 'sort' => 145),
												array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
												array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
												array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
												array('separator' => true, 'compact' => false, 'sort' => 200),
												array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
												array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
												array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
												array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
												array('separator' => true, 'compact' => false, 'sort' => 290),
												array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
												array('id' => 'More', 'compact' => true, 'sort' => 400)
											),
										));
										break;
									case "T":
										for ($i = 0; $i<$inputNum; $i++)
										{

											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
											}
											else
											{
												$value = "";
											}
										?>
										<div class="value">
											<div class="input type_1" id="input_value">
												<textarea
												            cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"
											            	rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>"
												            name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
												<?if(!in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
													<span class="starrequired"></span>
												<?endif?>
											</div>
										</div>
											<?
										}
									break;

									case "S":
									case "N":
										for ($i = 0; $i<$inputNum; $i++)
										{
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

											}
											else
											{
												$value = "";
											}
										?>
											<?if ($propertyID!=65 && $propertyID!=66){?><div class="value" <?=$propertyID == "64" || $propertyID  =="65" || $propertyID =="66" ? "id = 'address_list_value'" : ""?>><?}?>
												<div class="<?
													switch ($propertyID)
													{
														case 64:
															echo "input type_2";
															break;
														case 65:
															echo "input type_3";
															break;
														case 66:
															echo "input type_4";
															break;
														default:
															echo "input type_1";
													}
												?>"
													 id="<?=$propertyID == "64" || $propertyID  =="65" || $propertyID =="66" ? "address_value" : "input_value"?>">
													<input
														   type="<?=$propertyID == "62" ? "email" : "text";?>"
														   name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
														   size="55"
														   value="<?=$value?>"
														   placeholder="<?if($propertyID == 64){echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];}if($propertyID == 65){echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];}if($propertyID == 66){echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];} ?>"

													/>
													<?if(!in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
														<span class="starrequired"></span>
													<?endif?>
												<?if ($propertyID!=64 && $propertyID!=65){?></div><?}?>
											<?if ($propertyID!=64 && $propertyID!=65){?></div><?}?>
											<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
											$APPLICATION->IncludeComponent(
												'bitrix:main.calendar',
												'',
												array(
													'FORM_NAME' => 'iblock_add',
													'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
													'INPUT_VALUE' => $value,
												),
												null,
												array('HIDE_ICONS' => 'Y')
											);
											?><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
										endif
										?><?
										}
									break;

									case "F":?>
										<div class="item" id="input_file1" >
											<div class="label">
												<span class="val">Вложенные файлы:</span>
											</div>
											<div class="value">
													<div class="input_file">
														<div class="hide">
															<div id="file_<?=$propertyID?>_0">
																<input type="hidden" name="PROPERTY[<?=$propertyID?>][0]">
																<input type="file" name="PROPERTY_FILE_<?=$propertyID?>_0" onchange="AddFileName(<?=$propertyID?>, this.value);">
															</div>
														</div>
													</div>
													<div class="input file">
														<a class="type_file"  onclick="addFileToForm(<?=$propertyID?>,0);" data-number="0">Выбрать файл</a>
													</div>
													<div class="available_files">
														<div class="lbl">
															<span class="title">Допустимые типы файлов:</span>
														</div>
														<div class="file_list">
															<div class="elm"><span class="val">*.zip</span></div>,
															<div class="elm"><span class="val">*.rar</span></div>,
															<div class="elm"><span class="val">*.doc</span></div>,
															<div class="elm"><span class="val">*.docx</span></div>,
															<div class="elm"><span class="val">*.xls</span></div>,
															<div class="elm"><span class="val">*.xlsx</span></div>,
															<div class="elm"><span class="val">*.rtf</span></div>,
															<div class="elm"><span class="val">*.pdf</span></div>,
															<div class="elm"><span class="val">*.jpeg</span></div>,
															<div class="elm"><span class="val">*.gif</span></div>,
															<div class="elm"><span class="val">*.png</span></div>
														</div>
													</div>
											</div>
										</div>

										<div class="item" id="input_file_<?=$propertyID?>" >
											<div class="label">
												<span class=" val">Загруженные файлы:</span>
											</div>
											<div class="value" id="fileUpload_<?=$propertyID?>">
												<div class="uploaded_files">
													<div class="elms"></div>
													<div class="prompt">
														<span class="val">Размер файла не может превышать 1 Мб.</span>
													</div>
												</div>

											</div>
										</div>


											<?

											if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
											{
												?>
							<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
												<?

												if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
												{
													?>
							<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
													<?
												}
												else
												{
													?>
							<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
							<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
							[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
													<?
												}

										} ?><?

									break;?><?
									case "L":

										if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
											$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
										else
											$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

										switch ($type):
											case "checkbox":
											case "radio":
												foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
												{
													$checked = false;
													if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
													{
														if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
														{
															foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
															{
																if ($arElEnum["VALUE"] == $key)
																{
																	$checked = true;
																	break;
																}
															}
														}
													}
													else
													{
														if ($arEnum["DEF"] == "Y") $checked = true;
													}

													?>
									<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
													<?
												}
											break;

											case "dropdown":
											case "multiselect":
											?>
									<select 
                                        name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>" 
                                        id="select_form" 												
									    <?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>required<?endif?>>
										<option value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
											<?
												if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
												else $sKey = "ELEMENT";

												foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
												{
													$checked = false;
													if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
													{
														foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
														{
															if ($key == $arElEnum["VALUE"])
															{
																$checked = true;
																break;
															}
														}
													}
													else
													{
														if ($arEnum["DEF"] == "Y") $checked = true;
													}
													?>
										<option value="<?=$key?>" data-form="<?=$arEnum["XML_ID"]?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
													<?
												}
											?>
									</select>
											<?
											break;

										endswitch;
									break;
								endswitch;?>
							<? if($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"]!="F"){?></div><? }?>
					<?endforeach;?>
					<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
						<div class="item">
						<div class="label">
							<span class=" val">Защита от роботов:</span>
						</div>
						<div class="value">
							<div class="captcha">
								<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
								<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" class="pic type_3" />
							</div>
								<?//=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><!--<span class="starrequired">*</span>:-->
							<div class="input type_2"  id="input_value">
								<input type="text" name="captcha_word" maxlength="50" value="">
							</div>
						</div>
					</div>
					<?endif?>
				<?endif?>
							<div class="item">
								<div class="input submit">
									<input type="submit" name="iblock_submit" value="Отправить обращение" />
								</div>
							</div>
								<?if (strlen($arParams["LIST_URL"]) > 0):?>
							<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
								<input
									type="button"
									name="iblock_cancel"
									value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
									onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"])?>';"
								>
							<?endif?>
		</form>
	</div>
</div>
<?if (!empty($arResult["ERRORS"])):?>
	<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif;?>