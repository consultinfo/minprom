$(document).on('submit','.online_reception_wrp form',function(e){
    var flag_complite = true;
    BX.closeWait();

    $.each($(this).find(".item").find(".value").find("#input_value"), function(){
        var input = $(this);
        var item = $(this);
        if (input.find(".starrequired").size())
        {
            return;
        }
        if(input.find("input,textarea").val().trim() == "")
        {
            flag_complite = false;
            input.addClass("error");
        }
        else if (input.hasClass("error")) {
            input.removeClass("error");
        }
    });

    if(!flag_complite)
    {
        alert("Заполните все обязательные поля!");
        e.preventDefault();
    }
});


$( "#select_form" ).change(function() {
    var current_item = $( "#select_form option:selected" ).data("form");
    var email = $("#email").parents(".item");
    var label = email.find(".label .val");
    var address = $("#address_list_value").parents(".item");
    var label_address = address.find(".label .val");
    var html = "<span class='starrequired'>*</span>";
    if (current_item == "written_form") {
        label.find(".starrequired").remove();
        email.find(".value .input input").removeAttr("required");
        label_address.append(html);
        $( "#address_list_value" ).each(function( index ) {
            $(this).find(".input input").attr("required","required");
        });
    }
    if (current_item == "electronic") {
        label_address.find(".starrequired").remove();
        email.find(".value .input input").attr("required","required");
        label.append(html);
        $( "#address_list_value" ).each(function( index ) {
            $(this).find(".input input").removeAttr("required");
        });
    }
    if (typeof(current_item) == "undefined") {
        label.find(".starrequired").remove();
        label_address.find(".starrequired").remove();
        email.find(".value .input input").removeAttr("required");
        $( "#address_list_value" ).each(function( index ) {
            $(this).find(".input input").removeAttr("required");
        });
    }
   
});

function addFileToForm(id, number){
    var new_file = $("#file_"+id+"_"+number).find("[type='file']");
    if (new_file.size())
    {
          new_file.trigger( "click" );
    }
}

function AddFileName(id, val){
    val = val.split('\\');
    if (val == "") return false;

    var wrp = $("#input_file_"+id),
        wrp1 = $("#input_file1"),
        btn = wrp1.find(".type_file"),
        number = btn.data("number")*1,
        next_number = number + 1,
        input_html = ''+
            '<div id="file_'+id+'_'+next_number+'">'+
            '<input type="hidden" name="PROPERTY['+id+']['+next_number+']">'+
            '<input type="file" name="PROPERTY_FILE_'+id+'_'+next_number+'" onchange="AddFileName('+id+',this.value);">'+
            '</div>',
        html = ''+
                 '<div class="file" id="file_'+number+'">'+
                        '<a href="javascript:void(0);" title="Для удаления нажмите на ссылку" onclick="removeFile('+id+','+number+');">'+val[2]+'</a>'+
                        '<span class="file_size"></span>'+
                 '</div>';

    btn.data("number", next_number);
    wrp.find(".elms").append(html);
    wrp1.find(".hide").append(input_html);
    btn.attr("onclick", 'addFileToForm('+id+','+next_number+');');

    return false;
}

function removeFile(id, number) {
    $("#file_"+number).remove();
    $("#file_"+id+"_"+number).remove();

    return false;
}