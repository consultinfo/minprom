<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Значение вступления России в ВТО для Калининградской области");
?> 
<p> 
  <br />
 </p>
 
<p>В соответствии с официальной информацией Минэкономразвития России о результатах заключительного раунда переговоров о вступлении России в ВТО обязательства, принятые нашей страной в качестве члена данной организации затрагивают всю совокупность инструментов, применяемых в международной торговле. </p>
 
<p>Указанные инструменты определены Международными правовыми актами в рамках ВТО. Такими как: </p>
 
<p>&middot; Многосторонние соглашения по торговле товарами (14 соглашений); </p>
 
<p>· Генеральное соглашение по торговле услугами; </p>
 
<p>· Соглашение по торговым аспектам прав интеллектуальной собственности. </p>
 
<p>Необходимость оценки влияния последствий вступления России в ВТО на порядок осуществления хозяйственной деятельности в Калининградской области и ее социально-экономическое развитие обусловлена высокой степенью ответственности органов власти за судьбу жителей и бизнес сообщества, определяющих темпы экономического роста самого западного региона России. </p>
 
<p> </p>
 
<p class="MsoNormal" align="center" style="margin-bottom: 0.0001pt; text-align: center; "> </p>
 
<p align="center"><b>1. Обязательства по товарам </b></p>
 
<div style="text-align: justify; ">Анализ влияния обязательств России, связанных с тарифными мерами международной торговли на экономику Калининградской области целесообразно осуществлять, основываясь на данных о существующей и планируемой (в рамках концепции развития промышленности Калининградской области) структуры экономики региона.</div>
 
<div style="text-align: justify; "> 
  <br />
 </div>
 
<div style="text-align: center; "><img src="/upload/medialibrary/ff9/ieqlhumeuszjvudlthxp.jpg" title="инвестиции.jpg" border="0" alt="инвестиции.jpg" width="520" height="353"  /></div>
 
<div style="text-align: center; "> 
  <p style="text-align: justify; ">Данные об изменении ставок ввозных таможенных пошлин, применяемых при ввозе товаров на территорию Российской Федерации в отношении номенклатуры товаров, производимых предприятиями Калининградской области, имеющих максимальную долю в структуре экономики региона с учетом связывания таможенного тарифа приведены в следующей таблице. </p>
 
  <p style="text-align: justify; "> 
    <table style="border-collapse: collapse; "> 
      <tbody> 
        <tr> <td style="border-image: initial; "> 
            <p align="center"><b>Товарные группы </b></p>
           </td> <td style="border-image: initial; "> 
            <p align="center"><b>Средняя по группе ставка тарифа (%) </b></p>
           </td> <td style="border-image: initial; "> 
            <p align="center"><b>Ставка тарифа по обязательствам ВТО</b></p>
           </td> <td style="border-image: initial; "> 
            <p align="center"><b>Переходный период </b></p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Жир растительный, текстурированный, пригодный для употребления в пищу</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">20 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">15, но не менее 0,12 &euro;/кг </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">0 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Консервы мясные в жестяных банках</p>
           </td><td style="border-image: initial; "> 
            <p align="center">25, но не менее 0,63 €/кг </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">20, но не менее 0,5 €/кг </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Продукты переработки и консервирования рыбо- и морепродуктов</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">15 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">14 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2013 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Глазурь шоколадная</p>
           </td><td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">0 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Готовые пищевые продукты, содержащие какао</p>
           </td><td style="border-image: initial; "> 
            <p align="center">0,6 €/кг </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">0,2 €/кг </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2016 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Коньяк российский, в стеклянных бутылках</p>
           </td><td style="border-image: initial; "> 
            <p align="center">2 €/литр </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">1,5 €/литр </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Пленка из полиэтилена</p>
           </td><td style="border-image: initial; "> 
            <p align="center">10 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">6,5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Оборудование санитарно-техническое из пластмасс</p>
           </td><td style="border-image: initial; "> 
            <p align="center">20 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">6,5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2017 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Шкурки пушных животных выделанные, целые</p>
           </td><td style="border-image: initial; "> 
            <p align="center">10 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2014 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Текстильные напольные покрытия</p>
           </td><td style="border-image: initial; "> 
            <p align="center">20, но не менее 0,5 €/кв.м. </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">0,25 €/кв.м. </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Изделия столярные из дерева</p>
           </td><td style="border-image: initial; "> 
            <p align="center">20 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">10 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2016 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Бумага для гофрирования</p>
           </td><td style="border-image: initial; "> 
            <p align="center">15 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">10 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Чековая лента для кассовых аппаратов</p>
           </td><td style="border-image: initial; "> 
            <p align="center">15 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2016 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Этикетки самоклеющиеся</p>
           </td><td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">0 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Изделия формированные из искусственного камня</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">15 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">13 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Зеркала стеклянные</p>
           </td><td style="border-image: initial; "> 
            <p align="center">15 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">9 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2016 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Банка металлическая</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">22 €/1000 шт. </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">15 €/1000 шт. </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2018 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Профили полые из алюминия</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">20 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">12 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Строительные металлические конструкции из алюминия</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">20 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">12 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2017 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Крышка металлическая для консервирования пищевой продукции </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">20 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">12 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2017 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Подъемно-транспортное оборудование</p>
           </td><td style="border-image: initial; "> 
            <p align="center">13 </p>
           
            <p align="center"> </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Электрическая распределительная аппаратура</p>
           </td><td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">5 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">0 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Теле- радио аппаратура</p>
           </td><td style="border-image: initial; "> 
            <p align="center">20 </p>
           
            <p align="center"> </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">8 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2015 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Автомобили</p>
           </td><td style="border-image: initial; "> 
            <p align="center">25, но не менее 1,1 €/см.куб. </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">15 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2019 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Суда морские (яхты)</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">20 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">15 </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2014 </p>
           </td> </tr>
       
        <tr> <td style="border-image: initial; "> 
            <p>Мебель</p>
           </td> <td style="border-image: initial; "> 
            <p align="center">20, но не менее 0,7 €/кг. </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">10, но не менее 0,08 €/кг. </p>
           </td> <td style="border-image: initial; "> 
            <p align="center">2018 </p>
           </td> </tr>
       </tbody>
     </table>
   </p>
 
  <p style="text-align: justify; ">Указанные данные свидетельствуют, что среднее снижение уровня таможенного тарифа в связи с исполнением Россией своих обязательств для основных групп товаров, производимых предприятиями Калининградской области составляет от 7 до 68%. Такое же положение дел и в группах товаров, являющихся сырьем и комплектующими для их производства. </p>
 
  <p style="text-align: justify; ">Таким образом, положительный эффект от применения тарифных преференций, связанных с размещением производства на территории Особой экономической зоны в Калининградской области утрачивает свой экономический смысл. </p>
 
  <div style="text-align: justify; ">Наличие переходного периода сглаживает шоковое воздействие на экономику. Однако, имеющиеся сроки не позволяют обеспечить масштабную структурную перестройку технологических производственных процессов и техническое перевооружение действующих предприятий в условиях острого дефицита инвестиционных ресурсов и дешевых источников их привлечения.</div>
 
  <p></p>
 
  <p style="text-align: justify; "> </p>
 
  <p class="MsoNormal" align="center" style="text-align: center; margin-bottom: 0.0001pt; line-height: 150%; "><b><span style="line-height: 150%; font-family: 'Times New Roman', serif; ">2. Обязательства по услугам<font size="4"><o:p></o:p></font></span></b></p>
 
  <p class="Style1" style="text-align: justify; line-height: 150%; "><span style="line-height: 150%; ">В рамках обязательств по секторам услуг (116 секторов) основное влияние на социально-экономическое развитие Калининградской области могут оказать изменения в сторону увеличения доступа иностранных компаний банковского и страхового сектора. Указанные обязательства скорее окажут позитивное воздействие на соответствующие рынки услуг, расширив доступ потребителей к новым видам услуг, и повысив уровень конкуренции.<o:p></o:p></span></p>
 
  <p class="1" style="text-align: justify; margin-left: 0cm; line-height: 150%; "><span style="line-height: 150%; ">Некоторые обязательства России предусматривают возможность введения и более жестких мер по сравнению с существующим режимом. Так, например, Россия сможет при необходимости ввести государственную монополию на оптовую торговлю  алкоголем. Указанная мера не может привести к изменениям, ухудшающим положение производителей алкогольной продукции. расположенных на территории Калининградской области, по сравнению с аналогичными предприятиями, функционирующими на остальной территории Таможенного союза.<font size="4"><o:p></o:p></font></span></p>
 
  <p class="1" style="margin-left: 0cm; line-height: 150%; "> </p>
 
  <p align="center" style="text-align: center; "><b>3. Системные обязательства </b></p>
 
  <p> </p>
 
  <p align="center" style="text-align: center; "><b>3.1. Субсидии </b></p>
 
  <p style="text-align: justify; ">Обязательства России в части субсидирования промышленности предусматривают запрет осуществления прямого субсидирования экспортеров, а также предоставление субсидий, получение которых предусматривает использование исключительно отечественных товаров (например, при субсидировании закупок техники, которая должна быть только отечественной или использования отечественных компонентов в производстве). </p>
 
  <p style="text-align: justify; ">На сегодняшний день такие субсидии в качестве государственной поддержки предприятий Калининградской области не применяется, в то время как системные меры поддержки, осуществляемые, в том числе, в рамках Федеральных целевых программ правилам ВТО не противоречат и Россия сможет их осуществлять без ограничения объема субсидирования. </p>
 
  <p align="center" style="text-align: center; "><b>3.2. Особые экономические зоны </b></p>
 
  <p style="text-align: justify; ">Переходный период по предоставлению инвесторам в Калининградской ОЭЗ льгот, противоречащих правилам ВТО, позволит в полной мере реализовать инвестиционные проекты на условиях, предусмотренных нашим законодательством. Действующее с 18 июля 2010 г. &laquo;Соглашение по вопросам свободных (специальных, особых) экономических зон на таможенной территории Таможенного союза и таможенной процедуры свободной таможенной зоны&raquo; полностью соответствует требованиям ВТО. </p>
 
  <p align="center" style="text-align: center; "><b>3.3. Промсборка </b></p>
 
  <p style="text-align: justify; ">Переходный период, в течение которого могут сохраняться противоречащие правилам ВТО элементы режима «промышленной сборки» автомобилей и автокомпонентов, истекает 1 июля 2018 года, когда автосборочные предприятия, входящие в группу «Автотор» должны будут выйти на полную окупаемость инвестиционных проектов и начнут получать чистую прибыль на вложенные инвестиции. Договоренности по ВТО не приведут к расторжению инвестиционных соглашений. </p>
 
  <p align="center" style="text-align: center; "><b>3.4. Охрана прав интеллектуальной собственности </b></p>
 
  <p style="text-align: justify; ">Действующее российское законодательство, регулирующее защиту прав интеллектуальной собственности полностью соответствует нормам и правилам ВТО. </p>
 
  <p align="center" style="text-align: center; "><b>3.5. Ценообразование на энергоносители </b></p>
 
  <p style="text-align: justify; ">В области ценообразования в сфере поставок газа после присоединения к ВТО Российская Федерация сохраняет право применять регулирование цен на природный газ, поставляемый некоммерческим потребителям, которое будет обеспечивать социально-экономические цели и задачи России. Таким образом, обязательства ВТО соответствуют нынешнему законодательству и не приведет к ухудшению социального климата региона. </p>
 
  <p align="center" style="text-align: center; "><b>3.6. Санитарные и фитосанитарные меры и техническое регулирование </b></p>
 
  <p style="text-align: justify; ">Россия сохранит право применять более жесткие требования по сравнению с указанными международными стандартами, если того требует уровень защиты, установленный в Российской Федерации. </p>
 
  <p style="text-align: justify; ">В тоже время, присоединение России к ВТО обеспечит прозрачность процедуры, согласно которой импортер сможет обжаловать приостановление, аннулирование или отказ в разрешении на импорт подконтрольных товаров и получить письменный ответ, разъясняющий причины принятия соответствующего решения и меры, которые заявитель должен предпринять для получения разрешения. </p>
 
  <p style="text-align: justify; ">Россельхознадзор будет обязан, прежде чем принять меры по приостановке импорта, предоставить стране-экспортеру возможность принять соответствующие корректирующие меры. Данное обязательство не распространяется на случаи, связанные с существенными рисками для здоровья людей и животных. </p>
 
  <p style="text-align: justify; ">В части технического регулирования Российской Федерацией будет обеспечено соответствие законодательства в данной сфере требованиям Соглашения ВТО по техническим барьерам в торговле. </p>
 
  <p align="center" style="text-align: center; "><b>3.7. Транспарентность</b><u> </u></p>
 
  <p style="text-align: justify; ">Выполняя обязательства в рамках ВТО Российская Федерация при разработке нормативных правовых актов Россия будет обязана предоставлять всем заинтересованным лицам возможность в течение разумного периода времени представить свои комментарии и предложения по проектам таких актов до того, как эти акты принимаются. Это обеспечит более высокий уровень защиты участников ВЭД Калининградской области и надлежащий уровень предсказуемости правовой среды в Российской Федерации. </p>
 
  <p align="center" style="text-align: center; "><b>3.8. Обязательства по поддержке сельского хозяйства</b> </p>
 
  <p style="text-align: justify; ">В соответствии правилами ВТО, обязуется сокращать общий объем поддержки сельхозпроизводителей, искажающей торговлю. </p>
 
  <p style="text-align: justify; ">Для России разрешенный уровень поддержки будет составлять 9 млрд. долларов США. Затем разрешенный уровень поддержки будет постепенно сокращаться, а с 2018 года &ndash; будет «связан» на нынешнем уровне. </p>
 
  <p style="text-align: justify; ">Россия подтвердила, что после присоединения к ВТО (как и в настоящее время) не будет использоваться экспортные субсидии сельскому хозяйству. </p>
 
  <p style="text-align: justify; ">На основании изложенного можно сделать вывод, что присоединение России к ВТО изменит, а по некоторым вопросам и ужесточит требования, предъявляемые к хозяйствующим субъектам, осуществляющим свою деятельность на территории Калининградской области, в тоже время, не приведет к замедлению социально-экономического развития региона.  </p>
 
  <p></p>
 
  <p></p>
 </div>
 
<p></p>
 
<p></p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>