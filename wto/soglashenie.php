<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Соглашения ВТО");
?> 
<br />
<font size="3"><a href="/documents/01.doc.docx" >1. Генеральное соглашение по тарифам и торговле 1994 года</a> 
  <br />
<a href="/documents/1.doc.docx" >2. Генеральное соглашение по торговле услугами</a> 
  <br />
<a href="/documents/0012.docx" >3. Соглашение по применению статьи VI генерального соглашения по тарифам и торговле 1994 года </a> 
  <br />
<a href="/documents/001.docx" >4. Соглашение по применению статьи VII генерального соглашения по тарифам и торговле 1994 года</a> 
  <br />
<a href="/documents/2.docx" >5. Соглашение по процедурам импортного лицензирования</a> <span style="font-family: 'Times New Roman',serif;"> 
    <div><a href="/documents/3.docx" >6. Соглашение по связанным с торговлей инвестиционным мерам</a></div>
   
    <div><a href="/documents/4.docx" >7. Механизм обзора торговой политики</a></div>
   
    <div><a href="/documents/5.docx" >8. Торговые соглашения с ограниченным кругом участников</a></div>
   
    <div><a href="/documents/6.docx" >9. Соглашение по предотгрузочной инспекции</a></div>
   
    <div><a href="/documents/7.docx" >10. Договоренность о правилах и процедурах, регулирующих разрешение споров</a></div>
   
    <div><a href="/documents/08.docx" >11. Соглашение по применению санитарных и фитосанитарных мер</a></div>
   
    <div><a href="/documents/09.docx" >12. Соглашение по сельскому хозяйству</a></div>
   
    <div><a href="/documents/10.docx" >13. Соглашение по правилам происхождения</a></div>
   
    <div><a href="/documents/11.docx" >14. Соглашение по субсидиям и компенсационным мерам</a></div>
   
    <div><a href="/documents/12.docx" >15. Соглашение по текстилю и одежде</a></div>
   
    <div><a href="/documents/13.docx" >16. Соглашение по техническим барьерам в торговле</a></div>
   
    <div><a href="/documents/14.doc" ><font size="3">17. </font>Соглашение по торговым аспектам прав интеллектуальной собственности</a></div>
  </span></font>
<br />
<span style="font-family: 'Times New Roman',serif;"> </span><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>