<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("show_title", "false");
$APPLICATION->SetPageProperty("title", "Министерство по промышленной политике");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Главная страница");
?><span class="Apple-style-span" style="line-height: 19px; background-color: #f9f8f3;">
<p style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: center; font-size: 14px;">
</p>
<p align="center" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; font-size: 14px;">
</p>
<p align="center" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; font-size: 14px;">
</p>
<p style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: justify; font-size: 14px;">
</p>
<p style="font-family: Arial, Helvetica, sans-serif; margin: 0px 0px 20px; padding: 0px; border: 0px currentcolor; text-align: center; color: #3e3939; line-height: 20px; font-size: 13px; vertical-align: baseline; background-color: #ffffff;">
</p>
<p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: center;">
 <span style="text-align: center; font-size: 22pt;">Добро пожаловать</span><span style="text-align: center; font-size: xx-large;">!</span>
</p>
<p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: center;">
</p>
<p class="MsoNormal" style="text-align: center;">
</p>
 </span><span class="Apple-style-span" style="line-height: 19px; background-color: #f9f8f3;">
<p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: center;">
	 <iframe width="560" height="315" src="https://www.youtube.com/embed/ziFNr_NjMHM" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
</p>
<p>
 <br>
</p>
<p style="text-align: center;">
	Представляем пре­зен­та­цию уни­каль­ных пре­иму­ществ ин­вес­ти­ци­он­но­го ре­жи­ма <br>
 <b>ОЭЗ в Ка­ли­нинг­радс­кой об­лас­ти</b> в кон­текс­те при­ня­тия па­ке­та сти­му­ли­ру­ющих <br>
	 мер для ин­вес­то­ров, осу­щест­вля­ющих ин­вес­ти­ции в ре­ги­он, <br>
	<br>
</p>
<p style="text-align: center;">
	 На­пом­ним, с 1 ян­ва­ря 2018 го­да ОЭЗ в Ка­ли­нинг­радс­кой об­лас­ти нач­нет ра­бо­тать по но­вым пра­ви­лам. Для про­ек­тов в об­лас­ти здра­во­ох­ра­не­ния, в сфе­ре IT и на­уч­ных раз­ра­бо­ток пре­дус­мот­ре­ны до­пол­ни­тель­ные воз­мож­нос­ти.
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
	 &nbsp;О но­вых воз­мож­нос­тях и ус­ло­ви­ях прив­ле­че­ния ин­вес­ти­ций рассказывает гла­ва <br>
	 ре­ги­она&nbsp;<b>Ан­тон Анд­ре­евич Али­ха­нов</b>.
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
	 Так­же выступают спи­ке­ры:&nbsp;&nbsp;
</p>
<p style="text-align: center;">
 <b>Эль­ми­ра Зиль­бер</b>, про­рек­тор по на­уч­ной ра­бо­те и меж­ду­на­род­ным свя­зям <br>
	 БФУ им. И. Кан­та.
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
 <b>Алек­сандр&nbsp; Крав­чен­ко</b>, ми­нистр здра­во­ох­ра­не­ния Ка­ли­нинг­радс­кой об­лас­ти;&nbsp;
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
</p>
<p style="text-align: center;">
	 Для просмотра запустите указанное видео.
</p>
<p style="text-align: center;">
</p>
<p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: center; font-size: 14px;">
</p>
<p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: justify; font-size: 14px;">
 <br>
</p>
<p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; text-align: justify; font-size: 14px;">
</p>
<p class="MsoNormal" style="text-align: justify;">
</p>
 <br>
<p style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; font-size: 14px;">
</p>
<p style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; font-size: 14px;">
</p>
<p style="font-family: &quot;Times New Roman&quot;, Times, Tahoma; font-size: 14px;">
</p>
 </span><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>