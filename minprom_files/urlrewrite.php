<?
$arUrlRewrite = array(
	array(
		"CONDITION"	=>	"#^/news/([0-9a-z_]+)/([0-9]+).php#",
		"RULE"	=>	"ID=$2",
		"ID"	=>	"",
		"PATH"	=>	"/news/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/interviews/([0-9]+).php#",
		"RULE"	=>	"ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/interviews/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/documents/([0-9]+).php#",
		"RULE"	=>	"ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/documents/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/calendar/([0-9]+).php#",
		"RULE"	=>	"ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/calendar/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/experts/([0-9]+).php#",
		"RULE"	=>	"ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/experts/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/opinion/([0-9]+).php#",
		"RULE"	=>	"ID=$1",
		"ID"	=>	"",
		"PATH"	=>	"/opinion/detail.php",
	),
	array(
		"CONDITION"	=>	"#^/news/([0-9a-z_]+)/#",
		"RULE"	=>	"CODE=$1",
		"ID"	=>	"",
		"PATH"	=>	"/news/index.php",
	),
);

?>