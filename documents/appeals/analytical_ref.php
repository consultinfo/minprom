<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Аналитическая справка");
?><p class="MsoNormal" style="text-align: center;">
 <b><span style="color: #438ccb;">Аналитическая справка о письменных обращениях граждан в Министерство по промышленной политике, развитию предпринимательства и торговли Калининградской области&nbsp;</span></b>
</p>
<p class="MsoNormal" style="text-align: center;">
	<b><span style="color: #438ccb;"><a href="/msp/Doklad_1_kvartal_2016.doc">Доклад по обращениям граждан за 1 квартал 2016 года</a></span></b>
</p>
<p class="MsoNormal" style="text-align: center;">
	<b><span style="color: #438ccb;"><a href="/msp/Doklad_2_kvartal_2016.doc">Доклад по обращениям граждан за 2 квартал 2016 года</a></span></b>
</p>
<p class="MsoNormal" style="text-align: center;">
	<b><span style="color: #438ccb;"><a href="/msp/Doklad_3_kvartal_2016.doc">Доклад по обращениям граждан за 3 квартал 2016 года</a></span></b>
</p>
<p class="MsoNormal" style="text-align: center;">
	<b><span style="color: #438ccb;"><a href="/msp/Doklad_4_kvartal_2016.doc">Доклад по обращениям граждан за 4 квартал 2016 года</a></span></b>
</p>
<p class="MsoNormal" style="text-align: center;">
	<b><span style="color: #438ccb;"><a href="/msp/Doklad_ 1 kvartal_2017.doc">Доклад по обращениям граждан за 1 квартал 2017 года</a></span></b>
</p>
<p class="MsoNormal">
 <br>
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>