<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лицензирование оборота лома черного и цветного металлов");
?><h1 style="text-align: left; margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;"> <br>
 </span></h1>
<h1 style="text-align: left; margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;">Реестры</span></h1>
<ul style="margin: 0px; padding: 0px 0px 0px 30px;">
	<li style="margin: 0px 0px 10px; padding: 0px;"><a href="http://gov39.ru/biznesu/licensing/zip/reestr_lom_license_ok.xlsx">Реестр лицензий на деятельность по заготовке, хранению, переработке и реализации лома черных металлов, цветных металлов</a></li>
</ul>
<h1 style="text-align: left; margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;"> <br>
 </span></h1>
<h1 style="text-align: left; margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;">Доrументы для лицензирования деятельности</span></h1>
<ul style="margin: 0px; padding: 0px 0px 0px 30px;">
	<li style="margin: 0px 0px 10px; padding: 0px;"><a href="http://gov39.ru/biznesu/zip/licenzirovanie/doc-license-metall.zip">Документы для лицензирования (2015)</a></li>
</ul>
<h1 style="text-align: left; margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;"> <br>
 </span></h1>
<h1 style="text-align: left; margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;">Нормативные правовые акты</span></h1>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/post_11052001_369.rtf">Постановление Правительства РФ от 11.05.2001 № 369 "Об утверждении Правил обращения с ломом и отходами черных металлов и их отчуждения"</a>
</p>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/post_11052001_370.rtf">Постановление Правительства РФ от 11.05.2001 № 370 "Об утверждении Правил обращения с ломом и отходами цветных металлов и их отчуждения"</a>
</p>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/zakon-27052011-99-fz.zip">Федеральный закон РФ от 27.05.2011 г. № 99-ФЗ "О лицензировании отдельных видов деятельности"</a>
</p>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/post_12122012_1287.rtf">Постановление Правительства РФ от 12.12.2012 № 1287 "О лицензировании деятельности по заготовке, хранению, переработке и реализации лома черных и цветных металлов"</a>
</p>
<p style="margin-bottom: 10px;">
	 &nbsp;
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>