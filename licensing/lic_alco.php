<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лицензирование розничной продажи алкогольной продукции");
?><h1 style="margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;"> <br>
 </span></h1>
<h1 style="margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;">Реестры</span></h1>
<ul style="margin: 0px; padding: 0px 0px 0px 30px;">
	<li style="margin: 0px 0px 10px; padding: 0px;"><a href="http://fsrar.ru/licens/reestr">Реестр лицензиатов, имеющих действующие лицензии на территории Калининградской области</a></li>
	<li style="margin: 0px 0px 10px; padding: 0px;"><a href="http://gov39.ru/biznesu/licensing/zip/reestr_alco_license_stop.xls">Приостановленные лицензии на розничную продажу алкогольной продукции</a></li>
</ul>
<h1 style="margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;"> <br>
 </span></h1>
<h1 style="margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;">Документы для лицензирования деятельности</span></h1>
<ul style="margin: 0px; padding: 0px 0px 0px 30px;">
	<li style="margin: 0px 0px 10px; padding: 0px;"><a href="http://gov39.ru/biznesu/zip/licenzirovanie/alko_obyava_podacha_documentov.doc">Объявление по подаче документов</a></li>
	<li style="margin: 0px 0px 10px; padding: 0px;"><a href="http://gov39.ru/biznesu/zip/licenzirovanie/doc-license-alcohol-2016-2017.zip">Документы для лицензирования (2016-2017)</a></li>
</ul>
<h1 style="margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;"> <br>
 </span></h1>
<h1 style="margin: 0px 0px 20px; padding: 0px;"><span style="font-size: 9px; color: #438ccb;">Нормативные правовые акты</span></h1>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/post-05072006-493.pdf">ПОСТАНОВЛЕНИЕ Правительства Калининградской области от 5 июля 2006 г. № 493 "О мерах по выполнению нормативных правовых актов в области государственного регулирования оборота алкогольной продукции"</a>
</p>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/post-19011998-55.pdf">ПОСТАНОВЛЕНИЕ Правительства Российской Федерации от 19 января 1998 г. № 55 "ОБ УТВЕРЖДЕНИИ ПРАВИЛ ПРОДАЖИ ОТДЕЛЬНЫХ ВИДОВ ТОВАРОВ, ПЕРЕЧНЯ ТОВАРОВ ДЛИТЕЛЬНОГО ПОЛЬЗОВАНИЯ, НА КОТОРЫЕ НЕ РАСПРОСТРАНЯЕТСЯ ТРЕБОВАНИЕ ПОКУПАТЕЛЯ О БЕЗВОЗМЕЗДНОМ ПРЕДОСТАВЛЕНИИ ЕМУ НА ПЕРИОД РЕМОНТА ИЛИ ЗАМЕНЫ АНАЛОГИЧНОГО ТОВАРА, И ПЕРЕЧНЯ НЕПРОДОВОЛЬСТВЕННЫХ ТОВАРОВ НАДЛЕЖАЩЕГО КАЧЕСТВА, НЕ ПОДЛЕЖАЩИХ ВОЗВРАТУ ИЛИ ОБМЕНУ НА АНАЛОГИЧНЫЙ ТОВАР ДРУГИХ РАЗМЕРА, ФОРМЫ, ГАБАРИТА, ФАСОНА, РАСЦВЕТКИ ИЛИ КОМПЛЕКТАЦИИ</a>"
</p>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/171_fz.doc">ФЕДЕРАЛЬНЫЙ ЗАКОН РФ от 22 ноября 1995 года № 171-ФЗ "О ГОСУДАРСТВЕННОМ РЕГУЛИРОВАНИИ ПРОИЗВОДСТВА И ОБОРОТА ЭТИЛОВОГО СПИРТА, АЛКОГОЛЬНОЙ И СПИРТОСОДЕРЖАЩЕЙ ПРОДУКЦИИ</a>"
</p>
<p style="margin-bottom: 10px;">
 <a href="http://gov39.ru/biznesu/zip/licenzirovanie/zakon_03022003_222.doc">Закон Калининградской области от 03.02.2003 г. № 222 "О розничной продаже алкогольной продукции на территории Калининградской области"</a>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>